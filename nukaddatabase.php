<?php
class nukaddata {
    private $prdbh = null;
    public function __construct() {
        include_once ("dbdetails.php");
        include "phpqrcode/qrlib.php";
        try {
            $this->prdbh = new PDO("mysql:host=" . $GLOBALS['hostname'] . ";dbname=" . $GLOBALS['dbname'] . ";", $GLOBALS['username'], $GLOBALS['password']);
        }
        catch(PDOException $e) {
            print json_encode($e->getMessage());
        }
    }
    public function __destruct() {
        $this->prdbh = null;
    }
    public function sendPushNotificationToGCMSever($token, $message, $UpushId) {
        sort($token);
        //print_r($token);die;
        $path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';
        $fields = array('registration_ids' => $token, 'priority' => 'high', 'notification' => array('title' => $message['UrHd2'], 'body' => $message['message'], 'image' => 'https://pbs.twimg.com/profile_images/487481154635710464/M3RZssAR_400x400.jpeg'), //'sound'=>'nukad_sound.caf'),,'icon'=>'https://s3.amazonaws.com/mynukad-ios/alphabet/ic_notification.png'
        'data' => $message);
        $headers = array('Authorization:key=' . SERVER_KEY, 'Content-Type:application/json');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
    public function sendPushNotificationToGCMSever250($token, $message, $UpushId, $FlatNbr, $Society, $VisitorName, $VisitorImageUrl) {
        ignore_user_abort();
        ob_start();
        array_push($token);
        // sort($token);
        $path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';
        $fields = array('to' => $token,
        // 'registration_ids' =>$token,
        'priority' => 'high', 'notification' => array('title' => $message['UrHd2'], 'body' => $message['message'], 'image' => 'https://pbs.twimg.com/profile_images/487481154635710464/M3RZssAR_400x400.jpeg'), //'sound'=>'nukad_sound.caf'),,'icon'=>'https://s3.amazonaws.com/mynukad-ios/alphabet/ic_notification.png'
        'data' => $message);
        $headers = array('Authorization:key=' . SERVER_KEY, 'Content-Type:application/json');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
    public function firebasenotification($UpushId, $message1) {
        $androidToken = array();
        $iosToken = array();
        //commented by deepraj
        // $sqltoken = $this->prdbh->prepare("SELECT DeviceUniqueID,DeviceType FROM UserDeviceMapping WHERE UserID IN($UpushId)");
        //end
        $sqltoken = $this->prdbh->prepare("SELECT DeviceUniqueID,DeviceType FROM UserDeviceMapping as a inner join ResidentRWAMapping as b on a.UserID = b.ResidentID WHERE a.UserID IN($UpushId) and ApprovalStatus = 'Y'");
        $sqltoken->execute();
        while ($rowtoken = $sqltoken->fetch(PDO::FETCH_ASSOC)) {
            if ($rowtoken['DeviceType'] == 'A') {
                array_push($androidToken, $rowtoken['DeviceUniqueID']);
            } else {
                array_push($iosToken, $rowtoken['DeviceUniqueID']);
            }
        }
        if (count($androidToken)) {
            $androidToken = array_unique($androidToken);
            $jsonString = $this->sendPushNotificationToGCMSever($androidToken, $message1, $UpushId);
            $jsonObject = json_decode($jsonString);
            if (isset($jsonObject->success) && $jsonObject->success == 1) {
                $response['Anotificationmsg'] = "Firebase Push Notification successfully sent";
            } else {
                $response['Anotificationmsg'] = "Invalid Registration. Make sure your have provided the correct details";
            }
        }
        if (count($iosToken)) {
            $iosToken = array_unique($iosToken);
            $jsonString = $this->sendPushNotificationToGCMSever($iosToken, $message1, $UpushId);
            $jsonObject = json_decode($jsonString);
            if (isset($jsonObject->success) && $jsonObject->success == 1) {
                $response['Inotificationmsg'] = "Firebase Push Notification successfully sent";
            } else {
                $response['Inotificationmsg'] = "Invalid Registration. Make sure your have provided the correct details";
            }
        }
        return $response;
    }
    public function firebasenotificationdecember($UpushId, $message1) {
        $androidToken = array();
        $iosToken = array();
        $sqltoken = $this->prdbh->prepare("SELECT DeviceUniqueID,DeviceType from (SELECT DeviceUniqueID,DeviceType FROM UserDeviceMapping WHERE UserID IN($UpushId))as a where  (a.DeviceUniqueID != '' and a.DeviceUniqueID is not null)");
        $sqltoken->execute();
        while ($rowtoken = $sqltoken->fetch(PDO::FETCH_ASSOC)) {
            if ($rowtoken['DeviceType'] == 'A') {
                array_push($androidToken, $rowtoken['DeviceUniqueID']);
            } else {
                array_push($iosToken, $rowtoken['DeviceUniqueID']);
            }
        }
        if (count($androidToken)) {
            $androidToken = array_unique($androidToken);
            $jsonString = $this->sendPushNotificationToGCMSever($androidToken, $message1, $UpushId);
            $jsonObject = json_decode($jsonString);
            if (isset($jsonObject->success) && $jsonObject->success == 1) {
                $response['Anotificationmsg'] = "Firebase Push Notification successfully sent";
            } else {
                $response['Anotificationmsg'] = "Invalid Registration. Make sure your have provided the correct details";
            }
        }
        if (count($iosToken)) {
            $iosToken = array_unique($iosToken);
            $jsonString = $this->sendPushNotificationToGCMSever($iosToken, $message1, $UpushId);
            $jsonObject = json_decode($jsonString);
            if (isset($jsonObject->success) && $jsonObject->success == 1) {
                $response['Inotificationmsg'] = "Firebase Push Notification successfully sent";
            } else {
                $response['Inotificationmsg'] = "Invalid Registration. Make sure your have provided the correct details";
            }
        }
        return $response;
    }
    public function firebasenotification250($UpushId, $message1, $VisitorName, $VisitorImageUrl, $message3) {
        $androidToken = array();
        $iosToken = array();
        $sqltoken = $this->prdbh->prepare(" SELECT 
                                DeviceUniqueID, DeviceType, FirstName,FlatNbr,Name as Society
                            FROM
                                (SELECT 
                                    a.DeviceUniqueID, a.DeviceType, b.FirstName,d.FlatNbr,e.Name
                                FROM
                                    UserDeviceMapping AS a
                                INNER JOIN User AS b ON a.UserID = b.ID
                                INNER JOIN ResidentRWAMapping AS c ON a.UserID = c.ResidentID
                                INNER JOIN ResidentFlatMapping AS d ON c.ResidentRWAID = d.ResidentRWAID
                                left join RWA as e on c.RWAID = e.ID
                                WHERE
                                    UserID IN ($UpushId)) AS a
                            WHERE
                                (a.DeviceUniqueID != ''
                                    AND a.DeviceUniqueID IS NOT NULL) group by DeviceUniqueID");
        // print_r($sqltoken);
        $sqltoken->execute();
        while ($rowtoken = $sqltoken->fetch(PDO::FETCH_ASSOC)) {
            if ($rowtoken['DeviceType'] == 'A') {
                $androidToken = $rowtoken['DeviceUniqueID'];
                $FlatNbr = $rowtoken['FlatNbr'];
                $Society = $rowtoken['Society'];
                if ($androidToken != null && !empty($androidToken)) {
                    $jsonString = $this->sendPushNotificationToGCMSever250($androidToken, $message1, $UpushId, $FlatNbr, $Society, $VisitorName, $VisitorImageUrl);
                    $jsonObject = json_decode($jsonString);
                    if (isset($jsonObject->success) && $jsonObject->success == 1) {
                        $response['Anotificationmsg'] = "Firebase Push Notification successfully sent";
                    } else {
                        $response['Anotificationmsg'] = "Invalid Registration. Make sure your have provided the correct details";
                    }
                }
            } else {
                array_push($iosToken, $rowtoken['DeviceUniqueID']);
            }
            // else{
            //  $iosToken = $rowtoken['DeviceUniqueID'];
            //  $FlatNbr = $rowtoken['FlatNbr'];
            //  $Society = $rowtoken['Society'];
            //  if(!empty($iosToken) && $iosToken != null )
            //  {
            //      $jsonString = $this->sendPushNotificationToGCMSever250($iosToken,$message1,$UpushId,$FlatNbr,$Society,$VisitorName,$VisitorImageUrl);
            //      $jsonObject = json_decode($jsonString);
            //      // print_r($jsonObject );
            //      if(isset($jsonObject->success) && $jsonObject->success==1)
            //      {
            //          $response['Inotificationmsg']="Firebase Push Notification successfully sent";
            //      } else{
            //          $response['Inotificationmsg']="Invalid Registration. Make sure your have provided the correct details";
            //      }
            //  }
            // }
            
        }
        if (count($iosToken)) {
            $iosToken = array_unique($iosToken);
            $jsonString = $this->sendPushNotificationToGCMSever($iosToken, $message3, $UpushId);
            $jsonObject = json_decode($jsonString);
            if (isset($jsonObject->success) && $jsonObject->success == 1) {
                $response['Inotificationmsg'] = "Firebase Push Notification successfully sent";
            } else {
                $response['Inotificationmsg'] = "Invalid Registration. Make sure your have provided the correct details";
            }
        }
        return $response;
    }
    public function firebasenotification1($UpushId2, $message12) {
        $androidToken = array();
        $iosToken = array();
        $sqltoken = $this->prdbh->prepare("SELECT DeviceUniqueID,DeviceType FROM UserDeviceMapping WHERE UserID IN($UpushId2)");
        //print_r($sqltoken);die;
        $sqltoken->execute();
        while ($rowtoken = $sqltoken->fetch(PDO::FETCH_ASSOC)) {
            if ($rowtoken['DeviceType'] == 'A') {
                array_push($androidToken, $rowtoken['DeviceUniqueID']);
            } else {
                array_push($iosToken, $rowtoken['DeviceUniqueID']);
            }
        }
        if (count($androidToken)) {
            $androidToken = array_unique($androidToken);
            $jsonString = $this->sendPushNotificationToGCMSever($androidToken, $message12, $UpushId2);
            $jsonObject = json_decode($jsonString);
            if (isset($jsonObject->success) && $jsonObject->success == 1) {
                // print_r($jsonObject);
                $response['Anotificationmsg'] = "Firebase Push Notification successfully sent";
            } else {
                $response['Anotificationmsg'] = "Invalid Registration. Make sure your have provided the correct details";
            }
        }
        if (count($iosToken)) {
            $iosToken = array_unique($iosToken);
            $jsonString = $this->sendPushNotificationToGCMSever($iosToken, $message12, $UpushId2);
            $jsonObject = json_decode($jsonString);
            if (isset($jsonObject->success) && $jsonObject->success == 1) {
                $response['Inotificationmsg'] = "Firebase Push Notification successfully sent";
            } else {
                $response['Inotificationmsg'] = "Invalid Registration. Make sure your have provided the correct details";
            }
        }
        return $response;
    }
    public function rwabyname($param) {
        if (isset($param->Name)) {
            $Name = $param->Name;
        }
        if (isset($Name) && empty($Name)) {
            $response['Status'] = 0;
        } else {
            $response = array();
            $sql = $this->prdbh->prepare("SELECT `ID`, `Name`, `Address1`, `Address2`, `Locality`, `City`, `PostalCode`, `Latitude`, `Longitude`, `PrimaryPhoneNbr`, `PhoneNbr2`, `PhoneNbr3`, `BannerImage`, `Image2`, `Image3`, `AddedOn`, `AddedBy`, `UpdatedOn`, `UpdatedBy`,SocietyPrivateinfo,SocietyPublicInfo,SocietyBanner,SocietyPendingInfo,`IsActive` FROM `RWA` WHERE Name like '%" . $Name . "%' AND IsActive='1' ORDER BY Name ASC ");
            $sql->execute();
            if ($sql) {
                $items = array();
                $count = $sql->rowCount();
                if ($count >= 1) {
                    while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                        $items[] = $row;
                    }
                    $response['Societyname Details'] = $items;
                    $response['Status'] = '1';
                } else {
                    $response['Societyname Details'] = [];
                    $response['Status'] = '0';
                }
            }
        }
        return $response;
    }
    public function sendmailsenahed($rec) {
        $response = array();
        $umail = $rec->EmailID;
        $uname = $rec->UName;
        $em = $rec->passcode;
        $usub = '[Sean Ahead]  Your Password for register Here';
        $d = 'Your Password is Here : ' . $em . "\r\r\n\r\n\n\n\n\n\n\n\n\n\n\n\n" . 'Thanks' . ',' . "\r\n\n" . 'Team,Sean Ahed';
        $umsg = 'Hi ' . $uname . "\r\n\n\n" . $d;
        require 'sendgrid/vendor/autoload.php';
        require 'sendgrid/lib/SendGrid.php';
        $sendgrid = new SendGrid('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');
        $email = new SendGrid\Email();
        $email->addTo($umail)->setFrom('noreply@seanahead.com')->setSubject($usub)->setText($umsg)->setHtml($dt);
        try {
            $sendgrid->send($email);
            $response['message'] = "Sent mail successfully";
            $response['status'] = 1;
        }
        catch(\SendGrid\Exception $e) {
            echo $e->getCode();
            $response['status'] = 0;
            $response['message'] = "Something went wrong.Mail not sent";
            foreach ($e->getErrors() as $er) {
                echo $er;
            }
        }
        return $response;
    }
    public function sendmail($rec) {
        $response = array();
        $umail = $rec->EmailID;
        $uname = $rec->UName;
        $umsg1 = base64_decode($rec->Message);
        $em = $rec->SenderMailId;
        $name = $rec->Name;
        $flatno = $rec->Flatno;
        $usub = '[mynukad] ' . $name . ',' . $flatno . ' Sent a message to you';
        $d = '**Please reply to his/her email ' . $em . "\r\n\r\n" . "\r\n\r\n\n\n\n\n\n\n\n\n\n\n\n" . 'Thanks' . ',' . "\r\n\n" . 'Team,mynukad';
        $umsg = 'Hi ' . $uname . ',' . "\r\n\n" . $umsg1 . "\r\n\n\n" . $d;
        require 'sendgrid/vendor/autoload.php';
        require 'sendgrid/lib/SendGrid.php';
        $sendgrid = new SendGrid('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');
        $email = new SendGrid\Email();
        $email->addTo($umail)->setFrom('noreply@mynukad.com')
        //->setcc($em)
        ->setSubject($usub)->setText($umsg)->setHtml($dt);
        try {
            $sendgrid->send($email);
            $response['message'] = "Sent mail successfully";
            $response['status'] = 1;
        }
        catch(\SendGrid\Exception $e) {
            echo $e->getCode();
            $response['status'] = 0;
            $response['message'] = "Something went wrong.Mail not sent";
            foreach ($e->getErrors() as $er) {
                echo $er;
            }
        }
        return $response;
    }
    public function servicelist($param) {
        $response = array();
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        $sql = $this->prdbh->prepare("SELECT ServiceID,ServiceName,IconImage From ServicesTable INNER JOIN SocietyServicesMapping ON SocietyServicesMapping.ServiceID = ServicesTable.ID WHERE RWAID='" . $RWAID . "' ");
        $sql->execute();
        if ($sql) {
            $items = array();
            if ($sql->rowCount() >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['ServiceList'] = $items;
                $response['Status'] = '1';
            } else {
                $response['ServiceList'] = [];
                $response['Status'] = '0';
            }
        }
        $sql1 = $this->prdbh->prepare("SELECT CampaignMerchantShopMapping.ID, CampaignMerchantShopMapping.DiscountType, CampaignMerchantShopMapping.CampaignUrl, CampaignMerchantShopMapping.StartDate, CampaignMerchantShopMapping.EndDate, CampaignMerchantShopMapping.ShopID,NearBy.Name, CampaignMerchantShopMapping.MerchantID, CampaignMerchantShopMapping.NoOfCoupenPerUser, CampaignMerchantShopMapping.DiscountValue From CampaignMerchantShopMapping INNER JOIN NearBy ON NearBy.ID= CampaignMerchantShopMapping.ShopID  WHERE RWAID='" . $RWAID . "' ");
        $sql1->execute();
        if ($sql1->execute()) {
            $items1 = array();
            if ($sql1->rowCount() >= 1) {
                while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                    $items1[] = $row1;
                }
                $response['Coupen'] = $items1;
                $response['status'] = '1';
            } else {
                $sql1 = $this->prdbh->prepare("SELECT CampaignMerchantShopMapping.ID, CampaignMerchantShopMapping.DiscountType, CampaignMerchantShopMapping.CampaignUrl, CampaignMerchantShopMapping.StartDate, CampaignMerchantShopMapping.EndDate, CampaignMerchantShopMapping.ShopID,NearBy.Name, CampaignMerchantShopMapping.MerchantID, CampaignMerchantShopMapping.NoOfCoupenPerUser, CampaignMerchantShopMapping.DiscountValue From CampaignMerchantShopMapping INNER JOIN NearBy ON NearBy.ID= CampaignMerchantShopMapping.ShopID  WHERE RWAID is NULL"); // AND IsActive = '1'  AND '".$c."'>StartDate AND EndDate>'".$c."' ");
                $sql1->execute();
                $items1 = array();
                if ($sql1->rowCount() >= 1) {
                    while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                        $items1[] = $row1;
                    }
                    $response['Coupen'] = $items1;
                    $response['status'] = '1';
                }
            }
        } else {
            $response['Coupen'] = [];
            $response['status'] = '0';
        }
        return $response;
    }
    public function couponreedim($rec) {
        $tdat = date("U");
        $tdate = $tdat;
        $rid = $rec->ResidentRWAID;
        $cupid = $rec->CampaignID;
        $sql1 = $this->prdbh->prepare("SELECT NoOfCoupenPerUser FROM CampaignMerchantShopMapping WHERE  ID='" . $cupid . "' ");
        $sql1->execute();
        if ($sql1->rowCount() > 0) {
            while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                $NOCPU = $row1['NoOfCoupenPerUser'];
                $ShopID = $row1['ShopID'];
            }
        }
        $sql2 = $this->prdbh->prepare("SELECT ID FROM CampaignUserMapping WHERE ResidentRWAID = '" . $rid . "' AND CampaignID='" . $cupid . "' AND Availed='0' ");
        $sql2->execute();
        $count2 = $sql2->rowCount();
        if ($count2 < $NOCPU) {
            $code = mt_rand(10000, 99999);
            $sql3 = $this->prdbh->prepare("SELECT ID FROM CompainUserMapping Where QrCode ='" . $code . "'");
            $sql3->execute();
            if ($sql3->rowCount() > 0) {
                $code = mt_rand(10000, 99999);
            }
            $sql5 = $this->prdbh->prepare("INSERT INTO UserFavouriteNearBy( NearByID, UserID, AddedOn) VALUES(?,?,?)");
            $sql5->bindParam(1, $ShopID);
            $sql5->bindParam(2, $rid);
            $sql5->bindParam(3, $tdate);
            $this->prdbh->beginTransaction();
            $sql5->execute();
            $count5 = $sql5->rowCount();
            if ($count5 > 0) {
                $id = $this->prdbh->lastInsertId();
                $this->prdbh->commit();
                $response['Msg'] = "Shop added to fev";
            }
            $sql = $this->prdbh->prepare("INSERT INTO CampaignUserMapping( ResidentRWAID, CampaignID, QrCode,IssueDate, Availed) VALUES(?,?,?,?,1)");
            $sql->bindParam(1, $rec->ResidentRWAID);
            $sql->bindParam(2, $rec->CampaignID);
            $sql->bindParam(3, $code);
            $sql->bindParam(4, $tdate);
            $this->prdbh->beginTransaction();
            $sql->execute();
            $count = $sql->rowCount();
            if ($count > 0) {
                $id = $this->prdbh->lastInsertId();
                $this->prdbh->commit();
                $response['Message'] = "Coupon redeem";
                $response['Coupon Id'] = $id;
                $response['Status'] = "1";
                $response['QrCode'] = $code;
            } else {
                $response['Message'] = $sql->errorInfo();
                $response['Status'] = "0";
            }
        } else {
            $response['Message'] = "Redeeming limit exhausted";
            $response['Status'] = "0";
        }
        return $response;
    }
    public function serviceagentlist($param) {
        $response = array();
        if (isset($param->AssociatedServiceID)) {
            $AssociatedServiceID = $param->AssociatedServiceID;
        }
        $sql = $this->prdbh->prepare("SELECT (ID) as AgentID,Name,PhoneNumber,PhoneNumber1,PhoneNumber2,AgentProfilePic, AboutService From ServiceAgentList  WHERE AssociatedServiceID='" . $AssociatedServiceID . "' ");
        $sql->execute();
        if ($sql) {
            $items = array();
            if ($sql->rowCount() >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['ServiceAgentList'] = $items;
                $response['Status'] = '1';
            } else {
                $response['ServiceAgentList'] = [];
                $response['Status'] = '0';
            }
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function agentfeedback($rec) {
        $response = array();
        $tdat = date("U");
        $tdate = $tdat;
        $AgentID = $rec->AgentID;
        $AddedBy = $rec->AddedBy;
        $sql = $this->prdbh->prepare("SELECT ID, AgentID, Rating FROM AgentReview WHERE AgentID='" . $AgentID . "' AND AddedBy='" . $AddedBy . "' ");
        $sql->execute();
        if ($sql->rowCount() > 0) {
            $sql1 = $this->prdbh->prepare("UPDATE AgentReview SET Rating=?,Comments=?,AddedOn=?  WHERE AgentID=? AND AddedBy=?");
        } else {
            $sql1 = $this->prdbh->prepare("INSERT INTO AgentReview(Rating, Comments,AddedOn,AgentID, AddedBy) VALUES(?,?,?,?,?)");
        }
        $sql1->bindParam(1, $rec->Rating);
        $sql1->bindParam(2, $rec->Comments);
        $sql1->bindParam(3, $tdate);
        $sql1->bindParam(4, $rec->AgentID);
        $sql1->bindParam(5, $rec->AddedBy);
        $this->prdbh->beginTransaction();
        if ($sql1->execute()) {
            $this->prdbh->commit();
            $response['Message'] = "Feedback added";
            $response['Status'] = 1;
        } else {
            $response['Message'] = $sql1->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function serviceagentdetail($param) {
        $response = array();
        if (isset($param->AgentID)) {
            $AgentID = $param->AgentID;
        }
        $sql = $this->prdbh->prepare("SELECT (ServiceAgentList.ID) as AgentID,ServiceAgentList.Name,ServicesTable.ServiceName,ServiceAgentList.PhoneNumber,ServiceAgentList.PhoneNumber1,ServiceAgentList.PhoneNumber2,ServiceAgentList.AgentProfilePic, ServiceAgentList.AboutService,ServiceAgentList.WorkImages  From ServiceAgentList INNER JOIN ServicesTable ON ServicesTable.ID = ServiceAgentList.AssociatedServiceID WHERE ServiceAgentList.ID='" . $AgentID . "' ");
        $sql->execute();
        if ($sql) {
            $items = array();
            if ($sql->rowCount() >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $response['ServiceAgentDetail'] = $row;
                    $sql1 = $this->prdbh->prepare("SELECT DISTINCT AgentReview.ID, AgentReview.AgentID,(AgentReview.Rating + 0.0001) as Rating,AgentReview.Comments,AgentReview.AddedBy,SocietyFlatMapping. FlatNbr,(RWA.Name) as FlatNbr, AgentReview.AddedOn,ResidentRWAMapping.ResidentID,User.FirstName,User.ProfilePic,ResidentRWAMapping.ResidentRWAID FROM AgentReview INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID=AgentReview.AddedBy INNER JOIN ResidentFlatMapping ON ResidentRWAMapping.ResidentRWAID=ResidentFlatMapping.ResidentRWAID LEFT JOIN User ON User.ID=ResidentRWAMapping.ResidentID INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID LEFT JOIN RWA ON RWA.ID= ResidentRWAMapping.RWAID  WHERE AgentReview.AgentID= '" . $AgentID . "' GROUP by User.ID ORDER by RAND() ");
                    $sql1->execute();
                    $count1 = $sql1->rowCount();
                    if ($count1 > 0) {
                        while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                            $subitems[] = $row1;
                        }
                        $response['ServiceAgentDetail']['Rating & Comment'] = $subitems;
                    }
                }
                $response['Status'] = '1';
            } else {
                $response['ServiceAgentDetail'] = [];
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function rwa($param) {
        $response = array();
        $lat = '';
        $long = '';
        if (isset($param->lat)) {
            $lat = $param->lat;
        }
        if (isset($param->long)) {
            $long = $param->long;
        }
        if (isset($param->PageNo)) {
            $pageno = $param->PageNo;
        }
        $Start = $pageno * 10 - 10;
        if ($lat != '' && $long != '') {
            $sql = $this->prdbh->prepare("SELECT ID,Name,Address1,Address2,Locality,City,PostalCode,Latitude,Longitude,PrimaryPhoneNbr,PhoneNbr2,PhoneNbr3,BannerImage,Image2,Image3,AddedOn,AddedBy,UpdatedOn,UpdatedBy,SocietyPrivateinfo,SocietyPublicInfo,SocietyBanner,SocietyPendingInfo,ChatUrl,IsActive,
                ((ACOS(SIN($lat * PI() / 180) * SIN(latitude * PI() / 180) + COS($lat * PI() / 180) * COS(latitude * PI() / 180) * COS(($long - longitude) * PI() / 180)) * 180 / PI()) * 60 * 1.8532) AS distance FROM RWA WHERE IsActive='1' ORDER BY distance ASC Limit $Start,10");
        } else {
            $sql = $this->prdbh->prepare("SELECT ID,Name,Address1,Address2,Locality,City,PostalCode,Latitude,Longitude,PrimaryPhoneNbr,PhoneNbr2,PhoneNbr3,BannerImage,Image2,Image3,AddedOn,AddedBy,UpdatedOn,UpdatedBy,SocietyPrivateinfo,SocietyPublicInfo,SocietyBanner,SocietyPendingInfo,ChatUrl,IsActive
                 FROM RWA WHERE IsActive='1' ORDER BY Name ASC Limit $Start,10");
        }
        $sql->execute();
        //print_r($sql);die;
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['SocietyDetails'] = $items;
                $response['Status'] = '1';
            } else {
                $response['SocietyDetails'] = [];
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function nearby($param) {
        $response = array();
        $distance = 150000;
        if (isset($param->lat)) {
            $lat = $param->lat;
        }
        if (isset($param->long)) {
            $long = $param->long;
        }
        if (isset($param->CategoryID)) {
            $CategoryID = $param->CategoryID;
        }
        $sql = $this->prdbh->prepare("SELECT NearBy.ID,NearBy.Name,NearBy.CategoryID,NearByCategory.Description as Descripti,NearBy.Address1,NearBy.Address2,NearBy.City,NearBy.Latitude,NearBy.Longitude,NearBy.Is247,NearBy.IsHomeDelivery,NearBy.PhoneNbr1,NearBy.PhoneNbr2,NearBy.PhoneNbr3,NearBy.BannerImage,NearBy.Image2,NearBy.Image3,NearBy.Description,NearBy.AddedBy,NearBy.AddedOn,NearBy.IsActive,
            ((ACOS(SIN($lat * PI() / 180) * SIN(latitude * PI() / 180) + COS($lat * PI() / 180) * COS(latitude * PI() / 180) * COS(($long - longitude) * PI() / 180)) * 180 / PI()) * 60 * 1.1515) AS distance FROM NearBy  INNER JOIN NearByCategory ON NearBy.CategoryID = NearByCategory.ID  having distance < $distance ORDER BY distance ASC");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['shopbydistance'] = $items;
                $response['Status'] = '1';
            } else {
                $response['shopbydistance'] = [];
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function yourcomplain($param) {
        if (isset($param->AssignedBy)) {
            $AssignedBy = $param->AssignedBy;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT `ID`, `RWAID`, `CategoryID`, `RWAResidentFlatID`, `ComplaintDetails`, `Image1`, `Image2`, `Image3`, `Status`, from_unixtime((DateCreated + 19660),'%d/%m/%Y  %h:%i %p') as Datecreated, `AssignedTo`, `DateAssigned`, `AssignedBy`, `ResolvedOn`, `ResolutionNote`, `ResolutionAcknowledgedOn`, `AcknowledgementNote` FROM `ComplaintRegister` WHERE AssignedBy = '" . $AssignedBy . "' LIMIT 5 ");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['yourcomplain'] = $items;
                $response['Status'] = '1';
            } else {
                $response['yourcomplain'] = [];
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function complaindetail($param) {
        if (isset($param->ID)) {
            $ID = $param->ID;
        }
        $sql = $this->prdbh->prepare("SELECT ComplaintRegister.ID, ComplaintRegister.RWAID,(RWA.Name) as RWAName, ComplaintRegister.CategoryID, ComplaintRegister.RWAResidentFlatID,SocietyFlatMapping.FlatNbr, ComplaintRegister.ComplaintDetails, ComplaintRegister.IsCommonAreaComplaint,IFNULL(ComplaintRegister.Image1,'')as Image1, IFNULL(ComplaintRegister.Image2,'')as Image2, IFNULL(ComplaintRegister.Image3,'')as Image3 , ComplaintRegister.Status, from_unixtime((ComplaintRegister.DateCreated + 19660),'%d/%m/%Y  %h:%i %p') as Datecreated,IFNULL(ComplaintRegister.AssignedTo,'')as AssignedTo,IFNULL(MaintenanceStaff.Name,'')as AssignedToName,IFNULL(ComplaintRegister.AssignedBy,'')as AssignedBy ,(MaintenanceStaff.PrimaryContactNbr) as AssignToNumber,IFNULL( ComplaintRegister.DateAssigned,'')as DateAssigned,IFNULL(User.ID,'')as ComplainByID ,IFNULL(User.FirstName,'') as ComplainBy, IFNULL(ComplaintRegister.ResolvedOn,'')as ResolvedOn,IFNULL(ComplaintRegister.ResolutionNote,'')as ResolutionNote, IFNULL(ComplaintRegister.ResolutionAcknowledgedOn,'')as ResolutionAcknowledgedOn ,IFNULL(ComplaintRegister.AcknowledgementNote,'')as AcknowledgementNote,ComplaintCategory.Description,IFNULL(ComplaintRegister.Rating,'')as Rating FROM ComplaintRegister LEFT JOIN ComplaintCategory ON ComplaintRegister.CategoryID = ComplaintCategory.ID
               LEFT JOIN  ResidentFlatMapping ON ComplaintRegister.RWAResidentFlatID = ResidentFlatMapping.ID LEFT JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID
               LEFT join User on User.ID=ResidentRWAMapping.ResidentID LEFT JOIN
               MaintenanceStaff ON MaintenanceStaff.ID= ComplaintRegister.AssignedTo  LEFT JOIN RWA ON RWA.ID=ComplaintRegister.RWAID INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID
               WHERE ComplaintRegister.ID =  '" . $ID . "' ");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                $i = 0;
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items = $row;
                    $response['Show Complain'][$i] = $items;
                    $id = $row['ID'];
                    $sql1 = $this->prdbh->prepare("SELECT CompRemark.ID, CompRemark.Remark, CompRemark.CompID, CompRemark.RemarkDate, CompRemark.EnteredBy,User.FirstName,SocietyFlatMapping.FlatNbr
            FROM CompRemark INNER JOIN ResidentRWAMapping oN ResidentRWAMapping.ResidentRWAID=CompRemark.EnteredBy
            LEFT JOIN User ON User.ID=ResidentRWAMapping.ResidentID
            LEFT JOIN ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID =ResidentRWAMapping.ResidentRWAID
            INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID
            WHERE CompID=  '" . $id . "' group By ID");
                    $sql1->execute();
                    $count1 = $sql1->rowCount();
                    if ($count1 > 0) {
                        $subitems = array();
                        while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                            $subitems[] = $row1;
                            $response['Show Complain'][$i]['Remark'] = $subitems;
                        }
                    }
                    $i++;
                }
                $response['Status'] = '1';
            } else {
                $response['Show Complain'] = [];
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function complaindetaildecember($param) {
        if (isset($param->ID)) {
            $ID = $param->ID;
        }
        $sql = $this->prdbh->prepare("SELECT ComplaintRegister.ID, ComplaintRegister.RWAID,(RWA.Name) as RWAName, ComplaintRegister.CategoryID, ComplaintRegister.RWAResidentFlatID,SocietyFlatMapping.FlatNbr, ComplaintRegister.ComplaintDetails, ComplaintRegister.IsCommonAreaComplaint,IFNULL(ComplaintRegister.Image1,'')as Image1, IFNULL(ComplaintRegister.Image2,'')as Image2, IFNULL(ComplaintRegister.Image3,'')as Image3 , ComplaintRegister.Status, from_unixtime((ComplaintRegister.DateCreated + 19660),'%d/%m/%Y  %h:%i %p') as Datecreated,IFNULL(ComplaintRegister.AssignedTo,'')as AssignedTo,IFNULL(MaintenanceStaff.Name,'')as AssignedToName,IFNULL(ComplaintRegister.AssignedBy,'')as AssignedBy ,(MaintenanceStaff.PrimaryContactNbr) as AssignToNumber,IFNULL( ComplaintRegister.DateAssigned,'')as DateAssigned,IFNULL(User.ID,'')as ComplainByID ,IFNULL(User.FirstName,'') as ComplainBy, IFNULL(ComplaintRegister.ResolvedOn,'')as ResolvedOn,IFNULL(ComplaintRegister.ResolutionNote,'')as ResolutionNote, IFNULL(ComplaintRegister.ResolutionAcknowledgedOn,'')as ResolutionAcknowledgedOn ,IFNULL(ComplaintRegister.AcknowledgementNote,'')as AcknowledgementNote,ComplaintCategory.Description,IFNULL(ComplaintRegister.Rating,'')as Rating FROM ComplaintRegister LEFT JOIN ComplaintCategory ON ComplaintRegister.CategoryID = ComplaintCategory.ID 
               LEFT JOIN  ResidentFlatMapping ON ComplaintRegister.RWAResidentFlatID = ResidentFlatMapping.ID LEFT JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID 
               LEFT join User on User.ID=ResidentRWAMapping.ResidentID LEFT JOIN
               MaintenanceStaff ON MaintenanceStaff.ID= ComplaintRegister.AssignedTo  LEFT JOIN RWA ON RWA.ID=ComplaintRegister.RWAID INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID
               WHERE ComplaintRegister.ID =  '" . $ID . "' ");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                $i = 0;
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items = $row;
                    $response['Show Complain'][$i] = $items;
                    $id = $row['ID'];
                    $sql1 = $this->prdbh->prepare("SELECT CompRemark.ID, CompRemark.Remark, CompRemark.CompID, CompRemark.RemarkDate, CompRemark.EnteredBy,User.FirstName,SocietyFlatMapping.FlatNbr 
            FROM CompRemark INNER JOIN ResidentRWAMapping oN ResidentRWAMapping.ResidentRWAID=CompRemark.EnteredBy 
            LEFT JOIN User ON User.ID=ResidentRWAMapping.ResidentID 
            LEFT JOIN ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID =ResidentRWAMapping.ResidentRWAID
            INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID
            WHERE CompID=  '" . $id . "' group By ID");
                    $sql1->execute();
                    $count1 = $sql1->rowCount();
                    if ($count1 > 0) {
                        $subitems = array();
                        while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                            $subitems[] = $row1;
                            $response['Show Complain'][$i]['Remark'] = $subitems;
                        }
                    }
                    $i++;
                }
                $response['Status'] = '1';
            } else {
                $response['Show Complain'] = [];
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function showcomplain($param) {
        $ComplaintDetails = '';
        if (isset($param->IsCommonAreaComplaint)) {
            $IsCommonAreaComplaint = $param->IsCommonAreaComplaint;
        }
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        if (isset($param->Status)) {
            $Status = $param->Status;
        }
        if (isset($param->PageNo)) {
            $PageNo = $param->PageNo;
        }
        $Start = $PageNo * 10 - 10;
        $response = array();
        if ($Status == 0) {
            $Status = "0,1";
        } elseif ($Status == 1) {
            $Status = "2,3,4";
        }
        if (isset($param->ComplaintDetails)) {
            $ComplaintDetails1 = $param->ComplaintDetails;
            $ComplaintDetails = $ComplaintDetails1;
        }
        if (isset($param->FlatID)) {
            $FlatID = $param->FlatID;
        }
        if (isset($param->RoleID)) {
            $roleid = $param->RoleID;
        }
        if (isset($param->RWAResidentFlatID)) {
            $RWAResidentFlatID = $param->RWAResidentFlatID;
        }
        $sql7 = $this->prdbh->prepare("SELECT  UserRoles.RoleID,ResidentFlatMapping.FlatID FROM UserRoles LEFT JOIN ResidentFlatMapping ON UserRoles.ResidentRWAId=ResidentFlatMapping.ResidentRWAID  WHERE ResidentFlatMapping.ID='" . $RWAResidentFlatID . "'");
        $sql7->execute();
        if ($sql7->rowCount() >= 1) {
            $FunctionID = array();
            while ($row7 = $sql7->fetch(PDO::FETCH_ASSOC)) {
                $FlatID = $row7['FlatID'];
                $roleid = $row7['RoleID'];
            }
        }
        $sql6 = $this->prdbh->prepare("SELECT FunctionID FROM RoleFunctions WHERE RoleFunctions.RoleID='" . $roleid . "'  ");
        $sql6->execute();
        $count6 = $sql6->rowCount();
        if ($sql6->rowCount() >= 1) {
            $FunctionID = array();
            while ($row6 = $sql6->fetch(PDO::FETCH_ASSOC)) {
                $FunctionID[] = $row6['FunctionID'];
            }
        }
        if (in_array(2, $FunctionID)) {
            if ($IsCommonAreaComplaint == 0) {
                if ($ComplaintDetails != '') {
                    $sql = $this->prdbh->prepare("SELECT ComplaintRegister.ID, ComplaintRegister.RWAID,(RWA.Name) as RWAName, ComplaintRegister.CategoryID, ComplaintRegister.RWAResidentFlatID,(SocietyFlatMapping.FlatNbr) as ComplainBy, ComplaintRegister.ComplaintDetails,ComplaintRegister.ManualComplaintNumber,ComplaintRegister.ShortDescription,ComplaintRegister.IsCommonAreaComplaint,IFNULL(ComplaintRegister.Image1,'')as Image1, IFNULL(ComplaintRegister.Image2,'')as Image2, IFNULL(ComplaintRegister.Image3,'')as Image3 , ComplaintRegister.Status, from_unixtime((ComplaintRegister.DateCreated + 19660),'%d/%m/%Y  %h:%i %p') as Datecreated,IFNULL(ComplaintRegister.AssignedTo,'')as AssignedTo,IFNULL(MaintenanceStaff.Name,'')as AssignedToName,IFNULL(ComplaintRegister.AssignedBy,'')as AssignedBy ,(MaintenanceStaff.PrimaryContactNbr) as AssignToNumber,IFNULL( ComplaintRegister.DateAssigned,'')as DateAssigned,IFNULL(User.ID,'')as ComplainByID ,IFNULL(User.FirstName,'') as FlatNbr, IFNULL(ComplaintRegister.ResolvedOn,'')as ResolvedOn,IFNULL(ComplaintRegister.ResolutionNote,'')as ResolutionNote, IFNULL(ComplaintRegister.ResolutionAcknowledgedOn,'')as ResolutionAcknowledgedOn ,IFNULL(ComplaintRegister.AcknowledgementNote,'')as AcknowledgementNote,ComplaintCategory.Description,IFNULL(ComplaintRegister.Rating,'')as Rating FROM ComplaintRegister LEFT JOIN ComplaintCategory ON ComplaintRegister.CategoryID = ComplaintCategory.ID 
        Left JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ComplaintRegister.FlatID
                LEFT JOIN  ResidentFlatMapping ON ComplaintRegister.RWAResidentFlatID = ResidentFlatMapping.ID
                LEFT JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID
                LEFT join User on User.ID=ResidentRWAMapping.ResidentID LEFT JOIN
                MaintenanceStaff ON MaintenanceStaff.ID= ComplaintRegister.AssignedTo  LEFT JOIN RWA ON RWA.ID=ComplaintRegister.RWAID 
        WHERE ComplaintRegister.Status IN ($Status)  AND   ComplaintRegister.RWAID = '" . $RWAID . "' AND (ComplaintRegister.IsCommonAreaComplaint IN (1,0) AND ComplaintCategory.Description like '%" . $ComplaintDetails . "%' AND ComplaintRegister.Status IN ($Status)  AND ComplaintRegister.FlatID='" . $FlatID . "'  ) 
        OR (FROM_BASE64(ComplaintRegister.ComplaintDetails) like '%" . $ComplaintDetails . "%' AND ComplaintRegister.Status IN ($Status) AND ComplaintRegister.FlatID='" . $FlatID . "'  )
        OR (ComplaintRegister.ID like '%" . $ComplaintDetails . "%' AND ComplaintRegister.Status IN ($Status) AND ComplaintRegister.FlatID='" . $FlatID . "' AND  ComplaintRegister.RWAID = '" . $RWAID . "'  ) GROUP BY ComplaintRegister.ID DESC");
                } else {
                    $sql = $this->prdbh->prepare("SELECT ComplaintRegister.ID, ComplaintRegister.RWAID,(RWA.Name) as RWAName, ComplaintRegister.CategoryID, ComplaintRegister.RWAResidentFlatID,(SocietyFlatMapping.FlatNbr) as ComplainBy, ComplaintRegister.ComplaintDetails,ComplaintRegister.ManualComplaintNumber,ComplaintRegister.ShortDescription, ComplaintRegister.IsCommonAreaComplaint,IFNULL(ComplaintRegister.Image1,'')as Image1, IFNULL(ComplaintRegister.Image2,'')as Image2, IFNULL(ComplaintRegister.Image3,'')as Image3 , ComplaintRegister.Status, from_unixtime((ComplaintRegister.DateCreated + 19660),'%d/%m/%Y  %h:%i %p') as Datecreated,IFNULL(ComplaintRegister.AssignedTo,'')as AssignedTo,IFNULL(MaintenanceStaff.Name,'')as AssignedToName,IFNULL(ComplaintRegister.AssignedBy,'')as AssignedBy ,(MaintenanceStaff.PrimaryContactNbr) as AssignToNumber,IFNULL( ComplaintRegister.DateAssigned,'')as DateAssigned,IFNULL(User.ID,'')as ComplainByID ,IFNULL(User.FirstName,'') as FlatNbr, IFNULL(ComplaintRegister.ResolvedOn,'')as ResolvedOn,IFNULL(ComplaintRegister.ResolutionNote,'')as ResolutionNote, IFNULL(ComplaintRegister.ResolutionAcknowledgedOn,'')as ResolutionAcknowledgedOn ,IFNULL(ComplaintRegister.AcknowledgementNote,'')as AcknowledgementNote,ComplaintCategory.Description,IFNULL(ComplaintRegister.Rating,'')as Rating FROM ComplaintRegister LEFT JOIN ComplaintCategory ON ComplaintRegister.CategoryID = ComplaintCategory.ID 
                Left JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ComplaintRegister.FlatID
                LEFT JOIN  ResidentFlatMapping ON ComplaintRegister.RWAResidentFlatID = ResidentFlatMapping.ID
                LEFT JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID
                LEFT join User on User.ID=ResidentRWAMapping.ResidentID LEFT JOIN
                MaintenanceStaff ON MaintenanceStaff.ID= ComplaintRegister.AssignedTo  LEFT JOIN RWA ON RWA.ID=ComplaintRegister.RWAID 
                WHERE ComplaintRegister.Status IN ($Status)  AND  ComplaintRegister.RWAID ='" . $RWAID . "' AND ComplaintRegister.FlatID='" . $FlatID . "' GROUP BY ComplaintRegister.ID DESC Limit $Start,10 ");
                }
            } elseif ($IsCommonAreaComplaint == 1) {
                if ($ComplaintDetails != '') {
                    $sql = $this->prdbh->prepare("SELECT ComplaintRegister.ID, ComplaintRegister.RWAID,(RWA.Name) as RWAName, ComplaintRegister.CategoryID, ComplaintRegister.RWAResidentFlatID,(SocietyFlatMapping.FlatNbr) as ComplainBy, ComplaintRegister.ComplaintDetails, ComplaintRegister.IsCommonAreaComplaint,ComplaintRegister.ManualComplaintNumber,ComplaintRegister.ShortDescription,IFNULL(ComplaintRegister.Image1,'')as Image1, IFNULL(ComplaintRegister.Image2,'')as Image2, IFNULL(ComplaintRegister.Image3,'')as Image3 , ComplaintRegister.Status, from_unixtime((ComplaintRegister.DateCreated + 19660),'%d/%m/%Y  %h:%i %p') as Datecreated,IFNULL(ComplaintRegister.AssignedTo,'')as AssignedTo,IFNULL(MaintenanceStaff.Name,'')as AssignedToName,IFNULL(ComplaintRegister.AssignedBy,'')as AssignedBy ,(MaintenanceStaff.PrimaryContactNbr) as AssignToNumber,IFNULL( ComplaintRegister.DateAssigned,'')as DateAssigned,IFNULL(User.ID,'')as ComplainByID ,IFNULL(User.FirstName,'') as FlatNbr, IFNULL(ComplaintRegister.ResolvedOn,'')as ResolvedOn,IFNULL(ComplaintRegister.ResolutionNote,'')as ResolutionNote, IFNULL(ComplaintRegister.ResolutionAcknowledgedOn,'')as ResolutionAcknowledgedOn ,IFNULL(ComplaintRegister.AcknowledgementNote,'')as AcknowledgementNote,ComplaintCategory.Description,IFNULL(ComplaintRegister.Rating,'')as Rating FROM ComplaintRegister LEFT JOIN ComplaintCategory ON ComplaintRegister.CategoryID = ComplaintCategory.ID 
                        Left JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ComplaintRegister.FlatID
                LEFT JOIN  ResidentFlatMapping ON ComplaintRegister.RWAResidentFlatID = ResidentFlatMapping.ID
                LEFT JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID
                LEFT join User on User.ID=ResidentRWAMapping.ResidentID LEFT JOIN
                MaintenanceStaff ON MaintenanceStaff.ID= ComplaintRegister.AssignedTo  LEFT JOIN RWA ON RWA.ID=ComplaintRegister.RWAID 
                        WHERE ComplaintRegister.Status IN ($Status)  AND  ComplaintRegister.RWAID = '" . $RWAID . "' AND (ComplaintRegister.IsCommonAreaComplaint IN (1,0) AND ComplaintCategory.Description like '%" . $ComplaintDetails1 . "%' AND ComplaintRegister.Status IN ($Status)) 
                                OR (FROM_BASE64(ComplaintRegister.ComplaintDetails) like '%" . $ComplaintDetails . "%' COLLATE Latin1_general_ci AND ComplaintRegister.Status IN ($Status))
                                OR (ComplaintRegister.ID like '%" . $ComplaintDetails1 . "%' AND ComplaintRegister.Status IN ($Status) AND   ComplaintRegister.RWAID = '" . $RWAID . "' ) GROUP BY ComplaintRegister.ID DESC ");
                } else {
                    $sql = $this->prdbh->prepare("SELECT ComplaintRegister.ID, ComplaintRegister.RWAID,(RWA.Name) as RWAName, ComplaintRegister.CategoryID, ComplaintRegister.RWAResidentFlatID,(SocietyFlatMapping.FlatNbr) as ComplainBy, ComplaintRegister.ComplaintDetails, ComplaintRegister.IsCommonAreaComplaint,ComplaintRegister.ManualComplaintNumber,ComplaintRegister.ShortDescription,IFNULL(ComplaintRegister.Image1,'')as Image1, IFNULL(ComplaintRegister.Image2,'')as Image2, IFNULL(ComplaintRegister.Image3,'')as Image3 , ComplaintRegister.Status, from_unixtime((ComplaintRegister.DateCreated + 19660),'%d/%m/%Y  %h:%i %p') as Datecreated,IFNULL(ComplaintRegister.AssignedTo,'')as AssignedTo,IFNULL(MaintenanceStaff.Name,'')as AssignedToName,IFNULL(ComplaintRegister.AssignedBy,'')as AssignedBy ,(MaintenanceStaff.PrimaryContactNbr) as AssignToNumber,IFNULL( ComplaintRegister.DateAssigned,'')as DateAssigned,IFNULL(User.ID,'')as ComplainByID ,IFNULL(User.FirstName,'') as FlatNbr, IFNULL(ComplaintRegister.ResolvedOn,'')as ResolvedOn,IFNULL(ComplaintRegister.ResolutionNote,'')as ResolutionNote, IFNULL(ComplaintRegister.ResolutionAcknowledgedOn,'')as ResolutionAcknowledgedOn ,IFNULL(ComplaintRegister.AcknowledgementNote,'')as AcknowledgementNote,ComplaintCategory.Description,IFNULL(ComplaintRegister.Rating,'')as Rating FROM ComplaintRegister LEFT JOIN ComplaintCategory ON ComplaintRegister.CategoryID = ComplaintCategory.ID 
                Left JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ComplaintRegister.FlatID
                LEFT JOIN  ResidentFlatMapping ON ComplaintRegister.RWAResidentFlatID = ResidentFlatMapping.ID
                LEFT JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID
                LEFT join User on User.ID=ResidentRWAMapping.ResidentID LEFT JOIN
                MaintenanceStaff ON MaintenanceStaff.ID= ComplaintRegister.AssignedTo LEFT JOIN RWA ON RWA.ID=ComplaintRegister.RWAID 
                WHERE ComplaintRegister.IsCommonAreaComplaint IN (1,0) AND ComplaintRegister.Status IN ($Status)  AND  ComplaintRegister.RWAID ='" . $RWAID . "' AND ComplaintRegister.ID NOT IN (SELECT ID FROM ComplaintRegister  WHERE Status IN ('" . $Status . "') AND RWAID ='" . $RWAID . "' AND FlatID='" . $FlatID . "' AND IsCommonAreaComplaint='0')  GROUP BY ComplaintRegister.ID DESC Limit $Start,10");
                }
            }
        } else {
            if ($IsCommonAreaComplaint == 0) {
                if ($ComplaintDetails != '') {
                    $sql = $this->prdbh->prepare("SELECT ComplaintRegister.ID, ComplaintRegister.RWAID,(RWA.Name) as RWAName, ComplaintRegister.CategoryID, ComplaintRegister.RWAResidentFlatID,(SocietyFlatMapping.FlatNbr) as ComplainBy, ComplaintRegister.ComplaintDetails, ComplaintRegister.IsCommonAreaComplaint,ComplaintRegister.ManualComplaintNumber,ComplaintRegister.ShortDescription,IFNULL(ComplaintRegister.Image1,'')as Image1, IFNULL(ComplaintRegister.Image2,'')as Image2, IFNULL(ComplaintRegister.Image3,'')as Image3 , ComplaintRegister.Status, from_unixtime((ComplaintRegister.DateCreated + 19660),'%d/%m/%Y  %h:%i %p') as Datecreated,IFNULL(ComplaintRegister.AssignedTo,'')as AssignedTo,IFNULL(MaintenanceStaff.Name,'')as AssignedToName,IFNULL(ComplaintRegister.AssignedBy,'')as AssignedBy ,(MaintenanceStaff.PrimaryContactNbr) as AssignToNumber,IFNULL( ComplaintRegister.DateAssigned,'')as DateAssigned,IFNULL(User.ID,'')as ComplainByID ,IFNULL(User.FirstName,'') as FlatNbr, IFNULL(ComplaintRegister.ResolvedOn,'')as ResolvedOn,IFNULL(ComplaintRegister.ResolutionNote,'')as ResolutionNote, IFNULL(ComplaintRegister.ResolutionAcknowledgedOn,'')as ResolutionAcknowledgedOn ,IFNULL(ComplaintRegister.AcknowledgementNote,'')as AcknowledgementNote,ComplaintCategory.Description,IFNULL(ComplaintRegister.Rating,'')as Rating FROM ComplaintRegister LEFT JOIN ComplaintCategory ON ComplaintRegister.CategoryID = ComplaintCategory.ID 
                    Left JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ComplaintRegister.FlatID
                LEFT JOIN  ResidentFlatMapping ON ComplaintRegister.RWAResidentFlatID = ResidentFlatMapping.ID
                LEFT JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID
                LEFT join User on User.ID=ResidentRWAMapping.ResidentID LEFT JOIN
                MaintenanceStaff ON MaintenanceStaff.ID= ComplaintRegister.AssignedTo  LEFT JOIN RWA ON RWA.ID=ComplaintRegister.RWAID 
                    WHERE ComplaintRegister.RWAID = '" . $RWAID . "' AND (ComplaintCategory.Description like '%" . $ComplaintDetails1 . "%' AND ComplaintRegister.Status IN ($Status) AND ComplaintRegister.FlatID='" . $FlatID . "' )
                             OR (FROM_BASE64(ComplaintRegister.ComplaintDetails) like '%" . $ComplaintDetails . "%' COLLATE Latin1_general_ci AND ComplaintRegister.Status IN ($Status) AND ComplaintRegister.FlatID='" . $FlatID . "')
                             OR (ComplaintRegister.ID like '%" . $ComplaintDetails1 . "%' AND ComplaintRegister.Status IN ($Status) AND ComplaintRegister.FlatID='" . $FlatID . "')
                             GROUP BY ComplaintRegister.ID DESC ");
                } else {
                    $sql = $this->prdbh->prepare("SELECT ComplaintRegister.ID, ComplaintRegister.RWAID,(RWA.Name) as RWAName, ComplaintRegister.CategoryID, ComplaintRegister.RWAResidentFlatID,(SocietyFlatMapping.FlatNbr) as ComplainBy, ComplaintRegister.ComplaintDetails, ComplaintRegister.IsCommonAreaComplaint,ComplaintRegister.ManualComplaintNumber,ComplaintRegister.ShortDescription,IFNULL(ComplaintRegister.Image1,'')as Image1, IFNULL(ComplaintRegister.Image2,'')as Image2, IFNULL(ComplaintRegister.Image3,'')as Image3 , ComplaintRegister.Status, from_unixtime((ComplaintRegister.DateCreated + 19660),'%d/%m/%Y  %h:%i %p') as Datecreated,IFNULL(ComplaintRegister.AssignedTo,'')as AssignedTo,IFNULL(MaintenanceStaff.Name,'')as AssignedToName,IFNULL(ComplaintRegister.AssignedBy,'')as AssignedBy ,(MaintenanceStaff.PrimaryContactNbr) as AssignToNumber,IFNULL( ComplaintRegister.DateAssigned,'')as DateAssigned,IFNULL(User.ID,'')as ComplainByID ,IFNULL(User.FirstName,'') as FlatNbr, IFNULL(ComplaintRegister.ResolvedOn,'')as ResolvedOn,IFNULL(ComplaintRegister.ResolutionNote,'')as ResolutionNote, IFNULL(ComplaintRegister.ResolutionAcknowledgedOn,'')as ResolutionAcknowledgedOn ,IFNULL(ComplaintRegister.AcknowledgementNote,'')as AcknowledgementNote,ComplaintCategory.Description,IFNULL(ComplaintRegister.Rating,'')as Rating FROM ComplaintRegister LEFT JOIN ComplaintCategory ON ComplaintRegister.CategoryID = ComplaintCategory.ID 
                    Left JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ComplaintRegister.FlatID
                LEFT JOIN  ResidentFlatMapping ON ComplaintRegister.RWAResidentFlatID = ResidentFlatMapping.ID
                LEFT JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID
                LEFT join User on User.ID=ResidentRWAMapping.ResidentID LEFT JOIN
                MaintenanceStaff ON MaintenanceStaff.ID= ComplaintRegister.AssignedTo LEFT JOIN RWA ON RWA.ID=ComplaintRegister.RWAID 
                    WHERE  ComplaintRegister.Status IN ( $Status )  AND  ComplaintRegister.RWAID ='" . $RWAID . "' AND ComplaintRegister.FlatID='" . $FlatID . "' GROUP BY ComplaintRegister.ID DESC Limit $Start,10 ");
                }
            } elseif ($IsCommonAreaComplaint == 1) {
                if ($ComplaintDetails != '') {
                    $sql = $this->prdbh->prepare("SELECT ComplaintRegister.ID, ComplaintRegister.RWAID,(RWA.Name) as RWAName, ComplaintRegister.CategoryID, ComplaintRegister.RWAResidentFlatID,(SocietyFlatMapping.FlatNbr) as ComplainBy, ComplaintRegister.ComplaintDetails, ComplaintRegister.IsCommonAreaComplaint,ComplaintRegister.ManualComplaintNumber,ComplaintRegister.ShortDescription,IFNULL(ComplaintRegister.Image1,'')as Image1, IFNULL(ComplaintRegister.Image2,'')as Image2, IFNULL(ComplaintRegister.Image3,'')as Image3 , ComplaintRegister.Status, from_unixtime((ComplaintRegister.DateCreated + 19660),'%d/%m/%Y  %h:%i %p') as Datecreated,IFNULL(ComplaintRegister.AssignedTo,'')as AssignedTo,IFNULL(MaintenanceStaff.Name,'')as AssignedToName,IFNULL(ComplaintRegister.AssignedBy,'')as AssignedBy ,(MaintenanceStaff.PrimaryContactNbr) as AssignToNumber,IFNULL( ComplaintRegister.DateAssigned,'')as DateAssigned,IFNULL(User.ID,'')as ComplainByID ,IFNULL(User.FirstName,'') as FlatNbr, IFNULL(ComplaintRegister.ResolvedOn,'')as ResolvedOn,IFNULL(ComplaintRegister.ResolutionNote,'')as ResolutionNote, IFNULL(ComplaintRegister.ResolutionAcknowledgedOn,'')as ResolutionAcknowledgedOn ,IFNULL(ComplaintRegister.AcknowledgementNote,'')as AcknowledgementNote,ComplaintCategory.Description,IFNULL(ComplaintRegister.Rating,'')as Rating FROM ComplaintRegister LEFT JOIN ComplaintCategory ON ComplaintRegister.CategoryID = ComplaintCategory.ID 
                Left JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ComplaintRegister.FlatID
                LEFT JOIN  ResidentFlatMapping ON ComplaintRegister.RWAResidentFlatID = ResidentFlatMapping.ID
                LEFT JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID
                LEFT join User on User.ID=ResidentRWAMapping.ResidentID LEFT JOIN
                MaintenanceStaff ON MaintenanceStaff.ID= ComplaintRegister.AssignedTo  LEFT JOIN RWA ON RWA.ID=ComplaintRegister.RWAID 
                    WHERE ComplaintRegister.RWAID = '" . $RWAID . "' AND (ComplaintCategory.Description like '%" . $ComplaintDetails1 . "%' AND ComplaintRegister.Status IN ($Status) AND ComplaintRegister.IsCommonAreaComplaint ='1' )
                             OR (FROM_BASE64(ComplaintRegister.ComplaintDetails) like '%" . $ComplaintDetails . "%' COLLATE Latin1_general_ci AND ComplaintRegister.Status IN ($Status) AND ComplaintRegister.IsCommonAreaComplaint ='1')
                             OR (ComplaintRegister.ID like '%" . $ComplaintDetails1 . "%' AND ComplaintRegister.Status IN ($Status) AND ComplaintRegister.IsCommonAreaComplaint ='1' )
                             GROUP BY ComplaintRegister.ID DESC ");
                } else {
                    $sql = $this->prdbh->prepare("SELECT ComplaintRegister.ID, ComplaintRegister.RWAID,(RWA.Name) as RWAName, ComplaintRegister.CategoryID,(SocietyFlatMapping.FlatNbr) as ComplainBy, ComplaintRegister.ComplaintDetails, ComplaintRegister.IsCommonAreaComplaint,ComplaintRegister.ManualComplaintNumber,ComplaintRegister.ShortDescription,IFNULL(ComplaintRegister.Image1,'')as Image1, IFNULL(ComplaintRegister.Image2,'')as Image2, IFNULL(ComplaintRegister.Image3,'')as Image3 , ComplaintRegister.Status, from_unixtime((ComplaintRegister.DateCreated + 19660),'%d/%m/%Y  %h:%i %p') as Datecreated,IFNULL(ComplaintRegister.AssignedTo,'')as AssignedTo,IFNULL(MaintenanceStaff.Name,'')as AssignedToName,IFNULL(ComplaintRegister.AssignedBy,'')as AssignedBy ,(MaintenanceStaff.PrimaryContactNbr) as AssignToNumber,IFNULL( ComplaintRegister.DateAssigned,'')as DateAssigned,IFNULL(User.ID,'')as ComplainByID ,IFNULL(User.FirstName,'') as FlatNbr, IFNULL(ComplaintRegister.ResolvedOn,'')as ResolvedOn,IFNULL(ComplaintRegister.ResolutionNote,'')as ResolutionNote, IFNULL(ComplaintRegister.ResolutionAcknowledgedOn,'')as ResolutionAcknowledgedOn ,IFNULL(ComplaintRegister.AcknowledgementNote,'')as AcknowledgementNote,ComplaintCategory.Description,IFNULL(ComplaintRegister.Rating,'')as Rating FROM ComplaintRegister LEFT JOIN ComplaintCategory ON ComplaintRegister.CategoryID = ComplaintCategory.ID 
                Left JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ComplaintRegister.FlatID
                LEFT JOIN  ResidentFlatMapping ON ComplaintRegister.RWAResidentFlatID = ResidentFlatMapping.ID
                LEFT JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID
                LEFT join User on User.ID=ResidentRWAMapping.ResidentID LEFT JOIN
                MaintenanceStaff ON MaintenanceStaff.ID= ComplaintRegister.AssignedTo  LEFT JOIN RWA ON RWA.ID=ComplaintRegister.RWAID 
                WHERE ComplaintRegister.IsCommonAreaComplaint =  '1' AND ComplaintRegister.Status IN ( $Status )  AND  ComplaintRegister.RWAID ='" . $RWAID . "'  GROUP BY ComplaintRegister.ID DESC Limit $Start,10");
                }
            }
        }
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count == 0) {
                $response['Show Complain'] = [];
                $response['Status'] = '0';
            } else {
                $i = 0;
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items = $row;
                    $response['Show Complain'][$i] = $items;
                    $id = $row['ID'];
                    $sql1 = $this->prdbh->prepare("SELECT CompRemark.ID, CompRemark.Remark, CompRemark.CompID, CompRemark.RemarkDate, CompRemark.EnteredBy,User.FirstName,SocietyFlatMapping.FlatNbr 
            FROM CompRemark INNER JOIN ResidentRWAMapping oN ResidentRWAMapping.ResidentRWAID=CompRemark.EnteredBy 
            LEFT JOIN User ON User.ID=ResidentRWAMapping.ResidentID 
            LEFT JOIN ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID =ResidentRWAMapping.ResidentRWAID
            INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID WHERE CompID=  '" . $id . "' group By ID");
                    $sql1->execute();
                    $count1 = $sql1->rowCount();
                    if ($count1 > 0) {
                        $subitems = array();
                        while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                            $subitems[] = $row1;
                            $response['Show Complain'][$i]['Remark'] = $subitems;
                        }
                    }
                    $i++;
                }
                $response['Status'] = '1';
            }
        }
        return $response;
    }
    public function showcomplain1($param) {
        if (isset($param->IsCommonAreaComplaint)) {
            $IsCommonAreaComplaint = $param->IsCommonAreaComplaint;
        }
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        if (isset($param->Status)) {
            $Status = $param->Status;
        }
        if (isset($param->PageNo)) {
            $PageNo = $param->PageNo;
        }
        $Start = $PageNo * 10 - 10;
        $response = array();
        if ($Status == 0) {
            $Status = "0,1";
        } elseif ($Status == 1) {
            $Status = "2,3,4";
        }
        if (isset($param->ComplaintDetails)) {
            $ComplaintDetails1 = $param->ComplaintDetails;
            $ComplaintDetails = $ComplaintDetails1;
        }
        if (isset($param->RWAResidentFlatID)) {
            $RWAResidentFlatID = $param->RWAResidentFlatID;
        }
        $sql2 = $this->prdbh->prepare("SELECT   UserRoles.RoleID FROM UserRoles LEFT JOIN ResidentFlatMapping ON UserRoles.ResidentRWAId=ResidentFlatMapping.ResidentRWAID INNER JOIN RoleFunctions ON UserRoles.RoleID=RoleFunctions.RoleID WHERE ResidentFlatMapping.ID='" . $RWAResidentFlatID . "' AND RoleFunctions.FunctionID ='2' ");
        $sql2->execute();
        if ($sql2->execute()) {
            $count2 = $sql2->rowCount();
            if ($count2 >= 1) {
                if ($IsCommonAreaComplaint == 0) {
                    if ($ComplaintDetails != '') {
                        $sql = $this->prdbh->prepare("SELECT ComplaintRegister.ID, ComplaintRegister.RWAID,(RWA.Name) as RWAName, ComplaintRegister.CategoryID, ComplaintRegister.RWAResidentFlatID,SocietyFlatMapping.FlatNbr, ComplaintRegister.ComplaintDetails,ComplaintRegister.ManualComplaintNumber,ComplaintRegister.ShortDescription,ComplaintRegister.IsCommonAreaComplaint,IFNULL(ComplaintRegister.Image1,'')as Image1, IFNULL(ComplaintRegister.Image2,'')as Image2, IFNULL(ComplaintRegister.Image3,'')as Image3 , ComplaintRegister.Status, from_unixtime((ComplaintRegister.DateCreated + 19660),'%d/%m/%Y  %h:%i %p') as Datecreated,IFNULL(ComplaintRegister.AssignedTo,'')as AssignedTo,IFNULL(MaintenanceStaff.Name,'')as AssignedToName,IFNULL(ComplaintRegister.AssignedBy,'')as AssignedBy ,(MaintenanceStaff.PrimaryContactNbr) as AssignToNumber,IFNULL( ComplaintRegister.DateAssigned,'')as DateAssigned,IFNULL(User.ID,'')as ComplainByID ,IFNULL(User.FirstName,'') as ComplainBy, IFNULL(ComplaintRegister.ResolvedOn,'')as ResolvedOn,IFNULL(ComplaintRegister.ResolutionNote,'')as ResolutionNote, IFNULL(ComplaintRegister.ResolutionAcknowledgedOn,'')as ResolutionAcknowledgedOn ,IFNULL(ComplaintRegister.AcknowledgementNote,'')as AcknowledgementNote,ComplaintCategory.Description,IFNULL(ComplaintRegister.Rating,'')as Rating FROM ComplaintRegister LEFT JOIN ComplaintCategory ON ComplaintRegister.CategoryID = ComplaintCategory.ID 
        LEFT JOIN  ResidentFlatMapping ON ComplaintRegister.RWAResidentFlatID = ResidentFlatMapping.ID LEFT JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID 
        LEFT join User on User.ID=ResidentRWAMapping.ResidentID LEFT JOIN
        MaintenanceStaff ON MaintenanceStaff.ID= ComplaintRegister.AssignedTo  LEFT JOIN RWA ON RWA.ID=ComplaintRegister.RWAID INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID
        WHERE ComplaintRegister.Status IN ($Status)  AND   ComplaintRegister.RWAID = '" . $RWAID . "' AND (ComplaintRegister.IsCommonAreaComplaint IN (1,0) AND ComplaintCategory.Description like '%" . $ComplaintDetails . "%' AND ComplaintRegister.Status IN ($Status)  AND ComplaintRegister.RWAResidentFlatID='" . $RWAResidentFlatID . "'  ) 
        OR (FROM_BASE64(ComplaintRegister.ComplaintDetails) like '%" . $ComplaintDetails . "%' AND ComplaintRegister.Status IN ($Status) AND ComplaintRegister.RWAResidentFlatID='" . $RWAResidentFlatID . "'  )
        OR (ComplaintRegister.ID like '%" . $ComplaintDetails . "%' AND ComplaintRegister.Status IN ($Status) AND ComplaintRegister.RWAResidentFlatID='" . $RWAResidentFlatID . "' AND  ComplaintRegister.RWAID = '" . $RWAID . "'  ) GROUP BY ComplaintRegister.ID DESC");
                    } else {
                        $sql = $this->prdbh->prepare("SELECT ComplaintRegister.ID, ComplaintRegister.RWAID,(RWA.Name) as RWAName, ComplaintRegister.CategoryID, ComplaintRegister.RWAResidentFlatID,SocietyFlatMapping.FlatNbr, ComplaintRegister.ComplaintDetails,ComplaintRegister.ManualComplaintNumber,ComplaintRegister.ShortDescription, ComplaintRegister.IsCommonAreaComplaint,IFNULL(ComplaintRegister.Image1,'')as Image1, IFNULL(ComplaintRegister.Image2,'')as Image2, IFNULL(ComplaintRegister.Image3,'')as Image3 , ComplaintRegister.Status, from_unixtime((ComplaintRegister.DateCreated + 19660),'%d/%m/%Y  %h:%i %p') as Datecreated,IFNULL(ComplaintRegister.AssignedTo,'')as AssignedTo,IFNULL(MaintenanceStaff.Name,'')as AssignedToName,IFNULL(ComplaintRegister.AssignedBy,'')as AssignedBy ,(MaintenanceStaff.PrimaryContactNbr) as AssignToNumber,IFNULL( ComplaintRegister.DateAssigned,'')as DateAssigned,IFNULL(User.ID,'')as ComplainByID ,IFNULL(User.FirstName,'') as ComplainBy, IFNULL(ComplaintRegister.ResolvedOn,'')as ResolvedOn,IFNULL(ComplaintRegister.ResolutionNote,'')as ResolutionNote, IFNULL(ComplaintRegister.ResolutionAcknowledgedOn,'')as ResolutionAcknowledgedOn ,IFNULL(ComplaintRegister.AcknowledgementNote,'')as AcknowledgementNote,ComplaintCategory.Description,IFNULL(ComplaintRegister.Rating,'')as Rating FROM ComplaintRegister LEFT JOIN ComplaintCategory ON ComplaintRegister.CategoryID = ComplaintCategory.ID 
                LEFT JOIN  ResidentFlatMapping ON ComplaintRegister.RWAResidentFlatID = ResidentFlatMapping.ID LEFT JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID 
                LEFT join User on User.ID=ResidentRWAMapping.ResidentID LEFT JOIN MaintenanceStaff ON MaintenanceStaff.ID= ComplaintRegister.AssignedTo  LEFT JOIN RWA ON RWA.ID=ComplaintRegister.RWAID INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID
                WHERE ComplaintRegister.Status IN ($Status)  AND  ComplaintRegister.RWAID ='" . $RWAID . "' AND ComplaintRegister.RWAResidentFlatID='" . $RWAResidentFlatID . "' GROUP BY ComplaintRegister.ID DESC Limit $Start,10 ");
                    }
                } elseif ($IsCommonAreaComplaint == 1) {
                    if ($ComplaintDetails != '') {
                        $sql = $this->prdbh->prepare("SELECT ComplaintRegister.ID, ComplaintRegister.RWAID,(RWA.Name) as RWAName, ComplaintRegister.CategoryID, ComplaintRegister.RWAResidentFlatID,SocietyFlatMapping.FlatNbr, ComplaintRegister.ComplaintDetails, ComplaintRegister.IsCommonAreaComplaint,ComplaintRegister.ManualComplaintNumber,ComplaintRegister.ShortDescription,IFNULL(ComplaintRegister.Image1,'')as Image1, IFNULL(ComplaintRegister.Image2,'')as Image2, IFNULL(ComplaintRegister.Image3,'')as Image3 , ComplaintRegister.Status, from_unixtime((ComplaintRegister.DateCreated + 19660),'%d/%m/%Y  %h:%i %p') as Datecreated,IFNULL(ComplaintRegister.AssignedTo,'')as AssignedTo,IFNULL(MaintenanceStaff.Name,'')as AssignedToName,IFNULL(ComplaintRegister.AssignedBy,'')as AssignedBy ,(MaintenanceStaff.PrimaryContactNbr) as AssignToNumber,IFNULL( ComplaintRegister.DateAssigned,'')as DateAssigned,IFNULL(User.ID,'')as ComplainByID ,IFNULL(User.FirstName,'') as ComplainBy, IFNULL(ComplaintRegister.ResolvedOn,'')as ResolvedOn,IFNULL(ComplaintRegister.ResolutionNote,'')as ResolutionNote, IFNULL(ComplaintRegister.ResolutionAcknowledgedOn,'')as ResolutionAcknowledgedOn ,IFNULL(ComplaintRegister.AcknowledgementNote,'')as AcknowledgementNote,ComplaintCategory.Description,IFNULL(ComplaintRegister.Rating,'')as Rating FROM ComplaintRegister LEFT JOIN ComplaintCategory ON ComplaintRegister.CategoryID = ComplaintCategory.ID 
                        LEFT JOIN  ResidentFlatMapping ON ComplaintRegister.RWAResidentFlatID = ResidentFlatMapping.ID LEFT JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID 
                        LEFT join User on User.ID=ResidentRWAMapping.ResidentID LEFT JOIN
                        MaintenanceStaff ON MaintenanceStaff.ID= ComplaintRegister.AssignedTo  LEFT JOIN RWA ON RWA.ID=ComplaintRegister.RWAID INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID
                        WHERE ComplaintRegister.Status IN ($Status)  AND  ComplaintRegister.RWAID = '" . $RWAID . "' AND (ComplaintRegister.IsCommonAreaComplaint IN (1,0) AND ComplaintCategory.Description like '%" . $ComplaintDetails1 . "%' AND ComplaintRegister.Status IN ($Status)) 
                                OR (FROM_BASE64(ComplaintRegister.ComplaintDetails) like '%" . $ComplaintDetails . "%' COLLATE Latin1_general_ci AND ComplaintRegister.Status IN ($Status))
                                OR (ComplaintRegister.ID like '%" . $ComplaintDetails1 . "%' AND ComplaintRegister.Status IN ($Status) AND   ComplaintRegister.RWAID = '" . $RWAID . "' ) GROUP BY ComplaintRegister.ID DESC ");
                    } else {
                        $sql = $this->prdbh->prepare("SELECT ComplaintRegister.ID, ComplaintRegister.RWAID,(RWA.Name) as RWAName, ComplaintRegister.CategoryID, ComplaintRegister.RWAResidentFlatID,SocietyFlatMapping.FlatNbr, ComplaintRegister.ComplaintDetails, ComplaintRegister.IsCommonAreaComplaint,ComplaintRegister.ManualComplaintNumber,ComplaintRegister.ShortDescription,IFNULL(ComplaintRegister.Image1,'')as Image1, IFNULL(ComplaintRegister.Image2,'')as Image2, IFNULL(ComplaintRegister.Image3,'')as Image3 , ComplaintRegister.Status, from_unixtime((ComplaintRegister.DateCreated + 19660),'%d/%m/%Y  %h:%i %p') as Datecreated,IFNULL(ComplaintRegister.AssignedTo,'')as AssignedTo,IFNULL(MaintenanceStaff.Name,'')as AssignedToName,IFNULL(ComplaintRegister.AssignedBy,'')as AssignedBy ,(MaintenanceStaff.PrimaryContactNbr) as AssignToNumber,IFNULL( ComplaintRegister.DateAssigned,'')as DateAssigned,IFNULL(User.ID,'')as ComplainByID ,IFNULL(User.FirstName,'') as ComplainBy, IFNULL(ComplaintRegister.ResolvedOn,'')as ResolvedOn,IFNULL(ComplaintRegister.ResolutionNote,'')as ResolutionNote, IFNULL(ComplaintRegister.ResolutionAcknowledgedOn,'')as ResolutionAcknowledgedOn ,IFNULL(ComplaintRegister.AcknowledgementNote,'')as AcknowledgementNote,ComplaintCategory.Description,IFNULL(ComplaintRegister.Rating,'')as Rating FROM ComplaintRegister LEFT JOIN ComplaintCategory ON ComplaintRegister.CategoryID = ComplaintCategory.ID 
                LEFT JOIN  ResidentFlatMapping ON ComplaintRegister.RWAResidentFlatID = ResidentFlatMapping.ID LEFT JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID 
                LEFT join User on User.ID=ResidentRWAMapping.ResidentID LEFT JOIN
                MaintenanceStaff ON MaintenanceStaff.ID= ComplaintRegister.AssignedTo  LEFT JOIN RWA ON RWA.ID=ComplaintRegister.RWAID INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID
                WHERE ComplaintRegister.IsCommonAreaComplaint IN (1,0) AND ComplaintRegister.Status IN ($Status)  AND  ComplaintRegister.RWAID ='" . $RWAID . "' AND ComplaintRegister.ID NOT IN (SELECT ID FROM ComplaintRegister  WHERE Status IN ('" . $Status . "') AND RWAID ='" . $RWAID . "' AND RWAResidentFlatID='" . $RWAResidentFlatID . "' AND IsCommonAreaComplaint='0')  GROUP BY ComplaintRegister.ID DESC Limit $Start,10");
                    }
                }
            } else {
                if ($IsCommonAreaComplaint == 0) {
                    if ($ComplaintDetails != '') {
                        $sql = $this->prdbh->prepare("SELECT ComplaintRegister.ID, ComplaintRegister.RWAID,(RWA.Name) as RWAName, ComplaintRegister.CategoryID, ComplaintRegister.RWAResidentFlatID,SocietyFlatMapping.FlatNbr, ComplaintRegister.ComplaintDetails, ComplaintRegister.IsCommonAreaComplaint,ComplaintRegister.ManualComplaintNumber,ComplaintRegister.ShortDescription,IFNULL(ComplaintRegister.Image1,'')as Image1, IFNULL(ComplaintRegister.Image2,'')as Image2, IFNULL(ComplaintRegister.Image3,'')as Image3 , ComplaintRegister.Status, from_unixtime((ComplaintRegister.DateCreated + 19660),'%d/%m/%Y  %h:%i %p') as Datecreated,IFNULL(ComplaintRegister.AssignedTo,'')as AssignedTo,IFNULL(MaintenanceStaff.Name,'')as AssignedToName,IFNULL(ComplaintRegister.AssignedBy,'')as AssignedBy ,(MaintenanceStaff.PrimaryContactNbr) as AssignToNumber,IFNULL( ComplaintRegister.DateAssigned,'')as DateAssigned,IFNULL(User.ID,'')as ComplainByID ,IFNULL(User.FirstName,'') as ComplainBy, IFNULL(ComplaintRegister.ResolvedOn,'')as ResolvedOn,IFNULL(ComplaintRegister.ResolutionNote,'')as ResolutionNote, IFNULL(ComplaintRegister.ResolutionAcknowledgedOn,'')as ResolutionAcknowledgedOn ,IFNULL(ComplaintRegister.AcknowledgementNote,'')as AcknowledgementNote,ComplaintCategory.Description,IFNULL(ComplaintRegister.Rating,'')as Rating FROM ComplaintRegister LEFT JOIN ComplaintCategory ON ComplaintRegister.CategoryID = ComplaintCategory.ID 
                    LEFT JOIN  ResidentFlatMapping ON ComplaintRegister.RWAResidentFlatID = ResidentFlatMapping.ID LEFT JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID 
                    LEFT join User on User.ID=ResidentRWAMapping.ResidentID LEFT JOIN
                    MaintenanceStaff ON MaintenanceStaff.ID= ComplaintRegister.AssignedTo  LEFT JOIN RWA ON RWA.ID=ComplaintRegister.RWAID INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID
                    WHERE ComplaintRegister.RWAID = '" . $RWAID . "' AND (ComplaintCategory.Description like '%" . $ComplaintDetails1 . "%' AND ComplaintRegister.Status IN ($Status) AND ComplaintRegister.RWAResidentFlatID='" . $RWAResidentFlatID . "' )
                             OR (FROM_BASE64(ComplaintRegister.ComplaintDetails) like '%" . $ComplaintDetails . "%' COLLATE Latin1_general_ci AND ComplaintRegister.Status IN ($Status) AND ComplaintRegister.RWAResidentFlatID='" . $RWAResidentFlatID . "')
                             OR (ComplaintRegister.ID like '%" . $ComplaintDetails1 . "%' AND ComplaintRegister.Status IN ($Status) AND ComplaintRegister.RWAResidentFlatID='" . $RWAResidentFlatID . "')
                             GROUP BY ComplaintRegister.ID DESC ");
                    } else {
                        $sql = $this->prdbh->prepare("SELECT ComplaintRegister.ID, ComplaintRegister.RWAID,(RWA.Name) as RWAName, ComplaintRegister.CategoryID, ComplaintRegister.RWAResidentFlatID,SocietyFlatMapping.FlatNbr, ComplaintRegister.ComplaintDetails, ComplaintRegister.IsCommonAreaComplaint,ComplaintRegister.ManualComplaintNumber,ComplaintRegister.ShortDescription,IFNULL(ComplaintRegister.Image1,'')as Image1, IFNULL(ComplaintRegister.Image2,'')as Image2, IFNULL(ComplaintRegister.Image3,'')as Image3 , ComplaintRegister.Status, from_unixtime((ComplaintRegister.DateCreated + 19660),'%d/%m/%Y  %h:%i %p') as Datecreated,IFNULL(ComplaintRegister.AssignedTo,'')as AssignedTo,IFNULL(MaintenanceStaff.Name,'')as AssignedToName,IFNULL(ComplaintRegister.AssignedBy,'')as AssignedBy ,(MaintenanceStaff.PrimaryContactNbr) as AssignToNumber,IFNULL( ComplaintRegister.DateAssigned,'')as DateAssigned,IFNULL(User.ID,'')as ComplainByID ,IFNULL(User.FirstName,'') as ComplainBy, IFNULL(ComplaintRegister.ResolvedOn,'')as ResolvedOn,IFNULL(ComplaintRegister.ResolutionNote,'')as ResolutionNote, IFNULL(ComplaintRegister.ResolutionAcknowledgedOn,'')as ResolutionAcknowledgedOn ,IFNULL(ComplaintRegister.AcknowledgementNote,'')as AcknowledgementNote,ComplaintCategory.Description,IFNULL(ComplaintRegister.Rating,'')as Rating FROM ComplaintRegister LEFT JOIN ComplaintCategory ON ComplaintRegister.CategoryID = ComplaintCategory.ID 
                    LEFT JOIN  ResidentFlatMapping ON ComplaintRegister.RWAResidentFlatID = ResidentFlatMapping.ID LEFT JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID 
                    LEFT join User on User.ID=ResidentRWAMapping.ResidentID LEFT JOIN
                    MaintenanceStaff ON MaintenanceStaff.ID= ComplaintRegister.AssignedTo  LEFT JOIN RWA ON RWA.ID=ComplaintRegister.RWAID INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID
                    WHERE  ComplaintRegister.Status IN ( $Status )  AND  ComplaintRegister.RWAID ='" . $RWAID . "' AND ComplaintRegister.RWAResidentFlatID='" . $RWAResidentFlatID . "' GROUP BY ComplaintRegister.ID DESC Limit $Start,10 ");
                    }
                } elseif ($IsCommonAreaComplaint == 1) {
                    if ($ComplaintDetails != '') {
                        $sql = $this->prdbh->prepare("SELECT ComplaintRegister.ID, ComplaintRegister.RWAID,(RWA.Name) as RWAName, ComplaintRegister.CategoryID, ComplaintRegister.RWAResidentFlatID,SocietyFlatMapping.FlatNbr, ComplaintRegister.ComplaintDetails, ComplaintRegister.IsCommonAreaComplaint,ComplaintRegister.ManualComplaintNumber,ComplaintRegister.ShortDescription,IFNULL(ComplaintRegister.Image1,'')as Image1, IFNULL(ComplaintRegister.Image2,'')as Image2, IFNULL(ComplaintRegister.Image3,'')as Image3 , ComplaintRegister.Status, from_unixtime((ComplaintRegister.DateCreated + 19660),'%d/%m/%Y  %h:%i %p') as Datecreated,IFNULL(ComplaintRegister.AssignedTo,'')as AssignedTo,IFNULL(MaintenanceStaff.Name,'')as AssignedToName,IFNULL(ComplaintRegister.AssignedBy,'')as AssignedBy ,(MaintenanceStaff.PrimaryContactNbr) as AssignToNumber,IFNULL( ComplaintRegister.DateAssigned,'')as DateAssigned,IFNULL(User.ID,'')as ComplainByID ,IFNULL(User.FirstName,'') as ComplainBy, IFNULL(ComplaintRegister.ResolvedOn,'')as ResolvedOn,IFNULL(ComplaintRegister.ResolutionNote,'')as ResolutionNote, IFNULL(ComplaintRegister.ResolutionAcknowledgedOn,'')as ResolutionAcknowledgedOn ,IFNULL(ComplaintRegister.AcknowledgementNote,'')as AcknowledgementNote,ComplaintCategory.Description,IFNULL(ComplaintRegister.Rating,'')as Rating FROM ComplaintRegister LEFT JOIN ComplaintCategory ON ComplaintRegister.CategoryID = ComplaintCategory.ID 
                    LEFT JOIN  ResidentFlatMapping ON ComplaintRegister.RWAResidentFlatID = ResidentFlatMapping.ID LEFT JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID 
                    LEFT join User on User.ID=ResidentRWAMapping.ResidentID LEFT JOIN
                    MaintenanceStaff ON MaintenanceStaff.ID= ComplaintRegister.AssignedTo  LEFT JOIN RWA ON RWA.ID=ComplaintRegister.RWAID INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID
                    WHERE ComplaintRegister.RWAID = '" . $RWAID . "' AND (ComplaintCategory.Description like '%" . $ComplaintDetails1 . "%' AND ComplaintRegister.Status IN ($Status) AND ComplaintRegister.IsCommonAreaComplaint ='1' )
                             OR (FROM_BASE64(ComplaintRegister.ComplaintDetails) like '%" . $ComplaintDetails . "%' COLLATE Latin1_general_ci AND ComplaintRegister.Status IN ($Status) AND ComplaintRegister.IsCommonAreaComplaint ='1')
                             OR (ComplaintRegister.ID like '%" . $ComplaintDetails1 . "%' AND ComplaintRegister.Status IN ($Status) AND ComplaintRegister.IsCommonAreaComplaint ='1' )
                             GROUP BY ComplaintRegister.ID DESC ");
                    } else {
                        $sql = $this->prdbh->prepare("SELECT ComplaintRegister.ID, ComplaintRegister.RWAID,(RWA.Name) as RWAName, ComplaintRegister.CategoryID, ComplaintRegister.RWAResidentFlatID,SocietyFlatMapping.FlatNbr, ComplaintRegister.ComplaintDetails, ComplaintRegister.IsCommonAreaComplaint,ComplaintRegister.ManualComplaintNumber,ComplaintRegister.ShortDescription,IFNULL(ComplaintRegister.Image1,'')as Image1, IFNULL(ComplaintRegister.Image2,'')as Image2, IFNULL(ComplaintRegister.Image3,'')as Image3 , ComplaintRegister.Status, from_unixtime((ComplaintRegister.DateCreated + 19660),'%d/%m/%Y  %h:%i %p') as Datecreated,IFNULL(ComplaintRegister.AssignedTo,'')as AssignedTo,IFNULL(MaintenanceStaff.Name,'')as AssignedToName,IFNULL(ComplaintRegister.AssignedBy,'')as AssignedBy ,(MaintenanceStaff.PrimaryContactNbr) as AssignToNumber,IFNULL( ComplaintRegister.DateAssigned,'')as DateAssigned,IFNULL(User.ID,'')as ComplainByID ,IFNULL(User.FirstName,'') as ComplainBy, IFNULL(ComplaintRegister.ResolvedOn,'')as ResolvedOn,IFNULL(ComplaintRegister.ResolutionNote,'')as ResolutionNote, IFNULL(ComplaintRegister.ResolutionAcknowledgedOn,'')as ResolutionAcknowledgedOn ,IFNULL(ComplaintRegister.AcknowledgementNote,'')as AcknowledgementNote,ComplaintCategory.Description,IFNULL(ComplaintRegister.Rating,'')as Rating FROM ComplaintRegister LEFT JOIN ComplaintCategory ON ComplaintRegister.CategoryID = ComplaintCategory.ID 
                LEFT JOIN  ResidentFlatMapping ON ComplaintRegister.RWAResidentFlatID = ResidentFlatMapping.ID LEFT JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID 
                LEFT join User on User.ID=ResidentRWAMapping.ResidentID LEFT JOIN
                MaintenanceStaff ON MaintenanceStaff.ID= ComplaintRegister.AssignedTo  LEFT JOIN RWA ON RWA.ID=ComplaintRegister.RWAID INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID
                WHERE ComplaintRegister.IsCommonAreaComplaint =  '1' AND ComplaintRegister.Status IN ( $Status )  AND  ComplaintRegister.RWAID ='" . $RWAID . "'  GROUP BY ComplaintRegister.ID DESC Limit $Start,10");
                    }
                }
            }
        }
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count == 0) {
                $response['Show Complain'] = [];
                $response['Status'] = '0';
            } else {
                $i = 0;
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items = $row;
                    $response['Show Complain'][$i] = $items;
                    $id = $row['ID'];
                    $sql1 = $this->prdbh->prepare("SELECT CompRemark.ID, CompRemark.Remark, CompRemark.CompID, CompRemark.RemarkDate, CompRemark.EnteredBy,User.FirstName,SocietyFlatMapping.FlatNbr 
            FROM CompRemark INNER JOIN ResidentRWAMapping oN ResidentRWAMapping.ResidentRWAID=CompRemark.EnteredBy 
            LEFT JOIN User ON User.ID=ResidentRWAMapping.ResidentID 
            LEFT JOIN ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID =ResidentRWAMapping.ResidentRWAID
            INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID WHERE CompID=  '" . $id . "' group By ID");
                    $sql1->execute();
                    $count1 = $sql1->rowCount();
                    if ($count1 > 0) {
                        $subitems = array();
                        while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                            $subitems[] = $row1;
                            $response['Show Complain'][$i]['Remark'] = $subitems;
                        }
                    }
                    $i++;
                }
                $response['Status'] = '1';
            }
        }
        return $response;
    }
    public function visitorsearchbycar($param) {
        if (isset($param->VehicleNbr)) {
            $VehicleNbr = $param->VehicleNbr;
        }
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        if (isset($VehicleNbr) && empty($VehicleNbr)) {
            $response['Status'] = 0;
        } else {
            $response = array();
            // $sql = $this->prdbh->prepare(" SELECT (SocietyFlatMapping.ID) as FlatOriginalID,SocietyFlatMapping.FlatNbr,VisitorTable.VisitorName,
            // VisitorTable.VisitorPhoneNumber FROM SocietyFlatMapping LEFT JOIN VisitorFlatMapping ON VisitorFlatMapping.FlatID= SocietyFlatMapping.ID LEFT JOIN VisitorDetails ON VisitorDetails.VisitorID=VisitorFlatMapping.VisitorID
            // LEFT JOIN VisitorTable ON VisitorDetails.VisitorID=VisitorTable.VisitorID
            // WHERE VisitorTable.VisitorInOut='1' AND VisitorTable.RWAID='" . $RWAID . "' AND VisitorDetails.VisitorVehicleNumber like '%" . $VehicleNbr . "%' ");
            $sql = $this->prdbh->prepare("SELECT 
            (SocietyFlatMapping.ID) AS FlatOriginalID,
            SocietyFlatMapping.FlatNbr,
            VisitorTable.VisitorName,
            VisitorTable.VisitorPhoneNumber,
            ParkingTable.ParkingNo
        FROM
            SocietyFlatMapping
                INNER JOIN
            VisitorFlatMapping ON VisitorFlatMapping.FlatID = SocietyFlatMapping.ID
                INNER JOIN
            VisitorDetails ON VisitorDetails.VisitorID = VisitorFlatMapping.VisitorID
                INNER JOIN
            VisitorTable ON VisitorDetails.VisitorID = VisitorTable.VisitorID
                INNER JOIN
            ParkingTable ON ParkingTable.VisitorID = VisitorTable.VisitorID
           WHERE VisitorTable.VisitorInOut='1' AND VisitorTable.RWAID='" . $RWAID . "'
             and VisitorDetails.VisitorEntryTime > (unix_timestamp()-172800) AND VisitorDetails.VisitorVehicleNumber like '%" . $VehicleNbr . "%' ");
           $sql->execute();
          
            $sql1 = $this->prdbh->prepare("SELECT ID,ParkingNo From ParkingTable where Flat_ID=0 AND RWAID ='" . $RWAID . "' ");
            $sql1->execute();
            $count = $sql->rowCount();
            $count1 = $sql1->rowCount();
            if ($count || $count1 )
            {
                $items = array();
                if ($count >= 1) 
                {
                    while ($row = $sql->fetch(PDO::FETCH_ASSOC)) 
                    {
                        $items[] = $row;
                    }
                    $response['Visitor Details'] = $items;
                   
                }
                else 
                    {
                        $response['Message'] = 'No parking assigned';
                        $response['Visitor Details'] = [];
                    }
                    $items1 = array();
                     if ($count1 >= 1) 
                    {
                        while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) 
                        {
                            $items1[] = $row1;
                        }
                        $response['Available Parking'] = $items1;
                    }
                else 
                    {
                        $response['Message'] = 'No parking available';
                        $response['Available Parking'] = [];
                    }
                $response['Status'] = '1';
                $response['Message'] = "";
            }
                else 
                    {
                        $response['Message'] = 'No data found';
                        $response['Visitor Details'] = [];
                        $response['Available Parking'] = [];
                        $response['Status'] = 0;
                    }
        }
        return $response;
    }
    public function visitorentryconformtwo($param) {
        if (isset($param->VisitorName)) {
            $VisitorName = $param->VisitorName;
        }
        if (isset($param->VisitorImageUrl)) {
            $VisitorImageUrl = $param->VisitorImageUrl;
        }
        if (isset($param->VisitorVehicleNumber)) {
            $VisitorVehicleNumber = $param->VisitorVehicleNumber;
        }
        if (isset($param->VisitorEntryTime)) {
            $VisitorEntryTime = $param->VisitorEntryTime;
        }
        if (isset($param->NumberOfPeople)) {
            $NumberOfPeople = $param->NumberOfPeople;
        }
        if (isset($param->VisitorID)) {
            $VisitorID = $param->VisitorID;
        }
        if (isset($param->VisitorQrCode)) {
            $VisitorQrCode = $param->VisitorQrCode;
        }
        if (isset($param->FlatID)) {
            $FlatID = $param->FlatID;
        }
        if (isset($param->parkingid)) {
            $parkingid = $param->parkingid;
        }
        if (isset($param->GateName)) {
            $GateName = $param->GateName;
        }
        if (isset($param->VisitorPurpose)) {
            $VisitorPurpose = $param->VisitorPurpose;
        }
        if (isset($param->VisitorCompany)) {
            $VisitorCompany = $param->VisitorCompany;
        }
        if (isset($param->VisitorAddress)) {
            $VisitorAddress = $param->VisitorAddress;
        }
        if ($VisitorQrCode != '') {
            $sql5 = $this->prdbh->prepare("SELECT * FROM VisitorTable WHERE VisitorInOut='1' AND VisitorQrCode = '" . $VisitorQrCode . "'");
            $sql5->execute();
            if ($sql5->rowcount() > 0) {
                $response['Message'] = "Card already linked to visitor";
                $response['Status'] = "0";
            } else {
                $sql4 = $this->prdbh->prepare("SELECT  DetailList FROM VisitorPurpos WHERE ID = '" . $VisitorPurpose . "'  ");
                $sql4->execute();
                if ($sql4->rowCount() > 0) {
                    while ($row4 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                        $Purpose = $row4['DetailList'];
                    }
                }
                $response = array();
                $sql1 = $this->prdbh->prepare("UPDATE VisitorTable SET VisitorImageUrl='" . $VisitorImageUrl . "', VisitorName='" . $VisitorName . "',VisitorInOut='1',VisitorQrCode= '" . $VisitorQrCode . "' WHERE VisitorID = '" . $VisitorID . "' ");
                $sql1->execute();
                $sql = $this->prdbh->prepare("UPDATE VisitorDetails SET VisitorCompany= '" . $VisitorCompany . "', VisitorAddress='" . $VisitorAddress . "', VisitorPurpose='" . $VisitorPurpose . "',VisitorVehicleNumber='" . $VisitorVehicleNumber . "',VisitorEntryTime='" . $VisitorEntryTime . "' ,NumberOfPeople='" . $NumberOfPeople . "'  WHERE VisitorID = '" . $VisitorID . "' ");
                $sql->execute();
                if ($sql && $sql1) {
                    $items = array();
                    $count = $sql->rowCount();
                    if ($count >= 1) {
                        $sql3 = $this->prdbh->prepare("UPDATE ParkingTable SET  VisitorID='" . $VisitorID . "',sticker_issued='1' WHERE ID = '" . $parkingid . "' ");
                        $sql3->execute();
                        if ($sql->rowCount() >= 1) {
                            $response['Data'] = 'Updated successfully';
                            $response['Status'] = '1';
                        } else {
                            $response['Data'] = 'Updated successfully parking not added';
                            $response['Status'] = '1';
                        }
                    } else {
                        $response['Data'] = 'Not Updated';
                        $response['Status'] = '1';
                    }
                    $sql2 = $this->prdbh->prepare("SELECT ResidentRWAMapping.ResidentID FROM ResidentRWAMapping LEFT JOIN ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID= ResidentRWAMapping.ResidentRWAID WHERE  ResidentFlatMapping.FlatID IN ($FlatID)  ");
                    $sql2->execute();
                    if ($sql2->rowCount() > 0) {
                        $UpushId1 = array();
                        while ($row2 = $sql2->fetch(PDO::FETCH_ASSOC)) {
                            $UpushId1[] = $row2['ResidentID'];
                        }
                        $UpushId = implode(',', $UpushId1);
                        $date = date('h:i a ', $VisitorEntryTime + 19660);
                        $ID = '' . $VisitorID;
                        if ($VisitorName != '') {
                            $message = $VisitorName . ' with purpose -' . $Purpose . ' has arrived at ' . $GateName;
                        } else {
                            $message = 'Someone with purpose -' . $Purpose . ' has arrived at ' . $GateName;
                        }
                        $UrHd1 = 'mynukad Visitor';
                        $message1 = array("message" => $message, "UrHd2" => $UrHd1, "body" => $ID);
                        $result = $this->firebasenotification($UpushId, $message1);
                        $response['PushStatus'] = $result;
                    }
                }
            }
        } else {
            $sql4 = $this->prdbh->prepare("SELECT  DetailList FROM VisitorPurpos WHERE ID = '" . $VisitorPurpose . "'  ");
            $sql4->execute();
            if ($sql4->rowCount() > 0) {
                while ($row4 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                    $Purpose = $row4['DetailList'];
                }
            }
            $response = array();
            $sql1 = $this->prdbh->prepare("UPDATE VisitorTable SET VisitorImageUrl='" . $VisitorImageUrl . "', VisitorName='" . $VisitorName . "',VisitorInOut='1',VisitorQrCode= '" . $VisitorQrCode . "' WHERE VisitorID = '" . $VisitorID . "' ");
            $sql1->execute();
            $sql = $this->prdbh->prepare("UPDATE VisitorDetails SET VisitorCompany= '" . $VisitorCompany . "', VisitorAddress='" . $VisitorAddress . "', VisitorPurpose='" . $VisitorPurpose . "',VisitorVehicleNumber='" . $VisitorVehicleNumber . "',VisitorEntryTime='" . $VisitorEntryTime . "' ,NumberOfPeople='" . $NumberOfPeople . "'  WHERE VisitorID = '" . $VisitorID . "' ");
            $sql->execute();
            if ($sql && $sql1) {
                $items = array();
                $count = $sql->rowCount();
                if ($count >= 1) {
                    $sql3 = $this->prdbh->prepare("UPDATE ParkingTable SET  VisitorID='" . $VisitorID . "',sticker_issued='1' WHERE ID = '" . $parkingid . "' ");
                    $sql3->execute();
                    if ($sql->rowCount() >= 1) {
                        $response['Data'] = 'Updated successfully';
                        $response['Status'] = '1';
                    } else {
                        $response['Data'] = 'Updated successfully parking not added';
                        $response['Status'] = '1';
                    }
                } else {
                    $response['Data'] = 'Not Updated';
                    $response['Status'] = '1';
                }
                $sql2 = $this->prdbh->prepare("SELECT ResidentRWAMapping.ResidentID FROM ResidentRWAMapping LEFT JOIN ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID= ResidentRWAMapping.ResidentRWAID WHERE  ResidentFlatMapping.FlatID IN ($FlatID)  ");
                $sql2->execute();
                if ($sql2->rowCount() > 0) {
                    $UpushId1 = array();
                    while ($row2 = $sql2->fetch(PDO::FETCH_ASSOC)) {
                        $UpushId1[] = $row2['ResidentID'];
                    }
                    $UpushId = implode(',', $UpushId1);
                    $date = date('h:i a ', $VisitorEntryTime + 19660);
                    $ID = '' . $VisitorID;
                    if ($VisitorName != '') {
                        $message = $VisitorName . ' with purpose -' . $Purpose . ' has arrived at ' . $GateName;
                    } else {
                        $message = 'Someone with purpose -' . $Purpose . ' has arrived at ' . $GateName;
                    }
                    $UrHd1 = 'mynukad Visitor';
                    $message1 = array("message" => $message, "UrHd2" => $UrHd1, "body" => $ID);
                    $result = $this->firebasenotification($UpushId, $message1);
                    $response['PushStatus'] = $result;
                }
            }
        }
        return $response;
    }
    public function visitorentryconform($param) {
        if (isset($param->VisitorName)) {
            $VisitorName = $param->VisitorName;
        }
        if (isset($param->VisitorImageUrl)) {
            $VisitorImageUrl = $param->VisitorImageUrl;
        }
        if (isset($param->VisitorVehicleNumber)) {
            $VisitorVehicleNumber = $param->VisitorVehicleNumber;
        }
        if (isset($param->VisitorEntryTime)) {
            $VisitorEntryTime = $param->VisitorEntryTime;
        }
        if (isset($param->NumberOfPeople)) {
            $NumberOfPeople = $param->NumberOfPeople;
        }
        if (isset($param->VisitorID)) {
            $VisitorID = $param->VisitorID;
        }
        if (isset($param->ID)) {
            $ID = $param->ID;
        }
        if (isset($param->parkingid)) {
            $parkingid = $param->parkingid;
        }
        if (isset($param->GateName)) {
            $GateName = $param->GateName;
        }
        if (isset($param->VisitorPurpose)) {
            $VisitorPurpose = $param->VisitorPurpose;
        }
        if (isset($param->VisitorCompany)) {
            $VisitorCompany = $param->VisitorCompany;
        }
        if (isset($param->VisitorAddress)) {
            $VisitorAddress = $param->VisitorAddress;
        }
        $sql4 = $this->prdbh->prepare("SELECT DetailList FROM VisitorPurpos WHERE ID = '" . $VisitorPurpose . "'  ");
        $sql4->execute();
        if ($sql4->rowCount() > 0) {
            while ($row4 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                $Purpose = $row4['DetailList'];
            }
        }
        $response = array();
        $sql1 = $this->prdbh->prepare("UPDATE VisitorTable SET VisitorImageUrl='" . $VisitorImageUrl . "', VisitorName='" . $VisitorName . "',VisitorInOut='1' WHERE VisitorID = '" . $VisitorID . "' ");
        $sql1->execute();
        $sql = $this->prdbh->prepare("UPDATE VisitorDetails SET VisitorCompany= '" . $VisitorCompany . "', VisitorAddress='" . $VisitorAddress . "', VisitorPurpose='" . $VisitorPurpose . "',VisitorVehicleNumber='" . $VisitorVehicleNumber . "',VisitorEntryTime='" . $VisitorEntryTime . "' ,NumberOfPeople='" . $NumberOfPeople . "'  WHERE VisitorID = '" . $VisitorID . "' ");
        $sql->execute();
        if ($sql && $sql1) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                $sql3 = $this->prdbh->prepare("UPDATE ParkingTable SET  VisitorID='" . $VisitorID . "',sticker_issued='1' WHERE ID = '" . $parkingid . "' ");
                $sql3->execute();
                if ($sql->rowCount() >= 1) {
                    $response['Data'] = 'Updated successfully';
                    $response['Status'] = '1';
                } else {
                    $response['Data'] = 'Updated successfully parking not added';
                    $response['Status'] = '1';
                }
            } else {
                $response['Data'] = 'Not Updated';
                $response['Status'] = '1';
            }
            $sql2 = $this->prdbh->prepare("SELECT ResidentRWAMapping.ResidentID FROM ResidentRWAMapping LEFT JOIN ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID= ResidentRWAMapping.ResidentRWAID WHERE  ResidentFlatMapping.FlatID IN ($ID)  ");
            $sql2->execute();
            if ($sql2->rowCount() > 0) {
                $UpushId1 = array();
                while ($row2 = $sql2->fetch(PDO::FETCH_ASSOC)) {
                    $UpushId1[] = $row2['ResidentID'];
                }
                $UpushId = implode(',', $UpushId1);
                $date = date('h:i a ', $VisitorEntryTime + 19660);
                $ID = '' . $VisitorID;
                if ($VisitorName != '') {
                    $message = $VisitorName . ' with purpose -' . $Purpose . ' has arrived at ' . $GateName;
                } else {
                    $message = 'Someone with purpose -' . $Purpose . ' has arrived at ' . $GateName;
                }
                $UrHd1 = 'mynukad Visitor';
                $message1 = array("message" => $message, "UrHd2" => $UrHd1, "body" => $ID);
                $result = $this->firebasenotification($UpushId, $message1);
                $response['PushStatus'] = $result;
            }
        } else {
            $response['Data'] = 'Not Updated';
            $response['Status'] = '0';
        }
        return $response;
    }
    public function editvisitorentry($param) {
        if (isset($param->VisitorName)) {
            $VisitorName = $param->VisitorName;
        }
        if (isset($param->VisitorImageUrl)) {
            $VisitorImageUrl = $param->VisitorImageUrl;
        }
        if (isset($param->VisitorPurpose)) {
            $VisitorPurpose = $param->VisitorPurpose;
        }
        if (isset($param->VisitorVehicleNumber)) {
            $VisitorVehicleNumber = $param->VisitorVehicleNumber;
        }
        if (isset($param->ResidentRWAID)) {
            $ResidentRWAID = $param->ResidentRWAID;
        }
        if (isset($param->VisitorPhoneNumber)) {
            $value = $param->VisitorPhoneNumber;
            $count = strlen($value) - 10;
            $VisitorPhoneNumber = substr($value, $count);
        }
        if (isset($param->VisitorAddress)) {
            $VisitorAddress = $param->VisitorAddress;
        }
        if (isset($param->VisitorETA)) {
            $VisitorETA = $param->VisitorETA;
        }
        if (isset($param->NumberOfPeople)) {
            $NumberOfPeople = $param->NumberOfPeople;
        }
        if (isset($param->VisitorID)) {
            $VisitorID = $param->VisitorID;
        }
        $response = array();
        $sql1 = $this->prdbh->prepare("UPDATE VisitorTable SET VisitorImageUrl='" . $VisitorImageUrl . "', VisitorName='" . $VisitorName . "',VisitorPhoneNumber='" . $VisitorPhoneNumber . "' WHERE VisitorID = '" . $VisitorID . "' AND ResidentRWAID='" . $ResidentRWAID . "' ");
        $sql1->execute();
        $sql = $this->prdbh->prepare("UPDATE VisitorDetails SET  VisitorPurpose='" . $VisitorPurpose . "',NumberOfPeople='" . $NumberOfPeople . "',VisitorVehicleNumber='" . $VisitorVehicleNumber . "',VisitorAddress='" . $VisitorAddress . "',VisitorETA='" . $VisitorETA . "' WHERE VisitorID = '" . $VisitorID . "' ");
        $sql->execute();
        if ($sql && $sql1) {
            $count = $sql->rowCount();
            $count1 = $sql1->rowCount();
            if ($count1 >= 1 && $count >= 1) {
                $response['Data'] = 'Updated successfully'; // sql1 responce if success
                $response['Status'] = '1';
            } else {
                $response['Data'] = 'Updated successfully'; // Sql responce  if success
                $response['Status'] = '1';
            }
        } else {
            $response['Data'] = 'Not Updated';
            $response['Status'] = '0';
        }
        return $response;
    }
    public function visiterqrlink($param) {
        if (isset($param->VisitorID)) {
            $VisitorID = $param->VisitorID;
        }
        if (isset($param->VisitorQrCode)) {
            $VisitorQrCode = $param->VisitorQrCode;
        }
        $response = array();
        $sql1 = $this->prdbh->prepare("SELECT VisitorQrCode FROM VisitorTable  WHERE VisitorQrCode ='" . $VisitorQrCode . "'");
        $sql1->execute();
        //print_r($sql1);die;
        if ($sql1->rowCount() >= 1) {
            $response['Message'] = 'Qr already linked with visitor';
            $response['Status'] = '0';
        } else {
            $sql = $this->prdbh->prepare("UPDATE VisitorTable SET VisitorInOut='1', VisitorQrCode ='" . $VisitorQrCode . "' 
                WHERE VisitorID = '" . $VisitorID . "' ");
            $sql->execute();
            if ($sql) {
                $items = array();
                $count = $sql->rowCount();
                if ($count >= 1) {
                    $response['Message'] = 'Qr linked with visitor';
                    $response['Status'] = '1';
                } else {
                    $response['Message'] = 'Qr already linked with visitor';
                    $response['Status'] = '0';
                }
            }
        }
        return $response;
    }
    public function visitercheckout($param) {
        if (isset($param->VisitorID)) {
            $VisitorID = $param->VisitorID;
        }
        if (isset($param->VisitorExitTime)) {
            $VisitorExitTime = $param->VisitorExitTime;
        }
        $response = array();
        $sql1 = $this->prdbh->prepare("UPDATE VisitorTable SET OTP='NULL', VisitorInOut ='2' WHERE VisitorID = '" . $VisitorID . "' ");
        $sql1->execute();
        $sql = $this->prdbh->prepare("UPDATE VisitorDetails SET  VisitorExitTime ='" . $VisitorExitTime . "' WHERE VisitorID = '" . $VisitorID . "' ");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                $sql3 = $this->prdbh->prepare("UPDATE ParkingTable set VisitorID='0' WHERE  VisitorID = '" . $VisitorID . "' ");
                $sql3->execute();
                if ($sql3->rowCount() >= 1) {
                    $response['Message'] = 'Checkout';
                    $response['Status'] = '1';
                } else {
                    $response['Message'] = 'Checkout  ';
                    $response['Status'] = '1';
                }
            } else {
                $response['Message'] = 'Invalid Checkout';
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function visitercheckoutdecember($param) {
        if (isset($param->VisitorID)) {
            $VisitorID = $param->VisitorID;
        }
        if (isset($param->VisitorExitTime)) {
            $VisitorExitTime = $param->VisitorExitTime;
        }
        $response = array();
        $sql1 = $this->prdbh->prepare("UPDATE VisitorTable SET OTP='NULL', VisitorInOut ='2' WHERE VisitorID = '" . $VisitorID . "' ");
        $sql1->execute();
    //    $sql250 = $this->prdbh->prepare("UPDATE visitorApprovalStatus SET status = 0 and approvalStatus =0 and flatId=0 WHERE visitorId = '" . $VisitorID . "'");
    //    $sql250->execute();
       // $sql5 = $this->prdbh->prepare("UPDATE VisitorFlatMapping SET FlatID=0 WHERE VisitorID = '" . $VisitorID . "' ");
       // $sql5->execute();
        $sql = $this->prdbh->prepare("UPDATE VisitorDetails SET  VisitorExitTime ='" . $VisitorExitTime . "' WHERE VisitorID = '" . $VisitorID . "' ");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                // $sql3 = $this->prdbh->prepare("UPDATE ParkingTable set VisitorID='0' WHERE  VisitorID = '".$VisitorID."' ");
                $sql3 = $this->prdbh->prepare(" UPDATE ParkingTable set Flat_ID = 0,sticker_issued = 0, sticker_num = '' WHERE  VisitorID = '" . $VisitorID . "' ");
                $sql3->execute();
                if ($sql3->rowCount() >= 1) {
                    $response['Message'] = 'Checkout';
                    $response['Status'] = '1';
                } else {
                    $response['Message'] = 'Please checkout again';
                    $response['Status'] = '1';
                }
            } else {
                $response['Message'] = 'Invalid Checkout';
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function visitertowercheckinout($param) {
        if (isset($param->VisitorQrcode)) {
            $VisitorQrcode = $param->VisitorQrcode;
        }
        if (isset($param->VisitorExitTime)) {
            $VisitorExitTime = $param->VisitorExitTime;
        }
        if (isset($param->TowerPrefix)) {
            $TowerPrefix = $param->TowerPrefix;
        }
        $towername = substr($flatName, 0, 1);
        if (isset($param->TowerID)) {
            $TowerID = $param->TowerID;
        }
        if (isset($param->Status)) {
            $Status = $param->Status;
        }
        $response = array();
        $sql3 = $this->prdbh->prepare("SELECT VisitorID FROM VisitorTable WHERE VisitorQrCode ='" . $VisitorQrcode . "' ");
        $sql3->execute();
        if ($sql3->rowCount()) {
            $row = $sql3->fetch(PDO::FETCH_ASSOC);
            $visitorid = $row['VisitorID'];
        }
        $sql2 = $this->prdbh->prepare("SELECT FlatNbr FROM SocietyFlatMapping INNER JOIN VisitorFlatMapping ON VisitorFlatMapping.FlatID=SocietyFlatMapping.ID WHERE VisitorFlatMapping.VisitorID = '" . $visitorid . "'");
        $sql2->execute();
        $count2 = $sql2->rowCount();
        $Towerpermit = array();
        if ($count2 >= 1) {
            while ($row2 = $sql2->fetch(PDO::FETCH_ASSOC)) {
                $Towerpermit[] = ucfirst(substr($row2['FlatNbr'], 0, 1));
            }
        } elseif ($count2 > 0) {
            $Towerpermit = ucfirst(substr($row2['FlatNbr'], 0, 1));
        } else {
            $Towerpermit[] = '0';
        }
        //print_r($Towerpermit);die;
        if (in_array($TowerPrefix, $Towerpermit)) {
            $sql = $this->prdbh->prepare("INSERT INTO VisitorTowerAuditTrail(VisitorID, TowerID, Timestamp, Status) VALUES('" . $visitorid . "','" . $TowerID . "','" . $VisitorExitTime . "','" . $Status . "')");
            if ($sql->execute()) {
                $items = array();
                $count = $sql->rowCount();
                if ($count >= 1) {
                    if ($Status == 1) {
                        $response['Message'] = 'Checkin';
                    } else {
                        $response['Message'] = 'Checkout';
                    }
                    $response['Status'] = '1';
                } else {
                    $response['Message'] = 'Invalid Checkout';
                    $response['Status'] = '0';
                }
            }
        } else {
            $response['Message'] = 'you are not permited for this tower';
            $response['Status'] = '0';
        }
        return $response;
    }
    public function visitercheckoutbyflat($param) {
        if (isset($param->VisitorID)) {
            $VisitorID = $param->VisitorID;
        }
        if (isset($param->VisitorExitTime)) {
            $VisitorExitTime = $param->VisitorExitTime;
        }
        if (isset($param->FlatID)) {
            $FlatID = $param->FlatID;
        }
        if (isset($param->TawerID)) {
            $TawerID = $param->TawerID;
        }
        $response = array();
        $sql = $this->prdbh->prepare("INSERT INTO VisitorFlatAuditTrail(FlatID, TawerID, VisitorID, ExitTime) VALUES('" . $FlatID . "','" . $TawerID . "','" . $VisitorID . "','" . $VisitorExitTime . "')");
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                $response['Message'] = 'Checkout';
                $response['Status'] = '1';
            } else {
                $response['Message'] = 'Invalid Checkout';
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function visitercheckoutbyexternalqr($param) {
        if (isset($param->VisitorQrCode)) {
            $VisitorQrCode = $param->VisitorQrCode;
        }
        if (isset($param->VisitorExitTime)) {
            $VisitorExitTime = $param->VisitorExitTime;
        }
        $sql3 = $this->prdbh->prepare("SELECT  VisitorID FROM VisitorTable 
            WHERE VisitorQrCode = '" . $VisitorQrCode . "' AND   VisitorInOut ='1' ");
        $sql3->execute();
        if ($sql3->rowCount() > 0) {
            while ($row3 = $sql3->fetch(PDO::FETCH_ASSOC)) {
                $VisitorID = $row3['VisitorID'];
            }
            $sql1 = $this->prdbh->prepare("UPDATE VisitorTable SET OTP='NULL',VisitorQrCode='NULL', VisitorInOut ='2' WHERE VisitorQrCode = '" . $VisitorQrCode . "' ");
            $sql1->execute();
            $sql = $this->prdbh->prepare("UPDATE VisitorDetails SET  VisitorExitTime ='" . $VisitorExitTime . "' WHERE VisitorID = '" . $VisitorID . "' ");
            $sql->execute();
         //   $sqlVAS = $this->prdbh->prepare("UPDATE visitorApprovalStatus SET  approvalStatus =0 and flatId=0 WHERE VisitorID = '" . $VisitorID . "' ");
         //   $sqlVAS->execute();
            if ($sql && $sql1 && $sqlVAS) {
                $items = array();
                $count = $sql->rowCount();
                if ($count >= 1) {
                    $sql3 = $this->prdbh->prepare("UPDATE ParkingTable set Flat_ID = 0,sticker_issued = 0, sticker_num = '' WHERE  VisitorID = '" . $VisitorID . "' ");
                    if ($sql3->execute()) {
                        $response['Message'] = 'Checkout';
                        $response['Status'] = '1';
                    }
                } else {
                    $response['Message'] = 'Invalid Checkout';
                    $response['Status'] = '0';
                }
            }
        } else {
            $response['Message'] = 'Please check in first';
            $response['Status'] = '0';
        }
        return $response;
    }
    public function visitercheckoutbyotp($param) {
        if (isset($param->Visitorotp)) {
            $Visitorotp = $param->Visitorotp;
        }
        $Visitorotp = substr($Visitorotp, 2);
        if (isset($param->VisitorExitTime)) {
            $VisitorExitTime = $param->VisitorExitTime;
        }
        $sql3 = $this->prdbh->prepare("SELECT  VisitorID FROM VisitorTable WHERE OTP = '" . $Visitorotp . "' AND   VisitorInOut ='1' ");
        $sql3->execute();
        if ($sql3->rowCount() > 0) {
            while ($row3 = $sql3->fetch(PDO::FETCH_ASSOC)) {
                $VisitorID = $row3['VisitorID'];
            }
            $sql1 = $this->prdbh->prepare("UPDATE VisitorTable SET OTP='NULL', VisitorInOut ='2' WHERE OTP = '" . $Visitorotp . "' ");
            $sql1->execute();
            $sql = $this->prdbh->prepare("UPDATE VisitorDetails SET  VisitorExitTime ='" . $VisitorExitTime . "' WHERE VisitorID = '" . $VisitorID . "' ");
            $sql->execute();
            if ($sql && $sql1) {
                $items = array();
                $count = $sql->rowCount();
                if ($count >= 1) {
                    $sql3 = $this->prdbh->prepare("UPDATE ParkingTable set VisitorID='0' WHERE  VisitorID = '" . $VisitorID . "' ");
                    $sql3->execute();
                    if ($sql3->rowCount >= 1) {
                        $response['Message'] = 'Checkout';
                        $response['Status'] = '1';
                    } else {
                        $response['Message'] = 'Checkout no parking';
                        $response['Status'] = '1';
                    }
                } else {
                    $response['Message'] = 'Invalid Checkout';
                    $response['Status'] = '0';
                }
            }
        } else {
            $response['Message'] = 'Please check in first';
            $response['Status'] = '0';
        }
        return $response;
    }
    public function vistordetailbynumber($rec) {
        $tdat = date("U");
        $tdate = $tdat;
        $VisitorPhoneNumber = $rec->VisitorPhoneNumber;
        $sql3 = $this->prdbh->prepare("SELECT  IFNULL(VisitorDetails.VisitorCompany,'') as VisitorCompany,IFNULL(VisitorDetails.VisitorVehicleNumber,'') as VisitorVehicleNumber ,IFNULL(VisitorDetails.VisitorAddress,'') as VisitorAddress ,IFNULL(VisitorTable.VisitorName ,'') as VisitorName FROM VisitorTable INNER JOIN VisitorDetails ON VisitorDetails.VisitorID=VisitorTable.VisitorID WHERE  VisitorTable.VisitorPhoneNumber='" . $VisitorPhoneNumber . "' AND VisitorTable.VisitorName!='' ORDER BY VisitorDetails.ID DESC  LIMIT 1 ");
        $sql3->execute();
        if ($sql3->rowCount() > 0) {
            $items = array();
            while ($row3 = $sql3->fetch(PDO::FETCH_ASSOC)) {
                $items = $row3;
            }
            $response['VisitorDetail'] = $items;
            $response['Status'] = 1;
        } else {
            $response['VisitorDetail'] = [];
            $response['Status'] = 0;
        }
        return $response;
    }
    public function vistordetailbynumberdecember($rec) {
         $tdat = date("U");
        $tdate = $tdat;
        $value = $rec->VisitorPhoneNumber;
        $count = strlen($value) - 10;
        $VisitorPhoneNumber = substr($value, $count);
        // $VisitorPhoneNumber = $rec->VisitorPhoneNumber;
        $VerificationIDType = $rec->VerificationIDType;
        $VerificationIDNumber = $rec->VerificationIDNumber;
        $validationType = $rec->validationType;
        $VisitorExitTime = $rec->VisitorExitTime;
        $rwaid = $rec->rwaid;
        $gateId = $rec->gateID;

        if (isset($param->VisitorExitTime)) {
            $VisitorExitTime = $param->VisitorExitTime;
        }
        if ($validationType == 1 &&  $VerificationIDNumber !='') {
            $sql250 = $this->prdbh->prepare("SELECT VisitorID, VisitorInOut from VisitorTable where VerificationIDNumber = '" . $VerificationIDNumber . "' ");
        } else if ($validationType == 0 && $VisitorPhoneNumber !='') {
            $sql250 = $this->prdbh->prepare("SELECT VisitorID,VisitorInOut from VisitorTable where VisitorPhoneNumber =  '" . $VisitorPhoneNumber . "' ");
        }
        if (!$sql250->execute()) {
            $response['Status'] = "0";
            $response['Message'] = $sql250->errorInfo();
            return $response;
        }
     
        if ($sql250->rowCount() < 1) {
            if ($validationType == 1) {
                $sql = $this->prdbh->prepare("INSERT INTO VisitorTable (VerificationIDType,VerificationIDNumber,RWAID) VALUES (?,?,?)");
                $sql->bindParam(1, $VerificationIDType);
                $sql->bindParam(2, $VerificationIDNumber);
                $sql->bindParam(3, $rwaid);
            } else if ($validationType == 0) {
                $sql = $this->prdbh->prepare("INSERT INTO VisitorTable (VisitorPhoneNumber,RWAID) VALUES (?,?)");
                $sql->bindParam(1, $VisitorPhoneNumber);
                $sql->bindParam(2, $rwaid);
            }
            $this->prdbh->beginTransaction();
            if ($sql->execute()) {
              
                $id = $this->prdbh->lastInsertId();
                $this->prdbh->commit();
                $response['VisitorID'] = $id;
                $response['visitorExists'] = 0;
                $response['Status'] = "1";
                return $response;
            } else {
                $response['Status'] = "0";
                $response['Message'] = $sql->errorInfo();
                return $response;
            }
        } else {
            if ( $VisitorPhoneNumber !='') {
            $sqlout = $this->prdbh->prepare("SELECT a.VisitorID,a.VisitorName,a.VisitorInOut from VisitorTable as a inner join VisitorDetails as b on a.VisitorID = b.VisitorID WHERE (a.VisitorPhoneNumber='" . $VisitorPhoneNumber . "') order by VisitorID desc limit 1 ");
            $sqlout->execute();
            }
            if ( $VerificationIDNumber !='') {
            $sqlout = $this->prdbh->prepare("SELECT a.VisitorID,a.VisitorName,a.VisitorInOut from VisitorTable as a inner join VisitorDetails as b on a.VisitorID = b.VisitorID WHERE (a.VerificationIDNumber='" . $VerificationIDNumber . "') order by VisitorID desc limit 1 ");
            $sqlout->execute();
            }
            if ($sqlout->rowCount() > 0){
                while ($row = $sqlout->fetch(PDO::FETCH_ASSOC)) {
                    $VisitorInOut=$row['VisitorInOut'];
                    $VisitorID=$row['VisitorID'];
                    }
                   
                if ($VisitorInOut == 1){
                    $response = array();
                   
                    $sql1 = $this->prdbh->prepare("UPDATE VisitorTable SET OTP='NULL', VisitorInOut ='2' WHERE VisitorID = '" . $VisitorID . "' ");
                    $sql1->execute();
                    
             //       $sql2 = $this->prdbh->prepare("UPDATE visitorApprovalStatus SET status = 0 and approvalStatus =2 and flatId=0 WHERE visitorId = '" . $VisitorID . "'");
              //      $sql2->execute();
                 
                    $sql3= $this->prdbh->prepare("UPDATE VisitorDetails SET  VisitorExitTime = UNIX_TIMESTAMP() WHERE VisitorID = '" . $VisitorID . "' ");
                    $sql3->execute(); 

                //    $sql5 = $this->prdbh->prepare("UPDATE VisitorFlatMapping SET FlatID=0 WHERE VisitorID = '" . $VisitorID . "' ");
                //    $sql5->execute();
                 
                    $sql4= $this->prdbh->prepare(" UPDATE ParkingTable set Flat_ID = 0,sticker_issued = 0, sticker_num = '' WHERE  VisitorID = '" . $VisitorID . "' ");
                    $sql4->execute();
                    }
                }
       
                if ( $VisitorPhoneNumber !='') {
                    
                    $sqlAll= $this->prdbh->prepare("SELECT VisitorID, VisitorName, VisitorImageUrl, VisitorPhoneNumber, Saved, RWAID, FlatID, OTP, ResidentRWAID, VisitorInOut, IsActive, QrPath, VerificationIDType, VerificationIDNumber, VisitorQrCode, EntryAllow from VisitorTable  WHERE VisitorPhoneNumber='" . $VisitorPhoneNumber . "' order by VisitorID desc limit 1 ");
                    $sqlAll->execute();
                    $count = $sqlAll->rowCount();
                    if ($count >= 1) {
                        while ($row = $sqlAll->fetch(PDO::FETCH_ASSOC)) 
                        {
                            $items1 = $row['VisitorName'];
                            $items2= $row['VisitorImageUrl'];
                            $items3 = $row['VisitorPhoneNumber'];
                            $items4 = $row['Saved'];
                            $items5 = $row['RWAID'];
                            $items6 = $row['FlatID'];
                            $items7 = $row['OTP'];
                            $items8 = $row['ResidentRWAID'];
                            $items9 = $row['VisitorInOut'];
                            $items10 = $row['IsActive'];
                            $items11 = $row['QrPath'];
                            $items12 = $row['VerificationIDType'];
                            $items13 = $row['VerificationIDNumber'];
                            $items14 = $row['VisitorQrCode'];
                            $items15 = $row['EntryAllow'];
                            
                        
                        }
                    }
                   
                $sqlInsertT = $this->prdbh->prepare("INSERT INTO VisitorTable (VisitorName, VisitorImageUrl, VisitorPhoneNumber, Saved, RWAID, FlatID, OTP, ResidentRWAID, VisitorInOut, IsActive, QrPath, VerificationIDType, VerificationIDNumber, VisitorQrCode, EntryAllow) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                $sqlInsertT->bindParam(1, $items1);
                $sqlInsertT->bindParam(2, $items2);
                $sqlInsertT->bindParam(3, $items3);
                $sqlInsertT->bindParam(4, $items4);
                $sqlInsertT->bindParam(5, $items5);
                $sqlInsertT->bindParam(6, $items6);
                $sqlInsertT->bindParam(7, $items7);
                $sqlInsertT->bindParam(8, $items8);
                $sqlInsertT->bindParam(9, $items9);
                $sqlInsertT->bindParam(10, $items10);
                $sqlInsertT->bindParam(11, $items11);
                $sqlInsertT->bindParam(12, $items12);
                $sqlInsertT->bindParam(13, $items13);
                $sqlInsertT->bindParam(14, $items14);
                $sqlInsertT->bindParam(15, $items15);
                //$this->prdbh->beginTransaction();
                $sqlInsertT->execute();

                    $sqlAll1= $this->prdbh->prepare("SELECT a.VisitorID, VisitorType, VisitorAddress, VisitorETA, VisitorVehicleNumber, VisitorPurpose, NumberOfPeople, VisitorEntryTime, VisitorCompany from VisitorTable as a inner join VisitorDetails as b on a.VisitorID = b.VisitorID WHERE (a.VisitorPhoneNumber='" . $VisitorPhoneNumber . "') order by a.VisitorID desc limit 1 ");
                    $sqlAll1->execute();
                    $count = $sqlAll1->rowCount();
                    if ($count >= 1) {
                        while ($row = $sqlAll1->fetch(PDO::FETCH_ASSOC)) 
                        {
                           
                            $item1= $row['a.VisitorID'];
                            $item2 = $row['VisitorType'];
                            $item3 = $row['VisitorAddress'];
                            $item4 = $row['VisitorETA'];
                            $item5 = $row['VisitorVehicleNumber'];
                            $item6 = $row['VisitorPurpose'];
                            $item7 = $row['NumberOfPeople'];
                            $item8 = $row['VisitorEntryTime'];
                            $item9 = $row['VisitorCompany'];
                            
                        
                        }
                    }
                    $sqlID= $this->prdbh->prepare("SELECT VisitorID  from VisitorTable  WHERE VisitorPhoneNumber='" . $VisitorPhoneNumber . "' order by VisitorID desc limit 1 ");
                    $sqlID->execute();
                    $count = $sqlID->rowCount();
                    if ($count >= 1) {
                        while ($row = $sqlID->fetch(PDO::FETCH_ASSOC)) 
                        {
                           
                            $VisitorIdNew= $row['VisitorID'];
                        }
                    }
                  
                $sqlInsertD = $this->prdbh->prepare("INSERT INTO VisitorDetails(VisitorID,VisitorType,VisitorAddress,VisitorETA,VisitorVehicleNumber,VisitorPurpose,NumberOfPeople,VisitorEntryTime,VisitorCompany)VALUES (?,?,?,?,?,?,?,?,?)");
                $sqlInsertD->bindParam(1, $VisitorIdNew);
                $sqlInsertD->bindParam(2, $item2);
                $sqlInsertD->bindParam(3, $item3);
                $sqlInsertD->bindParam(4, $item4);
                $sqlInsertD->bindParam(5, $item5);
                $sqlInsertD->bindParam(6, $item6);
                $sqlInsertD->bindParam(7, $item7);
                $sqlInsertD->bindParam(8, $item8);
                $sqlInsertD->bindParam(9, $item9);
               // $this->prdbh->beginTransaction();
            
               $sqlInsertD->execute();
                $sql= $this->prdbh->prepare("SELECT a.VisitorID,a.VisitorName,a.VisitorImageUrl,a.VisitorPhoneNumber,a.VerificationIDNumber,b.VisitorAddress,b.VisitorVehicleNumber from VisitorTable as a inner join VisitorDetails as b on a.VisitorID = b.VisitorID WHERE a.VisitorID='" . $VisitorIdNew . "' order by VisitorID desc limit 1 ");
                    $sql->execute();
                }
                if ( $VerificationIDNumber !='') {
                    $sqlAll= $this->prdbh->prepare("SELECT VisitorID, VisitorName, VisitorImageUrl, VisitorPhoneNumber, Saved, RWAID, FlatID, OTP, ResidentRWAID, VisitorInOut, IsActive, QrPath, VerificationIDType, VerificationIDNumber, VisitorQrCode, EntryAllow from VisitorTable  WHERE VerificationIDNumber='" . $VerificationIDNumber . "' order by VisitorID desc limit 1 ");
                    $sqlAll->execute();
                    $count = $sqlAll->rowCount();
                    if ($count >= 1) {
                        while ($row = $sqlAll->fetch(PDO::FETCH_ASSOC)) 
                        {
                            $items1 = $row['VisitorName'];
                            $items2= $row['VisitorImageUrl'];
                            $items3 = $row['VisitorPhoneNumber'];
                            $items4 = $row['Saved'];
                            $items5 = $row['RWAID'];
                            $items6 = $row['FlatID'];
                            $items7 = $row['OTP'];
                            $items8 = $row['ResidentRWAID'];
                            $items9 = $row['VisitorInOut'];
                            $items10 = $row['IsActive'];
                            $items11 = $row['QrPath'];
                            $items12 = $row['VerificationIDType'];
                            $items13 = $row['VerificationIDNumber'];
                            $items14 = $row['VisitorQrCode'];
                            $items15 = $row['EntryAllow'];
                            
                        
                        }
                    }
                   
                $sqlInsertT = $this->prdbh->prepare("INSERT INTO VisitorTable (VisitorName, VisitorImageUrl, VisitorPhoneNumber, Saved, RWAID, FlatID, OTP, ResidentRWAID, VisitorInOut, IsActive, QrPath, VerificationIDType, VerificationIDNumber, VisitorQrCode, EntryAllow) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                $sqlInsertT->bindParam(1, $items1);
                $sqlInsertT->bindParam(2, $items2);
                $sqlInsertT->bindParam(3, $items3);
                $sqlInsertT->bindParam(4, $items4);
                $sqlInsertT->bindParam(5, $items5);
                $sqlInsertT->bindParam(6, $items6);
                $sqlInsertT->bindParam(7, $items7);
                $sqlInsertT->bindParam(8, $items8);
                $sqlInsertT->bindParam(9, $items9);
                $sqlInsertT->bindParam(10, $items10);
                $sqlInsertT->bindParam(11, $items11);
                $sqlInsertT->bindParam(12, $items12);
                $sqlInsertT->bindParam(13, $items13);
                $sqlInsertT->bindParam(14, $items14);
                $sqlInsertT->bindParam(15, $items15);
//$this->prdbh->beginTransaction();
                $sqlInsertT->execute();
                    
                $sqlAll1= $this->prdbh->prepare("SELECT a.VisitorID, VisitorType, VisitorAddress, VisitorETA, VisitorVehicleNumber, VisitorPurpose, NumberOfPeople, VisitorEntryTime, VisitorCompany from VisitorTable as a inner join VisitorDetails as b on a.VisitorID = b.VisitorID WHERE (a.VerificationIDNumber='" . $VerificationIDNumber . "') order by a.VisitorID desc limit 1 ");
                $sqlAll1->execute();
                $count = $sqlAll1->rowCount();
                    if ($count >= 1) {
                        while ($row = $sqlAll1->fetch(PDO::FETCH_ASSOC)) 
                        {
                           
                            $item1= $row['a.VisitorID'];
                            $item2 = $row['VisitorType'];
                            $item3 = $row['VisitorAddress'];
                            $item4 = $row['VisitorETA'];
                            $item5 = $row['VisitorVehicleNumber'];
                            $item6 = $row['VisitorPurpose'];
                            $item7 = $row['NumberOfPeople'];
                            $item8 = $row['VisitorEntryTime'];
                            $item9 = $row['VisitorCompany'];
                            
                        
                        }
                    }
                    $sqlID= $this->prdbh->prepare("SELECT VisitorID  from VisitorTable  WHERE VerificationIDNumber='" . $VerificationIDNumber . "' order by VisitorID desc limit 1 ");
                    $sqlID->execute();
                    $count = $sqlID->rowCount();
                    if ($count >= 1) {
                        while ($row = $sqlID->fetch(PDO::FETCH_ASSOC)) 
                        {
                           
                            $VisitorIdNew= $row['VisitorID'];
                        }
                    }
                  
                $sqlInsertD = $this->prdbh->prepare("INSERT INTO VisitorDetails(VisitorID,VisitorType,VisitorAddress,VisitorETA,VisitorVehicleNumber,VisitorPurpose,NumberOfPeople,VisitorEntryTime,VisitorCompany)VALUES (?,?,?,?,?,?,?,?,?)");
                $sqlInsertD->bindParam(1, $VisitorIdNew);
                $sqlInsertD->bindParam(2, $item2);
                $sqlInsertD->bindParam(3, $item3);
                $sqlInsertD->bindParam(4, $item4);
                $sqlInsertD->bindParam(5, $item5);
                $sqlInsertD->bindParam(6, $item6);
                $sqlInsertD->bindParam(7, $item7);
                $sqlInsertD->bindParam(8, $item8);
                $sqlInsertD->bindParam(9, $item9);
               // $this->prdbh->beginTransaction();
                $sqlInsertD->execute();
                $sql= $this->prdbh->prepare("SELECT a.VisitorID,a.VisitorName,a.VisitorImageUrl,a.VisitorPhoneNumber,a.VerificationIDNumber,b.VisitorAddress,b.VisitorVehicleNumber from VisitorTable as a inner join VisitorDetails as b on a.VisitorID = b.VisitorID WHERE a.VisitorID='" . $VisitorIdNew . "' order by VisitorID desc limit 1 ");
                $sql->execute();
                }
            if ($sql->rowCount() > 0) {
                $items = array();
                $count = $sql->rowCount();
                if ($count >= 1) {
                    while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                        $items[] = $row;
                    }
                   
                    $response['VisitorDetails'] = $items;
                    $response['visitorExists'] = 1;
                    $response['Status'] = 1;
                }
            } else {
                $response['Status'] = "0";
                $response['Message'] = $sql->errorInfo();
                return $response;
            }
        }
        return $response;
    }
    public function vehiclemovementstatusby($rec) {
        $response = array();
        $myArray = json_decode(json_encode($rec), true);
        for ($j = 0;$j < count($myArray['Vehiclemovemnt']);$j++) {
            $sql = $this->prdbh->prepare("INSERT INTO VehicleMovementStatus(VehicleID, EntryType, DateTimeStamp, GateNbr) VALUES (?,?,?,?)");
            $sql->bindParam(1, $myArray['Vehiclemovemnt'][$j]['VehicleID']);
            $sql->bindParam(2, $myArray['Vehiclemovemnt'][$j]['EntryType']);
            $sql->bindParam(3, $myArray['Vehiclemovemnt'][$j]['DateTimeStamp']);
            $sql->bindParam(4, $myArray['Vehiclemovemnt'][$j]['GateNbr']);
            $this->prdbh->beginTransaction();
            if ($sql->execute()) {
                $id = $this->prdbh->lastInsertId();
                $this->prdbh->commit();
                $response['Status'] = "1";
            } else {
                $response['Status'] = "0";
                $response['Message2'] = $sql->errorInfo();
                return $response;
            }
        }
        $response['Status'] = "1";
        return $response;
    }
    public function vistorunplanentrytwo($rec) {
        $response = array();
        $myArray = json_decode(json_encode($rec), true);
        $RWAID = $rec->RWAID;
        for ($j = 0;$j < count($myArray['Visitors']);$j++) {
            $sql = $this->prdbh->prepare("INSERT INTO VisitorTable (RWAID,VerificationIDType,VerificationIDNumber,VisitorPhoneNumber,VisitorInOut,VisitorName)     VALUES (?,?,?,?,?,?)");
            $sql->bindParam(1, $RWAID);
            $sql->bindParam(2, $myArray['Visitors'][$j]['VerificationIDType']);
            $sql->bindParam(3, $myArray['Visitors'][$j]['VerificationIDNumber']);
            $sql->bindParam(4, $myArray['Visitors'][$j]['VisitorPhoneNumber']);
            $sql->bindParam(5, $myArray['Visitors'][$j]['VisitorInOut']);
            $sql->bindParam(6, $myArray['Visitors'][$j]['VisitorName']);
            $this->prdbh->beginTransaction();
            if ($sql->execute()) {
                $id = $this->prdbh->lastInsertId();
                $this->prdbh->commit();
                $sql1 = $this->prdbh->prepare("INSERT INTO VisitorDetails(VisitorID,VisitorType,VisitorAddress,VisitorVehicleNumber,VisitorPurpose,NumberOfPeople,VisitorEntryTime)VALUES (?,?,?,?,?,?,?)");
                $sql1->bindParam(1, $id);
                $sql1->bindParam(2, $myArray['Visitors'][$j]['VisitorType']);
                $sql1->bindParam(3, $myArray['Visitors'][$j]['VisitorAddress']);
                $sql1->bindParam(4, $myArray['Visitors'][$j]['VisitorVehicleNumber']);
                $sql1->bindParam(5, $myArray['Visitors'][$j]['VisitorPurpose']);
                $sql1->bindParam(6, $myArray['Visitors'][$j]['NumberOfPeople']);
                $sql1->bindParam(7, $myArray['Visitors'][$j]['VisitorEntryTime']);
                $this->prdbh->beginTransaction();
                if (!$sql1->execute()) {
                    $response['Status'] = "0";
                    $response['Message1'] = $sql1->errorInfo();
                    return $response;
                }
                $vid = $this->prdbh->lastInsertId();
                $this->prdbh->commit();
            } else {
                $response['Status'] = "0";
                $response['Message2'] = $sql->errorInfo();
                return $response;
            }
            if (isset($myArray['Visitors'][$j]['FlatID'])) {
                $FlatID = $myArray['Visitors'][$j]['FlatID'];
                if (!empty($FlatID)) {
                    $visitorflatarray = explode(",", $FlatID);
                    for ($i = 0;$i < count($visitorflatarray);$i++) {
                        $sql2 = $this->prdbh->prepare("INSERT INTO VisitorFlatMapping(FlatID,VisitorID) VALUES (?,?)");
                        $sql2->bindParam(1, $visitorflatarray[$i]);
                        $sql2->bindParam(2, $id);
                        $this->prdbh->beginTransaction();
                        if (!$sql2->execute()) {
                            $response['Status'] = "0";
                            $response['Message3'] = $sql2->errorInfo();
                            return $response;
                        }
                        $this->prdbh->commit();
                    }
                }
            }
        }
        $response['Status'] = "1";
        return $response;
    }
    public function vistorunplanentryform($rec) {
        $tdat = date("U");
        $extqrcode = $rec->VisitorQrCode;
        $tdate = $tdat + 120;
        $items = '';
        $value = $rec->VisitorPhoneNumber;
        $count = strlen($value) - 10;
        $VisitorPhoneNumber2 = substr($value, $count);
        //$VisitorPhoneNumber=$rec->VisitorPhoneNumber;
        $sql3 = $this->prdbh->prepare("SELECT  IFNULL(VisitorDetails.VisitorCompany,'') as VisitorCompany,IFNULL(VisitorDetails.VisitorVehicleNumber,'') as VisitorVehicleNumber ,IFNULL(VisitorDetails.VisitorAddress,'') as VisitorAddress ,IFNULL(VisitorTable.VisitorName ,'') as VisitorName FROM VisitorTable INNER JOIN VisitorDetails ON VisitorDetails.VisitorID=VisitorTable.VisitorID WHERE  VisitorTable.VisitorPhoneNumber='" . $VisitorPhoneNumber2 . "' AND VisitorTable.VisitorName!='' ORDER BY VisitorDetails.ID DESC  LIMIT 1 ");
        $sql3->execute();
        if ($sql3->rowCount() > 0) {
            $items = array();
            while ($row3 = $sql3->fetch(PDO::FETCH_ASSOC)) {
                $items = $row3;
            }
        }
        $sql = $this->prdbh->prepare("INSERT INTO VisitorTable(RWAID,VerificationIDType,VerificationIDNumber,VisitorPhoneNumber,VisitorQrCode)VALUES (?,?,?,?,?)");
        $sql->bindParam(1, $rec->RWAID);
        $sql->bindParam(2, $rec->VerificationIDType);
        $sql->bindParam(3, $rec->VerificationIDNumber);
        $sql->bindParam(4, $VisitorPhoneNumber2);
        $sql->bindParam(5, $rec->VisitorQrCode);
        $this->prdbh->beginTransaction();
        if ($sql->execute()) {
            $id = $this->prdbh->lastInsertId();
            $this->prdbh->commit();
            $sql1 = $this->prdbh->prepare("INSERT INTO VisitorDetails(VisitorID,VisitorType,VisitorAddress,VisitorETA,VisitorVehicleNumber,VisitorPurpose,NumberOfPeople)VALUES (?,?,?,?,?,?,?)");
            $sql1->bindParam(1, $id);
            $sql1->bindParam(2, $rec->VisitorType);
            $sql1->bindParam(3, $rec->VisitorAddress);
            $sql1->bindParam(4, $rec->VisitorETA);
            $sql1->bindParam(5, $rec->VisitorVehicleNumber);
            $sql1->bindParam(6, $rec->VisitorPurpose);
            $sql1->bindParam(7, $rec->NumberOfPeople);
            $this->prdbh->beginTransaction();
            if ($sql1->execute()) {
                $this->prdbh->commit();
            }
            if ((isset($rec->FlatID)) && ($rec->FlatID != '')) {
                $FlatID = $rec->FlatID;
                $visitorflatarray = explode(",", $FlatID);
                for ($i = 0;$i < count($visitorflatarray);$i++) {
                    $sql2 = $this->prdbh->prepare("INSERT INTO VisitorFlatMapping(FlatID,VisitorID)VALUES (?,?)");
                    $sql2->bindParam(1, $visitorflatarray[$i]);
                    $sql2->bindParam(2, $id);
                    $this->prdbh->beginTransaction();
                    if ($sql2->execute()); {
                        $this->prdbh->commit();
                    }
                }
            }
            if ($extqrcode != '') {
                $response['Message'] = "Visitor with Qr code " . $extqrcode . " entered successfully";
            } else {
                $response['Message'] = "Visitor  entered successfully";
            }
            //$response['Message']="Form filled successfully";
            $response['Status'] = "1";
            $response['VisitorID'] = $id;
            $response['VisitorDetail'] = $items;
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function vistorunplanentryonlinetwo($rec) {
        $tdat = date("U");
        $tdate = $tdat;
        $VisitorName = $rec->VisitorName;
        $value = $rec->VisitorPhoneNumber;
        $count = strlen($value) - 10;
        $VisitorPhoneNumber2 = substr($value, $count);
        $VisitorPurpose = $rec->VisitorPurpose;
        if ($VisitorPurpose != '') {
            $VisitorPurpose = $VisitorPurpose;
        } else {
            $VisitorPurpose = $rec->VisitorPurposeId;
        }
        $VisitorEntryTime = $rec->VisitorEntryTime;
        $parkingid = $rec->parkingid;
        $VisitorQrCode = $rec->VisitorQrCode;
        if ($VisitorQrCode != '') {
            $sql5 = $this->prdbh->prepare("SELECT * FROM VisitorTable WHERE VisitorInOut='1' AND VisitorQrCode = '" . $VisitorQrCode . "'");
            $sql5->execute();
            if ($sql5->rowcount() > 0) {
                $response['Message'] = "Card already linked to visitor";
                $response['Status'] = "0";
            } else {
                $sql = $this->prdbh->prepare("INSERT INTO VisitorTable(RWAID,VerificationIDType,VerificationIDNumber,VisitorPhoneNumber,VisitorQrCode,VisitorName,VisitorImageUrl,VisitorInOut)VALUES (?,?,?,?,?,?,?,1)");
                $sql->bindParam(1, $rec->RWAID);
                $sql->bindParam(2, $rec->VerificationIDType);
                $sql->bindParam(3, $rec->VerificationIDNumber);
                $sql->bindParam(4, $VisitorPhoneNumber2);
                $sql->bindParam(5, $rec->VisitorQrCode);
                $sql->bindParam(6, $rec->VisitorName);
                $sql->bindParam(7, $rec->VisitorImageUrl);
                //$sql->bindParam(8,$rec->VisitorInOut);
                $this->prdbh->beginTransaction();
                if ($sql->execute()) {
                    $id = $this->prdbh->lastInsertId();
                    $this->prdbh->commit();
                    $sql1 = $this->prdbh->prepare("INSERT INTO VisitorDetails(VisitorID,VisitorType,VisitorAddress,VisitorETA,VisitorVehicleNumber,VisitorPurpose,NumberOfPeople,VisitorEntryTime,VisitorCompany)VALUES (?,?,?,?,?,?,?,?,?)");
                    //print_r($sql1);
                    $sql1->bindParam(1, $id);
                    $sql1->bindParam(2, $rec->VisitorType);
                    $sql1->bindParam(3, $rec->VisitorAddress);
                    $sql1->bindParam(4, $rec->VisitorETA);
                    $sql1->bindParam(5, $rec->VisitorVehicleNumber);
                    $sql1->bindParam(6, $rec->VisitorPurpose);
                    $sql1->bindParam(7, $rec->NumberOfPeople);
                    $sql1->bindParam(8, $rec->VisitorEntryTime);
                    $sql1->bindParam(9, $rec->VisitorCompany);
                    $this->prdbh->beginTransaction();
                    if ($sql1->execute()) {
                        $this->prdbh->commit();
                        $sql3 = $this->prdbh->prepare("UPDATE ParkingTable SET  VisitorID='" . $id . "',sticker_issued='1' WHERE ID = '" . $parkingid . "' ");
                        $sql3->execute();
                        if ($sql->rowCount() >= 1) {
                            $response['Data'] = 'Updated successfully';
                            $response['Status'] = '1';
                        } else {
                            $response['Data'] = 'Updated successfully parking not added';
                            $response['Status'] = '1';
                        }
                    }
                    if ((isset($rec->FlatID)) && ($rec->FlatID != '')) {
                        $FlatID = $rec->FlatID;
                        $visitorflatarray = explode(",", $FlatID);
                        for ($i = 0;$i < count($visitorflatarray);$i++) {
                            $sql2 = $this->prdbh->prepare("INSERT INTO VisitorFlatMapping(FlatID,VisitorID)VALUES (?,?)");
                            //print_r($sql2);
                            $sql2->bindParam(1, $visitorflatarray[$i]);
                            $sql2->bindParam(2, $id);
                            $this->prdbh->beginTransaction();
                            if ($sql2->execute()); {
                                $this->prdbh->commit();
                            }
                        }
                        $sql5 = $this->prdbh->prepare("SELECT DetailList FROM VisitorPurpos WHERE ID = '" . $VisitorPurpose . "'  ");
                        $sql5->execute();
                        if ($sql5->rowCount() > 0) {
                            while ($row5 = $sql5->fetch(PDO::FETCH_ASSOC)) {
                                $Purpose = $row5['DetailList'];
                            }
                        }
                        $sql4 = $this->prdbh->prepare("SELECT ResidentRWAMapping.ResidentID FROM ResidentRWAMapping LEFT JOIN ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID= ResidentRWAMapping.ResidentRWAID WHERE  ResidentFlatMapping.FlatID IN ($FlatID)  ");
                        $sql4->execute();
                        if ($sql4->rowCount() > 0) {
                            $UpushId1 = array();
                            while ($row4 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                                $UpushId1[] = $row4['ResidentID'];
                            }
                            $UpushId = implode(',', $UpushId1);
                            $date = date('h:i a ', $VisitorEntryTime + 19660);
                            $ID = $id;
                            if ($VisitorName != '') {
                                $message = $VisitorName . ' with purpose -' . $Purpose . ' will be arriving at your flat soon.';
                            } else {
                                $message = 'Someone with purpose -' . $Purpose . ' will be arriving at your flat soon.';
                            }
                            $UrHd1 = 'mynukad Visitor';
                            $message1 = array("message" => $message, "UrHd2" => $UrHd1, "body" => $ID);
                            $result = $this->firebasenotification($UpushId, $message1);
                            $response['PushStatus'] = $result;
                        }
                    }
                    if ($VisitorQrCode != '') {
                        $response['Message'] = "Visitor with Qr code " . $VisitorQrCode . " entered successfully";
                    } else {
                        $response['Message'] = "Visitor details entered successfully";
                    }
                    $response['Status'] = "1";
                    $response['VisitorID'] = $id;
                }
            }
        } else {
            $sql = $this->prdbh->prepare("INSERT INTO VisitorTable(RWAID,VerificationIDType,VerificationIDNumber,VisitorPhoneNumber,VisitorQrCode,VisitorName,VisitorImageUrl,VisitorInOut)VALUES (?,?,?,?,?,?,?,1)");
            $sql->bindParam(1, $rec->RWAID);
            $sql->bindParam(2, $rec->VerificationIDType);
            $sql->bindParam(3, $rec->VerificationIDNumber);
            $sql->bindParam(4, $VisitorPhoneNumber2);
            $sql->bindParam(5, $rec->VisitorQrCode);
            $sql->bindParam(6, $rec->VisitorName);
            $sql->bindParam(7, $rec->VisitorImageUrl);
            //$sql->bindParam(8,$rec->VisitorInOut);
            $this->prdbh->beginTransaction();
            if ($sql->execute()) {
                $id = $this->prdbh->lastInsertId();
                $this->prdbh->commit();
                $sql1 = $this->prdbh->prepare("INSERT INTO VisitorDetails(VisitorID,VisitorType,VisitorAddress,VisitorETA,VisitorVehicleNumber,VisitorPurpose,NumberOfPeople,VisitorEntryTime,VisitorCompany)VALUES (?,?,?,?,?,?,?,?,?)");
                //print_r($sql1);
                $sql1->bindParam(1, $id);
                $sql1->bindParam(2, $rec->VisitorType);
                $sql1->bindParam(3, $rec->VisitorAddress);
                $sql1->bindParam(4, $rec->VisitorETA);
                $sql1->bindParam(5, $rec->VisitorVehicleNumber);
                $sql1->bindParam(6, $rec->VisitorPurpose);
                $sql1->bindParam(7, $rec->NumberOfPeople);
                $sql1->bindParam(8, $rec->VisitorEntryTime);
                $sql1->bindParam(9, $rec->VisitorCompany);
                $this->prdbh->beginTransaction();
                if ($sql1->execute()) {
                    $this->prdbh->commit();
                    $sql3 = $this->prdbh->prepare("UPDATE ParkingTable SET  VisitorID='" . $id . "',sticker_issued='1' WHERE ID = '" . $parkingid . "' ");
                    $sql3->execute();
                    if ($sql->rowCount() >= 1) {
                        $response['Data'] = 'Updated successfully';
                        $response['Status'] = '1';
                    } else {
                        $response['Data'] = 'Updated successfully parking not added';
                        $response['Status'] = '1';
                    }
                }
                if ((isset($rec->FlatID)) && ($rec->FlatID != '')) {
                    $FlatID = $rec->FlatID;
                    $visitorflatarray = explode(",", $FlatID);
                    for ($i = 0;$i < count($visitorflatarray);$i++) {
                        $sql2 = $this->prdbh->prepare("INSERT INTO VisitorFlatMapping(FlatID,VisitorID)VALUES (?,?)");
                        //print_r($sql2);
                        $sql2->bindParam(1, $visitorflatarray[$i]);
                        $sql2->bindParam(2, $id);
                        $this->prdbh->beginTransaction();
                        if ($sql2->execute()); {
                            $this->prdbh->commit();
                        }
                    }
                    $sql5 = $this->prdbh->prepare("SELECT DetailList FROM VisitorPurpos WHERE ID = '" . $VisitorPurpose . "'  ");
                    $sql5->execute();
                    if ($sql5->rowCount() > 0) {
                        while ($row5 = $sql5->fetch(PDO::FETCH_ASSOC)) {
                            $Purpose = $row5['DetailList'];
                        }
                    }
                    $sql4 = $this->prdbh->prepare("SELECT ResidentRWAMapping.ResidentID FROM ResidentRWAMapping LEFT JOIN ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID= ResidentRWAMapping.ResidentRWAID WHERE  ResidentFlatMapping.FlatID IN ($FlatID)  ");
                    $sql4->execute();
                    if ($sql4->rowCount() > 0) {
                        $UpushId1 = array();
                        while ($row4 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                            $UpushId1[] = $row4['ResidentID'];
                        }
                        $UpushId = implode(',', $UpushId1);
                        $date = date('h:i a ', $VisitorEntryTime + 19660);
                        $ID = $id;
                        if ($VisitorName != '') {
                            $message = $VisitorName . ' with purpose -' . $Purpose . ' will be arriving at your flat soon.';
                        } else {
                            $message = 'Someone with purpose -' . $Purpose . ' will be arriving at your flat soon.';
                        }
                        $UrHd1 = 'mynukad Visitor';
                        $message1 = array("message" => $message, "UrHd2" => $UrHd1, "body" => $ID);
                        $result = $this->firebasenotification($UpushId, $message1);
                        $response['PushStatus'] = $result;
                    }
                }
                if ($VisitorQrCode != '') {
                    $response['Message'] = "Visitor with Qr code " . $VisitorQrCode . " entered successfully";
                } else {
                    $response['Message'] = "Visitor details entered successfully";
                }
                $response['Status'] = "1";
                $response['VisitorID'] = $id;
            }
        }
        return $response;
    }
    public function vistorunplanentryonlinetwodecember($rec) {
        $tdat = date("U");
        $tdate = $tdat;
        $RWAID = $rec->RWAID;
        $VerificationIDType = $rec->VerificationIDType;
        $VerificationIDNumber = $rec->VerificationIDNumber;
        $VisitorName = $rec->VisitorName;
        $value = $rec->VisitorPhoneNumber;
        $count = strlen($value) - 10;
        $VisitorPhoneNumber2 = substr($value, $count);
        $VisitorImageUrl = $rec->VisitorImageUrl;
        $VisitorID = $rec->VisitorID;
        $flag = 0;
        $visitorDetailEntryCount = 0;
        $VisitorQrCode = '';
        $id = '';
        $VisitorPurpose = $rec->VisitorPurpose;
        if ($VisitorPurpose != '') {
            $VisitorPurpose = $VisitorPurpose;
        } else {
            $VisitorPurpose = $rec->VisitorPurposeId;
        }
        if (isset($rec->VisitorQrCode)) {
            $VisitorQrCode = $rec->VisitorQrCode;
        }
        if (isset($rec->parkingid)) {
            $parkingid = $rec->parkingid;
        }
        if (isset($rec->VisitorEntryTime)) {
            $VisitorEntryTime = $rec->VisitorEntryTime;
        }
        if ($VisitorQrCode != '') {
            $sql5 = $this->prdbh->prepare("SELECT * FROM VisitorTable WHERE VisitorInOut='1' AND VisitorQrCode = '" . $VisitorQrCode . "'");
            $sql5->execute();
            if ($sql5->rowcount() > 0) {
                $response['Message'] = "Card already linked to visitor";
                $response['Status'] = "0";
            } else {
                // $sql = $this->prdbh->prepare("UPDATE VisitorTable SET (RWAID = '" . $RWAID . "',VerificationIDType = '" . $VerificationIDType . "',VerificationIDNumber = '" . $VerificationIDNumber . "',
                // VisitorPhoneNumber = '" . $VisitorPhoneNumber . "',VisitorQrCode = '" . $VisitorQrCode . "',VisitorName = '" . $VisitorName . "',VisitorImageUrl = '" . $VisitorImageUrl . "' WHERE VisitorID = '" . $VisitorID . "' AND VisitorInOut=1");

                $sql = $this->prdbh->prepare("INSERT INTO VisitorTable(RWAID,VerificationIDType,VerificationIDNumber,VisitorPhoneNumber,VisitorQrCode,VisitorName,VisitorImageUrl,VisitorInOut)VALUES (?,?,?,?,?,?,?,1)");
                $sql->bindParam(1, $rec->RWAID);
                $sql->bindParam(2, $rec->VerificationIDType);
                $sql->bindParam(3, $rec->VerificationIDNumber);
                $sql->bindParam(4, $VisitorPhoneNumber2);
                $sql->bindParam(5, $rec->VisitorQrCode);
                $sql->bindParam(6, $rec->VisitorName);
                $sql->bindParam(7, $rec->VisitorImageUrl);
                // $sql->bindParam(8,$rec->VisitorInOut);
                $this->prdbh->beginTransaction();
                if ($sql->execute()) {
                    $id = $this->prdbh->lastInsertId();
                    $this->prdbh->commit();
                    $sql1 = $this->prdbh->prepare("INSERT INTO VisitorDetails(VisitorID,VisitorType,VisitorAddress,VisitorETA,VisitorVehicleNumber,VisitorPurpose,NumberOfPeople,VisitorEntryTime,VisitorCompany)VALUES (?,?,?,?,?,?,?,?,?)");
                    //print_r($sql1);
                    $sql1->bindParam(1, $id);
                    $sql1->bindParam(2, $rec->VisitorType);
                    $sql1->bindParam(3, $rec->VisitorAddress);
                    $sql1->bindParam(4, $rec->VisitorETA);
                    $sql1->bindParam(5, $rec->VisitorVehicleNumber);
                    $sql1->bindParam(6, $rec->VisitorPurpose);
                    $sql1->bindParam(7, $rec->NumberOfPeople);
                    $sql1->bindParam(8, $rec->VisitorEntryTime);
                    $sql1->bindParam(9, $rec->VisitorCompany);
                    $this->prdbh->beginTransaction();
                    if ($sql1->execute()) {
                        $this->prdbh->commit();
                        $sql3 = $this->prdbh->prepare("UPDATE ParkingTable SET  VisitorID='" . $id . "',sticker_issued='1' WHERE ID = '" . $parkingid . "' ");
                        $sql3->execute();
                        if ($sql->rowCount() >= 1) {
                            $response['Data'] = 'Updated successfully';
                            $response['Status'] = '1';
                        } else {
                            $response['Data'] = 'Updated successfully parking not added';
                            $response['Status'] = '1';
                        }
                    }
                    if ((isset($rec->FlatID)) && ($rec->FlatID != '')) {
                        $FlatID = $rec->FlatID;
                        $visitorflatarray = explode(",", $FlatID);
                        for ($i = 0;$i < count($visitorflatarray);$i++) {
                            $sql2 = $this->prdbh->prepare("INSERT INTO VisitorFlatMapping(FlatID,VisitorID)VALUES (?,?)");
                            //print_r($sql2);
                            $sql2->bindParam(1, $visitorflatarray[$i]);
                            $sql2->bindParam(2, $id);
                            $this->prdbh->beginTransaction();
                            if ($sql2->execute()); {
                                $this->prdbh->commit();
                            }
                        }
                        $sql5 = $this->prdbh->prepare("SELECT DetailList FROM VisitorPurpos WHERE ID = '" . $VisitorPurpose . "'  ");
                        $sql5->execute();
                        if ($sql5->rowCount() > 0) {
                            while ($row5 = $sql5->fetch(PDO::FETCH_ASSOC)) {
                                $Purpose = $row5['DetailList'];
                            }
                        }
                        $sql4 = $this->prdbh->prepare("SELECT ResidentRWAMapping.ResidentID FROM ResidentRWAMapping LEFT JOIN ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID= ResidentRWAMapping.ResidentRWAID WHERE  ResidentFlatMapping.FlatID IN ($FlatID)  ");
                        $sql4->execute();
                        if ($sql4->rowCount() > 0) {
                            $UpushId1 = array();
                            while ($row4 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                                $UpushId1[] = $row4['ResidentID'];
                            }
                            $UpushId = implode(',', $UpushId1);
                            $date = date('h:i a ', $VisitorEntryTime + 19660);
                            $ID = $id;
                            if ($VisitorName != '') {
                                $message = array("VisitorId" => $VisitorId, "VisitorName" => $VisitorName, "VisitorProfileUrl" => $VisitorImageUrl, "Purpose" => $VisitorPurpose);
                            } else {
                                // $message='Someone with purpose -'.$Purpose.' will be arriving at your flat soon.';
                                $message = array("VisitorId" => $VisitorId, "VisitorName" => $VisitorName, "VisitorProfileUrl" => $VisitorImageUrl, "Purpose" => $VisitorPurpose);
                            }
                            $UrHd1 = 'mynukad Visitor';
                            $message1 = array("message" => $message, "UrHd2" => $UrHd1, "body" => $ID);
                            $result = $this->firebasenotification($UpushId, $message1);
                            $response['PushStatus'] = $result;
                        }
                    }
                    if ($VisitorQrCode != '') {
                        $response['Message'] = "Visitor with Qr code " . $VisitorQrCode . " entered successfully";
                    } else {
                        $response['Message'] = "Visitor details entered successfully";
                    }
                    $response['Status'] = "1";
                    $response['VisitorID'] = $id;
                    $response['VisitorImageUrl'] = $VisitorImageUrl;
                }
            }
        } else {
            if (isset($rec->VisitorID)) {
                $VisitorID = $rec->VisitorID;
                $flag = 1;
                // check to avoid re-entry in the DB
                $sql7 = $this->prdbh->prepare("SELECT ID FROM VisitorDetails where VisitorEntryTime is not null and VisitorExitTime is null and VisitorID  = '" . $VisitorID . "'");
                $sql7->execute();
                if ($sql7->rowcount() > 0) {
                    $response['Status'] = "1";
                    $response['Message'] = "Visitor details already entered";

                    $sqlP = $this->prdbh->prepare("SELECT ResidentRWAMapping.ResidentID FROM ResidentRWAMapping LEFT JOIN ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID= ResidentRWAMapping.ResidentRWAID WHERE  ResidentFlatMapping.FlatID IN ($FlatID)  ");
                    $sqlP->execute();
                    if ($sqlP->rowCount() > 0) {
                       
                        $UpushId1 = array();
                   
                        while ($rowP= $sqlP->fetch(PDO::FETCH_ASSOC)) {
                            $UpushId1[] = $rowP['ResidentID'];
                        }
                        $UpushId = implode(',', $UpushId1);
                        $date = date('h:i a ', $VisitorEntryTime + 19660);
                       
                        $ID = $id;
                        if ($VisitorName != '') {
                            $message = array("VisitorId" => $VisitorId, "VisitorName" => $VisitorName, "VisitorProfileUrl" => $VisitorImageUrl, "Purpose" => $VisitorPurpose);
                        } else {
                            // $message='Someone with purpose -'.$Purpose.' will be arriving at your flat soon.';
                            $message = array("VisitorId" => $VisitorId, "VisitorName" => $VisitorName, "VisitorProfileUrl" => $VisitorImageUrl, "Purpose" => $VisitorPurpose);
                        }
                       
                        $UrHd1 = 'mynukad Visitor';
                        $message1 = array("message" => $message, "UrHd2" => $UrHd1, "body" => $ID);
                        $result = $this->firebasenotification($UpushId, $message1);
                        
                        $response['PushStatus'] = $result;
                    
                    return $response;
                    }
                }
                //end
                $RWAID1 = $rec->RWAID;
                $VerificationIDType1 = $rec->VerificationIDType;
                $VerificationIDNumber1 = $rec->VerificationIDNumber;
                $VisitorQrCode1 = $rec->VisitorQrCode;
                $VisitorName1 = $rec->VisitorName;
                $VisitorImageUrl1 = $rec->VisitorImageUrl;
                $sqlupdate = $this->prdbh->prepare("UPDATE VisitorTable SET RWAID='" . $RWAID1 . "',VerificationIDType='" . $VerificationIDType1 . "', VisitorQrCode= '" . $VisitorQrCode1 . "',VisitorName='" . $VisitorName1 . "',VisitorImageUrl='" . $VisitorImageUrl1 . "',VisitorInOut = 0 WHERE VisitorID= '" . $VisitorID . "' ");
                $sqlupdate->execute();
                $id = $VisitorID;
            } else {
                $flag = 2;
                $sqlinsert = $this->prdbh->prepare("INSERT INTO VisitorTable(RWAID,VerificationIDType,VerificationIDNumber,VisitorPhoneNumber,VisitorQrCode,VisitorName,VisitorImageUrl,VisitorInOut)VALUES (?,?,?,?,?,?,?,0)");
                $sql->bindParam(1, $rec->RWAID);
                $sql->bindParam(2, $rec->VerificationIDType);
                $sql->bindParam(3, $rec->VerificationIDNumber);
                $sql->bindParam(4, $VisitorPhoneNumber2);
                $sql->bindParam(5, $rec->VisitorQrCode);
                $sql->bindParam(6, $rec->VisitorName);
                $sql->bindParam(7, $rec->VisitorImageUrl);
                //$sql->bindParam(8,$rec->VisitorInOut);
                $this->prdbh->beginTransaction();
                $sqlinsert->execute();
                $id = $this->prdbh->lastInsertId();
                $this->prdbh->commit();
            }
            if ($flag == 1) {
                $sqlRecordEntryCheck = $this->prdbh->prepare("SELECT VisitorID from VisitorDetails where VisitorID = '" . $id . "' ");
                $sqlRecordEntryCheck->execute();
                $visitorDetailEntryCount = $sqlRecordEntryCheck->rowCount();

                if ($visitorDetailEntryCount >= 1) {

                    $VisitorType1 = $rec->VisitorType;
                    $VisitorAddress1 = $rec->VisitorAddress;
                    $VisitorETA1 = $rec->VisitorETA;
                    $VisitorVehicleNumber1 = $rec->VisitorVehicleNumber;
                    $VisitorPurpose1 = $rec->VisitorPurpose;
                    $NumberOfPeople1 = $rec->NumberOfPeople;
                    $VisitorEntryTime1 = $rec->VisitorEntryTime;
                    $VisitorCompany1 = $rec->VisitorCompany;

                    $updateVisitordetailEntry = $this->prdbh->prepare("UPDATE VisitorDetails SET VisitorType = '" . $VisitorType1 . "' , VisitorAddress = '" . $VisitorAddress1 . "', VisitorETA = '" . $VisitorETA1 . "', VisitorVehicleNumber= '" . $VisitorVehicleNumber1 . "', VisitorPurpose = '" . $VisitorPurpose1 . "',NumberOfPeople= '" . $NumberOfPeople1 . "', VisitorEntryTime = '" . $VisitorEntryTime1 . "', VisitorCompany = '" .$VisitorCompany1. "' WHERE VisitorID = '" . $id . "' ");

                    if ($updateVisitordetailEntry->execute()) {
                        $sqlParkingUpdate = $this->prdbh->prepare("UPDATE ParkingTable SET  VisitorID='" . $id . "',sticker_issued='1' WHERE ID = '" . $parkingid . "' ");
                        if ($sqlParkingUpdate->execute()) {
                            $response['Data'] = 'Parking updated';
                            $response['Status'] = '1';
                        } else {
                            $response['SQLerror2'] = $sqlParkingUpdate->errorInfo();
                            $response['Status'] = '0';
                        }
                    } else {
                        $response['SQLerror1'] = $updateVisitordetailEntry->errorInfo();
                        $response['Status'] = '0';
                    }
                }
            }
            if ($flag == 2 || ($flag == 1 && $visitorDetailEntryCount == 0)) {

                $sql1 = $this->prdbh->prepare("INSERT INTO VisitorDetails(VisitorID,VisitorType,VisitorAddress,VisitorETA,VisitorVehicleNumber,VisitorPurpose,NumberOfPeople,VisitorEntryTime,VisitorCompany)VALUES (?,?,?,?,?,?,?,?,?)");
                //print_r($sql1);
                $sql1->bindParam(1, $id);
                $sql1->bindParam(2, $rec->VisitorType);
                $sql1->bindParam(3, $rec->VisitorAddress);
                $sql1->bindParam(4, $rec->VisitorETA);
                $sql1->bindParam(5, $rec->VisitorVehicleNumber);
                $sql1->bindParam(6, $rec->VisitorPurpose);
                $sql1->bindParam(7, $rec->NumberOfPeople);
                $sql1->bindParam(8, $rec->VisitorEntryTime);
                $sql1->bindParam(9, $rec->VisitorCompany);
                $this->prdbh->beginTransaction();

                if ($sql1->execute()) {
                    $this->prdbh->commit();
                    $sql3 = $this->prdbh->prepare("UPDATE ParkingTable SET  VisitorID='" . $id . "',sticker_issued='1' WHERE ID = '" . $parkingid . "' ");
                    if ($sql3->execute()) {
                        $response['Data'] = 'Updated successfully';
                        $response['Status'] = '1';
                    } else {
                        $response['Data'] = 'Updated successfully parking not added';
                        $response['Status'] = '1';
                    }
                }
            }
            if ((isset($rec->FlatID)) && ($rec->FlatID != '')) {
                $FlatID = $rec->FlatID;
                $visitorflatarray = explode(",", $FlatID);
                for ($i = 0;$i < count($visitorflatarray);$i++) {
                    $sql2 = $this->prdbh->prepare("INSERT INTO VisitorFlatMapping(FlatID,VisitorID)VALUES (?,?)");
                    //print_r($sql2);
                    $sql2->bindParam(1, $visitorflatarray[$i]);
                    $sql2->bindParam(2, $id);
                    $this->prdbh->beginTransaction();
                    if ($sql2->execute()); {
                        $this->prdbh->commit();
                    }
                    //visitorApprovalStatus
                    $sql251 = $this->prdbh->prepare("INSERT INTO visitorApprovalStatus(flatId,visitorId,approvalStatus,status)VALUES (?,?,2,1)");
                    $sql251->bindParam(1, $visitorflatarray[$i]);
                    $sql251->bindParam(2, $id);
                    $this->prdbh->beginTransaction();
                    if ($sql251->execute()); {
                        $this->prdbh->commit();
                    }
                    //end
                    
                }
                $sql5 = $this->prdbh->prepare("SELECT DetailList FROM VisitorPurpos WHERE ID = '" . $VisitorPurpose . "'  ");
                $sql5->execute();
                if ($sql5->rowCount() > 0) {
                    while ($row5 = $sql5->fetch(PDO::FETCH_ASSOC)) {
                        $Purpose = $row5['DetailList'];
                    }
                }
                $sql4 = $this->prdbh->prepare("SELECT ResidentRWAMapping.ResidentID FROM ResidentRWAMapping LEFT JOIN ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID= ResidentRWAMapping.ResidentRWAID WHERE  ResidentFlatMapping.FlatID IN ($FlatID)  ");
                $sql4->execute();
                if ($sql4->rowCount() > 0) {
                    $UpushId1 = array();
                    while ($row4 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                        $UpushId1[] = $row4['ResidentID'];
                    }
                    $UpushId = implode(',', $UpushId1);
                    $date = date('h:i a ', $VisitorEntryTime + 19660);
                    $ID = $id;
                    if ($VisitorName != '') {
                        $message = array("VisitorId" => $VisitorID, "VisitorName" => $VisitorName, "VisitorProfileUrl" => $VisitorImageUrl, "Purpose" => $$VisitorPurpose);
                        //$message = .$VisitorName.$VisitorProfileUrl.$Purpose;
                        $message2 = $VisitorName . ' with purpose -' . $Purpose . ' will be arriving at your flat soon.';
                    } else {
                        $message2 = 'Someone with purpose -' . $Purpose . ' will be arriving at your flat soon.';
                        $message = array("VisitorId" => $VisitorID, "VisitorName" => $VisitorName, "VisitorProfileUrl" => $VisitorImageUrl, "Purpose" => $VisitorPurpose);
                    }
                    $UrHd1 = 'mynukad Unplanned Visitor';
                    $message1 = array("message" => $message, "UrHd2" => $UrHd1, "body" => $ID);
                    $message3 = array("message" => $message2, "UrHd2" => $UrHd1, "body" => $ID);
                    $result = $this->firebasenotification250($UpushId, $message1, $VisitorName, $VisitorImageUrl, $message3);
                    $response['PushStatus'] = $result;
                }
            }
            if ($VisitorQrCode != '') {
                $response['Message'] = "Visitor with Qr code " . $VisitorQrCode . " entered successfully";
            } else {
                $response['Message'] = "Visitor details entered successfully";
            }
            $response['Status'] = "1";
            $response['VisitorID'] = $id;
            $response['VisitorImageUrl'] = $VisitorImageUrl;
        }
        return $response;
    }
    public function helperentrytawerlevel($rec) {
        $HelperQRCode = $rec->HelperQRCode;
        $HelperEntryTime = $rec->HelperEntryTime;
        $HelperExitTime = $rec->HelperExitTime;
        $TowerPrefix = $rec->TowerPrefix;
        $TowerID = $rec->TowerID;
        $Status = $rec->Status;
        $sql1 = $this->prdbh->prepare("SELECT ID,RWAID,Name,ProfilePhoto,is_in FROM Helpers WHERE IsActive= 1 AND  HelperQRCode='" . $HelperQRCode . "' ");
        if ($sql1->execute()) {
            $count = $sql1->rowCount();
            if ($count >= 1) {
                while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                    $helperid = $row1['ID'];
                    $rwaid = $row1['RWAID'];
                    $name = $row1['Name'];
                    $is_in = $row1['is_in'];
                }
                $sql5 = $this->prdbh->prepare("SELECT FlatNbr FROM SocietyFlatMapping INNER JOIN SocietyHelperWorkingFlat ON 
                        SocietyHelperWorkingFlat.SocietyFlatID=SocietyFlatMapping.ID WHERE SocietyHelperWorkingFlat.HelperID = '" . $helperid . "'  ");
                if ($sql5->execute()) {
                    if ($sql5->rowCount() >= 1) {
                        $FlatNbr = array();
                        while ($row5 = $sql5->fetch(PDO::FETCH_ASSOC)) {
                            $FlatNbr[] = ucfirst(substr($row5['FlatNbr'], 0, 1));
                        }
                    }
                }
                if (in_array($TowerPrefix, $FlatNbr)) {
                    $sql4 = $this->prdbh->prepare("SELECT ID FROM Attendance WHERE HelperID = '" . $helperid . "'  ");
                    if ($sql4->execute()) {
                        if ($sql4->rowCount() >= 1) {
                            while ($row4 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                                $attendanceid = $row4['ID'];
                            }
                        }
                    }
                    if ($is_in == '1') {
                        if ($HelperEntryTime != '') {
                            $sql2 = $this->prdbh->prepare("INSERT INTO HelperTowerAuditTrail (HelperID, TowerID, HelperAttendanceID, HelperEntryTime,Status) VALUES($helperid,'" . $TowerID . "','" . $attendanceid . "','" . $HelperEntryTime . "','" . $Status . "')");
                        } elseif ($HelperExitTime != '') {
                            $sql2 = $this->prdbh->prepare("INSERT INTO HelperTowerAuditTrail (HelperID, TowerID, HelperAttendanceID, HelperExitTime,Status) VALUES($helperid,'" . $TowerID . "','" . $attendanceid . "','" . $HelperExitTime . "','" . $Status . "')");
                        }
                        //print_r($sql2);die;
                        $this->prdbh->beginTransaction();
                        $sql2->execute();
                        if ($sql2->execute()) {
                            $this->prdbh->commit();
                            $response['Status'] = "1";
                            $response['Message'] = "successfully";
                        } else {
                            $response['Status'] = "1";
                            $response['Message'] = "First checkin from main gate";
                        }
                    } else {
                        $response['Status'] = "0";
                        $response['Message'] = "First checkin from main gate";
                    }
                } else {
                    $response['Status'] = "0";
                    $response['Message'] = "you are not permited for this tower";
                }
            } else {
                $response['Message'] = "Invalid QR code";
                $response['Status'] = 0;
            }
        } else {
            $response['Message'] = $sql1->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function helperisinnew($rec) {
        $RWAID = $rec->RWAID;
        $tdat = date("U");
        $timestamp = $rec->timestamp;
        //$OTP = substr($OTP, 2);
        $timestamp1 = rtrim($timestamp, ",");
        $timestamparray = explode(",", $timestamp1);
        $is_in = $rec->is_in;
        $is_in1 = rtrim($is_in, ",");
        $is_inarray = explode(",", $is_in1);
        $HelperQRCode = $rec->HelperQRCode;
        $HelperQRCode1 = rtrim($HelperQRCode, ",");
        $HelperQRCodearray = explode(",", $HelperQRCode1);
        $counta = count($HelperQRCode);
        for ($i = 0;$i < count($HelperQRCodearray);$i++) {
            $sql1 = $this->prdbh->prepare("SELECT ID,RWAID,Name,ProfilePhoto FROM Helpers WHERE  HelperQRCode='" . $HelperQRCodearray[$i] . "'  AND (EntryCardExpiry >= '" . $tdat . "' or EntryCardExpiry='null' or EntryCardExpiry='0'  )");
            //print_r($sql1);die;
            $sql1->execute();
            if ($sql1->execute()) {
                $count = $sql1->rowCount();
                if ($count >= 1) {
                    while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                        $helperid = $row1['ID'];
                        $rwaid = $row1['RWAID'];
                        $name = $row1['Name'];
                    }
                    if ($RWAID == $rwaid) {
                        $sql2 = $this->prdbh->prepare("INSERT INTO Attendance (HelperID,timestamp,RWAID,is_in,GateNnrID,GatekeeperID) VALUES(?,?,?,?,?,?)");
                        $sql2->bindParam(1, $helperid);
                        $sql2->bindParam(2, $timestamparray[$i]);
                        $sql2->bindParam(3, $rwaid);
                        $sql2->bindParam(4, $is_inarray[$i]);
                        $sql2->bindParam(5, $rec->GateNnrID);
                        $sql2->bindParam(6, $rec->GatekeeperID);
                        $this->prdbh->beginTransaction();
                        if ($sql2->execute()) {
                            $this->prdbh->commit();
                            $response['Message'] = "Successful";
                            $response['Status'] = "1";
                        }
                        $sql = $this->prdbh->prepare("UPDATE Helpers SET  is_in='" . $is_inarray[$i] . "',LastScaneTimestamp= '" . $timestamparray[$i] . "' WHERE  HelperQRCode='" . $HelperQRCodearray[$i] . "' ");
                        $this->prdbh->beginTransaction();
                        if ($sql->execute()) {
                            $this->prdbh->commit();
                        } else {
                            $response['message'] = "NO UPDATE";
                        }
                        $sql4 = $this->prdbh->prepare("SELECT GateName FROM GatenoEntry WHERE ID='" . $rec->GateNnrID . "'  ");
                        $sql4->execute();
                        if ($sql4->rowCount() > 0) {
                            while ($row4 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                                $gatenbr = $row4['GateName'];
                            }
                        }
                        $sql3 = $this->prdbh->prepare("SELECT ResidentID FROM ResidentRWAMapping LEFT JOIN HelperSubscribe ON ResidentRWAMapping.ResidentRWAID = HelperSubscribe.ResidentRWAID WHERE ResidentRWAMapping.RWAID='" . $rwaid . "' AND HelperSubscribe.HelperID='" . $helperid . "' AND HelperSubscribe.Subscribe='1' ");
                        $sql3->execute();
                        if ($sql3->rowCount() > 0) {
                            $UpushId1 = array();
                            while ($row3 = $sql3->fetch(PDO::FETCH_ASSOC)) {
                                $UpushId1[] = $row3['ResidentID'];
                            }
                            $UpushId = implode(',', $UpushId1);
                            $in = '';
                            $date = date('h:i a', $timestamp + 19660);
                            if ($is_in == '1') {
                                $in = "in";
                            } elseif ($is_in == '2') {
                                $in = "out";
                            }
                            $message = $name . ' has checked ' . $in . ' at ' . $date . ' from  ' . $gatenbr;
                            $UrHd1 = 'mynukad helper attendance';
                            //$response['PushStatus']=$result;
                            
                        }
                    } else {
                        $response['Message'] = "Invalid QR code for this society";
                        $response['Status'] = 0;
                    }
                } else {
                    $response['Message'] = "Invalid QR code OR Helper Card validity expired, please contact Maintenance or relevant authority";
                    $response['Status'] = 0;
                }
            } else {
                $response['Message'] = $sql1->errorInfo();
                $response['Status'] = 0;
            }
        }
        return $response;
    }
    public function shorturl($qrpath) {
        $data = array('longUrl' => $qrpath);
        $input = json_encode($data);
        $curl = curl_init();
        curl_setopt_array($curl, array(CURLOPT_URL => "https://www.googleapis.com/urlshortener/v1/url?key=AIzaSyAKwL858bcviDftnk_Nu-dLgGFrvxTV9As", CURLOPT_RETURNTRANSFER => true, CURLOPT_ENCODING => "", CURLOPT_MAXREDIRS => 10, CURLOPT_TIMEOUT => 30, CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1, CURLOPT_CUSTOMREQUEST => "POST", CURLOPT_POSTFIELDS => $input, CURLOPT_HTTPHEADER => array("content-type: application/json",),));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        $res = json_decode($response);
        //$res->id;
        return $res;
    }
    public function shorturlnew($qrpath) {
        $data = array('destination' => $qrpath);
        $input = json_encode($data);
        $curl = curl_init();
        curl_setopt_array($curl, array(CURLOPT_URL => "https://api.rebrandly.com/v1/links", CURLOPT_RETURNTRANSFER => true, CURLOPT_ENCODING => "", CURLOPT_MAXREDIRS => 10, CURLOPT_TIMEOUT => 30, CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1, CURLOPT_CUSTOMREQUEST => "POST", CURLOPT_POSTFIELDS => $input, CURLOPT_HTTPHEADER => array("Content-Type: application/json", "accept-encoding: gzip, deflate", "apikey: f9dacaa161224f9fa4725da70503113d",),));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        $res = json_decode($response);
        $value = $res->shortUrl;
        return $value;
    }
    public function generateqrcodeforuser($filepath, $codeContents) {
        // Path where the images will be saved
        //$filepath = 'myimage.png';
        // Image (logo) to be drawn
        $logopath = ConstantUrl . 'Visitor_Qrcode/MyNukad_Logo50.png';
        // qr code content
        //$codeContents = 'hi how are you';
        //$codeContents = 'some othertext 1234';
        $back_color = 0xFFFFFF;
        $fore_color = 0x3C5B9B;
        // Create the file in the providen path
        // Customize how you want
        QRcode::png($codeContents, $filepath, QR_ECLEVEL_H, 8, 1, true, $back_color, $fore_color);
        //QRcode::png('some othertext 1234', $filepath, 'QR_ECLEVEL_H', 10, 2, false, $back_color, $fore_color);
        // Start DRAWING LOGO IN QRCODE
        $QR = imagecreatefrompng($filepath);
        // START TO DRAW THE IMAGE ON THE QR CODE
        $logo = imagecreatefromstring(file_get_contents($logopath));
        //Fix for the transparent background
        imagecolortransparent($logo, imagecolorallocatealpha($logo, 0, 0, 0, 127));
        imagealphablending($logo, false);
        imagesavealpha($logo, true);
        $QR_width = imagesx($QR);
        $QR_height = imagesy($QR);
        $logo_width = imagesx($logo);
        $logo_height = imagesy($logo);
        // Scale logo to fit in the QR Code
        $logo_qr_width = $QR_width / 3;
        $scale = $logo_width / $logo_qr_width;
        $logo_qr_height = $logo_height / $scale;
        //imagecopyresampled($QR, $logo, $QR_width/3, $QR_height/3, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);
        imagecopyresampled($QR, $logo, 67, 67, 0, 0, 49, 49, 50, 50);
        // Save QR code again, but with logo on it
        imagepng($QR, $filepath);
        imagedestroy($QR);
        // End DRAWING LOGO IN QR CODE
        // Ouput image in the browser
        //echo '<img src="'.$filepath.'" />';
        return $filepath;
    }
    public function vistorentryform($rec) {
        $tdat = date("U");
        $tdate = $tdat;
        //$token = uniqid(rand(),TRUE);
        //$otp=substr($token,27);
        $otp = mt_rand(100000, 999999);
        $qr = "V:" . $otp;
        $value = $rec->VisitorPhoneNumber;
        $count = strlen($value) - 10;
        $VisitorPhoneNumber2 = substr($value, $count);
        // $qrpath="https://chart.googleapis.com/chart?cht=qr&chs=200x200&chld=H|0&chl=".$qr;
        // $vn=$this->shorturl($qrpath);
        // $vs=$vn->id;
        //print_r($vs);die;
        $sql2 = $this->prdbh->prepare("SELECT VisitorName, VisitorPhoneNumber,RWAID FROM VisitorTable Where OTP ='" . $otp . "'"); // ORDER BY VisitorID DESC LIMIT 1
        $sql2->execute();
        if ($sql2->rowCount() > 0) {
            $otp = mt_rand(100000, 999999);
            $qr = "V:" . $otp;
        }
        $sql = $this->prdbh->prepare("INSERT INTO VisitorTable(VisitorName, VisitorPhoneNumber,RWAID,FlatID,ResidentRWAID,OTP,Saved)VALUES (?,?,?,?,?,?,1)");
        $sql->bindParam(1, $rec->VisitorName);
        $sql->bindParam(2, $VisitorPhoneNumber2);
        $sql->bindParam(3, $rec->RWAID);
        $sql->bindParam(4, $rec->FlatID);
        $sql->bindParam(5, $rec->ResidentRWAID);
        $sql->bindParam(6, $otp);
        //$sql->bindParam(7,$vs);
        $this->prdbh->beginTransaction();
        if ($sql->execute()) {
            $id = $this->prdbh->lastInsertId();
            $this->prdbh->commit();
            // CREATE QRCODE FOR USER
            $filepath ="Visitor_Qrcode/" . $id . ".png";
            $QRCODE_URL = ConstantUrl . "Visitor_Qrcode/" . $id . ".png";
            $QRCODE_URL1 = $this->generateqrcodeforuser($filepath, $qr);
            $vn = $this->shorturl($QRCODE_URL);
            //print_r($QRCODE_URL);die;
            $vs = $QRCODE_URL;
            //$vs=$vn->id;
            $sql2 = $this->prdbh->prepare("UPDATE VisitorTable SET QrPath='" . $vs . "' WHERE VisitorID=$id");
            $this->prdbh->beginTransaction();
            $sql2->execute();
            $this->prdbh->commit();
            // END OF CREATE QRCODE FOR USER
            $sql1 = $this->prdbh->prepare("INSERT INTO VisitorDetails(VisitorID,VisitorType,VisitorAddress,VisitorETA,VisitorVehicleNumber,VisitorPurpose,NumberOfPeople)VALUES (?,?,?,?,?,?,?)");
            //print_r($sql1);
            $sql1->bindParam(1, $id);
            $sql1->bindParam(2, $rec->VisitorType);
            $sql1->bindParam(3, $rec->VisitorAddress);
            $sql1->bindParam(4, $rec->VisitorETA);
            $sql1->bindParam(5, $rec->VisitorVehicleNumber);
            $sql1->bindParam(6, $rec->VisitorPurpose);
            $sql1->bindParam(7, $rec->NumberOfPeople);
            $this->prdbh->beginTransaction();
            if ($sql1->execute()) {
                $this->prdbh->commit();
                $response['Message'] = "Form filled successfully";
                $response['Status'] = "1";
                $response['OTP'] = $otp;
                $response['ID'] = $id;
                $response['QrCode'] = $vs;
                //$response['Token'] = $token
                $fltid = $rec->Flatorignalid;
                $flatidarray = explode(",", $fltid);
                //print_r($flatidarray);die;
                for ($i = 0;$i < count($flatidarray);$i++) {
                    $sql3 = $this->prdbh->prepare("INSERT INTO VisitorFlatMapping(FlatID,VisitorID)VALUES ('$fltid','$id')");
                    //print_r($sql3);
                    $this->prdbh->beginTransaction();
                    if ($sql3->execute()) {
                        $this->prdbh->commit();
                    }
                }
            } else {
                $response['Message'] = $sql1->errorInfo();
                $response['Status'] = "0";
            }
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function vistorentryformdecember($rec) {
        $tdat = date("U");
        $tdate = $tdat;
        //$token = uniqid(rand(),TRUE);
        //$otp=substr($token,27);
        $otp = mt_rand(100000, 999999);
        $qr = "V:" . $otp;
        $value = $rec->VisitorPhoneNumber;
        $count = strlen($value) - 10;
        $VisitorPhoneNumber2 = substr($value, $count);
        // $qrpath="https://chart.googleapis.com/chart?cht=qr&chs=200x200&chld=H|0&chl=".$qr;
        // $vn=$this->shorturl($qrpath);
        // $vs=$vn->id;
        //print_r($vs);die;
        $sql2 = $this->prdbh->prepare("SELECT VisitorName, VisitorPhoneNumber,RWAID FROM VisitorTable Where OTP ='" . $otp . "'"); // ORDER BY VisitorID DESC LIMIT 1
        $sql2->execute();
        if ($sql2->rowCount() > 0) {
            $otp = mt_rand(100000, 999999);
            $qr = "V:" . $otp;
        }
        $sql = $this->prdbh->prepare("INSERT INTO VisitorTable(VisitorName, VisitorPhoneNumber,RWAID,FlatID,ResidentRWAID,OTP,VisitorQrCode,Saved)VALUES (?,?,?,?,?,?,?,1)");
        $sql->bindParam(1, $rec->VisitorName);
        $sql->bindParam(2, $VisitorPhoneNumber2);
        $sql->bindParam(3, $rec->RWAID);
        $sql->bindParam(4, $rec->FlatID);
        $sql->bindParam(5, $rec->ResidentRWAID);
        $sql->bindParam(6, $otp);
        $sql->bindParam(7, $qr);
        //$sql->bindParam(7,$vs);
        $this->prdbh->beginTransaction();
        if ($sql->execute()) {
            $id = $this->prdbh->lastInsertId();
            $this->prdbh->commit();
            // CREATE QRCODE FOR USER
            $filepath = "/var/www/html/Nukkad/"."Visitor_Qrcode/".$id.".png";
            $QRCODE_URL = ConstantUrl. "Visitor_Qrcode/" . $id . ".png";
            $QRCODE_URL1 = $this->generateqrcodeforuser($filepath, $qr);
            $vn = $this->shorturl($QRCODE_URL);
            //print_r($QRCODE_URL);die;
            $vs = $QRCODE_URL;
            //$vs=$vn->id;
            $sql2 = $this->prdbh->prepare("UPDATE VisitorTable SET QrPath='" . $vs . "' WHERE VisitorID=$id");
            $this->prdbh->beginTransaction();
            $sql2->execute();
            $this->prdbh->commit();
            // END OF CREATE QRCODE FOR USER
            $sql1 = $this->prdbh->prepare("INSERT INTO VisitorDetails(VisitorID,VisitorType,VisitorAddress,VisitorETA,VisitorVehicleNumber,VisitorPurpose,NumberOfPeople)VALUES (?,?,?,?,?,?,?)");
            //print_r($sql1);
            $sql1->bindParam(1, $id);
            $sql1->bindParam(2, $rec->VisitorType);
            $sql1->bindParam(3, $rec->VisitorAddress);
            $sql1->bindParam(4, $rec->VisitorETA);
            $sql1->bindParam(5, $rec->VisitorVehicleNumber);
            $sql1->bindParam(6, $rec->VisitorPurpose);
            $sql1->bindParam(7, $rec->NumberOfPeople);
            $this->prdbh->beginTransaction();
            if ($sql1->execute()) {
                $this->prdbh->commit();
                $response['Message'] = "Form filled successfully";
                $response['Status'] = "1";
                $response['OTP'] = $otp;
                $response['ID'] = $id;
                $response['QrCode'] = $vs;
                //$response['Token'] = $token
                $fltid = $rec->Flatorignalid;
                $flatidarray = explode(",", $fltid);
                //print_r($flatidarray);die;
                for ($i = 0;$i < count($flatidarray);$i++) {
                    $sql3 = $this->prdbh->prepare("INSERT INTO VisitorFlatMapping(FlatID,VisitorID)VALUES ('$fltid','$id')");
                    //print_r($sql3);
                    $this->prdbh->beginTransaction();
                    if ($sql3->execute()) {
                        $this->prdbh->commit();
                    }
                }
            } else {
                $response['Message'] = $sql1->errorInfo();
                $response['Status'] = "0";
            }
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function deletevisitor($param) {
        if (isset($param->VisitorID)) {
            $VisitorID = $param->VisitorID;
        }
        $response = array();
        $sql = $this->prdbh->prepare("UPDATE VisitorTable SET  IsActive='0' WHERE VisitorID = '" . $VisitorID . "'  ");
        //print_r($sql);die;
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['Visitor'] = 'Removed successfully';
                $response['Status'] = '1';
            } else {
                $response['Visitor'] = 'Not Removed';
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function onedayvisitorlist($param) {
        $Search = '';
        if (isset($param->Search)) {
            $Search = $param->Search;
        }
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        if (isset($param->timestamp)) {
            $timestamp = $param->timestamp;
        }
        $endtimestamp = $timestamp + 86399;
        $PageNo = '';
        if (isset($param->PageNo)) {
            $PageNo = $param->PageNo;
        }
        $Start = $PageNo * 10 - 10;
        $response = array();
        if ($Search != '') {
            $sql = $this->prdbh->prepare("SELECT VisitorTable.VisitorID, VisitorTable.VisitorName, VisitorTable.VisitorImageUrl, TRIM(LEADING '+91' FROM VisitorPhoneNumber) as VisitorPhoneNumber, VisitorTable.Saved, VisitorTable.RWAID, VisitorTable.OTP, VisitorTable.ResidentRWAID, VisitorTable.VisitorInOut, VisitorTable.IsActive, VisitorTable.QrPath, VisitorTable.VerificationIDType, VisitorTable.VerificationIDNumber, VisitorTable.VisitorQrCode,VisitorDetails.ID, VisitorDetails.VisitorType, VisitorDetails.VisitorAddress, VisitorDetails.VisitorPicPath, VisitorDetails.VisitorDocPath, VisitorDetails.VisitorETA, VisitorDetails.VisitorEntryTime, VisitorDetails.VisitorExitTime, VisitorDetails.VisitorVehicleNumber, VisitorDetails.VisitorPurpose, VisitorDetails.NumberOfPeople, VisitorDetails.VisitorCompany,VisitorPurpos.DetailList FROM VisitorTable LEFT JOIN  VisitorDetails ON VisitorTable.VisitorID= VisitorDetails.VisitorID LEFT JOIN VisitorPurpos ON VisitorDetails.VisitorPurpose =VisitorPurpos.ID   WHERE VisitorTable.RWAID ='" . $RWAID . "' AND VisitorTable.IsActive='1' AND VisitorTable.VisitorInOut='1' AND  (VisitorTable.VisitorName like '%" . $Search . "%' OR  VisitorTable.VisitorPhoneNumber like '" . $Search . "%') AND VisitorDetails.VisitorEntryTime BETWEEN '" . $timestamp . "' AND '" . $endtimestamp . "'  ORDER BY VisitorTable.VisitorID DESC ");
        } else {
            $sql = $this->prdbh->prepare("SELECT VisitorTable.VisitorID, VisitorTable.VisitorName, VisitorTable.VisitorImageUrl, TRIM(LEADING '+91' FROM VisitorPhoneNumber) as VisitorPhoneNumber, VisitorTable.Saved, VisitorTable.RWAID, VisitorTable.OTP, VisitorTable.ResidentRWAID, VisitorTable.VisitorInOut, VisitorTable.IsActive,   VisitorTable.VerificationIDNumber,VisitorTable.VerificationIDType,  VisitorTable.VisitorQrCode,  VisitorDetails.VisitorAddress, VisitorDetails.VisitorPicPath,  VisitorDetails.VisitorETA, VisitorDetails.VisitorEntryTime, VisitorDetails.VisitorExitTime, VisitorDetails.VisitorVehicleNumber, VisitorDetails.VisitorPurpose, VisitorDetails.NumberOfPeople, VisitorDetails.VisitorCompany,VisitorPurpos.DetailList FROM VisitorTable LEFT JOIN  VisitorDetails ON VisitorTable.VisitorID= VisitorDetails.VisitorID LEFT JOIN VisitorPurpos ON VisitorDetails.VisitorPurpose =VisitorPurpos.ID   WHERE VisitorTable.RWAID ='" . $RWAID . "'  AND VisitorTable.IsActive='1' AND VisitorTable.VisitorInOut='1' AND VisitorDetails.VisitorEntryTime BETWEEN '" . $timestamp . "' AND '" . $endtimestamp . "' ORDER BY VisitorTable.VisitorID DESC Limit $Start,10   ");
        }
        //print_r($sql);die;
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                $i = 0;
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $VisitorID = $row['VisitorID'];
                    $items[] = $row;
                    $sql2 = '';
                    $sql2 = $this->prdbh->prepare("SELECT SocietyFlatMapping.FlatNbr FROM VisitorFlatMapping inner JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= VisitorFlatMapping.FlatID WHERE VisitorFlatMapping.VisitorID='" . $VisitorID . "' ");
                    if ($sql2->execute()) {
                        $FlatNbr = array();
                        $FlatNbrString = "";
                        $count2 = $sql2->rowCount();
                        if ($count2 >= 1) {
                            while ($row2 = $sql2->fetch(PDO::FETCH_ASSOC)) {
                                $FlatNbr[] = $row2['FlatNbr'];
                            }
                            $FlatNbrString = implode(',', $FlatNbr);
                        } else {
                            $FlatNbrString = "";
                        }
                    }
                    $items[$i]['FlatNbr'] = $FlatNbrString;
                    $i++;
                }
                $response['vistordetail'] = $items;
                $response['Status'] = 1;
            } else {
                $response['vistordetail'] = '';
                $response['message'] = "";
                $response['Status'] = 0;
            }
        } else {
            $response['vistordetail'] = '';
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function onedayvisitorlistdecember($param) {
        $Search = '';
        if (isset($param->Search)) {
            $Search = $param->Search;
        }
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        if (isset($param->timestamp)) {
            $timestamp = $param->timestamp;
        }
        $endtimestamp = $timestamp + 86399;
        $PageNo = '';
        if (isset($param->PageNo)) {
            $PageNo = $param->PageNo;
        }
        $Start = $PageNo * 10 - 10;
        $response = array();
       
        if ($Search != '') {
            $sql = $this->prdbh->prepare("SELECT VisitorTable.VisitorID, VisitorTable.VisitorName, VisitorTable.VisitorImageUrl, TRIM(LEADING '+91' FROM VisitorTable.VisitorPhoneNumber) as VisitorPhoneNumber, VisitorTable.Saved, VisitorTable.RWAID, VisitorTable.OTP, VisitorTable.ResidentRWAID, VisitorTable.VisitorInOut, VisitorTable.IsActive, VisitorTable.QrPath, VisitorTable.VerificationIDType, VisitorTable.VerificationIDNumber, VisitorTable.VisitorQrCode,VisitorDetails.ID, VisitorDetails.VisitorType, VisitorDetails.VisitorAddress, VisitorDetails.VisitorPicPath, VisitorDetails.VisitorDocPath, VisitorDetails.VisitorETA, VisitorDetails.VisitorEntryTime, VisitorDetails.VisitorExitTime, VisitorDetails.VisitorVehicleNumber, VisitorDetails.VisitorPurpose, VisitorDetails.NumberOfPeople, VisitorDetails.VisitorCompany,VisitorPurpos.DetailList FROM  VisitorTable LEFT JOIN  VisitorDetails ON VisitorTable.VisitorID= VisitorDetails.VisitorID LEFT JOIN VisitorPurpos ON VisitorDetails.VisitorPurpose =VisitorPurpos.ID   WHERE VisitorTable.RWAID ='" . $RWAID . "' AND VisitorTable.IsActive='1' AND VisitorTable.VisitorInOut in (0,1) AND  (VisitorTable.VisitorName like '%" . $Search . "%' OR  VisitorTable.VisitorPhoneNumber like '" . $Search . "%') AND VisitorDetails.VisitorEntryTime BETWEEN '" . $timestamp . "' AND '" . $endtimestamp . "'  ORDER BY VisitorDetails.VisitorEntryTime DESC ");
        } else {
            $sql = $this->prdbh->prepare("SELECT VisitorTable.VisitorID, VisitorTable.VisitorName, VisitorTable.VisitorImageUrl, TRIM(LEADING '+91' FROM VisitorTable.VisitorPhoneNumber) as VisitorPhoneNumber, VisitorTable.Saved, VisitorTable.RWAID, VisitorTable.OTP, VisitorTable.ResidentRWAID, VisitorTable.VisitorInOut, VisitorTable.IsActive,   VisitorTable.VerificationIDNumber,VisitorTable.VerificationIDType,  VisitorTable.VisitorQrCode,  VisitorDetails.VisitorAddress, VisitorDetails.VisitorPicPath,  VisitorDetails.VisitorETA, VisitorDetails.VisitorEntryTime, VisitorDetails.VisitorExitTime, VisitorDetails.VisitorVehicleNumber, VisitorDetails.VisitorPurpose, VisitorDetails.NumberOfPeople, VisitorDetails.VisitorCompany,VisitorPurpos.DetailList 
        FROM VisitorTable LEFT JOIN  VisitorDetails ON VisitorTable.VisitorID= VisitorDetails.VisitorID LEFT JOIN VisitorPurpos ON VisitorDetails.VisitorPurpose =VisitorPurpos.ID   WHERE VisitorTable.RWAID ='" . $RWAID . "'  AND VisitorTable.IsActive='1' AND VisitorTable.VisitorInOut in (0,1) AND VisitorDetails.VisitorEntryTime BETWEEN '" . $timestamp . "' AND '" . $endtimestamp . "'  ORDER BY VisitorDetails.VisitorEntryTime DESC Limit $Start,10   ");
        }
        //print_r($sql);die;
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                $i = 0;
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $VisitorID = $row['VisitorID'];
                    $items[] = $row;
                    $sql2 = '';
                    // $sql2 = $this->prdbh->prepare("SELECT SocietyFlatMapping.FlatNbr,VisitorFlatMapping.FlatID FROM VisitorFlatMapping inner JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= VisitorFlatMapping.FlatID WHERE VisitorFlatMapping.VisitorID='" . $VisitorID . "' ");

                    $sql2 = $this->prdbh->prepare("SELECT 
                    distinct SocietyFlatMapping.FlatNbr, VisitorFlatMapping.FlatID
                FROM
                    VisitorFlatMapping
                        INNER JOIN
                    SocietyFlatMapping ON SocietyFlatMapping.ID = VisitorFlatMapping.FlatID
                        INNER JOIN
                    visitorApprovalStatus ON visitorApprovalStatus.flatId = VisitorFlatMapping.FlatID
                WHERE
                    VisitorFlatMapping.VisitorID = '" . $VisitorID . "'
                         ");

                    $sql2->execute(); 
                  
                    $count = $sql2->rowCount();
                    if ($count >=1) {
                        //get qrcode issuance status
                        $sql3 = $this->prdbh->prepare("SELECT VisitorQrCode FROM VisitorTable WHERE VisitorID = '" . $VisitorID . "' AND VisitorQrCode LIKE 'VE:%'");
                        $sql3->execute();
                        
                        if ($sql3->rowCount() >= 1) {
                            $qrCodeAssigned = 1;
                        } else {
                            $qrCodeAssigned = 0;
                        }
                        //end
                        //$FlatNbr=array();
                        $FlatNbrString = "";
                        $FlatIdString = "";
                        // $FlatID[] =null;
                        // $FlatNbr[]=null;
                        $FlatID=null;
                        $FlatNbr=null;
                        $count2 = $sql2->rowCount();
                        if ($count2 >= 1) {
                            while ($row2 = $sql2->fetch(PDO::FETCH_ASSOC)) {
                                $FlatNbr[] = $row2['FlatNbr'];
                                $FlatID[] = $row2['FlatID'];
                            }
                            $FlatNbrString = implode(',', $FlatNbr);
                            $FlatIdString = implode(',', $FlatID);
                        } else {
                            $FlatNbrString = "";
                           
                        }
                    }
                   
                    if ($FlatNbrString && $FlatIdString !==""){
                    $items[$i]['FlatNbr'] = $FlatNbrString;
                    $items[$i]['FlatID'] = $FlatIdString;
                    
                    $items[$i]['qrCodeAssigned'] = $qrCodeAssigned;
                    $sql4 = $this->prdbh->prepare("SELECT 
                    a.flatId, a.approvalStatus, b.FlatNbr, a.visitorApprvalTime
                FROM
                    visitorApprovalStatus AS a
                        INNER JOIN
                    SocietyFlatMapping AS b ON a.flatId = b.ID
                        INNER JOIN
                    VisitorFlatMapping ON VisitorFlatMapping.FlatID = b.ID
                WHERE
                    VisitorFlatMapping.VisitorID = '" . $VisitorID . "'
                        AND a.flatId IN ($FlatIdString)
                        AND a.status != 0
                        AND VisitorFlatMapping.FlatID != 0
                ORDER BY a.visitorApprvalTime DESC
                LIMIT 1");
                    
                    $sql4->execute();
                    
                   
                    if ($sql4->rowCount() >= 1) {
                        while ($row4 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                            $items1[] = $row4;
                        }
                        $items[$i]['approvalStatus'] = $items1;
                    }
                }
                    $i++;
                    
                }
                $response['vistordetail'] = $items;
                $response['Status'] = 1;
            } else {
                $response['vistordetail'] = '';
                $response['message'] = "";
                $response['Status'] = 0;
            }
        } else {
            $response['vistordetail'] = '';
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function vistorlist($param) {
        $Search = '';
        if (isset($param->Search)) {
            $Search = $param->Search;
        }
        if (isset($param->ResidentRWAID)) {
            $ResidentRWAID = $param->ResidentRWAID;
        }
        if (isset($param->Flatorignalid)) {
            $Flatorignalid = $param->Flatorignalid;
        }
        $PageNo = '';
        if (isset($param->PageNo)) {
            $PageNo = $param->PageNo;
        }
        $Start = $PageNo * 10 - 10;
        $response = array();
        if ($Search != '') {
            $sql = $this->prdbh->prepare("SELECT * FROM (SELECT VisitorTable.VisitorID, VisitorTable.VisitorName, VisitorTable.VisitorImageUrl, TRIM(LEADING '+91' FROM VisitorPhoneNumber) as VisitorPhoneNumber, VisitorTable.Saved, VisitorTable.RWAID, VisitorFlatMapping.FlatID, VisitorTable.OTP, VisitorTable.ResidentRWAID, VisitorTable.VisitorInOut, VisitorTable.IsActive, VisitorTable.QrPath, VisitorTable.VerificationIDType, VisitorTable.VerificationIDNumber, VisitorTable.VisitorQrCode,VisitorDetails.ID, VisitorDetails.VisitorType, VisitorDetails.VisitorAddress, VisitorDetails.VisitorPicPath, VisitorDetails.VisitorDocPath, VisitorDetails.VisitorETA, VisitorDetails.VisitorEntryTime, VisitorDetails.VisitorExitTime, VisitorDetails.VisitorVehicleNumber, VisitorDetails.VisitorPurpose, VisitorDetails.NumberOfPeople, VisitorDetails.VisitorCompany,VisitorPurpos.DetailList FROM VisitorTable LEFT JOIN  VisitorDetails ON VisitorTable.VisitorID= VisitorDetails.VisitorID LEFT JOIN VisitorPurpos ON VisitorDetails.VisitorPurpose =VisitorPurpos.ID  INNER JOIN VisitorFlatMapping ON VisitorFlatMapping.VisitorID = VisitorTable.VisitorID  WHERE VisitorFlatMapping.FlatID IN($Flatorignalid) AND VisitorTable.IsActive='1' AND VisitorTable.VisitorName like '%" . $Search . "%' OR  VisitorTable.VisitorPhoneNumber like '" . $Search . "%' ) AS Z WHERE VisitorInOut NOT IN (0) ORDER BY VisitorEntryTime DESC ");
        } else {
            $sql = $this->prdbh->prepare("SELECT VisitorTable.VisitorID, VisitorTable.VisitorName, VisitorTable.VisitorImageUrl, TRIM(LEADING '+91' FROM VisitorPhoneNumber) as VisitorPhoneNumber, VisitorTable.Saved, VisitorTable.RWAID, VisitorFlatMapping.FlatID, VisitorTable.OTP, VisitorTable.ResidentRWAID, VisitorTable.VisitorInOut, VisitorTable.IsActive, VisitorTable.QrPath, VisitorTable.VerificationIDType, VisitorTable.VerificationIDNumber, VisitorTable.VisitorQrCode,VisitorDetails.ID, VisitorDetails.VisitorType, VisitorDetails.VisitorAddress, VisitorDetails.VisitorPicPath, VisitorDetails.VisitorDocPath, VisitorDetails.VisitorETA, VisitorDetails.VisitorEntryTime, VisitorDetails.VisitorExitTime, VisitorDetails.VisitorVehicleNumber, VisitorDetails.VisitorPurpose, VisitorDetails.NumberOfPeople, VisitorDetails.VisitorCompany,VisitorPurpos.DetailList FROM VisitorTable LEFT JOIN  VisitorDetails ON VisitorTable.VisitorID= VisitorDetails.VisitorID LEFT JOIN VisitorPurpos ON VisitorDetails.VisitorPurpose =VisitorPurpos.ID  INNER JOIN VisitorFlatMapping ON VisitorFlatMapping.VisitorID = VisitorTable.VisitorID  WHERE VisitorFlatMapping.FlatID IN($Flatorignalid) AND VisitorTable.IsActive='1' AND VisitorTable.VisitorInOut not in (0)  ORDER BY VisitorDetails.VisitorEntryTime DESC Limit $Start,10   ");
        }
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                $i = '0';
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                    $visitorid = $row['VisitorID'];
                    $sql1 = $this->prdbh->prepare("SELECT * FROM VisitorFlatAuditTrail WHERE VisitorID='" . $visitorid . "' AND FlatID IN ($Flatorignalid) ");
                    if ($sql1->execute()) {
                        $count1 = $sql1->rowCount();
                        if ($count1 > 0) {
                            $checkout = '1';
                        } else {
                            $checkout = '0';
                        }
                    } else {
                        $checkout = '0';
                    }
                    $items[$i]['checkout'] = $checkout;
                    $i++;
                }
                $response['vistordetail'] = $items;
                $response['Status'] = 1;
            } else {
                $response['vistordetail'] = '';
                $response['message'] = "";
                $response['Status'] = 0;
            }
        } else {
            $response['vistordetail'] = '';
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function gateuserregistration($rec) {
        $tdat = date("U");
        $tdate = $tdat;
        $sql = $this->prdbh->prepare("INSERT INTO GateUserLogin(LoginID, Password, RWAID, IsActive, DateCreated)VALUES (Anqwer123@,weqrtwuiasm123@,7,1,?)");
        $sql->bindParam(1, $rec->LoginID);
        $sql->bindParam(2, $rec->Password);
        $sql->bindParam(3, $rec->RWAID);
        $sql->bindParam(4, $rec->IsActive);
        $sql->bindParam(5, $tdate);
        $this->prdbh->beginTransaction();
        if ($sql->execute()) {
            $id = $this->prdbh->lastInsertId();
            $this->prdbh->commit();
            $response['Message'] = "user created successfully";
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function gateuserlogin($param) {
        if (isset($param->LoginID)) {
            $LoginID = $param->LoginID;
        }
        if (isset($param->Password)) {
            $Password = $param->Password;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT ID, LoginID, RWAID, IsActive, DateCreated,IsTower FROM GateUserLogin WHERE LoginID ='" . $LoginID . "' AND Password='" . $Password . "' AND IsActive=1  ");
        //print_r($sql);die;
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $ID = $row['ID'];
                    $sql1 = $this->prdbh->prepare("SELECT ID, GateUserID, Function, FunctionID, IsActive FROM Gateuserfunction WHERE GateUserID='" . $ID . "' AND IsActive ='1'  ");
                    if ($sql1->execute()) {
                        $item2 = array();
                        $count1 = $sql1->rowCount();
                        if ($count1 >= 1) {
                            while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                                $item2[] = $row1;
                            }
                        }
                    }
                    $items[] = $row;
                }
                $response['gateuserDetail'] = $items;
                $response['gateuserFunction'] = $item2;
                $response['Status'] = 1;
            } else {
                $response['gateuserDetail'] = '';
                $response['message'] = "Please enter valid username or password";
                $response['Status'] = 0;
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function likerusers($param) {
        if (isset($param->ResidentRWAID)) {
            $ResidentRWAID = $param->ResidentRWAID;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT (User.ID) as  UserID ,User.FirstName, User.ProfilePic,SocietyFlatMapping.FlatNbr FROM User LEFT JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentID = User.ID LEFT JOIN ResidentFlatMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID LEFT JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID WHERE ResidentRWAMapping.ResidentRWAID IN ($ResidentRWAID) Group by UserID ");
        // print_r($sql);die;
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['LikerUsers'] = $items;
                $response['Status'] = 1;
            } else {
                $response['LikerUsers'] = '';
                $response['message'] = "No Liker Users";
                $response['Status'] = 0;
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function selectgate($param) {
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT ID, GateName, RWAID, IsActive,IsTower,TowerPrefix FROM GatenoEntry WHERE RWAID='" . $RWAID . "' AND IsActive=1 ");
        //print_r($sql);die;
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['GateNbr'] = $items;
                $response['Status'] = 1;
            } else {
                $response['GateNbr'] = '';
                $response['message'] = "No Gate Number";
                $response['Status'] = 0;
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function visitordetailtawerlevel($param) {
        if (isset($param->VisitorID)) {
            $VisitorID = $param->VisitorID;
        }
        $response = array();
        $sql3 = $this->prdbh->prepare("SELECT VisitorTable.VisitorID, VisitorTable.VisitorName, VisitorTable.VisitorImageUrl, TRIM(LEADING '+' FROM VisitorPhoneNumber) as VisitorPhoneNumber, VisitorTable.Saved, VisitorTable.RWAID, VisitorFlatMapping.FlatID, VisitorTable.OTP, VisitorTable.ResidentRWAID, VisitorTable.VisitorInOut, VisitorTable.IsActive, VisitorTable.QrPath, VisitorTable.VerificationIDType, VisitorTable.VerificationIDNumber, VisitorTable.VisitorQrCode,VisitorDetails.ID, VisitorDetails.VisitorType, VisitorDetails.VisitorAddress, VisitorDetails.VisitorPicPath, VisitorDetails.VisitorDocPath, VisitorDetails.VisitorETA, VisitorDetails.VisitorEntryTime, VisitorDetails.VisitorExitTime, VisitorDetails.VisitorVehicleNumber, VisitorDetails.VisitorPurpose, VisitorDetails.NumberOfPeople, VisitorDetails.VisitorCompany,VisitorPurpos.DetailList FROM VisitorTable LEFT JOIN  VisitorDetails ON VisitorTable.VisitorID= VisitorDetails.VisitorID LEFT JOIN VisitorPurpos ON VisitorDetails.VisitorPurpose =VisitorPurpos.ID  INNER JOIN VisitorFlatMapping ON VisitorFlatMapping.VisitorID = VisitorTable.VisitorID WHERE VisitorTable.VisitorID= '" . $VisitorID . "' ");
        if ($sql3->execute()) {
            $count3 = $sql3->rowCount();
            if ($count3 >= 1) {
                while ($row3 = $sql3->fetch(PDO::FETCH_ASSOC)) {
                    $items3 = $row3;
                }
                $response['Visitordata'] = $items3;
                $response['MainStatus'] = 1;
                $sql = $this->prdbh->prepare("SELECT  VisitorTowerAuditTrail.TowerID, VisitorTowerAuditTrail.Timestamp, VisitorTowerAuditTrail.Status,GatenoEntry.GateName FROM VisitorTowerAuditTrail INNER JOIN GatenoEntry ON GatenoEntry.ID=VisitorTowerAuditTrail.TowerID WHERE VisitorTowerAuditTrail.VisitorID ='" . $VisitorID . "' ");
                if ($sql->execute()) {
                    $items = array();
                    $count = $sql->rowCount();
                    if ($count >= 1) {
                        while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                            $items[] = $row;
                        }
                        $response['Visitordetail'] = $items;
                        $response['Status'] = 1;
                    } else {
                        $response['Visitordetail'] = [];
                        $response['message'] = "No data";
                        $response['Status'] = 0;
                    }
                    $sql2 = $this->prdbh->prepare("SELECT SocietyFlatMapping.FlatNbr,VisitorFlatAuditTrail.ExitTime FROM SocietyFlatMapping 
                        INNER JOIN  VisitorFlatAuditTrail ON SocietyFlatMapping.ID=VisitorFlatAuditTrail.FlatID  WHERE VisitorFlatAuditTrail.VisitorID ='" . $VisitorID . "' ");
                    if ($sql2->execute()) {
                        $items1 = array();
                        $count1 = $sql2->rowCount();
                        if ($count1 >= 1) {
                            while ($row2 = $sql2->fetch(PDO::FETCH_ASSOC)) {
                                $items1[] = $row2;
                            }
                            $response['VisitordetailFlat'] = $items1;
                            $response['StatusF'] = 1;
                        } else {
                            $response['VisitordetailFlat'] = [];
                            $response['StatusF'] = 0;
                        }
                    } else {
                        $response['StatusF'] = 0;
                    }
                } else {
                    $response['message'] = $sql->errorInfo();
                    $response['MainStatus'] = 0;
                }
            } else {
                $response['Status'] = 0;
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function societyinfo($param) {
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        if (isset($param->Category)) {
            $Category = $param->Category;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT ID, Category,Link, RWAID FROM SocietyInfo WHERE RWAID= '" . $RWAID . "' AND  Category= '" . $Category . "' ");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items = $row;
                }
                $response['societyinfo'] = $items;
                $response['Status'] = '1';
            } else {
                $response['societyinfo'] = [];
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function classified($param) {
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        $tdat = date("U");
        $c = $tdat;
        $d = $c - 518400; //1482699957
        $response = array();
        // AND Classifieds.AddedOn BETWEEN '".$d."' AND '".$c."'
        $sql = $this->prdbh->prepare("SELECT Classifieds.Id,Classifieds.IsActive, Classifieds.Title, Classifieds.Description, Classifieds.Image1, Classifieds.Image2, Classifieds.Image3, Classifieds.AddedBy,SocietyFlatMapping.FlatNbr,User.FirstName,(User.ID) as UserID,Classifieds.AddedOn, Classifieds.ExpiryDt FROM Classifieds 
LEFT join ResidentFlatMapping on ResidentFlatMapping.ID = Classifieds.AddedBy 
LEFT JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID 
LEFT join User on User.ID=ResidentRWAMapping.ResidentID INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID   WHERE  ResidentRWAMapping.RWAID='" . $RWAID . "' And Classifieds.IsActive='1' GROUP BY Classifieds.ID DESC ");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['Show classified'] = $items;
                $response['Status'] = '1';
            } else {
                $response['Show classified'] = [];
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function classifieddetail($param) {
        if (isset($param->id)) {
            $id = $param->id;
        }
        $tdat = date("U");
        $c = $tdat;
        $d = $c - 518400; //1482699957
        $response = array();
        $sql = $this->prdbh->prepare("SELECT Classifieds.Id,Classifieds.IsActive, Classifieds.Title, Classifieds.Description, Classifieds.Image1, Classifieds.Image2, Classifieds.Image3, Classifieds.AddedBy,SocietyFlatMapping.FlatNbr,User.FirstName,(User.ID) as UserID,Classifieds.AddedOn, Classifieds.ExpiryDt FROM Classifieds 
            LEFT join ResidentFlatMapping on ResidentFlatMapping.ID = Classifieds.AddedBy 
            LEFT JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID 
            LEFT join User on User.ID=ResidentRWAMapping.ResidentID INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID  WHERE  Classifieds.Id ='" . $id . "' And Classifieds.IsActive='1'  ");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items = $row;
                }
                $response['classified detail'] = $items;
                $response['Status'] = '1';
            } else {
                $response['classified detail'] = '';
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function flatnumber($param) {
        $flatnbrvisitor = '';
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        if (isset($param->flatnbrvisitor)) {
            $flatnbrvisitor = $param->flatnbrvisitor;
        }
        $response = array();
        if ($flatnbrvisitor != '') {
            $sql = $this->prdbh->prepare("SELECT ID,FlatNbr From SocietyFlatMapping WHERE RWAID ='" . $RWAID . "' And IsActive='1'");
        } else {
            $sql = $this->prdbh->prepare("SELECT ID,FlatNbr From SocietyFlatMapping WHERE RWAID ='" . $RWAID . "' And IsActive='1' AND categories='F' ");
        }
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['FlatNbr'] = $items;
                $response['Status'] = '1';
            } else {
                $response['FlatNbr'] = '';
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function usercallrecord($rec) {
        $tdat = date("U");
        $tdate = $tdat;
        $Name = $rec->Name;
        $namearray = explode(",", $Name);
        $PhoneNbr = $rec->PhoneNbr;
        $phonerray = explode(",", $PhoneNbr);
        $Type = $rec->Type;
        $typearray = explode(",", $Type);
        $mail = $rec->mail;
        $mailarray = explode(",", mail);
        if ($phonerray != '') {
            for ($i = 0;$i < count($phonerray);$i++) {
                $sql = $this->prdbh->prepare("INSERT INTO UserCallRecord(Name,PhoneNbr,Addedon)
                                                            VALUES(?,?,?)");
                $sql->bindParam(1, $namearray[$i]);
                $sql->bindParam(2, $phonerray[$i]);
                $sql->bindParam(3, $tdate);
                $this->prdbh->beginTransaction();
                $sql->execute();
                $count = $sql->rowCount();
                //print_r($count);die;
                if ($count > 0) {
                    $id = $this->prdbh->lastInsertId();
                    $this->prdbh->commit();
                }
            }
            $response['Message'] = "Successful";
            $response['Status'] = "1";
        } else {
            $response['Message'] = "Unsuccessful";
            $response['Status'] = "0";
        }
        $responseww = json_encode($response);
        // Print_r($responseww);
        // die;
        return $response;
    }
    public function flatlistforvisitor($param) {
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        $response = array();
        $sql1 = $this->prdbh->prepare("SELECT Name,SearchBy From Hot_Key WHERE RWAID ='" . $RWAID . "' GROUP BY Name");
        $sql1->execute();
        if ($count = $sql1->rowCount() >= 1) {
            while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                $items1[] = $row1;
            }
        }
        $sql = $this->prdbh->prepare("SELECT ID, FlatNbr, RWAID, categories From SocietyFlatMapping WHERE RWAID ='" . $RWAID . "' And IsActive='1' GROUP BY FlatNbr  ");
        $sql->execute();
        //Print_r($sql);die;
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['HotKey'] = $items1;
                $response['FlatNbr'] = $items;
                $response['Status'] = '1';
            } else {
                $response['FlatNbr'] = '';
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function photodetail($param) {
        if (isset($param->id)) {
            $id = $param->id;
        }
        if (isset($param->ResidentID)) {
            $ResidentID = $param->ResidentID;
        }
        $tdat = date("U");
        $c = $tdat;
        $d = $c - 518400; //1482699957
        //$response = array();
        $sql1 = $this->prdbh->prepare("select PhotoGallery.PhotoID,PhotoGallery.PhotoPath,PhotoGallery.PhotoCaption,
                        (ResidentRWAMapping.ResidentID) AS AddedBy,User.FirstName,ResidentRWAMapping.ResidentRWAID  FROM PhotoGallery
                        inner join ResidentRWAMapping on PhotoGallery.AddedBy = ResidentRWAMapping.ResidentRWAID 
                        INNER JOIN ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID = ResidentRWAMapping.ResidentRWAID  
                        LEFT JOIN User ON User.ID = ResidentRWAMapping.ResidentID 
                        where PhotoGallery.IsActive='1' AND PhotoGallery.PhotoID = '" . $id . "' ");
        $sql1->execute();
        $count1 = $sql1->rowCount();
        if ($count1 > 0) {
            $subitems = array();
            while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                $gal['PhotoID'] = $row1['PhotoID'];
                $gal['PhotoPath'] = $row1['PhotoPath'];
                $gal['PhotoCaption'] = $row1['PhotoCaption'];
                $gal['AddedBy'] = $row1['AddedBy'];
                $gal['FirstName'] = $row1['FirstName'];
                $gal['ResidentRWAID'] = $row1['ResidentRWAID'];
                $RerwaID = $row1['ResidentRWAID'];
                $PhotoID = $row1['PhotoID'];
                $sql3 = $this->prdbh->prepare("SELECT COUNT(PhotoID) AS LIKES FROM PhotoGalleryLike WHERE PhotoID = '" . $PhotoID . "' AND Likes =  '1'  ");
                $sql3->execute();
                if ($sql3->rowCount() > 0) {
                    while ($row3 = $sql3->fetch(PDO::FETCH_ASSOC)) {
                        $gal['LIKES'] = $row3['LIKES'];
                    }
                } else {
                    $gal['LIKES'] = '0';
                }
                $sql4 = $this->prdbh->prepare("SELECT PhotoID FROM PhotoGalleryLike WHERE Likes =  '1' AND ResidentID ='" . $ResidentID . "' AND PhotoID =  '" . $PhotoID . "' ");
                $sql4->execute();
                //print_r($sql4);die;
                if ($sql4->rowCount() > 0) {
                    $gal['IsLIKES'] = '1';
                } else {
                    $gal['IsLIKES'] = '0';
                }
                $sql2 = $this->prdbh->prepare("SELECT ResidentFlatMapping.ID, SocietyFlatMapping.FlatNbr FROM ResidentFlatMapping INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID  WHERE ResidentRWAID ='" . $RerwaID . "'");
                $sql2->execute();
                $FlatNbr = array();
                if ($sql2->rowCount() > 0) {
                    while ($row2 = $sql2->fetch(PDO::FETCH_ASSOC)) {
                        $FlatNbr[] = $row2['FlatNbr'];
                    }
                    $FlatNbrString = implode(',', $FlatNbr);
                } else {
                    $FlatNbrString = "";
                }
                $a['FlatNbr'] = $FlatNbrString;
                $row1 = array_merge($gal, $a);
                $subitems = $row1;
                $gal = array();
            }
            $response['PhotoDetail'] = $subitems;
            $response['Status'] = '1';
        } else {
            $response['PhotoDetail'] = '';
            $response['Status'] = '0';
        }
        return $response;
    }
    public function userdetailbyresidentrwaid($param) {
        if (isset($param->ResidentRWAID)) {
            $ID = $param->ResidentRWAID;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT User.ID, User.PhoneNbr, User.EmailID, User.FirstName, IFNULL(User.MiddleName,'') as MiddleName, User.LastName,
                                                User.ProfilePic, User.Gender, User.Occupation, User.AddedOn, User.IsActive,User.About,User.IsPhonePublic,
                                                ResidentRWAMapping.ResidentRWAID, ResidentRWAMapping.RWAID,RWA.Name,RWA.Latitude,RWA.Longitude,ResidentRWAMapping.AddedOn,
                                                ResidentRWAMapping.ResidentRWAID, ResidentRWAMapping.RWAID,RWA.Name,RWA.Latitude,RWA.Longitude,RWA.SocietyPrivateinfo,RWA.SocietyPublicInfo,RWA.SocietyBanner,RWA.SocietyPendingInfo,RWA.ChatUrl,ResidentRWAMapping.AddedOn,
                                                ResidentRWAMapping.ApprovalStatus, ResidentRWAMapping.ApprovedBy, ResidentRWAMapping.ApprovedOn,Roles.RoleID,(Roles.Name) as Roles
                                                FROM User INNER JOIN ResidentRWAMapping ON User.ID = ResidentRWAMapping.ResidentID
                                                LEFT JOIN UserRoles ON UserRoles.ResidentRWAId = ResidentRWAMapping.ResidentRWAID
                                                LEFT JOIN Roles ON Roles.RoleID = UserRoles.RoleID
                                                LEFT JOIN RWA ON ResidentRWAMapping.RWAID=RWA.ID
                                                WHERE ResidentRWAMapping.ResidentRWAID = '" . $ID . "'");
        $sql->execute();
        if ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
            $rid = $row['RoleID'];
            $rwaidpop = $row['RWAID'];
            if ($rid != '') {
                $sql4 = $this->prdbh->prepare(" select r.FunctionID,func.FunctionName from RoleFunctions as r inner join Functions as func on func.FunctionID=r.FunctionID where r.RoleID='" . $rid . "'");
            } else {
                $sql4 = $this->prdbh->prepare(" select r.FunctionID,func.FunctionName from RoleFunctions as r inner join Functions as func on func.FunctionID=r.FunctionID where r.RoleID='3' ");
            }
            if ($sql4->execute()) {
                $e = array();
                while ($row4 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                    $e[] = $row4;
                }
                $a['Function'] = $e;
            }
            $apid = $row['ApprovedBy'];
            $sql3 = $this->prdbh->prepare("SELECT  (User.FirstName) as ApName, (User.About)  as ApAbout,(User.ProfilePic) as ApProfilePic,(ResidentFlatMapping.FlatNbr) as ApFlatNbr FROM User LEFT JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentID = User.ID LEFT JOIN ResidentFlatMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID WHERE User.ID= '" . $apid . "'");
            $sql3->execute();
            if ($row3 = $sql3->fetch(PDO::FETCH_ASSOC)) {
                $items3 = $row3;
            }
            $a['approveby'] = $items3;
            $rid = $row['ResidentRWAID'];
            $sql1 = $this->prdbh->prepare("SELECT (ResidentFlatMapping.ID) as FlatID,(SocietyFlatMapping.ID) as Flatorignalid,SocietyFlatMapping.FlatNbr,IFNULL(ResidentFlatMapping.IsOwner,'') as IsOwner FROM ResidentFlatMapping INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID  WHERE ResidentRWAID ='" . $rid . "'");
            $sql1->execute();
            $items = array();
            $fid1 = array();
            if ($sql1->rowCount() > 0) {
                while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row1;
                    $fid1[] = $row1['FlatID'];
                }
                $a['Flats'] = $items;
                if (count($fid1)) {
                    $fid = implode(',', $fid1);
                    $sql2 = $this->prdbh->prepare("SELECT ID, VehicleNbr,IFNULL(VehicleType,'') as VehicleType FROM ResidentVehicleMapping WHERE ResidentRWAFlatID IN($fid) AND IsActive='1' ");
                    $sql2->execute();
                    $items1 = array();
                    if ($sql2->rowCount() > 0) {
                        while ($row2 = $sql2->fetch(PDO::FETCH_ASSOC)) {
                            $items1[] = $row2;
                        }
                        $a['Vehicles'] = $items1;
                    }
                }
            }
            $row = array_merge($row, $a);
            $response['User'] = $row;
            $response['Status'] = 1;
        }
        return $response;
    }
    public function notificationcount($param) {
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        if (isset($param->lstcount)) {
            $lstcount = $param->lstcount;
        }
        $tdate = date("Y-m-d h:i:sa");
        $a = strtotime($tdate);
        $c = '1296000';
        $b = $a - $c;
        $sql = $this->prdbh->prepare("SELECT Title, Text ,Image1,Image2 FROM Notifications WHERE RWAID = '" . $RWAID . "'  ");
        $sql->execute();
        $items = array();
        $count = $sql->rowcount();
        $a = $count - $lstcount;
        $response['Count'] = $a;
        $responce['Status'] = '1';
        return $response;
    }
    public function userdetails($param) {
        $RWAID = null;
        $sql = null;
        if (isset($param->ID)) {
            $ID = $param->ID;
        }
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        $response = array();
        if ($RWAID != '') {
            $sql = $this->prdbh->prepare("SELECT  RWAID, RolesID as RoleID  FROM RWARoleMaster WHERE RWAID='" . $RWAID . "' ");
        } else {
            $sql = $this->prdbh->prepare("SELECT User.ID, User.PhoneNbr, User.EmailID, User.FirstName, IFNULL(User.MiddleName,'') as MiddleName, User.LastName,
                                                User.ProfilePic, User.Gender, User.Occupation, User.AddedOn, User.IsActive,User.About,User.IsPhonePublic,
                                                ResidentRWAMapping.ResidentRWAID as ResidentRWAID, ResidentRWAMapping.RWAID,RWA.Name,RWA.Latitude,RWA.Longitude,ResidentRWAMapping.AddedOn,
                                                ResidentRWAMapping.ResidentRWAID, ResidentRWAMapping.RWAID,RWA.Name,RWA.Latitude,RWA.Longitude,RWA.SocietyPrivateinfo,RWA.SocietyPublicInfo,RWA.SocietyBanner,RWA.SocietyPendingInfo,RWA.ChatUrl,ResidentRWAMapping.AddedOn,
                                                ResidentRWAMapping.ApprovalStatus, ResidentRWAMapping.ApprovedBy, ResidentRWAMapping.ApprovedOn,Roles.RoleID,(Roles.Name) as Roles ,RejectionReson.RejectionReson
                                                FROM User INNER JOIN ResidentRWAMapping ON User.ID = ResidentRWAMapping.ResidentID
                                                LEFT JOIN UserRoles ON UserRoles.ResidentRWAId = ResidentRWAMapping.ResidentRWAId
                                                LEFT JOIN Roles ON Roles.RoleID = UserRoles.RoleID
                                                LEFT JOIN RWA ON ResidentRWAMapping.RWAID=RWA.ID
                                                LEFT JOIN RejectionReson ON RejectionReson.ResidentRWAID = ResidentRWAMapping.ResidentRWAID
                                                WHERE User.ID = '" . $ID . "' ORDER BY RejectionReson.ID DESC");
        }
        if ($sql->execute()) {
            //$roleid1=$row['RoleID'];
            $rolid = array();
            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                $itemsuser = $row;
                $rolid[] = $row['RoleID'];
                $rwaidpop = $row['RWAID'];
                $apid = $row['ApprovedBy'];
                $rid = $row['ResidentRWAID'];
            }
            $roleid1 = implode(',', $rolid);
            if ($roleid1 != '') {
                $sql4 = $this->prdbh->prepare(" select r.FunctionID,func.FunctionName from RoleFunctions as r inner join Functions as func on func.FunctionID=r.FunctionID where r.RoleID IN (" . $roleid1 . ")");
            } else {
                $sql4 = $this->prdbh->prepare(" select r.FunctionID,func.FunctionName from RoleFunctions as r inner join Functions as func on func.FunctionID=r.FunctionID where r.RoleID='3' ");
            }
            //print_r($sql4);die;
            if ($sql4->execute()) {
                $e = array();
                while ($row4 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                    $e[] = $row4;
                }
                $a['Function'] = $e;
            }
            $items3 = '';
            $sql3 = $this->prdbh->prepare("SELECT  (User.FirstName) as ApName, (User.About)  as ApAbout,(User.ProfilePic) as ApProfilePic,(ResidentFlatMapping.FlatNbr) as ApFlatNbr FROM User LEFT JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentID = User.ID LEFT JOIN ResidentFlatMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID WHERE User.ID= '" . $apid . "'");
            $sql3->execute();
            if ($row3 = $sql3->fetch(PDO::FETCH_ASSOC)) {
                $items3 = $row3;
            }
            $a['approveby'] = $items3;
            $sql1 = $this->prdbh->prepare("SELECT (ResidentFlatMapping.ID) as FlatID,(SocietyFlatMapping.ID) as Flatorignalid,SocietyFlatMapping.FlatNbr,IFNULL(ResidentFlatMapping.IsOwner,'') as IsOwner FROM ResidentFlatMapping INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID  WHERE ResidentRWAID ='" . $rid . "'");
            $sql1->execute();
            $items = array();
            $fid1 = array();
            if ($sql1->rowCount() > 0) {
                while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row1;
                    $fid1[] = $row1['Flatorignalid'];
                }
                $a['Flats'] = $items;
                if (count($fid1)) {
                    $fid = implode(',', $fid1);
                    $sql2 = $this->prdbh->prepare("SELECT ID,ParkingNo FROM ParkingTable  WHERE Flat_ID IN ($fid)");
                    $sql2->execute();
                    $items1 = array();
                    $parkingid = array();
                    if ($sql2->rowCount() > 0) {
                        while ($row2 = $sql2->fetch(PDO::FETCH_ASSOC)) {
                            $items1[] = $row2;
                            $parkingid[] = $row2['ID'];
                        }
                        $a['Parking No'] = $items1;
                        if (count($parkingid)) {
                            $parkid = implode(',', $parkingid);
                            $sql3 = $this->prdbh->prepare("SELECT ID, VehicleNbr,IFNULL(VehicleType,'') as VehicleType FROM ResidentVehicleByParking WHERE IsActive=1 AND ParkingID IN ('" . $parkid . "') ");
                            //print_r($sql3);die;
                            $sql3->execute();
                            $items2 = array();
                            if ($sql3->rowCount() > 0) {
                                while ($row3 = $sql3->fetch(PDO::FETCH_ASSOC)) {
                                    $items2[] = $row3;
                                }
                                $a['Vehicles'] = $items2;
                            }
                        }
                    }
                }
            }
            if ($rid != '') {
                $items6 = '';
                $Timestamp = date("U");
                $sql10 = $this->prdbh->prepare("INSERT INTO LoginUserAudittrail(RWAID, Timestamp, ResidentRWAID)
                        VALUES ($rwaidpop,$Timestamp,$rid)");
                $this->prdbh->beginTransaction();
                if ($sql10->execute()) {
                    $id = $this->prdbh->lastInsertId();
                    $this->prdbh->commit();
                }
                $sql6 = $this->prdbh->prepare("SELECT ID, RWAID, Text, ImagePath,ButtonName,DisplayCount,Category,web_link 
                From HomePagePopup WHERE RWAID= '" . $rwaidpop . "' AND IsActive = '1' ORDER BY RAND() LIMIT 1 "); //ORDER BY RAND(ID) LIMIT 1
                $sql6->execute();
                $count6 = $sql6->rowCount();
                if ($count6 >= 1) {
                    if ($row6 = $sql6->fetch(PDO::FETCH_ASSOC)) {
                        $items6 = $row6;
                        $Popid = $row6['ID'];
                        $DisplayCount = $row6['DisplayCount'];
                        $sql8 = $this->prdbh->prepare("SELECT COUNT(*) as Count FROM UserPopupLog 
                       WHERE UserID = '" . $ID . "' AND PopID= '" . $Popid . "'");
                        $sql8->execute();
                        if ($row8 = $sql8->fetch(PDO::FETCH_ASSOC)) {
                            $items8 = $row8[Count];
                        }
                        $b['Popupcount'] = $items8;
                        if ($items8 <= $DisplayCount) {
                            $sql7 = $this->prdbh->prepare("INSERT INTO UserPopupLog(UserID, PopID)
                        VALUES ($ID,$Popid)");
                            $this->prdbh->beginTransaction();
                            if ($sql7->execute()) {
                                $id = $this->prdbh->lastInsertId();
                                $this->prdbh->commit();
                            }
                        }
                    }
                } else {
                    $sql9 = $this->prdbh->prepare("SELECT ID, RWAID, Text, ImagePath,ButtonName,DisplayCount,Category,web_link 
                    From HomePagePopup WHERE RWAID is NULL AND IsActive = '1' ORDER BY RAND() LIMIT 1  ");
                    $sql9->execute();
                    if ($row9 = $sql9->fetch(PDO::FETCH_ASSOC)) {
                        $items6 = $row9;
                        $Popid = $row9['ID'];
                        $DisplayCount = $row9['DisplayCount'];
                        $sql8 = $this->prdbh->prepare("SELECT COUNT(*) as Count FROM UserPopupLog 
                       WHERE UserID = '" . $ID . "' AND PopID= '" . $Popid . "'");
                        $sql8->execute();
                        if ($row8 = $sql8->fetch(PDO::FETCH_ASSOC)) {
                            $items8 = $row8[Count];
                        }
                        $b['Popupcount'] = $items8;
                        if ($items8 <= $DisplayCount) {
                            $sql7 = $this->prdbh->prepare("INSERT INTO UserPopupLog(UserID, PopID)
                        VALUES ($ID,$Popid)");
                            $this->prdbh->beginTransaction();
                            if ($sql7->execute()) {
                                $id = $this->prdbh->lastInsertId();
                                $this->prdbh->commit();
                            }
                        }
                    }
                }
                $b['Popup'] = $items6;
                $itemsuser = array_merge($itemsuser, $a, $b);
            } else {
                $itemsuser = array_merge($itemsuser, $a);
            }
            $response['User'] = $itemsuser;
            $response['Status'] = 1;
        }
        return $response;
    }
    public function userdetailsnew($param) {
        $RWAID = null;
        $sql = null;
        if (isset($param->ID)) {
            $ID = $param->ID;
        }
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        $tdat = date("U");
        $c = $tdat;
        $response = array();
        if ($RWAID != '') {
            $sql = $this->prdbh->prepare("SELECT  RWAID, RolesID as RoleID  FROM RWARoleMaster WHERE RWAID='" . $RWAID . "' ");
        } else {
            $sql = $this->prdbh->prepare("SELECT User.ID, User.PhoneNbr, User.EmailID, User.FirstName, IFNULL(User.MiddleName,'') as MiddleName, User.LastName,
                                                User.ProfilePic, User.Gender, User.Occupation, User.AddedOn, User.IsActive,User.About,User.IsPhonePublic,
                                                ResidentRWAMapping.ResidentRWAID as ResidentRWAID, ResidentRWAMapping.RWAID,RWA.Name,RWA.Latitude,RWA.Longitude,ResidentRWAMapping.AddedOn,
                                                ResidentRWAMapping.ResidentRWAID, ResidentRWAMapping.RWAID,RWA.Name,RWA.Latitude,RWA.Longitude,RWA.SocietyPrivateinfo,RWA.SocietyPublicInfo,RWA.SocietyBanner,RWA.SocietyPendingInfo,RWA.ChatUrl,ResidentRWAMapping.AddedOn,
                                                ResidentRWAMapping.ApprovalStatus, ResidentRWAMapping.ApprovedBy, ResidentRWAMapping.ApprovedOn,Roles.RoleID,(Roles.Name) as Roles ,RejectionReson.RejectionReson
                                                FROM User INNER JOIN ResidentRWAMapping ON User.ID = ResidentRWAMapping.ResidentID
                                                LEFT JOIN UserRoles ON UserRoles.ResidentRWAId = ResidentRWAMapping.ResidentRWAId
                                                LEFT JOIN Roles ON Roles.RoleID = UserRoles.RoleID
                                                LEFT JOIN RWA ON ResidentRWAMapping.RWAID=RWA.ID
                                                LEFT JOIN RejectionReson ON RejectionReson.ResidentRWAID = ResidentRWAMapping.ResidentRWAID
                                                WHERE User.ID = '" . $ID . "' ORDER BY RejectionReson.ID DESC");
        }
        if ($sql->execute()) {
            //$roleid1=$row['RoleID'];
            $rolid = array();
            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                $itemsuser = $row;
                $rolid[] = $row['RoleID'];
                $rwaidpop = $row['RWAID'];
                $apid = $row['ApprovedBy'];
                $rid = $row['ResidentRWAID'];
            }
            $roleid1 = implode(',', $rolid);
            if ($roleid1 != '') {
                $sql4 = $this->prdbh->prepare(" select r.FunctionID,func.FunctionName from RoleFunctions as r inner join Functions as func on func.FunctionID=r.FunctionID where r.RoleID IN (" . $roleid1 . ")");
            } else {
                $sql4 = $this->prdbh->prepare(" select r.FunctionID,func.FunctionName from RoleFunctions as r inner join Functions as func on func.FunctionID=r.FunctionID where r.RoleID='3' ");
            }
            //print_r($sql4);die;
            if ($sql4->execute()) {
                $e = array();
                while ($row4 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                    $e[] = $row4;
                }
                $a['Function'] = $e;
            }
            $items3 = '';
            $sql3 = $this->prdbh->prepare("SELECT  (User.FirstName) as ApName, (User.About)  as ApAbout,(User.ProfilePic) as ApProfilePic,(ResidentFlatMapping.FlatNbr) as ApFlatNbr FROM User LEFT JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentID = User.ID LEFT JOIN ResidentFlatMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID WHERE User.ID= '" . $apid . "'");
            $sql3->execute();
            if ($row3 = $sql3->fetch(PDO::FETCH_ASSOC)) {
                $items3 = $row3;
            }
            $a['approveby'] = $items3;
            $sql1 = $this->prdbh->prepare("SELECT (ResidentFlatMapping.ID) as FlatID,(SocietyFlatMapping.ID) as Flatorignalid,SocietyFlatMapping.FlatNbr,IFNULL(ResidentFlatMapping.IsOwner,'') as IsOwner FROM ResidentFlatMapping INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID  WHERE ResidentRWAID ='" . $rid . "'");
            $sql1->execute();
            $items = array();
            $fid1 = array();
            if ($sql1->rowCount() > 0) {
                while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row1;
                    $fid1[] = $row1['Flatorignalid'];
                    $FlatID = $row1['FlatID'];
                }
                $sql13 = $this->prdbh->prepare(" SELECT UserFacilityMapping.ID,Facility.Facility,Facility.Prefix, UserFacilityMapping.Expirydate FROM Facility inner join UserFacilityMapping on Facility.ID = UserFacilityMapping.FacilityID where  UserFacilityMapping.ResidentFlatID = '" . $FlatID . "'");
                $sql13->execute();
                $Facility = array();
                if ($sql13->rowCount() > 0) {
                    while ($row13 = $sql13->fetch(PDO::FETCH_ASSOC)) {
                        $Facility[] = $row13;
                    }
                } else {
                    $Facility = [];
                }
                $a['Flats'] = $items;
                $a['Facility'] = $Facility;
                if (count($fid1)) {
                    $fid = implode('\',\'', $fid1);
                    $sql21 = $this->prdbh->prepare("SELECT ID,BillAmount,BillingDate,DueAmount,PaymentDueDate FROM MaintenanceBillingFlat  WHERE FlatID IN ($fid) AND StatusOfPayment='2' GROUP BY ID");
                    $sql21->execute();
                    //$items11=array();
                    if ($sql21->rowCount() > 0) {
                        while ($row21 = $sql21->fetch(PDO::FETCH_ASSOC)) {
                            $items19 = $row21;
                            //$parkingid[]=$row2['ID'];
                            
                        }
                        $a['Flat Bill'] = $items19;
                    } else {
                        $a['Flat Bill'] = '';
                    }
                    $sql2 = $this->prdbh->prepare("SELECT ID,ParkingNo FROM ParkingTable  WHERE Flat_ID IN ($fid) GROUP BY ID");
                    $sql2->execute();
                    $items1 = array();
                    $parkingid = array();
                    if ($sql2->rowCount() > 0) {
                        while ($row2 = $sql2->fetch(PDO::FETCH_ASSOC)) {
                            $items1[] = $row2;
                            $parkingid[] = $row2['ID'];
                        }
                        $a['Parking No'] = $items1;
                        if (count($parkingid)) {
                            $parkid = implode('\',\'', $parkingid);
                            $sql3 = $this->prdbh->prepare("SELECT ID, VehicleNbr,IFNULL(VehicleType,'') as VehicleType FROM ResidentVehicleByParking WHERE IsActive=1 AND ParkingID IN ('" . $parkid . "') AND sticker_num !=''  GROUP BY ID ");
                            //print_r($sql3);die;
                            $sql3->execute();
                            $items2 = array();
                            if ($sql3->rowCount() > 0) {
                                while ($row3 = $sql3->fetch(PDO::FETCH_ASSOC)) {
                                    $items2[] = $row3;
                                }
                                $a['Vehicles'] = $items2;
                            }
                        }
                    }
                }
            }
            if ($rid != '') {
                $items6 = '';
                $Timestamp = date("U");
                $sql10 = $this->prdbh->prepare("INSERT INTO LoginUserAudittrail(RWAID, Timestamp, ResidentRWAID)
                        VALUES ($rwaidpop,$Timestamp,$rid)");
                $this->prdbh->beginTransaction();
                if ($sql10->execute()) {
                    $id = $this->prdbh->lastInsertId();
                    $this->prdbh->commit();
                }
                $sql11 = $this->prdbh->prepare("SELECT (EventListing.ID) as EventID, (EventListing.YoutubeChannelID) as text, (EventListing.Description) as EventDescption,EventListing.EventThumbnail,EventListing.Button, EventListing.PopupImage, EventListing.IsLive, EventListing.EventCoverHTMLPage,EventListing.VideoID FROM EventListing LEFT JOIN EventRWAMapping  on EventRWAMapping.EventID = EventListing.ID  Where ( EventRWAMapping.RWAID = '" . $rwaidpop . "' OR EventRWAMapping.RWAID is NULL ) AND EventListing.IsActive = '1' AND EventListing.IsLive='TRUE'  AND '" . $c . "'>EventListing.StartDate AND EventListing.EndDate>'" . $c . "' ");
                $sql11->execute();
                $count11 = $sql11->rowCount();
                if ($count11 >= 1) {
                    while ($row11 = $sql11->fetch(PDO::FETCH_ASSOC)) {
                        $items11[] = $row11;
                    }
                    $b['EventPopup'] = $items11;
                } else {
                    $sql6 = $this->prdbh->prepare("SELECT ID, RWAID, Text, ImagePath,ButtonName,DisplayCount,Category,web_link,BigImagePath 
                From HomePagePopup WHERE (RWAID ='" . $rwaidpop . "' OR RWAID is NULL )  AND IsActive = '1' ORDER BY RAND() LIMIT 1 "); //ORDER BY RAND(ID) LIMIT 1
                    $sql6->execute();
                    $count6 = $sql6->rowCount();
                    if ($count6 >= 1) {
                        $items6 = array();
                        if ($row6 = $sql6->fetch(PDO::FETCH_ASSOC)) {
                            $items6[] = $row6;
                            $Popid = $row6['ID'];
                            $DisplayCount = $row6['DisplayCount'];
                            $sql8 = $this->prdbh->prepare("SELECT COUNT(*) as Count FROM UserPopupLog 
                       WHERE UserID = '" . $ID . "' AND PopID= '" . $Popid . "'");
                            $sql8->execute();
                            if ($row8 = $sql8->fetch(PDO::FETCH_ASSOC)) {
                                $items8 = $row8[Count];
                            }
                            $b['Popupcount'] = $items8;
                            if ($items8 <= $DisplayCount) {
                                $sql7 = $this->prdbh->prepare("INSERT INTO UserPopupLog(UserID, PopID)
                        VALUES ($ID,$Popid)");
                                $this->prdbh->beginTransaction();
                                if ($sql7->execute()) {
                                    $id = $this->prdbh->lastInsertId();
                                    $this->prdbh->commit();
                                }
                            }
                        }
                    } else {
                        $sql9 = $this->prdbh->prepare("SELECT ID, RWAID, Text, ImagePath,ButtonName,DisplayCount,Category,web_link ,BigImagePath
                    From HomePagePopup WHERE RWAID is NULL AND IsActive = '1' ORDER BY RAND() LIMIT 1  ");
                        $sql9->execute();
                        $items6 = array();
                        if ($row9 = $sql9->fetch(PDO::FETCH_ASSOC)) {
                            $items6[] = $row9;
                            $Popid = $row9['ID'];
                            $DisplayCount = $row9['DisplayCount'];
                            $sql8 = $this->prdbh->prepare("SELECT COUNT(*) as Count FROM UserPopupLog 
                       WHERE UserID = '" . $ID . "' AND PopID= '" . $Popid . "'");
                            $sql8->execute();
                            if ($row8 = $sql8->fetch(PDO::FETCH_ASSOC)) {
                                $items8 = $row8[Count];
                            }
                            $b['Popupcount'] = $items8;
                            if ($items8 <= $DisplayCount) {
                                $sql7 = $this->prdbh->prepare("INSERT INTO UserPopupLog(UserID, PopID)
                        VALUES ($ID,$Popid)");
                                $this->prdbh->beginTransaction();
                                if ($sql7->execute()) {
                                    $id = $this->prdbh->lastInsertId();
                                    $this->prdbh->commit();
                                }
                            }
                        }
                    }
                    //$b['Popup']=$items6;
                    
                }
                $b['Popup'] = $items6;
                $itemsuser = array_merge($itemsuser, $a, $b);
            } else {
                $itemsuser = array_merge($itemsuser, $a);
            }
            $response['User'] = $itemsuser;
            $response['Status'] = 1;
        }
        return $response;
    }
    public function userdetail($param) {
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        if (isset($param->UID)) {
            $UID = $param->UID;
        }
        if (isset($param->PageNo)) {
            $PageNo = $param->PageNo;
        }
        $Start = $PageNo * 10 - 10;
        $response = array();
        $sql = $this->prdbh->prepare("SELECT User.ID, User.PhoneNbr,User.IsPhonePublic, User.EmailID, User.FirstName, User.MiddleName, User.LastName, User.ProfilePic, User.Gender, User.Occupation,User.About, from_unixtime((User.AddedOn+ 19660),'%d/%m/%Y  %h:%i %p') as AddedOn,ResidentRWAMapping.ApprovedBy,from_unixtime((ResidentRWAMapping.ApprovedOn+ 19660),'%d/%m/%Y  %h:%i %p') as ApprovedOn, User.IsActive FROM
                           User INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentID = User.ID INNER JOIN ResidentFlatMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID LEFT JOIN ResidentVehicleMapping ON ResidentVehicleMapping. ResidentRWAFlatID=ResidentFlatMapping.ID  WHERE ResidentRWAMapping.ApprovalStatus= 'Y' AND User.ID != '" . $UID . "' AND ResidentRWAMapping.RWAID = '" . $RWAID . "' ORDER BY User.FirstName Limit $Start,10");
        $sql->execute();
        if ($sql->execute()) {
            $items4 = array();
            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                $items4[] = $row;
                $apid = $row['ApprovedBy'];
                $sql3 = $this->prdbh->prepare("SELECT  (User.FirstName) as ApName, (User.About)  as ApAbout,(User.ProfilePic) as ApProfilePic,(ResidentFlatMapping.FlatNbr) as ApFlatNbr FROM User LEFT JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentID = User.ID LEFT JOIN ResidentFlatMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID WHERE User.ID= '" . $apid . "'");
                $sql3->execute();
                if ($row3 = $sql3->fetch(PDO::FETCH_ASSOC)) {
                    $items3 = $row3;
                }
                $a['approveby'] = $items3;
            }
            $rid = $row['ResidentRWAID'];
            $sql1 = $this->prdbh->prepare(" SELECT ID AS FlatID, FlatNbr, IFNULL(IsOwner,'') as IsOwner  FROM ResidentFlatMapping WHERE ResidentRWAID='" . $rid . "'");
            $sql1->execute();
            $items = array();
            $fid1 = array();
            if ($sql1->rowCount() > 0) {
                while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row1;
                    $fid1[] = $row1['FlatID'];
                }
                $a['Flats'] = $items;
                if (count($fid1)) {
                    $fid = implode(',', $fid1);
                    $sql2 = $this->prdbh->prepare("SELECT ID, VehicleNbr,IFNULL(VehicleType,'') as VehicleType FROM ResidentVehicleMapping WHERE ResidentRWAFlatID IN($fid)");
                    $sql2->execute();
                    $items1 = array();
                    if ($sql2->rowCount() > 0) {
                        while ($row2 = $sql2->fetch(PDO::FETCH_ASSOC)) {
                            $items1[] = $row2;
                        }
                        $a['Vehicles'] = $items1;
                    }
                }
            }
            $row = array_merge($row, $a);
            $response['neighbors'] = $row;
            $response['Status'] = 1;
        }
        return $response;
    }
    public function showhelper($param) {
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        if (isset($param->PageNo)) {
            $PageNo = $param->PageNo;
        }
        $Start = $PageNo * 10 - 10;
        $response = array();
        $sql = $this->prdbh->prepare("SELECT `ID`, `RWAID`, `Name`, `ServiceOffered`, `ResidentialAddress`, `PrimaryContactNbr`, `PhoneNbr2`, `EmailId`, `AddedBy`, `AddedOn`, `ProfilePhoto`, `PoilceVerificationScanImage1`, `PoliceVerificationScanImage2`, `PoliceVerificationScanImage3`, `EntryCardExpiry`, `IsActive` FROM `Helpers` WHERE RWAID ='" . $RWAID . "' Limit $Start,10 ");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                date('m/d/Y', DateCreated);
                $response['Show Helper'] = $items;
                $response['Status'] = '1';
            } else {
                $response['Show Helper'] = [];
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function flatno($param) {
        if (isset($param->FlatNbr)) {
            $FlatNbr = $param->FlatNbr;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT FlatNbr FROM ResidentFlatMapping WHERE   FlatNbr LIKE '" . $FlatNbr . "%' ");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['flatno'] = $items;
                $response['Status'] = '1';
            } else {
                $response['flatno'] = [];
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function profession($param) {
        if (isset($param->Occupation)) {
            $Occupation = $param->Occupation;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT Occupation FROM User WHERE   Occupation like '" . $Occupation . "%' ");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['profession'] = $items;
                $response['Status'] = '1';
            } else {
                $response['profession'] = [];
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function showpoles($param) {
        $response = array();
        $tdat = date("U");
        $c = $tdat;
        $d = $c - 518400;
        // AND PollQuestions.DateAdded BETWEEN '".$d."' AND '".$c."'
        if (isset($param->RWAID) && !empty($param->RWAID) && isset($param->ResidentRWAID) && !empty($param->ResidentRWAID)) {
            $RWAID = $param->RWAID;
            $RespId = $param->ResidentRWAID;
            $sql = $this->prdbh->prepare("SELECT DISTINCT PollQuestions.Id, PollQuestions.ResidentRWAID, PollQuestions.Question,
                                                    PollQuestions.DateAdded, PollQuestions.ExpirationDt, PollQuestions.IsActive,
                                                    (User.ID) as UserID,User.FirstName,ResidentRWAMapping.ResidentID
                                                    FROM PollQuestions
                                    INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID = PollQuestions.ResidentRWAID
                                    INNER JOIN ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID = ResidentRWAMapping.ResidentRWAID
                                    INNER JOIN User ON ResidentRWAMapping.ResidentID = User.ID
                                    WHERE ResidentRWAMapping.RWAID= '" . $RWAID . "' AND PollQuestions.IsActive='1'  ORDER BY PollQuestions.Id DESC");
            $sql->execute();
            //print_r($sql);die;
            $items = array();
            if ($sql->rowCount() > 0) {
                $e1 = array();
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $resrwaid = $row['UserID'];
                    $sql4 = $this->prdbh->prepare(" SELECT SocietyFlatMapping.FlatNbr FROM ResidentFlatMapping INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID  LEFT JOIN ResidentRWAMapping ON ResidentFlatMapping.ResidentRWAID = ResidentRWAMapping.ResidentRWAID  WHERE ResidentRWAMapping.ResidentID = $resrwaid");
                    $sql4->execute();
                    if ($sql4->execute()) {
                        while ($row1 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                            $e1[] = $row1;
                        }
                    }
                    $QId = $row['Id'];
                    $sql1 = $this->prdbh->prepare("SELECT Id, RespondentId, QuestionId, ResponseOptionId FROM PollResponses WHERE QuestionId=$QId AND ResponseOptionId=1");
                    $sql1->execute();
                    if ($sql1->rowCount() > 0) {
                        $a['YesNbr'] = $sql1->rowCount();
                    } else {
                        $a['YesNbr'] = 0;
                    }
                    $sql2 = $this->prdbh->prepare("SELECT Id, RespondentId, QuestionId, ResponseOptionId FROM PollResponses WHERE QuestionId=$QId AND ResponseOptionId=2");
                    $sql2->execute();
                    if ($sql2->rowCount() > 0) {
                        $a['NoNbr'] = $sql2->rowCount();
                    } else {
                        $a['NoNbr'] = 0;
                    }
                    $sql3 = $this->prdbh->prepare("SELECT Id, RespondentId, QuestionId, ResponseOptionId FROM PollResponses WHERE QuestionId=$QId AND RespondentId=$RespId");
                    $sql3->execute();
                    $res['id'] = $row['Id'];
                    $res['ResidentRWAID'] = $row['ResidentRWAID'];
                    $res['Question'] = $row['Question'];
                    $res['DateAdded'] = $row['DateAdded'];
                    $res['ExpirationDt'] = $row['ExpirationDt'];
                    $res['IsActive'] = $row['IsActive'];
                    $res['UserID'] = $row['UserID'];
                    $res['FirstName'] = $row['FirstName'];
                    $res['FlatNbr'] = $e1;
                    if ($sql3->rowCount() == 0) {
                        $b['Ispoll'] = 0;
                        $row = array_merge($res, $b, $row);
                        $items[] = $row;
                    } else {
                        $b['Ispoll'] = 1;
                        $row = array_merge($res, $row, $a, $b);
                        $items[] = $row;
                    }
                    $e1 = ''; // this is use for empty row value
                    
                }
                $response['ShowPoles'] = $items;
                $response['Status'] = '1';
            } else {
                $response['ShowPoles'] = [];
                $response['Status'] = '0';
            }
        } else {
            $response['message'] = "All fields are mandatory";
            $response['Status'] = 0;
        }
        return $response;
    }
    public function postpoledetail($param) {
        $response = array();
        $tdat = date("U");
        $c = $tdat;
        $d = $c - 518400;
        // AND PollQuestions.DateAdded BETWEEN '".$d."' AND '".$c."'
        if (isset($param->ID) && !empty($param->ID) && isset($param->ResidentRWAID) && !empty($param->ResidentRWAID)) {
            $ID = $param->ID;
            $RespId = $param->ResidentRWAID;
            $sql = $this->prdbh->prepare("SELECT DISTINCT PollQuestions.Id, PollQuestions.ResidentRWAID, PollQuestions.Question,
                                                    PollQuestions.DateAdded, PollQuestions.ExpirationDt, PollQuestions.IsActive,
                                                    (User.ID) as UserID,User.FirstName,ResidentRWAMapping.ResidentID
                                                    FROM PollQuestions
                                    INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID = PollQuestions.ResidentRWAID
                                    INNER JOIN ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID = ResidentRWAMapping.ResidentRWAID
                                    INNER JOIN User ON ResidentRWAMapping.ResidentID = User.ID
                                    WHERE PollQuestions.Id = '" . $ID . "'   ORDER BY PollQuestions.Id DESC");
            $sql->execute();
            $items = array();
            if ($sql->rowCount() > 0) {
                $e1 = array();
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $resrwaid = $row['UserID'];
                    $sql4 = $this->prdbh->prepare(" SELECT SocietyFlatMapping.FlatNbr FROM ResidentFlatMapping INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID  LEFT JOIN ResidentRWAMapping ON ResidentFlatMapping.ResidentRWAID = ResidentRWAMapping.ResidentRWAID  WHERE ResidentRWAMapping.ResidentID = $resrwaid");
                    $sql4->execute();
                    if ($sql4->execute()) {
                        while ($row1 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                            $e1[] = $row1;
                        }
                    }
                    $QId = $row['Id'];
                    $sql1 = $this->prdbh->prepare("SELECT Id, RespondentId, QuestionId, ResponseOptionId FROM PollResponses WHERE QuestionId=$QId AND ResponseOptionId=1");
                    $sql1->execute();
                    if ($sql1->rowCount() > 0) {
                        $a['YesNbr'] = $sql1->rowCount();
                    } else {
                        $a['YesNbr'] = 0;
                    }
                    $sql2 = $this->prdbh->prepare("SELECT Id, RespondentId, QuestionId, ResponseOptionId FROM PollResponses WHERE QuestionId=$QId AND ResponseOptionId=2");
                    $sql2->execute();
                    if ($sql2->rowCount() > 0) {
                        $a['NoNbr'] = $sql2->rowCount();
                    } else {
                        $a['NoNbr'] = 0;
                    }
                    $sql3 = $this->prdbh->prepare("SELECT Id, RespondentId, QuestionId, ResponseOptionId FROM PollResponses WHERE QuestionId=$QId AND RespondentId=$RespId");
                    $sql3->execute();
                    $res['id'] = $row['Id'];
                    $res['ResidentRWAID'] = $row['ResidentRWAID'];
                    $res['Question'] = $row['Question'];
                    $res['DateAdded'] = $row['DateAdded'];
                    $res['ExpirationDt'] = $row['ExpirationDt'];
                    $res['IsActive'] = $row['IsActive'];
                    $res['UserID'] = $row['UserID'];
                    $res['FirstName'] = $row['FirstName'];
                    $res['FlatNbr'] = $e1;
                    if ($sql3->rowCount() == 0) {
                        $b['Ispoll'] = 0;
                        $row = array_merge($res, $b, $row);
                        $items[] = $row;
                    } else {
                        $b['Ispoll'] = 1;
                        $row = array_merge($res, $row, $a, $b);
                        $items[] = $row;
                    }
                    $e1 = '';
                }
                $response['ShowPoles'] = $items;
                $response['Status'] = '1';
            } else {
                $response['ShowPoles'] = [];
                $response['Status'] = '0';
            }
        } else {
            $response['message'] = "All fields are mandatory";
            $response['Status'] = 0;
        }
        return $response;
    }
    public function poledetail($param) {
        $response = array();
        $tdat = date("U");
        $c = $tdat + 120;
        $d = $c - 518400;
        if (isset($param->id)) {
            $id = $param->id;
        }
        $sql = $this->prdbh->prepare("SELECT  PollQuestions.Id, PollQuestions.ResidentRWAID, PollQuestions.Question,
                                                    PollQuestions.DateAdded, PollQuestions.ExpirationDt, PollQuestions.IsActive,
                                                    (User.ID) as UserID,User.FirstName,ResidentRWAMapping.ResidentID
                                                    FROM PollQuestions
                                    INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID = PollQuestions.ResidentRWAID
                                    INNER JOIN ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID = ResidentRWAMapping.ResidentRWAID
                                    INNER JOIN User ON ResidentRWAMapping.ResidentID = User.ID
                                    WHERE  PollQuestions.Id='" . $id . "' ");
        $sql->execute();
        $res = array();
        if ($sql->rowCount() > 0) {
            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                $resrwaid = $row['UserID'];
                $sql4 = $this->prdbh->prepare(" SELECT ResidentFlatMapping.FlatNbr FROM ResidentFlatMapping LEFT JOIN ResidentRWAMapping ON ResidentFlatMapping.ResidentRWAID = ResidentRWAMapping.ResidentRWAID  WHERE ResidentRWAMapping.ResidentID = $resrwaid");
                $sql4->execute();
                if ($sql4->execute()) {
                    while ($row1 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                        $e1[] = $row1;
                    }
                }
                $res['ID'] = $row['Id'];
                $res['ResidentRWAID'] = $row['ResidentRWAID'];
                $res['Question'] = $row['Question'];
                $res['DateAdded'] = $row['DateAdded'];
                $res['ExpirationDt'] = $row['ExpirationDt'];
                $res['IsActive'] = $row['IsActive'];
                $res['UserID'] = $row['UserID'];
                $res['FirstName'] = $row['FirstName'];
                $res['FlatNbr'] = $e1;
            }
            $response['ShowPoles'] = $res;
            $response['Status'] = '1';
        } else {
            $response['ShowPoles'] = [];
            $response['Status'] = '0';
        }
        return $response;
    }
    public function societybanner($param) {
        if (isset($param->ID)) {
            $ID = $param->ID;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT `ID`, `Name`,  `BannerImage`, `Image2`, `Image3`, `AddedOn`, `AddedBy`, `IsActive` FROM `RWA` WHERE ID  = '" . $ID . "'  ");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['Banner Image'] = $items;
                $response['Status'] = '1';
            } else {
                $response['Banner Image'] = [];
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function allhelper($param) {
        $PageNo = '1';
        $UserID = '';
        if (isset($param->UserID)) {
            $UserID = $param->UserID;
        }
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        if (isset($param->PageNo)) {
            $PageNo = $param->PageNo;
        }
        $Start = $PageNo * 10 - 10;
        $response = array();
        $residentrwaid = '';
        $sql5 = $this->prdbh->prepare(" SELECT ResidentRWAID FROM ResidentRWAMapping WHERE ResidentID ='" . $UserID . "' ");
        $sql5->execute();
        $count5 = $sql5->rowCount();
        if ($count5 == 1) {
            while ($row5 = $sql5->fetch(PDO::FETCH_ASSOC)) {
                $residentrwaid = $row5['ResidentRWAID'];
            }
        }
        if (isset($param->Name)) {
            $Name = $param->Name;
            if ($Name != '') {
                $sql = $this->prdbh->prepare("select Helpers.ID,IFNULL(Helpers.LastScaneTimestamp,'0') as LastScaneTimestamp ,Helpers.ServiceCharges, Helpers.PrimaryContactNbr, Helpers.PhoneNbr2,Helpers.ProfilePhoto,trim(Helpers.Name) as Name,trim(Helpers.Name) as FirstName, IFNULL(Helpers.is_in,'2') as is_in,IFNULL(Helpers.is_in,'2') as isin,Helpers.ServiceOffered ,IFNULL(Helpers.PoliceVerification,'0') as PoliceVerification,Helpers.ServiceOffered as Occupation ,from_unixtime((Helpers.AddedOn + 19660),'%d/%m/%Y  %h:%i %p') as AddedOn,from_unixtime((Helpers.EntryCardExpiry + 19660),'%d/%m/%Y  %h:%i %p') as EntryCardExpiry, case When UserFavHelper.HelperID IS NULL Then 0 Else 1 END  as IsFavAdded from Helpers  left outer join UserFavHelper
                         On Helpers.ID = UserFavHelper.HelperID and UserFavHelper.UserID = '" . $UserID . "' WHERE Helpers.IsActive= '1' AND Helpers.RWAID= '" . $RWAID . "' AND Helpers.Name LIKE '%" . $Name . "%' ORDER BY isin, Name ASC ");
            } else {
                $sql = $this->prdbh->prepare("select Helpers.ID,IFNULL(Helpers.LastScaneTimestamp,'0') as LastScaneTimestamp ,Helpers.ServiceCharges, Helpers.PrimaryContactNbr, Helpers.PhoneNbr2,Helpers.ProfilePhoto,trim(Helpers.Name) as Name,trim(Helpers.Name) as FirstName,  IFNULL(Helpers.is_in,'2') as is_in,IFNULL(Helpers.is_in,'2') as isin,Helpers.ServiceOffered,IFNULL(Helpers.PoliceVerification,'0') as PoliceVerification,Helpers.ServiceOffered as Occupation ,from_unixtime((Helpers.AddedOn + 19660),'%d/%m/%Y  %h:%i %p') as AddedOn,from_unixtime((Helpers.EntryCardExpiry + 19660),'%d/%m/%Y  %h:%i %p') as EntryCardExpiry, case When UserFavHelper.HelperID IS NULL Then 0 Else 1 END  as IsFavAdded from Helpers  left outer join UserFavHelper
                         On Helpers.ID = UserFavHelper.HelperID and UserFavHelper.UserID = '" . $UserID . "' WHERE Helpers.IsActive= '1' AND Helpers.RWAID= '" . $RWAID . "'  ORDER BY isin, Name ASC ");
            }
        } elseif (isset($param->ServiceOffered)) {
            $ServiceOffered = $param->ServiceOffered;
            if ($ServiceOffered != '') {
                $sql = $this->prdbh->prepare("select Helpers.ID,IFNULL(Helpers.LastScaneTimestamp,'0') as LastScaneTimestamp,IFNULL(Helpers.is_in,'2') as is_in ,Helpers.ServiceCharges, Helpers.PrimaryContactNbr, Helpers.PhoneNbr2,Helpers.ProfilePhoto,trim(Helpers.Name) as Name,trim(Helpers.Name) as FirstName,IFNULL(Helpers.is_in,'2') as isin,Helpers.ServiceOffered ,Helpers.ServiceOffered as Occupation,IFNULL(Helpers.PoliceVerification,'0') as PoliceVerification,from_unixtime((Helpers.AddedOn + 19660),'%d/%m/%Y  %h:%i %p') as AddedOn,from_unixtime((Helpers.EntryCardExpiry + 19660),'%d/%m/%Y  %h:%i %p') as EntryCardExpiry, case When UserFavHelper.HelperID IS NULL Then 0 Else 1 END  as IsFavAdded from Helpers  left outer join UserFavHelper
                         On Helpers.ID = UserFavHelper.HelperID and UserFavHelper.UserID = '" . $UserID . "' WHERE Helpers.IsActive= '1' AND Helpers.RWAID= '" . $RWAID . "' AND Helpers.ServiceOffered LIKE '" . $ServiceOffered . "%'  ORDER BY isin, Name ASC ");
            } else {
                $sql = $this->prdbh->prepare("select Helpers.ID ,IFNULL(Helpers.LastScaneTimestamp,'0') as LastScaneTimestamp,IFNULL(Helpers.is_in,'2') as is_in,Helpers.ServiceCharges, Helpers.PrimaryContactNbr, Helpers.PhoneNbr2,Helpers.ProfilePhoto,trim(Helpers.Name) as Name,trim(Helpers.Name) as FirstName,  IFNULL(Helpers.is_in,'2') as isin,Helpers.ServiceOffered ,Helpers.ServiceOffered as Occupation ,IFNULL(Helpers.PoliceVerification,'0') as PoliceVerification,from_unixtime((Helpers.AddedOn + 19660),'%d/%m/%Y  %h:%i %p') as AddedOn,from_unixtime((Helpers.EntryCardExpiry + 19660),'%d/%m/%Y  %h:%i %p') as EntryCardExpiry, case When UserFavHelper.HelperID IS NULL Then 0 Else 1 END  as IsFavAdded from Helpers  left outer join UserFavHelper
                         On Helpers.ID = UserFavHelper.HelperID and UserFavHelper.UserID = '" . $UserID . "' WHERE Helpers.IsActive= '1' AND Helpers.RWAID= '" . $RWAID . "' ORDER BY isin, Name ASC ");
            }
        } else {
            $sql = $this->prdbh->prepare("Select Helpers.ID,IFNULL(Helpers.LastScaneTimestamp,'0') as LastScaneTimestamp ,IFNULL(Helpers.is_in,'2') as is_in, IFNULL(HelperSubscribe.Subscribe,'0') as Subscribe, Helpers.ServiceCharges, Helpers.PrimaryContactNbr, Helpers.PhoneNbr2,Helpers.ProfilePhoto,trim(Helpers.Name) as Name, IFNULL(Helpers.is_in,'2') as isin,
            Helpers.ServiceOffered,IFNULL(Helpers.PoliceVerification,'0') as PoliceVerification,from_unixtime((Helpers.AddedOn + 19660),'%d/%m/%Y  %h:%i %p') as AddedOn,
            from_unixtime((Helpers.EntryCardExpiry + 19660),'%d/%m/%Y  %h:%i %p') as EntryCardExpiry, case When UserFavHelper.HelperID IS NULL Then 0 Else 1 END  as IsFavAdded 
            from Helpers  left outer join UserFavHelper
            On Helpers.ID = UserFavHelper.HelperID and UserFavHelper.UserID = '" . $UserID . "'
            left join HelperSubscribe on HelperSubscribe.HelperID=Helpers.ID 
            and HelperSubscribe.ResidentRWAID='" . $residentrwaid . "'
            WHERE Helpers.IsActive= '1' AND Helpers.RWAID= '" . $RWAID . "'
            ORDER BY isin, Name ASC Limit $Start,10");
        } // order by name change 26/07/17
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                $i = 0;
                $response = array();
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                    $ID = $row['ID'];
                    $is_in = $row['is_in'];
                    $LastScaneTimestamp = $row['LastScaneTimestamp'];
                    $sql1 = $this->prdbh->prepare("SELECT (AVG(Rating) +0.0001) as rated from HelperReviews WHERE HelperID = '" . $ID . "'");
                    $sql1->execute();
                    $subitems = array();
                    $count1 = $sql->rowCount();
                    if ($count1 > 0) {
                        $subitems = array();
                        while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                            $subitems[] = $row1['rated'];
                            $a = implode('', $subitems);
                            $items[$i]['Rated'] = $a;
                        }
                    }
                    $tdat = date("U") + 600;
                    $t24 = $tdat - 86400;
                    $t48 = $tdat - 172800;
                    $color1 = '';
                    if ($LastScaneTimestamp != '') {
                        if ($LastScaneTimestamp <= $t48 && $is_in == 1) {
                            $color1 = "Red";
                        } else if (($LastScaneTimestamp > $t48 && $is_in == 1) && ($LastScaneTimestamp <= $t24 && $is_in == 1)) {
                            $color1 = "Yellow";
                        } else if (($LastScaneTimestamp > $t24 && $is_in == 1) && ($LastScaneTimestamp <= $tdat && $is_in == 1)) {
                            $color1 = "Green";
                        } elseif ($is_in == 2 || $is_in == 0) {
                            $color1 = "Grey";
                        }
                    } else {
                        $color1 = "Grey";
                    }
                    $items[$i]['color'] = $color1;
                    $i++;
                }
                $response['All  Helper'] = $items;
                $response['Status'] = '1';
            } else {
                $response['All Helper'] = [];
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function postdelete($FeedType, $PostRefID) {
        $input = "FeedType=$FeedType&PostRefID=$PostRefID";
        $curl = curl_init();
        curl_setopt_array($curl, array(CURLOPT_URL => ConstantUrlMongo . "postdelete.php", CURLOPT_RETURNTRANSFER => true, CURLOPT_ENCODING => "", CURLOPT_MAXREDIRS => 10, CURLOPT_TIMEOUT => 30, CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1, CURLOPT_CUSTOMREQUEST => "POST", CURLOPT_POSTFIELDS => $input, CURLOPT_HTTPHEADER => array("content-type: application/x-www-form-urlencoded"),));
        $result = curl_exec($curl);
        $err = curl_error($curl);
        return $result;
    }
    public function allhelperforscanner($param) {
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        if (isset($param->lastUpdated)) {
            $lastUpdated = $param->lastUpdated;
        }
        $tdat = date("U");
        $currenttime = $tdat + 120;
        $lastUpdatedtime = $lastUpdated;
        $response = array();
        $sql = $this->prdbh->prepare("SELECT ID,IFNULL(is_in,'2') as is_in,IFNULL(HelperQRCode,'') as HelperQRCode,Name,ProfilePhoto  FROM Helpers WHERE RWAID = '" . $RWAID . "' AND AddedOn BETWEEN '" . $lastUpdatedtime . "' AND '" . $currenttime . "'  GROUP BY ID DESC");
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['HelperQrDetail'] = $items;
                $response['Status'] = 1;
            } else {
                $response['HelperQrDetail'] = [];
                $response['message'] = "No Qr Data";
                $response['Status'] = 0;
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function allhelperforscannertwo($param) {
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        if (isset($param->lastUpdated)) {
            $lastUpdated = $param->lastUpdated;
        }
        $tdat = date("U");
        $currenttime = $tdat + 120;
        $lastUpdatedtime = $lastUpdated;
        $response = array();
        $sql = $this->prdbh->prepare("SELECT ID,IFNULL(is_in,'2') as is_in,IFNULL(HelperQRCode,'') as HelperQRCode,Name,ProfilePhoto  FROM Helpers WHERE RWAID = '" . $RWAID . "' AND AddedOn BETWEEN '" . $lastUpdatedtime . "' AND '" . $currenttime . "'  GROUP BY ID DESC");
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['HelperQrDetail'] = $items;
                $response['Status'] = 1;
            } else {
                $response['HelperQrDetail'] = [];
                $response['message'] = "No Qr Data";
                $response['Status'] = 0;
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        $sql2 = $this->prdbh->prepare("SELECT ID, Name, CONCAT('S:',StaffQRCode) As StaffQRCode,ProfilePic,Status  FROM MaintenanceStaff  WHERE RWAID = '" . $RWAID . "' group by MaintenanceStaff.ID");
        $sql2->execute();
        if ($sql2->execute()) {
            $staffitems = array();
            $count2 = $sql2->rowCount();
            if ($count2 >= 1) {
                while ($row2 = $sql2->fetch(PDO::FETCH_ASSOC)) {
                    $staffitems[] = $row2;
                }
                $response['Maintenance Staff'] = $staffitems;
                $response['StaffStatus'] = 1;
            } else {
                $response['Maintenance Staff'] = "No data";
                $response['message1'] = "No Data";
                $response['Status'] = 0;
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function helperdetailnew($param) {
        if (isset($param->ID)) {
            $ID = $param->ID;
        }
        if (isset($param->ResidentRWAID)) {
            $ResidentRWAID = $param->ResidentRWAID;
        }
        if (isset($param->SocietyFlatID)) {
            $SocietyFlatID = $param->SocietyFlatID;
        }
        if (isset($param->roleid)) {
            $roleid = $param->roleid;
        }
        $response = array();
        $sql6 = $this->prdbh->prepare("SELECT FunctionID FROM RoleFunctions WHERE RoleFunctions.RoleID='" . $roleid . "'  ");
        $sql6->execute();
        $count6 = $sql6->rowCount();
        if ($sql6->rowCount() >= 1) {
            $FunctionID = array();
            while ($row6 = $sql6->fetch(PDO::FETCH_ASSOC)) {
                $FunctionID[] = $row6['FunctionID'];
            }
        }
        if (in_array(44, $FunctionID)) {
            $sql = $this->prdbh->prepare("SELECT Helpers.ID, Helpers.RWAID, Helpers.Name as name, Helpers.Name, Helpers.ServiceOffered,Helpers.PoliceVerification, Helpers.ServiceCharges,
        Helpers.ResidentialAddress, Helpers.PrimaryContactNbr, Helpers.PhoneNbr2,Helpers.EmailId,ResidentRWAMapping.ResidentRWAID,
        (User.FirstName) as AddedbyName,(User.ID) as AddedBy, (SocietyFlatMapping.FlatNbr) as addedbyFlatNbr, Helpers.AddedOn, Helpers.ProfilePhoto,Helpers.OtherDocScanImage1, Helpers.OtherDocScanImage2, Helpers.OtherDocScanImage3,Helpers.EntryCardExpiry,Helpers.IsActive,Helpers.is_in,
        Helpers.HelperQRCode,(AVG(HelperReviews.Rating)+0.0001) as Rating 
        from Helpers left join HelperSubscribe ON Helpers.ID = HelperSubscribe.HelperID LEFT JOIN HelperReviews ON HelperReviews.HelperID=Helpers.ID
        LEFT JOIN ResidentRWAMapping on Helpers.AddedBy = ResidentRWAMapping.ResidentRWAID LEFT JOIN User ON ResidentRWAMapping.ResidentID=User.ID LEFT JOIN ResidentFlatMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID
        WHERE Helpers.IsActive= '1' AND Helpers.ID =  '" . $ID . "' ORDER BY ID ASC ");
        } else {
            $sql = $this->prdbh->prepare("SELECT Helpers.ID, Helpers.RWAID, Helpers.Name as name, Helpers.Name, Helpers.ServiceOffered, Helpers.PoliceVerification,Helpers.ServiceCharges,
        Helpers.ResidentialAddress, Helpers.PrimaryContactNbr, Helpers.PhoneNbr2,Helpers.EmailId,ResidentRWAMapping.ResidentRWAID,
        (User.FirstName) as AddedbyName,(User.ID) as AddedBy, (SocietyFlatMapping.FlatNbr) as addedbyFlatNbr, Helpers.AddedOn, Helpers.ProfilePhoto,Helpers.EntryCardExpiry,Helpers.IsActive,Helpers.is_in,
        Helpers.HelperQRCode,(AVG(HelperReviews.Rating)+0.0001) as Rating 
        from Helpers left join HelperSubscribe ON Helpers.ID = HelperSubscribe.HelperID LEFT JOIN HelperReviews ON HelperReviews.HelperID=Helpers.ID
        LEFT JOIN ResidentRWAMapping on Helpers.AddedBy = ResidentRWAMapping.ResidentRWAID LEFT JOIN User ON ResidentRWAMapping.ResidentID=User.ID LEFT JOIN ResidentFlatMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID
        WHERE Helpers.IsActive= '1' AND Helpers.ID =  '" . $ID . "' ORDER BY ID ASC ");
        }
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                $i = 0;
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $id = $row['ID'];
                    $name = $row['name'];
                    $data[] = $row;
                    $sql4 = $this->prdbh->prepare("SELECT ID, IFNULL(Subscribe,'0') as Subscribe FROM HelperSubscribe WHERE HelperID =  '" . $ID . "' AND ResidentRWAID= '" . $ResidentRWAID . "'");
                    $sql4->execute();
                    $counts = $sql4->rowCount();
                    if ($counts == 1) {
                        while ($row4 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                            $sub = $row4['Subscribe'];
                        }
                    } else {
                        $sub = 0;
                    }
                    $tdat = date("U") + 600;
                    $t24 = $tdat - 86400;
                    $t48 = $tdat - 172800;
                    $sql7 = $this->prdbh->prepare("SELECT timestamp,is_in  FROM Attendance WHERE HelperID =  '" . $ID . "'  ORDER BY timestamp DESC limit 1");
                    $sql7->execute();
                    $count1 = $sql7->rowCount();
                    if ($count1 == 1) {
                        while ($row7 = $sql7->fetch(PDO::FETCH_ASSOC)) {
                            $timestamp = $row7['timestamp'];
                            $is_in = $row7['is_in'];
                        }
                        if ($timestamp <= $t48 && $is_in == 1) {
                            $color1 = "Red";
                        } else if (($timestamp > $t48 && $is_in == 1) && ($timestamp <= $t24 && $is_in == 1)) {
                            $color1 = "Yellow";
                        } else if (($timestamp > $t24 && $is_in == 1) && ($timestamp <= $tdat && $is_in == 1)) {
                            $color1 = "Green";
                        } elseif ($is_in == 2 || $is_in == 0) {
                            $color1 = "Grey";
                        }
                    } else {
                        $color1 = "Grey";
                    }
                    $response['HelperDetail'][$i] = $row;
                    $response['HelperDetail'][$i]['Subscribe'] = $sub;
                    $response['HelperDetail'][$i]['color'] = $color1;
                    $sql5 = $this->prdbh->prepare("SELECT HelperID, IFNULL(SocietyFlatID,'0') SocietyFlatID FROM SocietyHelperWorkingFlat WHERE HelperID =  '" . $ID . "' AND SocietyFlatID= '" . $SocietyFlatID . "'");
                    $sql5->execute();
                    $counts1 = $sql5->rowCount();
                    if ($counts1 == 1) {
                        while ($row5 = $sql5->fetch(PDO::FETCH_ASSOC)) {
                            $Isworking = '1';
                        }
                    } else {
                        $Isworking = 0;
                    }
                    $response['HelperDetail'][$i]['Isworking'] = $Isworking;
                    $rid = $row['ID'];
                    $sql1 = $this->prdbh->prepare("SELECT DISTINCT HelperReviews.ID,HelperReviews.HelperID,(HelperReviews.Rating + 0.0001) as Rating,HelperReviews.Comments,HelperReviews.AddedBy,HelperReviews.AddedOn,ResidentRWAMapping.ResidentID,User.FirstName,User.ProfilePic,SocietyFlatMapping.FlatNbr,ResidentRWAMapping.ResidentRWAID FROM HelperReviews INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID=HelperReviews.AddedBy INNER JOIN ResidentFlatMapping ON ResidentRWAMapping.ResidentRWAID=ResidentFlatMapping.ResidentRWAID LEFT JOIN User ON User.ID=ResidentRWAMapping.ResidentID INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID  WHERE HelperReviews.HelperID= '" . $rid . "' GROUP by User.ID ");
                    $sql1->execute();
                    $count1 = $sql1->rowCount();
                    if ($count1 > 0) {
                        while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                            $rrwaid = $row1['ResidentRWAID'];
                            $sql3 = $this->prdbh->prepare("SELECT SocietyFlatMapping.FlatNbr FROM SocietyFlatMapping INNER JOIN ResidentFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID LEFT JOIN ResidentRWAMapping ON ResidentFlatMapping.ResidentRWAID = ResidentRWAMapping.ResidentRWAID  WHERE  ResidentRWAMapping.ResidentRWAID ='" . $rrwaid . "'");
                            if ($sql3->execute()) {
                                $a = array();
                                while ($row3 = $sql3->fetch(PDO::FETCH_ASSOC)) {
                                    $a[] = $row3;
                                }
                                $response['FlatNbr'] = $a;
                            }
                            $subitems[] = $row1;
                            $response['HelperDetail'][$i]['Rating & Comment'] = $subitems;
                        }
                    }
                    $i++;
                }
                $response['Status'] = '1';
            } else {
                $response['HelperDetail'] = [];
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function helperdetail($param) {
        if (isset($param->ID)) {
            $ID = $param->ID;
        }
        if (isset($param->ResidentRWAID)) {
            $ResidentRWAID = $param->ResidentRWAID;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT Helpers.ID, Helpers.RWAID, Helpers.Name, Helpers.ServiceOffered, Helpers.ServiceCharges,
        Helpers.ResidentialAddress, Helpers.PrimaryContactNbr, Helpers.PhoneNbr2,Helpers.EmailId,ResidentRWAMapping.ResidentRWAID,
        (User.FirstName) as AddedbyName,(User.ID) as AddedBy, (SocietyFlatMapping.FlatNbr) as addedbyFlatNbr, Helpers.AddedOn, Helpers.ProfilePhoto,
        Helpers.OtherDocScanImage1, Helpers.OtherDocScanImage2, Helpers.OtherDocScanImage3,EntryCardExpiry,Helpers.IsActive,Helpers.is_in,
        Helpers.HelperQRCode,(AVG(HelperReviews.Rating)+0.0001) as Rating 
        from Helpers left join HelperSubscribe ON Helpers.ID = HelperSubscribe.HelperID LEFT JOIN HelperReviews ON HelperReviews.HelperID=Helpers.ID
        LEFT JOIN ResidentRWAMapping on Helpers.AddedBy = ResidentRWAMapping.ResidentRWAID LEFT JOIN User ON ResidentRWAMapping.ResidentID=User.ID LEFT JOIN ResidentFlatMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID
        WHERE Helpers.IsActive= '1' AND Helpers.ID =  '" . $ID . "' ORDER BY ID ASC ");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                $i = 0;
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $id = $row['ID'];
                    $name = $row['name'];
                    $data[] = $row;
                    $sql4 = $this->prdbh->prepare("SELECT ID, IFNULL(Subscribe,'0') as Subscribe FROM HelperSubscribe WHERE HelperID =  '" . $ID . "' AND ResidentRWAID= '" . $ResidentRWAID . "'");
                    $sql4->execute();
                    $counts = $sql4->rowCount();
                    if ($counts == 1) {
                        while ($row4 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                            $sub = $row4['Subscribe'];
                        }
                    } else {
                        $sub = 0;
                    }
                    $response['HelperDetail'][$i] = $row;
                    $response['HelperDetail'][$i]['Subscribe'] = $sub;
                    $rid = $row['ID'];
                    $sql1 = $this->prdbh->prepare("SELECT DISTINCT HelperReviews.ID,HelperReviews.HelperID,(HelperReviews.Rating + 0.0001) as Rating,HelperReviews.Comments,HelperReviews.AddedBy,HelperReviews.AddedOn,ResidentRWAMapping.ResidentID,User.FirstName,User.ProfilePic,SocietyFlatMapping.FlatNbr,ResidentRWAMapping.ResidentRWAID FROM HelperReviews INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID=HelperReviews.AddedBy INNER JOIN ResidentFlatMapping ON ResidentRWAMapping.ResidentRWAID=ResidentFlatMapping.ResidentRWAID LEFT JOIN User ON User.ID=ResidentRWAMapping.ResidentID INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID  WHERE HelperReviews.HelperID= '" . $rid . "' GROUP by User.ID ");
                    $sql1->execute();
                    $count1 = $sql1->rowCount();
                    if ($count1 > 0) {
                        while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                            $rrwaid = $row1['ResidentRWAID'];
                            $sql3 = $this->prdbh->prepare("SELECT SocietyFlatMapping.FlatNbr FROM SocietyFlatMapping INNER JOIN ResidentFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID LEFT JOIN ResidentRWAMapping ON ResidentFlatMapping.ResidentRWAID = ResidentRWAMapping.ResidentRWAID  WHERE  ResidentRWAMapping.ResidentRWAID ='" . $rrwaid . "'");
                            if ($sql3->execute()) {
                                $a = array();
                                while ($row3 = $sql3->fetch(PDO::FETCH_ASSOC)) {
                                    $a[] = $row3;
                                }
                                $response['FlatNbr'] = $a;
                            }
                            $subitems[] = $row1;
                            $response['HelperDetail'][$i]['Rating & Comment'] = $subitems;
                        }
                    }
                    $i++;
                }
                $response['Status'] = '1';
            } else {
                $response['HelperDetail'] = [];
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function rwamember($param) {
        if (isset($param->MemberTypeId)) {
            $MemberTypeId = $param->MemberTypeId;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT MemberId,SocietyId,MemberTypeId,VehicleId,Name,Password,Image,Profession,MobileNo,EmailId,FlatNo,Address1,Address2,Address3,Address4,City,State,PinCode,Country FROM tblmember WHERE tblmember.MemberTypeId = '" . $MemberTypeId . "' ORDER BY MemberId ASC ");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['Member'] = $items;
                $response['Status'] = '1';
            } else {
                $response['Member'] = [];
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function clubentryconfirm($rec) {
        $response = array();
        $tdat = date("U");
        $tdate = $tdat + 120;
        $stat = $rec->Status;
        $sql1 = $this->prdbh->prepare("INSERT INTO ClubFacility(RWAID, ResidentFlatID, EntryTime, Status, FacilityID) VALUES(?,?,?,?,?)");
        $sql1->bindParam(1, $rec->RWAID);
        $sql1->bindParam(2, $rec->ResidentFlatID);
        $sql1->bindParam(3, $rec->EntryTime);
        $sql1->bindParam(4, $rec->Status);
        $sql1->bindParam(5, $rec->FacilityID);
        $this->prdbh->beginTransaction();
        if ($sql1->execute()) {
            $id = $this->prdbh->lastInsertId();
            $this->prdbh->commit();
            $response['Inserted'] = $id;
            if ($stat == 1) {
                $msg = 'Member is In';
            } elseif ($stat == 2) {
                $msg = 'Member is Out';
            }
            $response['Message'] = $msg;
            $response['Status'] = '1';
        } else {
            $response['Message'] = $sql1->errorInfo();
            $response['Status'] = '0';
        }
        //print_r($response);die;
        return $response;
    }
    public function helperfeedback($rec) {
        $response = array();
        $tdat = date("U");
        $tdate = $tdat + 120;
        $HelperID = $rec->HelperID;
        $AddedBy = $rec->AddedBy;
        $sql = $this->prdbh->prepare("SELECT Rating, Comments,AddedOn,HelperID, AddedBy FROM HelperReviews WHERE HelperID='" . $HelperID . "' AND AddedBy='" . $AddedBy . "' ");
        $sql->execute();
        if ($sql->rowCount() > 0) {
            $sql1 = $this->prdbh->prepare("UPDATE HelperReviews SET Rating=?,Comments=?,AddedOn=?  WHERE HelperID=? AND AddedBy=?");
        } else {
            $sql1 = $this->prdbh->prepare("INSERT INTO HelperReviews(Rating, Comments,AddedOn,HelperID, AddedBy) VALUES(?,?,?,?,?)");
        }
        $sql1->bindParam(1, $rec->Rating);
        $sql1->bindParam(2, $rec->Comments);
        $sql1->bindParam(3, $tdate);
        $sql1->bindParam(4, $rec->HelperID);
        $sql1->bindParam(5, $rec->AddedBy);
        $this->prdbh->beginTransaction();
        if ($sql1->execute()) {
            $this->prdbh->commit();
            $response['Message'] = "Feedback added. ";
            $response['Status'] = 1;
        } else {
            $response['Message'] = $sql1->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function isin($rec) {
        $HelperQRCode = $rec->HelperQRCode;
        $timestamp = $rec->timestamp;
        $is_in = $rec->is_in;
        $sql1 = $this->prdbh->prepare("SELECT ID,RWAID,Name FROM Helpers WHERE  HelperQRCode='" . $HelperQRCode . "' ");
        $sql1->execute();
        if ($sql1->execute()) {
            $count = $sql1->rowCount();
            if ($count >= 1) {
                while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                    $helperid = $row1['ID'];
                    $rwaid = $row1['RWAID'];
                    $name = $row1['Name'];
                }
                $sql2 = $this->prdbh->prepare("INSERT INTO Attendance (HelperID,timestamp,RWAID,is_in) VALUES(?,?,?,?)");
                $sql2->bindParam(1, $helperid);
                $sql2->bindParam(2, $timestamp);
                $sql2->bindParam(3, $rwaid);
                $sql2->bindParam(4, $is_in);
                $this->prdbh->beginTransaction();
                if ($sql2->execute()) {
                    $this->prdbh->commit();
                    if ($rec->is_in == '1') $response['Message'] = "Helper isin";
                    elseif ($rec->is_in == '2') $response['Message'] = "Helper isout";
                    $response['Status'] = "1";
                }
                $sql = $this->prdbh->prepare("UPDATE Helpers SET is_in= '" . $is_in . "' ,LastScaneTimestamp= '" . $timestamp . "'  WHERE HelperQRCode= '" . $HelperQRCode . "' ");
                $this->prdbh->beginTransaction();
                if ($sql->execute()) {
                    $this->prdbh->commit();
                } else {
                    $response['message'] = "NO UPDATE";
                }
                $sql3 = $this->prdbh->prepare("SELECT ResidentID FROM ResidentRWAMapping LEFT JOIN HelperSubscribe ON ResidentRWAMapping.ResidentRWAID = HelperSubscribe.ResidentRWAID WHERE ResidentRWAMapping.RWAID='" . $rwaid . "' AND HelperSubscribe.HelperID='" . $helperid . "' ");
                $sql3->execute();
                if ($sql3->rowCount() > 0) {
                    $UpushId1 = array();
                    while ($row3 = $sql3->fetch(PDO::FETCH_ASSOC)) {
                        $UpushId1[] = $row3['ResidentID'];
                    }
                    $UpushId = implode(',', $UpushId1);
                    $date = date('h:i:sa', $timestamp + 19660);
                    if ($is_in == '1') {
                        $in = "in";
                    } elseif ($is_in == '2') {
                        $in = "out";
                    }
                    $message = $name . ' has checked ' . $in . ' at ' . $date;
                    $UrHd1 = 'mynukad helper attendance';
                    //$ID='C'.$id;
                    $message1 = array("message" => $message, "UrHd2" => $UrHd1, "body" => $helperid);
                    $result = $this->firebasenotification($UpushId, $message1);
                    $response['PushStatus'] = $result;
                }
            } else {
                $response['Message'] = "No validate qr code";
                $response['Status'] = 0;
            }
        } else {
            $response['Message'] = $sql1->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function clubentry($rec) {
        $RWAID = $rec->RWAID;
        $QRCode = $rec->QRCode;
        $Prefix = $rec->Prefix;
        $PhoneNbr = $rec->PhoneNbr;
        $tdat = date("U");
        $maxdat = date("U") + 604400;
        if ($QRCode != '') {
            //AND UserFacilityMapping.Expirydate >'".$tdat."'
            $sql1 = $this->prdbh->prepare("SELECT User.ID, User.PhoneNbr,User.IsPhonePublic, User.EmailID,  User.FirstName, User.MiddleName, User.LastName, User.ProfilePic, User.Gender,SocietyFlatMapping.FlatNbr,(ResidentFlatMapping.ID) as flatmapinid  FROM
                User INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentID = User.ID INNER JOIN ResidentFlatMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID INNER JOIN SocietyFlatMapping on SocietyFlatMapping.ID = ResidentFlatMapping.FlatID  WHERE ResidentRWAMapping.ApprovalStatus= 'Y'  AND ResidentRWAMapping.RWAID ='" . $RWAID . "' AND ResidentFlatMapping.ID = '" . $QRCode . "' ");
        } else {
            $sql1 = $this->prdbh->prepare("SELECT User.ID, User.PhoneNbr,User.IsPhonePublic, User.EmailID,  User.FirstName, User.MiddleName, User.LastName, User.ProfilePic, User.Gender,SocietyFlatMapping.FlatNbr,(ResidentFlatMapping.ID) as flatmapinid  FROM
                User INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentID = User.ID INNER JOIN ResidentFlatMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID  INNER JOIN SocietyFlatMapping on SocietyFlatMapping.ID = ResidentFlatMapping.FlatID  WHERE ResidentRWAMapping.ApprovalStatus= 'Y'  AND ResidentRWAMapping.RWAID ='" . $RWAID . "' AND User.PhoneNbr = '" . $PhoneNbr . "'    ");
        }
        //print_r($sql1);
        if ($sql1->execute()) {
            $count = $sql1->rowCount();
            if ($count >= 1) {
                while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                    $helperid = $row1['ID'];
                    $rwaid = $row1['RWAID'];
                    $Gender = $row1['Gender'];
                    $FlatNbr = $row1['FlatNbr'];
                    $name = $row1['FirstName'];
                    $flatmapinid = $row1['flatmapinid'];
                    $ProfilePhoto = $row1['ProfilePic'];
                    if ($ProfilePhoto != '') {
                        $ProfilePhoto == $ProfilePhoto;
                    } else {
                        $ProfilePhoto = 'https://s3.amazonaws.com/mynukad-ios/HTMLFiles/helper_placeholder.png';
                    }
                    $sql12 = $this->prdbh->prepare("SELECT  FacilityID, Expirydate, Prefix From UserFacilityMapping WHERE Prefix= '" . $Prefix . "' AND ResidentFlatID= '" . $flatmapinid . "'  ");
                    //print_r($sql12);
                    if ($sql12->execute()) {
                        $count2 = $sql12->rowCount();
                        if ($count2 >= 1) {
                            $row12 = $sql12->fetch(PDO::FETCH_ASSOC);
                            $Expirydate = $row12['Expirydate'];
                        } else {
                            $Expirydate = 0;
                        }
                    }
                    if ($Expirydate == '0') {
                        $message = "you don't have access for this facility";
                    } elseif ($Expirydate <= $tdat) {
                        $message = 'your card is expired';
                    } elseif ($Expirydate <= $maxdat) {
                        $message = 'your card will expire soon';
                    } else {
                        $message = 'your detail';
                    }
                }
                $response['Status'] = "1";
                $response['Name'] = $name;
                $response['ProfilePic'] = $ProfilePhoto;
                $response['Gender'] = $Gender;
                $response['FlatNbr'] = $FlatNbr;
                $response['Expirydate'] = $Expirydate;
                $response['Message'] = $message;
            } else {
                $response['Status'] = "0";
                $response['Message'] = "your card/number is invalid";
            }
        }
        return $response;
    }
    public function helperisin($rec) {
        $HelperQRCode = $rec->HelperQRCode;
        $timestamp = $rec->timestamp;
        $is_in = $rec->is_in;
        $RWAID = $rec->RWAID;
        $tdat = date("U");
        $sql1 = $this->prdbh->prepare("SELECT ID,RWAID,Name,ProfilePhoto FROM Helpers WHERE IsActive= 1 AND  HelperQRCode='" . $HelperQRCode . "' AND( EntryCardExpiry >= '" . $tdat . "' or EntryCardExpiry='null' or EntryCardExpiry='0')  ");
        $sql1->execute();
        if ($sql1->execute()) {
            $count = $sql1->rowCount();
            if ($count >= 1) {
                while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                    $helperid = $row1['ID'];
                    $rwaid = $row1['RWAID'];
                    $name = $row1['Name'];
                    $ProfilePhoto = $row1['ProfilePhoto'];
                    if ($ProfilePhoto != '') {
                        $ProfilePhoto == $ProfilePhoto;
                    } else {
                        $ProfilePhoto = 'https://s3.amazonaws.com/mynukad-ios/HTMLFiles/helper_placeholder.png';
                    }
                }
                if ($RWAID == $rwaid) {
                    $sql2 = $this->prdbh->prepare("INSERT INTO Attendance (HelperID,timestamp,RWAID,is_in,GateNnrID,GatekeeperID) VALUES(?,?,?,?,?,?)");
                    $sql2->bindParam(1, $helperid);
                    $sql2->bindParam(2, $timestamp);
                    $sql2->bindParam(3, $rwaid);
                    $sql2->bindParam(4, $is_in);
                    $sql2->bindParam(5, $rec->GateNnrID);
                    $sql2->bindParam(6, $rec->GatekeeperID);
                    $this->prdbh->beginTransaction();
                    if ($sql2->execute()) {
                        $this->prdbh->commit();
                        if ($rec->is_in == '1') {
                            $response['Message'] = "Helper is in";
                        } elseif ($rec->is_in == '2') {
                            $response['Message'] = "Helper is out";
                        }
                        $response['Status'] = "1";
                        $response['HelperName'] = $name;
                        $response['HelperPhoto'] = $ProfilePhoto;
                    }
                    $sql = $this->prdbh->prepare("UPDATE Helpers SET is_in= '" . $is_in . "' ,LastScaneTimestamp= '" . $timestamp . "'  WHERE HelperQRCode= '" . $HelperQRCode . "' ");
                    $this->prdbh->beginTransaction();
                    if ($sql->execute()) {
                        $this->prdbh->commit();
                    } else {
                        $response['message'] = "NO UPDATE";
                    }
                    $sql4 = $this->prdbh->prepare("SELECT GateName FROM GatenoEntry WHERE ID='" . $rec->GateNnrID . "'  ");
                    $sql4->execute();
                    if ($sql4->rowCount() > 0) {
                        while ($row4 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                            $gatenbr = $row4['GateName'];
                        }
                    }
                    $sql3 = $this->prdbh->prepare("SELECT ResidentID FROM ResidentRWAMapping LEFT JOIN HelperSubscribe ON ResidentRWAMapping.ResidentRWAID = HelperSubscribe.ResidentRWAID WHERE ResidentRWAMapping.RWAID='" . $rwaid . "' AND HelperSubscribe.HelperID='" . $helperid . "' AND HelperSubscribe.Subscribe='1' ");
                    $sql3->execute();
                    if ($sql3->rowCount() > 0) {
                        $UpushId1 = array();
                        while ($row3 = $sql3->fetch(PDO::FETCH_ASSOC)) {
                            $UpushId1[] = $row3['ResidentID'];
                        }
                        $UpushId = implode(',', $UpushId1);
                        $date = date('h:i a', $timestamp + 19660);
                        if ($is_in == '1') {
                            $in = "in";
                        } elseif ($is_in == '2') {
                            $in = "out";
                        }
                        $message = $name . ' has checked ' . $in . ' at ' . $date . ' from  ' . $gatenbr;
                        $UrHd1 = 'mynukad helper attendance';
                        //$ID='C'.$id;
                        $message1 = array("message" => $message, "UrHd2" => $UrHd1, "body" => $helperid);
                        $result = $this->firebasenotification($UpushId, $message1);
                        $response['PushStatus'] = $result;
                    }
                } else {
                    $response['Message'] = "Invalid QR code for this society"; //change by noopur
                    $response['Status'] = 0;
                }
            } else {
                $response['Message'] = "Invalid QR code OR Helper Card validity expired, please contact Maintenance or relevant authority";
                $response['Status'] = 0;
            }
        } else {
            $response['Message'] = "Invalid QR code OR Helper Card validity expired, please contact Maintenance or relevant authority";
            $response['Status'] = 0;
        }
        return $response;
    }
    public function maintenancestaffattendancetwo($rec) {
        $Status = $rec->Status;
        $Status1 = rtrim($Status, ",");
        $Statusarray = explode(",", $Status1);
        $StaffQRCode = $rec->StaffQRCode;
        $StaffQRCode1 = rtrim($StaffQRCode, ",");
        $StaffQRCodearray = explode(",", $StaffQRCode1);
        $timestamp = $rec->timestamp;
        $timestamp1 = rtrim($timestamp, ",");
        $timestamparray = explode(",", $timestamp1);
        $RWAID = $rec->RWAID;
        for ($i = 0;$i < count($StaffQRCodearray);$i++) {
            $sql1 = $this->prdbh->prepare("SELECT ID,RWAID,Name,ProfilePic FROM MaintenanceStaff WHERE  StaffQRCode='" . $StaffQRCodearray[$i] . "' ");
            $sql1->execute();
            if ($sql1->execute()) {
                $count = $sql1->rowCount();
                if ($count >= 1) {
                    while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                        $staffid = $row1['ID'];
                        $rwaid = $row1['RWAID'];
                        $name = $row1['Name'];
                        $ProfilePhoto = $row1['ProfilePic'];
                        if ($ProfilePhoto != '') {
                            $ProfilePhoto == $ProfilePhoto;
                        } else {
                            $ProfilePhoto = 'https://s3.amazonaws.com/mynukad-ios/HTMLFiles/helper_placeholder.png';
                        }
                    }
                    if ($RWAID == $rwaid) {
                        $sql3 = $this->prdbh->prepare("UPDATE MaintenanceStaff SET Status= '" . $Status . "' WHERE ID= '" . $staffid . "' ");
                        $this->prdbh->beginTransaction();
                        if ($sql3->execute()) {
                            $this->prdbh->commit();
                        } else {
                            $response['MSG'] = "NO UPDATE";
                        }
                        $sql2 = $this->prdbh->prepare("INSERT INTO MaintenanceStaffAttendance (MaintenanceStaffID, timestamp, RWAID, GateNnrID, GatekeeperID, Status)  VALUES(?,?,?,?,?,?)");
                        $sql2->bindParam(1, $staffid);
                        $sql2->bindParam(2, $timestamparray[$i]);
                        $sql2->bindParam(3, $rwaid);
                        $sql2->bindParam(4, $rec->GateNnrID);
                        $sql2->bindParam(5, $rec->GatekeeperID);
                        $sql2->bindParam(6, $Statusarray[$i]);
                        $this->prdbh->beginTransaction();
                        if ($sql2->execute()) {
                            $this->prdbh->commit();
                            if ($Status == '1') {
                                $response['Message'] = "Staff is in";
                            } elseif ($Status == '2') {
                                $response['Message'] = "Staff is out";
                            }
                            $response['Status'] = "1";
                            $response['HelperName'] = $name;
                            $response['HelperPhoto'] = $ProfilePhoto;
                        }
                    } else {
                        $response['Message'] = "Invalid QR code for this society";
                        $response['Status'] = 0;
                    }
                } else {
                    $response['Message'] = "Invalid QR code";
                    $response['Status'] = 0;
                }
            } else {
                $response['Message'] = $sql1->errorInfo();
                $response['Status'] = 0;
            }
        }
        return $response;
    }
    public function maintenancestaffattendance($rec) {
        $StaffQRCode = $rec->StaffQRCode;
        $timestamp = $rec->timestamp;
        $Status = $rec->Status;
        $RWAID = $rec->RWAID;
        $StaffQRCode1 = substr($StaffQRCode, 2);
        $sql1 = $this->prdbh->prepare("SELECT ID,RWAID,Name,ProfilePic FROM MaintenanceStaff WHERE  StaffQRCode='" . $StaffQRCode1 . "' ");
        $sql1->execute();
        if ($sql1->execute()) {
            $count = $sql1->rowCount();
            if ($count >= 1) {
                while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                    $staffid = $row1['ID'];
                    $rwaid = $row1['RWAID'];
                    $name = $row1['Name'];
                    $ProfilePhoto = $row1['ProfilePic'];
                    if ($ProfilePhoto != '') {
                        $ProfilePhoto == $ProfilePhoto;
                    } else {
                        $ProfilePhoto = 'https://s3.amazonaws.com/mynukad-ios/HTMLFiles/helper_placeholder.png';
                    }
                }
                if ($RWAID == $rwaid) {
                    $sql3 = $this->prdbh->prepare("UPDATE MaintenanceStaff SET Status= '" . $Status . "' WHERE ID= '" . $staffid . "' ");
                    $this->prdbh->beginTransaction();
                    if ($sql3->execute()) {
                        $this->prdbh->commit();
                    } else {
                        $response['MSG'] = "NO UPDATE";
                    }
                    $sql2 = $this->prdbh->prepare("INSERT INTO MaintenanceStaffAttendance (MaintenanceStaffID, timestamp, RWAID, GateNnrID, GatekeeperID, Status)  VALUES(?,?,?,?,?,?)");
                    $sql2->bindParam(1, $staffid);
                    $sql2->bindParam(2, $timestamp);
                    $sql2->bindParam(3, $rwaid);
                    $sql2->bindParam(4, $rec->GateNnrID);
                    $sql2->bindParam(5, $rec->GatekeeperID);
                    $sql2->bindParam(6, $Status);
                    $this->prdbh->beginTransaction();
                    if ($sql2->execute()) {
                        $this->prdbh->commit();
                        if ($Status == '1') {
                            $response['Message'] = "Staff is in";
                        } elseif ($Status == '2') {
                            $response['Message'] = "Staff is out";
                        }
                        $response['Status'] = "1";
                        $response['HelperName'] = $name;
                        $response['HelperPhoto'] = $ProfilePhoto;
                    }
                } else {
                    $response['Message'] = "Invalid QR code for this society"; //change by noopur
                    $response['Status'] = 0;
                }
            } else {
                $response['Message'] = "Invalid QR code";
                $response['Status'] = 0;
            }
        } else {
            $response['Message'] = $sql1->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function neighbors($param) {
        if (isset($param->RWAID) && is_numeric($param->RWAID) && isset($param->UID) && is_numeric($param->UID)) {
            $RWAID = $param->RWAID;
            $UID = $param->UID;
        } else {
            $response['Neighbours'] = "all fields are mandatory";
            $response['Status'] = '0';
            return $response;
        }
        $PageNo = 1;
        $PageNo = $param->PageNo;
        if ($PageNo <= 0) {
            $PageNo = 1;
        }
        $Start = $PageNo * 10 - 10;
        $response = array();
        if (isset($param->FirstName) || isset($param->Occupation) || isset($param->FlatNbr) || isset($param->VehicleNbr)) {
            if (isset($param->FirstName) && $param->FirstName != '') {
                $FirstName = $param->FirstName;
                $sql = $this->prdbh->prepare("SELECT DISTINCT User.ID, User.PhoneNbr,User.IsPhonePublic, User.EmailID,SocietyFlatMapping.FlatNbr, User.FirstName, User.MiddleName, User.LastName, User.ProfilePic, User.Gender, User.Occupation,User.About,from_unixtime((User.AddedOn+ 19660),'%d/%m/%Y  %h:%i %p') as AddedOn,ResidentRWAMapping.ApprovedBy,from_unixtime((ResidentRWAMapping.ApprovedOn+ 19660),'%d/%m/%Y  %h:%i %p') as ApprovedOn, User.IsActive FROM
                           User INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentID = User.ID INNER JOIN ResidentFlatMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID  WHERE ResidentRWAMapping.ApprovalStatus= 'Y' AND User.ID != '" . $UID . "' AND ResidentRWAMapping.RWAID ='" . $RWAID . "' AND User.FirstName LIKE '%" . $FirstName . "%'  GROUP BY User.ID ");
            } else if (isset($param->Occupation) && $param->Occupation != '') {
                $Occupation = $param->Occupation;
                $sql = $this->prdbh->prepare("SELECT DISTINCT User.ID, User.PhoneNbr,User.IsPhonePublic, User.EmailID, SocietyFlatMapping.FlatNbr, User.FirstName, User.MiddleName, User.LastName, User.ProfilePic, User.Gender, User.Occupation,User.About, from_unixtime((User.AddedOn+ 19660),'%d/%m/%Y  %h:%i %p') as AddedOn,ResidentRWAMapping.ApprovedBy,from_unixtime((ResidentRWAMapping.ApprovedOn+ 19660),'%d/%m/%Y  %h:%i %p') as ApprovedOn, User.IsActive FROM
                           User INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentID = User.ID INNER JOIN ResidentFlatMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID  WHERE ResidentRWAMapping.ApprovalStatus= 'Y' AND User.ID != '" . $UID . "' AND ResidentRWAMapping.RWAID = '" . $RWAID . "' AND User.Occupation LIKE '%" . $Occupation . "%' GROUP BY User.ID ");
            } else if (isset($param->FlatNbr) && $param->FlatNbr != '') {
                $FlatNbr = $param->FlatNbr;
                $sql = $this->prdbh->prepare("SELECT User.ID, User.PhoneNbr,User.IsPhonePublic, User.EmailID, SocietyFlatMapping.FlatNbr, User.FirstName, User.MiddleName, User.LastName, User.ProfilePic, User.Gender, User.Occupation,User.About, from_unixtime((User.AddedOn+ 19660),'%d/%m/%Y  %h:%i %p') as AddedOn,ResidentRWAMapping.ApprovedBy,from_unixtime((ResidentRWAMapping.ApprovedOn+ 19660),'%d/%m/%Y  %h:%i %p') as ApprovedOn, User.IsActive FROM
                           User INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentID = User.ID INNER JOIN ResidentFlatMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID  INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID  WHERE ResidentRWAMapping.ApprovalStatus= 'Y' AND User.ID != '" . $UID . "' AND ResidentRWAMapping.RWAID =  '" . $RWAID . "' AND SocietyFlatMapping.FlatNbr LIKE '%" . $FlatNbr . "%' GROUP BY User.ID ");
            } else if (isset($param->VehicleNbr) && $param->VehicleNbr != '') {
                $VehicleNbr = $param->VehicleNbr;
                $sql = $this->prdbh->prepare("SELECT User.ID, User.PhoneNbr,User.IsPhonePublic, User.EmailID, SocietyFlatMapping.FlatNbr, User.FirstName, User.MiddleName, User.LastName, User.ProfilePic, User.Gender, User.Occupation,User.About, from_unixtime((User.AddedOn+ 19660),'%d/%m/%Y  %h:%i %p') as AddedOn,ResidentVehicleByParking.VehicleNbr,ResidentRWAMapping.ApprovedBy,from_unixtime((ResidentRWAMapping.ApprovedOn+ 19660),'%d/%m/%Y  %h:%i %p') as ApprovedOn, User.IsActive FROM
                           User INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentID = User.ID INNER JOIN ResidentFlatMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID   INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID LEFT JOIN ParkingTable ON ParkingTable.Flat_ID=SocietyFlatMapping.ID LEFT JOIN ResidentVehicleByParking ON ResidentVehicleByParking.   ParkingID=ParkingTable.ID WHERE ResidentRWAMapping.ApprovalStatus= 'Y' AND User.ID != '" . $UID . "' AND ResidentRWAMapping.RWAID =  '" . $RWAID . "' AND ResidentVehicleByParking.VehicleNbr LIKE '%" . $VehicleNbr . "%' GROUP BY User.ID ");
            }
            if (isset($sql)) {
                $sql->execute();
                $items = array();
                if ($sql->rowCount() > 0) {
                    while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                        $id = $row['ApprovedBy'];
                        $sql1 = $this->prdbh->prepare("SELECT (User.ID) as ApID, (User.FirstName) as ApName, (User.About)  as ApAbout,(User.ProfilePic) as ApProfilePic,(SocietyFlatMapping.FlatNbr) as ApFlatNbr FROM User LEFT JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentID = User.ID LEFT JOIN ResidentFlatMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID WHERE User.ID= '" . $id . "'");
                        $sql1->execute();
                        if ($sql1) {
                            $subitems = array();
                            $count1 = $sql->rowCount();
                            if ($count1 > 0) {
                                $subitems = array();
                                while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                                    $subitems['ApprovedByUser'] = $row1;
                                    $row = array_merge($row, $subitems);
                                }
                            }
                        }
                        $items[] = $row;
                    }
                    $response['Neighbours'] = $items;
                    $response['Status'] = '1';
                } else {
                    $response['Neighbours'] = [];
                    $response['Status'] = '0';
                }
            } else {
                $response['Neighbours'] = [];
                $response['Status'] = '0';
            }
        } else {
            $sql = $this->prdbh->prepare("SELECT DISTINCT User.ID, ResidentRWAMapping.ResidentRWAID as ResidentRWAID,User.PhoneNbr,User.IsPhonePublic, User.EmailID,(SocietyFlatMapping.FlatNbr) as FlatNbr, trim(User.FirstName) as FirstName, User.MiddleName, User.LastName, User.ProfilePic, User.Gender, User.Occupation,User.About,from_unixtime((User.AddedOn+ 19660),'%d/%m/%Y  %h:%i %p') as AddedOn,ResidentVehicleMapping.VehicleNbr,ResidentRWAMapping.ApprovedBy,from_unixtime((ResidentRWAMapping.ApprovedOn+ 19660),'%d/%m/%Y  %h:%i %p') as ApprovedOn, User.IsActive FROM
                           User INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentID = User.ID INNER JOIN ResidentFlatMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID LEFT JOIN ResidentVehicleMapping ON ResidentVehicleMapping. ResidentRWAFlatID=ResidentFlatMapping.ID LEFT JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID  WHERE ResidentRWAMapping.ApprovalStatus= 'Y' AND User.ID != '" . $UID . "' AND ResidentRWAMapping.RWAID ='" . $RWAID . "' GROUP BY User.ID ORDER BY FirstName Limit $Start,10");
            $sql->execute();
            $items = array();
            if ($sql->rowCount() > 0) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $id = $row['ApprovedBy'];
                    $ResidentRWAID = $row['ResidentRWAID'];
                    $sql1 = $this->prdbh->prepare("SELECT (User.ID) as ApID, (User.FirstName) as ApName, (User.About)  as ApAbout,(User.ProfilePic) as ApProfilePic,(SocietyFlatMapping.FlatNbr) as ApFlatNbr FROM User LEFT JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentID = User.ID LEFT JOIN ResidentFlatMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID LEFT JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID  WHERE User.ID= '" . $id . "'");
                    $sql1->execute();
                    if ($sql1) {
                        $subitems = array();
                        $count1 = $sql->rowCount();
                        if ($count1 > 0) {
                            $subitems = array();
                            while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                                $subitems['ApprovedByUser'] = $row1;
                                $row = array_merge($row, $subitems);
                            }
                        }
                    }
                    $items[] = $row;
                }
                $response['Neighbours'] = $items;
                $response['Status'] = '1';
            } else {
                $response['Neighbours'] = [];
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function shopdetail($param) {
        $distance = 150000;
        if (isset($param->lat)) {
            $lat = $param->lat;
        }
        if (isset($param->long)) {
            $long = $param->long;
        }
        if (isset($param->CategoryID)) {
            $CategoryID = $param->CategoryID;
        }
        if (isset($param->UserID)) {
            $UserID = $param->UserID;
        }
        $Name = '';
        $PageNo = '';
        if (isset($param->Name)) {
            $Name = $param->Name;
        }
        if (isset($param->PageNo)) {
            $PageNo = $param->PageNo;
        }
        $Start = $PageNo * 10 - 10;
        $response = array();
        if (!empty($lat) && !empty($long) && !empty($CategoryID) && !empty($distance)) {
            if ($Name != '') {
                $sql = $this->prdbh->prepare("SELECT DISTINCT NearBy.ID,NearBy.* ,(NearByCategory.Description) as Type,case When UserFavouriteNearBy.NearByID IS NULL THEN 0 Else 1 END as IsFavAdded , 
                       ((ACOS(SIN($lat * PI() / 180) * SIN(latitude * PI() / 180) + COS($lat * PI() / 180) * COS(latitude * PI() / 180) * COS(($long - longitude) * PI() / 180)) * 180 / PI()) * 60 * 1.8532) AS distance
                        from NearBy left outer join UserFavouriteNearBy ON NearBy.ID = UserFavouriteNearBy.NearByID And UserFavouriteNearBy.UserID = '" . $UserID . "'
                        INNER JOIN NearByCategory ON NearBy.CategoryID = NearByCategory.ID  having distance < '" . $distance . "'  AND NearBy.IsActive='1' AND NearBy.CategoryID IN($CategoryID) AND NearBy.Name like '" . $Name . "%' ORDER BY distance ASC ");
            } else {
                $sql = $this->prdbh->prepare("SELECT DISTINCT NearBy.ID,NearBy.* ,(NearByCategory.Description) as Type,case When UserFavouriteNearBy.NearByID IS NULL THEN 0 Else 1 END as IsFavAdded , 
                       ((ACOS(SIN($lat * PI() / 180) * SIN(latitude * PI() / 180) + COS($lat * PI() / 180) * COS(latitude * PI() / 180) * COS(($long - longitude) * PI() / 180)) * 180 / PI()) * 60 * 1.8532) AS distance
                        from NearBy left outer join UserFavouriteNearBy ON NearBy.ID = UserFavouriteNearBy.NearByID And UserFavouriteNearBy.UserID = '" . $UserID . "'
                        INNER JOIN NearByCategory ON NearBy.CategoryID = NearByCategory.ID  having distance < '" . $distance . "'  AND NearBy.IsActive='1' AND NearBy.CategoryID IN($CategoryID) ORDER BY distance ASC Limit $Start,10");
            }
        } elseif (!empty($lat) && !empty($long) && empty($CategoryID) && !empty($distance)) {
            if ($Name != '') {
                $sql = $this->prdbh->prepare("SELECT DISTINCT NearBy.ID,NearBy.* ,(NearByCategory.Description) as Type,case When UserFavouriteNearBy.NearByID IS NULL THEN 0 Else 1 END as IsFavAdded , 
                    ((ACOS(SIN($lat * PI() / 180) * SIN(latitude * PI() / 180) + COS($lat * PI() / 180) * COS(latitude * PI() / 180) * COS(($long - longitude) * PI() / 180)) * 180 / PI()) * 60 * 1.8532) AS distance
                    from NearBy left outer join UserFavouriteNearBy ON NearBy.ID = UserFavouriteNearBy.NearByID And UserFavouriteNearBy.UserID = '" . $UserID . "'
                    INNER JOIN NearByCategory ON NearBy.CategoryID = NearByCategory.ID  having distance < '" . $distance . "'  AND NearBy.IsActive='1' AND NearBy.Name like '" . $Name . "%' OR NearByCategory.Description  Like '" . $Name . "%' ORDER BY distance ASC ");
            } else {
                $sql = $this->prdbh->prepare("SELECT DISTINCT NearBy.ID,NearBy.* ,(NearByCategory.Description) as Type,case When UserFavouriteNearBy.NearByID IS NULL THEN 0 Else 1 END as IsFavAdded , 
                    ((ACOS(SIN($lat * PI() / 180) * SIN(latitude * PI() / 180) + COS($lat * PI() / 180) * COS(latitude * PI() / 180) * COS(($long - longitude) * PI() / 180)) * 180 / PI()) * 60 * 1.8532) AS distance
                    from NearBy left outer join UserFavouriteNearBy ON NearBy.ID = UserFavouriteNearBy.NearByID And UserFavouriteNearBy.UserID = '" . $UserID . "'
                    Left JOIN NearByCategory ON NearBy.CategoryID = NearByCategory.ID  having distance < '" . $distance . "'  AND NearBy.IsActive='1'  ORDER BY distance ASC Limit $Start,10");
            }
        } elseif (empty($lat) && empty($long) && !empty($CategoryID)) {
            if ($Name != '') {
                $sql = $this->prdbh->prepare("SELECT DISTINCT NearBy.ID,NearBy.* ,(NearByCategory.Description) as Type,case When UserFavouriteNearBy.NearByID IS NULL THEN 0 Else 1 END as IsFavAdded 
                    from NearBy left outer join UserFavouriteNearBy ON NearBy.ID = UserFavouriteNearBy.NearByID And UserFavouriteNearBy.UserID = '" . $UserID . "'
                    INNER JOIN NearByCategory ON NearBy.CategoryID = NearByCategory.ID WHERE NearBy.CategoryID IN($CategoryID)  AND NearBy.IsActive='1' AND NearBy.Name like '" . $Name . "%' OR NearByCategory.Description  Like '" . $Name . "%' ORDER BY NearBy.Name ASC ");
            } else {
                $sql = $this->prdbh->prepare("SELECT DISTINCT NearBy.ID, NearBy.* ,(NearByCategory.Description) as Type,case When UserFavouriteNearBy.NearByID IS NULL THEN 0 Else 1 END as IsFavAdded 
                    from NearBy left outer join UserFavouriteNearBy ON NearBy.ID = UserFavouriteNearBy.NearByID And UserFavouriteNearBy.UserID = '" . $UserID . "'
                    INNER JOIN NearByCategory ON NearBy.CategoryID = NearByCategory.ID WHERE NearBy.CategoryID IN($CategoryID)  AND NearBy.IsActive='1'  ORDER BY NearBy.Name ASC Limit $Start,10");
            }
        } else {
            if ($Name != '') {
                $sql = $this->prdbh->prepare("SELECT DISTINCT NearBy.ID, NearBy.* ,(NearByCategory.Description) as Type,case When UserFavouriteNearBy.NearByID IS NULL THEN 0 Else 1 END as IsFavAdded 
                    from NearBy left outer join UserFavouriteNearBy ON NearBy.ID = UserFavouriteNearBy.NearByID And UserFavouriteNearBy.UserID = '" . $UserID . "'
                    LEFT JOIN NearByCategory ON NearBy.CategoryID = NearByCategory.ID  AND NearBy.IsActive='1'  AND NearBy.Name like '" . $Name . "%' OR NearByCategory.Description  Like '" . $Name . "%' ORDER BY NearBy.Name ASC ");
            } else {
                $sql = $this->prdbh->prepare("SELECT DISTINCT NearBy.ID, NearBy.* ,(NearByCategory.Description) as Type,case When UserFavouriteNearBy.NearByID IS NULL THEN 0 Else 1 END as IsFavAdded 
                    from NearBy left outer join UserFavouriteNearBy ON NearBy.ID = UserFavouriteNearBy.NearByID And UserFavouriteNearBy.UserID = '" . $UserID . "'
                    INNER JOIN NearByCategory ON NearBy.CategoryID = NearByCategory.ID WHERE  NearBy.IsActive='1' ORDER BY NearBy.Name ASC Limit $Start,10");
            }
        }
        $sql->execute();
        //print_r($sql);die;
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['shop'] = $items;
                $response['Status'] = '1';
            } else {
                $response['shop'] = 'No more Shoplist';
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function shopdetailbyid($param) {
        if (isset($param->ShopID)) {
            $ShopID = $param->ShopID;
        }
        if (isset($param->UserID)) {
            $UserID = $param->UserID;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT DISTINCT NearBy.ID, NearBy.* ,(NearByCategory.Description) as Type,case When UserFavouriteNearBy.NearByID IS NULL THEN 0 Else 1 END as IsFavAdded 
        from NearBy left outer join UserFavouriteNearBy ON NearBy.ID = UserFavouriteNearBy.NearByID And UserFavouriteNearBy.UserID = '" . $UserID . "' INNER JOIN NearByCategory ON NearBy.CategoryID = NearByCategory.ID WHERE  NearBy.IsActive='1' AND NearBy.ID='" . $ShopID . "' ");
        $sql->execute();
        //print_r($sql);die;
        if ($sql) {
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items = $row;
                }
                $response['shop'] = $items;
                $response['Status'] = '1';
            } else {
                $response['shop'] = 'No more Shoplist';
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function showmaintenancestaffdetail($param) {
        if (isset($param->StaffID)) {
            $StaffID = $param->StaffID;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT MaintenanceStaff.ID, MaintenanceStaff.Name, MaintenanceStaff.ProfilePic, MaintenanceStaff.RWAID, (ComplaintCategory.Description ) as ServiceCategory,(MaintenanceStaff.ServiceCategory) as CategoryID, MaintenanceStaff.PrimaryContactNbr, MaintenanceStaff.SecondaryContactNbr, MaintenanceStaff.AddedOn,MaintenanceStaff.AddedBy,MaintenanceStaff.StaffDesignation,MaintenanceStaff.EmployeeCode, MaintenanceStaff.IsActive, MaintenanceStaff.StaffQRCode,MaintenanceStaff.OtherDocScanImage1,MaintenanceStaff.OtherDocScanImage2,MaintenanceStaff.OtherDocScanImage3,MaintenanceStaff.StaffAddress  FROM MaintenanceStaff  WHERE ID = '" . $StaffID . "' ");
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                $i = 0;
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $tdat = date("U") + 600;
                    $t24 = $tdat - 86400;
                    $t48 = $tdat - 172800;
                    $sql7 = $this->prdbh->prepare("SELECT timestamp ,Status FROM MaintenanceStaffAttendance WHERE MaintenanceStaffID =  '" . $staffid . "'  ORDER BY timestamp DESC limit 1");
                    $sql7->execute();
                    $count1 = $sql7->rowCount();
                    if ($count1 == 1) {
                        while ($row7 = $sql7->fetch(PDO::FETCH_ASSOC)) {
                            $timestamp = $row7['timestamp'];
                            $is_in = $row7['is_in'];
                        }
                        if ($timestamp <= $t48 && $is_in == 1) {
                            $color1 = "Red";
                        } else if (($timestamp > $t48 && $is_in == 1) && ($timestamp <= $t24 && $is_in == 1)) {
                            $color1 = "Yellow";
                        } else if (($timestamp > $t24 && $is_in == 1) && ($timestamp <= $tdat && $is_in == 1)) {
                            $color1 = "Green";
                        } elseif ($is_in == 2 || $is_in == 0) {
                            $color1 = "Grey";
                        }
                    } else {
                        $color1 = "Grey";
                    }
                    $response['MaintenanceStaff'][$i] = $row;
                    $response['MaintenanceStaff'][$i]['color'] = $color1;
                }
                $response['Status'] = 1;
            } else {
                $response['Maintenance Staff'] = "No data";
                $response['Status'] = 0;
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function showmaintenancestaff($param) {
        $type = '';
        $name = '';
        $PageNo = '';
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        if (isset($param->PageNo)) {
            $PageNo = $param->PageNo;
        }
        if (isset($param->Name)) {
            $name = $param->Name;
        }
        if (isset($param->Type)) {
            $type = $param->Type;
        }
        $tdat = date("U");
        $a = $tdat + 120;
        $Start = $PageNo * 10 - 10;
        $response = array();
        //print_r($name);
        if ($name != '') {
            $sql = $this->prdbh->prepare("SELECT MaintenanceStaff.ID, MaintenanceStaff.Name, MaintenanceStaff.ProfilePic, MaintenanceStaff.RWAID, (ComplaintCategory.Description ) as ServiceCategory,(MaintenanceStaff.ServiceCategory) as CategoryID, MaintenanceStaff.PrimaryContactNbr, MaintenanceStaff.SecondaryContactNbr, MaintenanceStaff.AddedOn,MaintenanceStaff.AddedBy,MaintenanceStaff.StaffDesignation,MaintenanceStaff.EmployeeCode, MaintenanceStaff.IsActive, MaintenanceStaff.StaffQRCode,MaintenanceStaff.OtherDocScanImage1,MaintenanceStaff.OtherDocScanImage2,MaintenanceStaff.OtherDocScanImage3,MaintenanceStaff.StaffAddress  FROM MaintenanceStaff INNER JOIN ComplaintCategory ON ComplaintCategory.ID=MaintenanceStaff.ServiceCategory  WHERE MaintenanceStaff.RWAID = '" . $RWAID . "' AND MaintenanceStaff.Name like '%" . $name . "%'  GROUP BY ID DESC ");
        } elseif ($type != '') {
            $sql = $this->prdbh->prepare("SELECT MaintenanceStaff.ID, MaintenanceStaff.Name, MaintenanceStaff.ProfilePic, MaintenanceStaff.RWAID, (ComplaintCategory.Description ) as ServiceCategory,(MaintenanceStaff.ServiceCategory) as CategoryID, MaintenanceStaff.PrimaryContactNbr, MaintenanceStaff.SecondaryContactNbr, MaintenanceStaff.AddedOn,MaintenanceStaff.AddedBy,MaintenanceStaff.StaffDesignation,MaintenanceStaff.EmployeeCode, MaintenanceStaff.IsActive, MaintenanceStaff.StaffQRCode,MaintenanceStaff.OtherDocScanImage1,MaintenanceStaff.OtherDocScanImage2,MaintenanceStaff.OtherDocScanImage3,MaintenanceStaff.StaffAddress  FROM MaintenanceStaff INNER JOIN ComplaintCategory ON ComplaintCategory.ID=MaintenanceStaff.ServiceCategory   WHERE MaintenanceStaff.RWAID = '" . $RWAID . "' AND MaintenanceStaff.ServiceCategory IN('" . $type . "')  GROUP BY ID DESC ");
        } else {
            $sql = $this->prdbh->prepare("SELECT MaintenanceStaff.ID, MaintenanceStaff.Name, MaintenanceStaff.ProfilePic, MaintenanceStaff.RWAID, (ComplaintCategory.Description ) as ServiceCategory,(MaintenanceStaff.ServiceCategory) as CategoryID, MaintenanceStaff.PrimaryContactNbr, MaintenanceStaff.SecondaryContactNbr, MaintenanceStaff.AddedOn,MaintenanceStaff.AddedBy,MaintenanceStaff.StaffDesignation,MaintenanceStaff.EmployeeCode, MaintenanceStaff.IsActive, MaintenanceStaff.StaffQRCode,MaintenanceStaff.OtherDocScanImage1,MaintenanceStaff.OtherDocScanImage2,MaintenanceStaff.OtherDocScanImage3,MaintenanceStaff.StaffAddress  FROM MaintenanceStaff INNER JOIN ComplaintCategory ON ComplaintCategory.ID=MaintenanceStaff.ServiceCategory  WHERE MaintenanceStaff.RWAID = '" . $RWAID . "' group by MaintenanceStaff.ID ORDER BY Status, Name ASC Limit $Start,10");
        }
        $sql->execute();
        //print_r($sql);die;
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                $i = 0;
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                    $staffid = $row['ID'];
                    $tdat = date("U") + 600;
                    $t24 = $tdat - 86400;
                    $t48 = $tdat - 172800;
                    $sql7 = $this->prdbh->prepare("SELECT timestamp ,Status FROM MaintenanceStaffAttendance WHERE MaintenanceStaffID =  '" . $staffid . "'  ORDER BY timestamp DESC limit 1");
                    $sql7->execute();
                    //print_r($sql7);die;
                    $count1 = $sql7->rowCount();
                    if ($count1 == 1) {
                        while ($row7 = $sql7->fetch(PDO::FETCH_ASSOC)) {
                            $timestamp = $row7['timestamp'];
                            $is_in = $row7['Status'];
                        }
                        if ($timestamp <= $t48 && $is_in == 1) {
                            $color1 = "Red";
                        } else if (($timestamp > $t48 && $is_in == 1) && ($timestamp <= $t24 && $is_in == 1)) {
                            $color1 = "Yellow";
                        } else if (($timestamp > $t24 && $is_in == 1) && ($timestamp <= $tdat && $is_in == 1)) {
                            $color1 = "Green";
                        } elseif ($is_in == 2 || $is_in == 0) {
                            $color1 = "Grey";
                        }
                    } else {
                        $color1 = "Grey";
                    }
                    $items[$i]['color'] = $color1;
                    $i++;
                }
                $response['Maintenance Staff'] = $items;
                $response['Status'] = 1;
            } else {
                $response['Maintenance Staff'] = "No data";
                $response['message'] = "No Data";
                $response['Status'] = 0;
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function notification($param) {
        $Search = '';
        $Filter = '';
        $UserID = '';
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        if (isset($param->PageNo)) {
            $PageNo = $param->PageNo;
        }
        if (isset($param->Search)) {
            $Search = $param->Search;
        }
        if (isset($param->UserID)) {
            $UserID = $param->UserID;
        }
        if (isset($param->Filter)) {
            $Filter = $param->Filter;
        }
        $tdat = date("U");
        $a = $tdat + 120;
        $c = '1296000'; // 1 day time stemp is 86400 s
        $b = $a - $c;
        $Start = $PageNo * 10 - 10;
        $response = array();
        $FunctionID = '';
        $sql6 = $this->prdbh->prepare("SELECT RoleFunctions.FunctionID FROM RoleFunctions INNER JOIN UserRoles ON UserRoles.RoleID=RoleFunctions.RoleID INNER JOIN ResidentRWAMapping ON
            ResidentRWAMapping.ResidentRWAID=UserRoles.ResidentRWAId WHERE ResidentRWAMapping.ResidentID='" . $UserID . "' ");
        $sql6->execute();
        $count6 = $sql6->rowCount();
        if ($sql6->rowCount() >= 1) {
            $FunctionID = array();
            while ($row6 = $sql6->fetch(PDO::FETCH_ASSOC)) {
                $FunctionID[] = $row6['FunctionID'];
            }
        }
        if (in_array(2, $FunctionID)) {
            if ($Search != '') {
                $sql = $this->prdbh->prepare("SELECT Notifications.ID, Notifications.RWAID, Notifications.Severity,Notifications.Title, Notifications.Text,Notifications.Image1,Notifications.Image2,Notifications.Image3,Notifications.IsCalendarEvent,from_unixtime((Notifications.Datecreated+ 19660),'%d/%m/%Y  %h:%i %p') as  Datecreated, Notifications.CreatedBy, Notifications.EventStartDate, Notifications.EventEndDateTime, Notifications.Status,Notifications.CategoryWise FROM Notifications   WHERE RWAID = '" . $RWAID . "'  AND (FROM_BASE64(Title) like '%" . $Search . "%'  OR FROM_BASE64(Text) like '%" . $Search . "%')    GROUP BY ID DESC ");
            } elseif ($Filter != '') {
                $sql = $this->prdbh->prepare("SELECT Notifications.ID, Notifications.RWAID, Notifications.Severity,Notifications.Title, Notifications.Text,Notifications.Image1,Notifications.Image2,Notifications.Image3,Notifications.IsCalendarEvent,from_unixtime((Notifications.Datecreated+ 19660),'%d/%m/%Y  %h:%i %p') as  Datecreated, Notifications.CreatedBy, Notifications.EventStartDate, Notifications.EventEndDateTime, Notifications.Status,Notifications.CategoryWise FROM Notifications WHERE RWAID = '" . $RWAID . "' AND Severity IN (" . $Filter . ")  GROUP BY ID DESC Limit $Start,10 ");
            } else {
                $sql = $this->prdbh->prepare("SELECT Notifications.ID, Notifications.RWAID, Notifications.Severity,Notifications.Title, Notifications.Text,Notifications.Image1,Notifications.Image2,Notifications.Image3,Notifications.IsCalendarEvent,from_unixtime((Notifications.Datecreated+ 19660),'%d/%m/%Y  %h:%i %p') as  Datecreated, Notifications.CreatedBy, Notifications.EventStartDate, Notifications.EventEndDateTime, Notifications.Status,Notifications.CategoryWise FROM Notifications  WHERE Notifications.RWAID = '" . $RWAID . "'  GROUP BY ID DESC Limit $Start,10");
            }
        } else {
            if ($Search != '') {
                $sql = $this->prdbh->prepare("SELECT Notifications.ID, Notifications.RWAID, Notifications.Severity,Notifications.Title, Notifications.Text,Notifications.Image1,Notifications.Image2,Notifications.Image3,Notifications.IsCalendarEvent,from_unixtime((Notifications.Datecreated+ 19660),'%d/%m/%Y  %h:%i %p') as  Datecreated, Notifications.CreatedBy, Notifications.EventStartDate, Notifications.EventEndDateTime, Notifications.Status,Notifications.CategoryWise FROM Notifications INNER JOIN RWANoticeMapping ON RWANoticeMapping.NoticeID= Notifications.ID  WHERE RWAID = '" . $RWAID . "' AND RWANoticeMapping.ResidentID='" . $UserID . "' AND (FROM_BASE64(Title) like '%" . $Search . "%'  OR FROM_BASE64(Text) like '%" . $Search . "%')    GROUP BY ID DESC ");
            } elseif ($Filter != '') {
                $sql = $this->prdbh->prepare("SELECT Notifications.ID, Notifications.RWAID, Notifications.Severity,Notifications.Title, Notifications.Text,Notifications.Image1,Notifications.Image2,Notifications.Image3,Notifications.IsCalendarEvent,from_unixtime((Notifications.Datecreated+ 19660),'%d/%m/%Y  %h:%i %p') as  Datecreated, Notifications.CreatedBy, Notifications.EventStartDate, Notifications.EventEndDateTime, Notifications.Status,Notifications.CategoryWise FROM Notifications INNER JOIN RWANoticeMapping ON RWANoticeMapping.NoticeID= Notifications.ID  WHERE RWAID = '" . $RWAID . "' AND Severity IN (" . $Filter . ") AND RWANoticeMapping.ResidentID='" . $UserID . "'  GROUP BY ID DESC Limit $Start,10 ");
            } else {
                $sql = $this->prdbh->prepare("SELECT Notifications.ID, Notifications.RWAID, Notifications.Severity,Notifications.Title, Notifications.Text,Notifications.Image1,Notifications.Image2,Notifications.Image3,Notifications.IsCalendarEvent,from_unixtime((Notifications.Datecreated+ 19660),'%d/%m/%Y  %h:%i %p') as  Datecreated, Notifications.CreatedBy, Notifications.EventStartDate, Notifications.EventEndDateTime, Notifications.Status,Notifications.CategoryWise FROM Notifications INNER JOIN RWANoticeMapping ON RWANoticeMapping.NoticeID= Notifications.ID WHERE Notifications.RWAID = '" . $RWAID . "' AND RWANoticeMapping.ResidentID='" . $UserID . "' GROUP BY ID DESC Limit $Start,10");
            }
        }
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $notificationid = $row['ID'];
                    $sql3 = $this->prdbh->prepare("SELECT  NotificationID, NotificationImage FROM SocietyNotificationImage 
                                                WHERE NotificationID='" . $notificationid . "'");
                    $sql3->execute();
                    if ($sql3->rowCount() > 0) {
                        $NotificationImag = array();
                        while ($row3 = $sql3->fetch(PDO::FETCH_ASSOC)) {
                            $NotificationImag[] = $row3['NotificationImage'];
                        }
                        $imageString = implode(',', $NotificationImag);
                        $img['Image'] = $imageString;
                    } else {
                        $img['Image'] = " ";
                    }
                    $row = array_merge($row, $img);
                    $items[] = $row;
                }
                $sql2 = $this->prdbh->prepare("SELECT ID,  Severity,Title, Text FROM Notifications WHERE RWAID = '" . $RWAID . "' ");
                $sql2->execute();
                $count2 = $sql2->rowCount();
                $response['Count'] = $count2;
                $response['notification'] = $items;
                $response['Status'] = 1;
            } else {
                $response['notification'] = "No data";
                $response['Status'] = 0;
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function versioncheck($param) {
        if (isset($param->OSVersion)) {
            $OSVersion = $param->OSVersion;
        }
        if (isset($param->APPVersion)) {
            $APPVersion = $param->APPVersion;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT  `OSVersion`, `CurrentVersion`, `ForceUpdateMinVersion`, `ForceUpdateMessage`, `SuggestUpdateMessage` FROM `os_version` WHERE OSVersion = '" . $OSVersion . "'   ");
        $sql->execute();
        $b = array();
        $c = array();
        $count = $sql->rowCount();
        if ($count >= 1) {
            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                $fm = $row['SuggestUpdateMessage'];
                $sm = $row['ForceUpdateMessage'];
                $b = $row['CurrentVersion'];
                $c = $row['ForceUpdateMinVersion'];
            }
        }
        if ($APPVersion < $c) {
            $response['message'] = $fm;
            $response['Status'] = 2;
        } elseif ($APPVersion > $c && $APPVersion < $b) {
            $response['message'] = $sm;
            $response['Status'] = 1;
        } else {
            $response['message'] = "App is uptodate";
            $response['Status'] = 0;
        }
        return $response;
    }
    public function nearbannerimage($param) {
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        $tdat = date("U");
        $c = $tdat + 120;
        $response = array();
        $sql = $this->prdbh->prepare("SELECT ID, RWAID, BannerImageUrl, BannerImageSmallUrl, DateUploded, StartDate, EndDate, ExternalLink, Addedby, IsActive,BannerType,RefID FROM Banners WHERE RWAID ='" . $RWAID . "' OR RWAID is NULL AND IsActive = '1'  AND '" . $c . "'>StartDate AND EndDate>'" . $c . "' ");
        $sql->execute();
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['Banner Image'] = $items;
                $response['Status'] = 1;
            } else {
                $sql1 = $this->prdbh->prepare("SELECT ID, RWAID, BannerImageUrl, BannerImageSmallUrl, DateUploded, StartDate, EndDate, ExternalLink, Addedby, IsActive FROM Banners WHERE RWAID is NULL AND IsActive = '1'  AND '" . $c . "'>StartDate AND EndDate>'" . $c . "' ");
                $sql1->execute();
                $count1 = $sql1->rowCount();
                if ($count1 >= 1) {
                    while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                        $items[] = $row1;
                    }
                    $response['Banner Image'] = $items;
                    $response['Status'] = 1;
                } else {
                    $response['Status'] = 0;
                }
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function offersonhomepage($param) {
        $HomePage = null;
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        if (isset($param->HomePage)) {
            $HomePage = $param->HomePage;
        }
        $tdat = date("U");
        $c = $tdat + 120;
        $response = array();
        if ($HomePage == 1) {
            $sql2 = $this->prdbh->prepare("SELECT (EventListing.ID) as EventID, EventListing.YoutubeChannelID, (EventListing.Description) as EventDescption, EventListing.EventThumbnail, EventListing.IsLive, EventListing.EventCoverHTMLPage,EventListing.VideoID FROM EventListing LEFT JOIN EventRWAMapping  on EventRWAMapping.EventID = EventListing.ID  Where ( EventRWAMapping.RWAID = '" . $RWAID . "' OR EventRWAMapping.RWAID is NULL ) AND EventListing.IsActive = '1'  AND '" . $c . "'>EventListing.StartDate AND EventListing.EndDate>'" . $c . "' ");
        } else {
            $sql2 = $this->prdbh->prepare("SELECT (EventListing.ID) as EventID,EventListing.YoutubeChannelID, (EventListing.Description) as EventDescption, EventListing.EventThumbnail, EventListing.IsLive, EventListing.EventCoverHTMLPage,EventListing.VideoID FROM EventListing LEFT JOIN EventRWAMapping  on EventRWAMapping.EventID = EventListing.ID  Where ( EventRWAMapping.RWAID = '" . $RWAID . "' OR EventRWAMapping.RWAID is NULL ) AND EventListing.IsActive = '1'  AND '" . $c . "'>EventListing.StartDate AND EventListing.EndDate>'" . $c . "'");
        }
        if ($sql2->execute()) {
            $items2 = array();
            $count2 = $sql2->rowCount();
            if ($count2 >= 1) {
                while ($row2 = $sql2->fetch(PDO::FETCH_ASSOC)) {
                    $items2[] = $row2;
                }
                $response['Eventlist'] = $items2;
                //$response['Status1'] =1;
                
            } else {
                if ($HomePage == 1) {
                    $sql3 = $this->prdbh->prepare("SELECT (EventListing.ID) as EventID,EventListing.YoutubeChannelID, (EventListing.Description) as EventDescption, EventListing.EventThumbnail, EventListing.IsLive, EventListing.EventCoverHTMLPage,EventListing.VideoID FROM EventListing LEFT JOIN EventRWAMapping  on EventRWAMapping.EventID = EventListing.ID  Where  EventRWAMapping.RWAID is NULL AND EventListing.IsActive = '1'  AND '" . $c . "'>EventListing.StartDate AND EventListing.EndDate>'" . $c . "' Limit 0,4 ");
                } else {
                    $sql3 = $this->prdbh->prepare("SELECT (EventListing.ID) as EventID,EventListing.YoutubeChannelID, (EventListing.Description) as EventDescption, EventListing.EventThumbnail, EventListing.IsLive, EventListing.EventCoverHTMLPage,EventListing.VideoID FROM EventListing LEFT JOIN EventRWAMapping  on EventRWAMapping.EventID = EventListing.ID  Where EventRWAMapping.RWAID is NULL AND EventListing.IsActive = '1'  AND '" . $c . "'>EventListing.StartDate AND EventListing.EndDate>'" . $c . "' ");
                }
                $sql3->execute();
                $count3 = $sql3->rowCount();
                if ($count3 >= 1) {
                    while ($row3 = $sql3->fetch(PDO::FETCH_ASSOC)) {
                        $items3[] = $row3;
                    }
                    $response['Eventlist'] = $items3;
                    //$response['Status1'] =1;
                    
                } else {
                    $response['Status1'] = 0;
                }
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status1'] = 0;
        }
        if ($HomePage == 1) {
            $sql = $this->prdbh->prepare("SELECT  Offers.OfferDescption, Offers.OfferType, Offers.ReferenceID, Offers.OfferStartDate, Offers.OfferEndDate, Offers.Image, Offers.ExternalUrl FROM Offers INNER JOIN OfferRWAMapping ON OfferRWAMapping.OfferID=Offers.ID WHERE OfferRWAMapping.RWAID ='" . $RWAID . "' OR OfferRWAMapping.RWAID is NULL AND Offers.IsActive = '1'  AND '" . $c . "'>OfferStartDate AND OfferEndDate>'" . $c . "' Limit 0,8 ");
        } else {
            $sql = $this->prdbh->prepare("SELECT  Offers.OfferDescption, Offers.OfferType, Offers.ReferenceID, Offers.OfferStartDate, Offers.OfferEndDate, Offers.Image, Offers.ExternalUrl FROM Offers INNER JOIN OfferRWAMapping ON OfferRWAMapping.OfferID=Offers.ID WHERE OfferRWAMapping.RWAID ='" . $RWAID . "' OR OfferRWAMapping.RWAID is NULL AND Offers.IsActive = '1'  AND '" . $c . "'>OfferStartDate AND OfferEndDate>'" . $c . "' ");
        }
        $sql->execute();
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['Offers'] = $items;
                $response['Status'] = 1;
            } else {
                if ($HomePage == 1) {
                    $sql1 = $this->prdbh->prepare("SELECT  Offers.OfferDescption, Offers.OfferType, Offers.ReferenceID, Offers.OfferStartDate, Offers.OfferEndDate, Offers.Image, Offers.ExternalUrl FROM Offers INNER JOIN OfferRWAMapping ON OfferRWAMapping.OfferID=Offers.ID WHERE OfferRWAMapping.RWAID is NULL AND Offers.IsActive = '1'  AND '" . $c . "'>OfferStartDate AND OfferEndDate>'" . $c . "' Limit 0,8  ");
                } else {
                    $sql1 = $this->prdbh->prepare("SELECT  Offers.OfferDescption, Offers.OfferType, Offers.ReferenceID, Offers.OfferStartDate, Offers.OfferEndDate, Offers.Image, Offers.ExternalUrl FROM Offers INNER JOIN OfferRWAMapping ON OfferRWAMapping.OfferID=Offers.ID WHERE OfferRWAMapping.RWAID is NULL AND Offers.IsActive = '1'  AND '" . $c . "'>OfferStartDate AND OfferEndDate>'" . $c . "' ");
                }
                $sql1->execute();
                $count1 = $sql1->rowCount();
                if ($count1 >= 1) {
                    while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                        $items[] = $row1;
                    }
                    $response['Offers'] = $items;
                    $response['Status'] = 1;
                } else {
                    $response['Status'] = 0;
                }
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function bannerimage($param) {
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        $tdat = date("U");
        $c = $tdat + 120;
        $response = array();
        $sql = $this->prdbh->prepare("SELECT ID, RWAID, BannerImageUrl, BannerImageSmallUrl, DateUploded, StartDate, EndDate, ExternalLink, Addedby, IsActive,BannerType,RefID FROM Banners WHERE ( RWAID ='" . $RWAID . "' OR RWAID is NULL ) AND IsActive = '1'  AND '" . $c . "'>StartDate AND EndDate>'" . $c . "' ");
        $sql->execute();
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['Banner Image'] = $items;
                $response['Status'] = 1;
            } else {
                $sql1 = $this->prdbh->prepare("SELECT ID, RWAID, BannerImageUrl, BannerImageSmallUrl, DateUploded, StartDate, EndDate, ExternalLink, Addedby, IsActive FROM Banners WHERE RWAID is NULL AND IsActive = '1'  AND '" . $c . "'>StartDate AND EndDate>'" . $c . "' ");
                $sql1->execute();
                $count1 = $sql1->rowCount();
                if ($count1 >= 1) {
                    while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                        $items[] = $row1;
                    }
                    $response['Banner Image'] = $items;
                    $response['Status'] = 1;
                } else {
                    $response['Status'] = 0;
                }
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function notificationdetail($param) {
        if (isset($param->ID)) {
            $ID = $param->ID;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT ID, RWAID, Severity,Title, Text ,Image1,Image2,Image3,IsCalendarEvent,from_unixtime((Datecreated+ 19660),'%d/%m/%Y  %h:%i %p') as  Datecreated, CreatedBy, EventStartDate, EventEndDateTime, Status FROM Notifications WHERE ID = '" . $ID . "' ");
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $notificationid = $row['ID'];
                    $sql3 = $this->prdbh->prepare("SELECT  NotificationID, NotificationImage FROM SocietyNotificationImage 
                                                WHERE NotificationID='" . $notificationid . "'");
                    $sql3->execute();
                    if ($sql3->rowCount() > 0) {
                        $NotificationImag = array();
                        while ($row3 = $sql3->fetch(PDO::FETCH_ASSOC)) {
                            $NotificationImag[] = $row3['NotificationImage'];
                        }
                        $imageString = implode(',', $NotificationImag);
                        $img['Image'] = $imageString;
                    } else {
                        $img['Image'] = " ";
                    }
                    $row = array_merge($row, $img);
                    $items[] = $row;
                }
                $response['notification'] = $items;
                $response['Status'] = 1;
            } else {
                $response['notification'] = "No data";
                $response['Status'] = 0;
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function allcount($param) {
        $tdat = date("U");
        $tdate = $tdat - 128800;
        if (isset($param->notification)) {
            $notification = $param->notification;
        }
        if (isset($param->classified)) {
            $classified = $param->classified;
        }
        if (isset($param->polling)) {
            $polling = $param->polling;
        }
        if (isset($param->photogallery)) {
            $photogallery = $param->photogallery;
        }
        if (isset($param->neighbors)) {
            $neighbors = $param->neighbors;
        }
        if (isset($param->UserID)) {
            $UserID = $param->UserID;
        }
        // $UserID='13';
        if (isset($param->complaints)) {
            $complaints = $param->complaints;
        }
        if (isset($param->helper)) {
            $helper = $param->helper;
        }
        if (isset($param->callout)) {
            $callout = $param->callout;
        }
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        $response = array();
        if ($classified <= $tdate) {
            $classified == $tdate;
        }
        if ($polling <= $tdate) {
            $polling == $tdate;
        }
        if ($photogallery <= $tdate) {
            $photogallery == $tdate;
        }
        if ($neighbors <= $tdate) {
            $neighbors == $tdate;
        }
        if ($complaints <= $tdate) {
            $complaints == $tdate;
        }
        if ($helper <= $tdate) {
            $helper == $tdate;
        }
        if ($callout <= $tdate) {
            $callout == $tdate;
        }
        $sql6 = $this->prdbh->prepare("SELECT RoleFunctions.FunctionID as FunctionID FROM RoleFunctions INNER JOIN UserRoles ON UserRoles.RoleID=RoleFunctions.RoleID INNER JOIN ResidentRWAMapping ON
            ResidentRWAMapping.ResidentRWAID=UserRoles.ResidentRWAId WHERE ResidentRWAMapping.ResidentID='" . $UserID . "' ");
        $sql6->execute();
        $count6 = $sql6->rowCount();
        if ($sql6->rowCount() >= 1) {
            $FunctionID = array();
            while ($row6 = $sql6->fetch(PDO::FETCH_ASSOC)) {
                $FunctionID[] = $row6['FunctionID'];
            }
        }
        if (in_array(2, $FunctionID)) {
            $sql = $this->prdbh->prepare("SELECT  Notifications.ID FROM Notifications INNER JOIN RWANoticeMapping ON RWANoticeMapping.NoticeID= Notifications.ID WHERE RWAID = '" . $RWAID . "' AND RWANoticeMapping.ResidentID='" . $UserID . "' AND DateCreated > '" . $notification . "' ");
        } else {
            $sql = $this->prdbh->prepare("SELECT  Notifications.ID FROM Notifications  WHERE RWAID = '" . $RWAID . "'  AND DateCreated > '" . $notification . "' ");
        }
        $sql->execute();
        $count = $sql->rowCount();
        if ($count >= 1) {
            $response['Notificationcount'] = $count;
        } else {
            $response['Notificationcount'] = '0';
        }
        $sql1 = $this->prdbh->prepare("SELECT  RWAID FROM ComplaintRegister WHERE RWAID = '" . $RWAID . "' AND IsCommonAreaComplaint='1' AND  DateCreated > '" . $complaints . "' ");
        $sql1->execute();
        $count1 = $sql1->rowCount();
        if ($count1 >= 1) {
            $response['Complaintcount'] = $count1;
        } else {
            $response['Complaintcount'] = '0';
        }
        $sql2 = $this->prdbh->prepare("SELECT  RWAID FROM Classifieds INNER JOIN ResidentFlatMapping ON ResidentFlatMapping.ID= Classifieds.AddedBy INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID= ResidentFlatMapping.ResidentRWAID  AND RWAID = '" . $RWAID . "'  WHERE Classifieds.AddedOn > '" . $classified . "' ");
        $sql2->execute();
        $count2 = $sql2->rowCount();
        if ($count2 >= 1) {
            $response['Classifiedscount'] = $count2;
        } else {
            $response['Classifiedscount'] = '0';
        }
        $sql3 = $this->prdbh->prepare("SELECT  RWAID FROM PollQuestions INNER JOIN  ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID=PollQuestions.ResidentRWAID  WHERE RWAID = '" . $RWAID . "' AND DateAdded > '" . $polling . "' ");
        $sql3->execute();
        $count3 = $sql3->rowCount();
        if ($count3 >= 1) {
            $response['Pollcount'] = $count3;
        } else {
            $response['Pollcount'] = '0';
        }
        $sql4 = $this->prdbh->prepare("SELECT PhotoGallery.PhotoID,PhotoGallery.PhotoPath from PhotoGallery inner join ResidentRWAMapping on PhotoGallery.AddedBy = ResidentRWAMapping.ResidentRWAID  WHERE  ResidentRWAMapping.RWAID = '" . $RWAID . "' AND Date > '" . $photogallery . "' ");
        $sql4->execute();
        $count4 = $sql4->rowCount();
        if ($count4 >= 1) {
            $response['Photogallery'] = $count4;
        } else {
            $response['Photogallery'] = '0';
        }
        $sql5 = $this->prdbh->prepare("SELECT  FirstName FROM User INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentID = User.ID WHERE RWAID = '" . $RWAID . "' AND ApprovalStatus='Y' AND AddedOn > '" . $neighbors . "' ");
        $sql5->execute();
        $count5 = $sql5->rowCount();
        if ($count4 >= 1) {
            $response['Neighbors'] = $count5;
        } else {
            $response['Neighbors'] = '0';
        }
        $sql6 = $this->prdbh->prepare("SELECT  RWAID, Name, ServiceOffered, ServiceCharges, EmailId,  AddedOn,  IsActive FROM Helpers WHERE RWAID= '" . $RWAID . "'  AND IsActive='1' AND AddedOn > '" . $helper . "' ");
        $sql6->execute();
        $count6 = $sql6->rowCount();
        if ($count6 >= 1) {
            $response['Helpercount'] = $count6;
        } else {
            $response['Helpercount'] = '0';
        }
        $sql7 = $this->prdbh->prepare("SELECT CallOuts.SenderID FROM CallOuts LEFT JOIN ResidentRWAMapping ON CallOuts.SenderID =ResidentRWAMapping.ResidentRWAID WHERE ResidentRWAMapping.RWAID = '" . $RWAID . "' AND DateSent > '" . $callout . "' ");
        $sql7->execute();
        $count7 = $sql7->rowCount();
        if ($count7 >= 1) {
            $response['Calloutcount'] = $count7;
        } else {
            $response['Calloutcount'] = '0';
        }
        return $response;
    }
    public function appnotification($param) {
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        if (isset($param->PageNo)) {
            $PageNo = $param->PageNo;
        }
        $Start = $PageNo * 10 - 10;
        $response = array();
        $sql = $this->prdbh->prepare("SELECT  ID, RWAID, Severity,Title, Text ,Image1,Image2,Image3,IsCalendarEvent,from_unixtime((Datecreated+ 19660),'%d/%m/%Y  %h:%i %p') as  Datecreated, CreatedBy, EventStartDate, EventEndDateTime, Status FROM AppNotifications WHERE RWAID= '" . $RWAID . "' GROUP BY ID DESC  Limit $Start,10 ");
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['Appnotification'] = $items;
                $response['Status'] = 1;
            } else {
                $response['Appnotification'] = '';
                $response['message'] = "No Data";
                $response['Status'] = 0;
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function appnotificationdetail($param) {
        if (isset($param->ID)) {
            $ID = $param->ID;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT  ID, RWAID, Severity,Title, Text ,Image1,Image2,Image3,IsCalendarEvent,from_unixtime((Datecreated+ 19660),'%d/%m/%Y  %h:%i %p') as  Datecreated, CreatedBy, EventStartDate, EventEndDateTime, Status FROM AppNotifications WHERE ID= '" . $ID . "' ");
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['Appnotification'] = $items;
                $response['Status'] = 1;
            } else {
                $response['Appnotification'] = '';
                $response['message'] = "No Data";
                $response['Status'] = 0;
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function helperattendance($param) {
        if (isset($param->time)) {
            $time1 = $param->time;
        }
        if (isset($param->HelperID)) {
            $HelperID = $param->HelperID;
        }
        $timea = date('y-m-d ', $time1);
        $time1 = strtotime("$timea");
        $time = $time1 - 19660;
        $endtime = $time + 86399; // 1 day time stemp is 86400 s
        $response = array();
        $sql1 = $this->prdbh->prepare("SELECT ID, RWAID, Name, ServiceOffered, ServiceCharges, ResidentialAddress, PrimaryContactNbr, PhoneNbr2, EmailId, AddedBy, AddedOn, ProfilePhoto, OtherDocScanImage1, OtherDocScanImage2, OtherDocScanImage3, EntryCardExpiry, IsActive, is_in, HelperQRCode, PoliceVerification, PoliceVerificationDate FROM Helpers WHERE ID='" . $HelperID . "' ");
        if ($sql1->execute()) {
            $items1 = array();
            $count1 = $sql1->rowCount();
            if ($count1 >= 1) {
                while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                    $items1[] = $row1;
                }
                $response['Helper'] = $items1;
            } else {
                $response['Helper'] = "No data";
                $response['Status'] = 0;
            }
        }
        $sql = $this->prdbh->prepare("SELECT Attendance.ID,Attendance.HelperID,Attendance.timestamp,Attendance.RWAID,Attendance.is_in,Attendance.GateNnrID,Attendance.GatekeeperID FROM Attendance LEFT JOIN Helpers ON Helpers.ID=Attendance.HelperID WHERE Attendance.timestamp BETWEEN  '" . $time . "' AND '" . $endtime . "' AND Attendance.HelperID=$HelperID Order by Attendance.timestamp");
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['Attendance'] = $items;
                $response['Status'] = 1;
            } else {
                $response['Attendance'] = "No data";
                $response['Status'] = 0;
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function flatmember($param) {
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        if (isset($param->FlatID)) {
            $FlatID = $param->FlatID;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT User.ID, User.PhoneNbr,User.IsPhonePublic, User.EmailID, SocietyFlatMapping.FlatNbr, User.FirstName, User.MiddleName, User.LastName, User.ProfilePic, User.Gender, User.Occupation,User.About, from_unixtime((User.AddedOn+ 19660),'%d/%m/%Y  %h:%i %p') as AddedOn,ResidentRWAMapping.ApprovedBy,ResidentFlatMapping.primary_dependent,from_unixtime((ResidentRWAMapping.ApprovedOn+ 19660),'%d/%m/%Y  %h:%i %p') as ApprovedOn, User.IsActive FROM
            User INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentID = User.ID INNER JOIN ResidentFlatMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID  INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID WHERE ResidentRWAMapping.ApprovalStatus= 'Y'  AND ResidentRWAMapping.RWAID ='" . $RWAID . "' AND ResidentFlatMapping.FlatID IN (" . $FlatID . ") GROUP BY User.ID ");
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['Detail'] = $items;
            } else {
                $response['Detail'] = '';
                $response['message'] = "No Data";
                $response['Status'] = 0;
            }
            $sql2 = $this->prdbh->prepare("SELECT ID,ParkingNo FROM ParkingTable  WHERE Flat_ID IN ('" . $FlatID . "')");
            $sql2->execute();
            $items1 = array();
            $parkingid = array();
            if ($sql2->rowCount() > 0) {
                while ($row2 = $sql2->fetch(PDO::FETCH_ASSOC)) {
                    $items1[] = $row2;
                    $parkingid[] = $row2['ID'];
                }
                $response['Parking Nbr'] = $items1;
                if (count($parkingid)) {
                    $parkid = implode(',', $parkingid);
                    $sql3 = $this->prdbh->prepare("SELECT ID, VehicleNbr,IFNULL(VehicleType,'') as VehicleType FROM ResidentVehicleByParking WHERE ParkingID IN ('" . $parkid . "') ");
                    //print_r($sql3);die;
                    $sql3->execute();
                    $items2 = array();
                    if ($sql3->rowCount() > 0) {
                        while ($row3 = $sql3->fetch(PDO::FETCH_ASSOC)) {
                            $items2[] = $row3;
                        }
                        $response['Vehicle Nbr'] = $items2;
                    }
                }
            }
            $response['Status'] = 1;
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function rwafunctionconfig($param) {
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT RWAConfigMaster.ConfigParamID, RWAConfigMaster.ConfigParamDESC,RWAConfigTable.ConfigParamValue FROM RWAConfigMaster INNER JOIN RWAConfigTable ON RWAConfigTable.ConfigParamID=RWAConfigMaster.ConfigParamID WHERE  RWAConfigTable.RWAID= '" . $RWAID . "' ");
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['FunctionConfig'] = $items;
                $response['Status'] = 1;
            } else {
                $response['FunctionConfig'] = [];
                $response['message'] = "No Data";
                $response['Status'] = 0;
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function makeflatprimarymember($param) {
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT RWAConfigMaster.ConfigParamID, RWAConfigMaster.ConfigParamDESC,RWAConfigTable.ConfigParamValue FROM RWAConfigMaster INNER JOIN RWAConfigTable ON RWAConfigTable.ConfigParamID=RWAConfigMaster.ConfigParamID WHERE  RWAConfigTable.RWAID= '" . $RWAID . "' ");
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['FunctionConfig'] = $items;
                $response['Status'] = 1;
            } else {
                $response['FunctionConfig'] = [];
                $response['message'] = "No Data";
                $response['Status'] = 0;
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function visitorparking($param) {
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT ID,ParkingNo From ParkingTable where Flat_ID=0 AND RWAID ='" . $RWAID . "' ");
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['VisitorParkingNo'] = $items;
                $response['Status'] = 1;
            } else {
                $response['VisitorParkingNo'] = '';
                $response['Status'] = 0;
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function complainslastaff() {
        require 'sendgrid/vendor/autoload.php';
        require 'sendgrid/lib/SendGrid.php';
        $response = array();
        $tdat = date("U"); // 1 day time stemp is 86400 s
        $sql = $this->prdbh->prepare(" SELECT ID, RWAID, Category, SLA1, SLA2, Email FROM MaintenanceSLAComplain ");
        if ($sql->execute()) {
            if ($sql->rowCount() >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $SLA1 = $row[SLA1];
                    $SLA2 = $row[SLA2];
                    $rwaid = $row[RWAID];
                    $CategoryID = $row[Category];
                    $email = $row[Email];
                    $subject = "SLA OF COMPLAIN";
                    $name = "admin";
                    $SLA1date = $SLA1 * 86400;
                    $SLAforcomplain = $tdat - $SLA1date;
                    $sql1 = $this->prdbh->prepare("SELECT ComplaintRegister.ID,User.FirstName,SocietyFlatMapping.FlatNbr,User.PhoneNbr,ComplaintCategory.Description,FROM_BASE64(ComplaintRegister.ComplaintDetails) as detail, ComplaintRegister.Status, from_unixtime((ComplaintRegister.DateCreated + 19660),'%d/%m/%Y  %h:%i:%s %p ') as Datecreated FROM ComplaintRegister
LEFT JOIN ComplaintCategory ON ComplaintRegister.CategoryID = ComplaintCategory.ID 
LEFT JOIN  ResidentFlatMapping ON ComplaintRegister.RWAResidentFlatID = ResidentFlatMapping.ID 
LEFT JOIN SocietyFlatMapping ON SocietyFlatMapping.ID=ResidentFlatMapping.FlatID
LEFT JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID 
LEFT join User on User.ID=ResidentRWAMapping.ResidentID WHERE ComplaintRegister.RWAID = '" . $rwaid . "' AND ComplaintRegister.CategoryID= '" . $CategoryID . "' AND ComplaintRegister.DateAssigned <= '" . $SLAforcomplain . "' AND ComplaintRegister.Status IN (0,1)");
                    $sql1->execute();
                    $items = array();
                    if ($sql1->rowCount() >= 1) {
                        while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                            $items[] = $row1;
                        }
                        $header = array("Complaint No.", "User Name", "FlatNbr", "Phone Number", "Category", "Description", "Status", "Complaint Date");
                        $file = fopen("file.csv", "w");
                        fputcsv($file, $header);
                        foreach ($items as $line) {
                            fputcsv($file, $line);
                        }
                        fclose($file);
                        $compmessage = ConstantUrl . "file.csv";
                        $result = $this->newcomplainmailstaff($compmessage, $subject, $email, $name);
                        $response['Status'] = 1;
                    } else {
                        $response['Status'] = 0;
                    }
                }
            }
        }
        return $response;
    }
    public function generatebill() {
        $response = array();
        $tdat = date("U"); // 1 day time stemp is 86400 s
        $sql = $this->prdbh->prepare(" SELECT RWAID FROM RWAConfigTable Where ConfigParamValue= 1 AND ConfigParamID=26 ");
        $sql->execute();
        //print_r($sql);die;
        if ($sql->rowCount() >= 1) {
            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                $rwaid = $row['RWAID'];
                $billdate = date("d");
                $billmonth1 = date("m");
                if ($billmonth1 == '03') {
                    $month = "1,3";
                } elseif ($billmonth1 == '06') {
                    $month = "1,3,6";
                } elseif ($billmonth1 == '09') {
                    $month = "1,3";
                } elseif ($billmonth1 == '12') {
                    $month = "1,3,6,12";
                } else {
                    $month = "1";
                }
                /* if($billmonth1=='01'){$billmonth='1';}elseif($billmonth1=='02'){$billmonth=1;}elseif($billmonth1=='03'){$billmonth=1,3;}elseif($billmonth1=='04'){$billmonth=1;}elseif($billmonth1=='05'){$billmonth=1;}elseif($billmonth1=='06'){$billmonth=1,3,6;}elseif($billmonth1=='07'){$billmonth=1;}elseif($billmonth1=='08'){$billmonth=1;}elseif($billmonth1=='09'){$billmonth=1,3;}elseif($billmonth1=='10'){$billmonth=1;}elseif($billmonth1=='11'){$billmonth=1;}elseif($billmonth1=='12'){$billmonth=1,3,6,12;}  */
                $sql1 = $this->prdbh->prepare("SELECT (SocietyFlatMapping.ID) as FlatID ,MaintenanceBillingCharge.BillAmount, MaintenanceBillingCharge.Addedby, MaintenanceBillingCharge.FlatType, MaintenanceBillingCharge.BillDate, MaintenanceBillingCharge.DueDate FROM SocietyFlatMapping INNER JOIN MaintenanceBillingCharge on MaintenanceBillingCharge.FlatType= SocietyFlatMapping.FlatType  WHERE SocietyFlatMapping.RWAID= $rwaid  AND  MaintenanceBillingCharge.BillDate='" . $billdate . "' AND MaintenanceBillingCharge.Period IN($month) ");
                $sql1->execute();
                //print_r($sql1);die;
                if ($sql1->rowCount() >= 1) {
                    while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                        $FlatID = $row1['FlatID'];
                        $FlatType = $row1['FlatType'];
                        $BillAmount = $row1['BillAmount'];
                        $BillDate = date("U");
                        $DueDate = $BillDate + (86400 * $row1['DueDate']);
                        //$sql3 = $this->prdbh->prepare("SELECT EmailID from User inner JOIN ResidentRWAMapping on ResidentRWAMapping.ResidentID=User.ID INNER JOIN ResidentFlatMapping on ResidentFlatMapping.ResidentRWAID= ResidentRWAMapping.ResidentRWAID Where ResidentFlatMapping.FlatID='".$FlatID."' ");
                        /* $sql3->execute();
                        if($sql3->rowCount()>=1)
                        {} */
                        $sql2 = $this->prdbh->prepare("INSERT INTO MaintenanceBillingFlat (FlatID, FlatType, BillAmount,BillingDate,PaymentDueDate, StatusOfPayment) VALUES ('$FlatID', '$FlatType','$BillAmount','$BillDate' ,'$DueDate','2')"); // StatusOfPayment -> 1= paid,2=unpaid
                        //print_r($sql2);
                        $this->prdbh->beginTransaction();
                        $sql2->execute();
                        if ($sql2->rowCount() > 0) {
                            $id1 = $this->prdbh->lastInsertId();
                            $this->prdbh->commit();
                            $response['Status'] = 1;
                        }
                    }
                } else {
                    $response['Status'] = 2;
                }
            }
        } else {
            $response['Status'] = 0;
        }
        return $response;
    }
    public function flatdueamount() {
        $response = array();
        $tdat = date("U");
        $tomdat = date("U") + 86400;
        // 1 day time stemp is 86400 s
        $sql = $this->prdbh->prepare(" SELECT  ID,BillAmount, DueAmount, PaymentDueDate, StatusOfPayment FROM MaintenanceBillingFlat Where PaymentDueDate BETWEEN '" . $tdat . "' AND '" . $tomdat . "' AND StatusOfPayment=2 ");
        $sql->execute();
        //print_r($sql);die;
        if ($sql->rowCount() >= 1) {
            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                $ID = $row['ID'];
                $BillAmount = $row['BillAmount'];
                $DueAmount = $row['DueAmount'];
                $fine = '50';
                $alldueamount = $fine + $DueAmount + $BillAmount;
                $sql2 = $this->prdbh->prepare("UPDATE MaintenanceBillingFlat SET  DueAmount='" . $alldueamount . "' WHERE ID = '" . $ID . "' ");
                //print_r($sql2);
                $this->prdbh->beginTransaction();
                $sql2->execute();
                if ($sql2->rowCount() > 0) {
                    $id1 = $this->prdbh->lastInsertId();
                    $this->prdbh->commit();
                    $response['Status'] = 1;
                }
            }
        } else {
            $response['Status'] = 0;
        }
        return $response;
    }
    public function pendingusersla() {
        require 'sendgrid/vendor/autoload.php';
        require 'sendgrid/lib/SendGrid.php';
        $response = array();
        $tdat = date("U"); // 1 day time stemp is 86400 s
        $sql = $this->prdbh->prepare(" SELECT ID, RWAID, Category, SLA1, SLA2, Email FROM MaintenanceSLAComplain ");
        if ($sql->execute()) {
            if ($sql->rowCount() >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $SLA1 = $row[SLA1];
                    $SLA2 = $row[SLA2];
                    $rwaid = $row[RWAID];
                    $CategoryID = $row[Category];
                    $email = $row[Email];
                    $subject = "SLA OF ALL PENDING USER";
                    $name = "admin";
                    $SLA1date = $SLA1 * 86400;
                    $SLAforcomplain = $tdat - $SLA1date;
                    $sql1 = $this->prdbh->prepare("SELECT User.FirstName,User.MiddleName, User.PhoneNbr, User.EmailID,  User.Gender, User.About, User.Occupation  FROM  User INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentID=User.ID WHERE ResidentRWAMapping.RWAID = '" . $rwaid . "' AND ResidentRWAMapping.ApprovalStatus='P' ");
                    $sql1->execute();
                    $items = array();
                    if ($sql1->rowCount() >= 1) {
                        while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                            $items[] = $row1;
                        }
                        //print_r($items);die;
                        $header = array("User Name", "FlatNbr", "Phone Number", "EmailID", "Gender", "About", "Occupation");
                        $file1 = fopen("file1.csv", "w");
                        fputcsv($file1, $header);
                        foreach ($items as $line) {
                            fputcsv($file1, $line);
                        }
                        fclose($file1);
                        $compmessage = ConstantUrl . "file1.csv";
                        $result = $this->pendinguserslamail($compmessage, $subject, $email, $name);
                        $response['Status'] = 1;
                    } else {
                        $response['Status'] = 0;
                    }
                }
            }
        }
        return $response;
    }
    public function complainsla($param) {
        if (isset($param->RWAID)) {
            $rwaid = $param->RWAID;
        }
        if (isset($param->CategoryID)) {
            $CategoryID = $param->CategoryID;
        }
        $response = array();
        $tdat = date("U"); // 1 day time stemp is 86400 s
        $sql = $this->prdbh->prepare(" SELECT ID, RWAID, Category, SLA1, SLA2, Email FROM MaintenanceSLAComplain WHERE RWAID = '" . $rwaid . "' AND Category= '" . $CategoryID . "'");
        if ($sql->execute()) {
            if ($sql->rowCount() >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $SLA1 = $row[SLA1];
                    $SLA2 = $row[SLA2];
                    $email = $row[Email];
                    $subject = "SLA OF COMPLAINT";
                    $name = "admin";
                }
                $SLA1date = $SLA1 * 86400;
                $SLAforcomplain = $tdat - $SLA1date;
            }
        }
        $sql1 = $this->prdbh->prepare("SELECT RWAID  FROM ComplaintRegister WHERE RWAID = '" . $rwaid . "' AND CategoryID= '" . $CategoryID . "' AND DateAssigned <= '" . $SLAforcomplain . "' AND Status IN (0,1)");
        $sql1->execute();
        $items = array();
        if ($sql1->rowCount() >= 1) {
            while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                $items[] = $row1;
            }
            $file = fopen("file.csv", "w");
            foreach ($items as $line) {
                fputcsv($file, $line);
            }
            fclose($file);
            $compmessage = ConstantUrl . "file.csv";
            $result = $this->newcomplainmail($compmessage, $subject, $email, $name);
            $response['Status'] = 1;
        } else {
            $response['Complain SLA'] = '';
            $response['message'] = "No Data";
            $response['Status'] = 0;
        }
        return $response;
    }
    public function complainslacsv($rec) {
        $a = date('Y-m-d');
        $b = 'complaintlist';
        $name = $b . $a;
        $ID = $rec->ID;
        $CategoryID = $rec->CategoryID;
        $filename = "$name.csv";
        $fp = fopen('php://output', 'w');
        $header = array("'User Name','FlatNbr','Phone Number','Category','Description','Status','Remark'");
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        fputcsv($fp, $header);
        $sql = ("SELECT ComplaintRegister.ID,SocietyFlatMapping.FlatNbr,ComplaintRegister.ComplaintDetails, ComplaintRegister.IsCommonAreaComplaint,ComplaintRegister.Status, from_unixtime((ComplaintRegister.DateCreated + 19660),'%d/%m/%Y  %h:%i:%s %p')as Datecreated,User.FirstName,User.PhoneNbr,ComplaintCategory.Description, ComplaintRegister.Rating FROM ComplaintRegister
        LEFT JOIN ComplaintCategory ON ComplaintRegister.CategoryID = ComplaintCategory.ID 
        LEFT JOIN  ResidentFlatMapping ON ComplaintRegister.RWAResidentFlatID = ResidentFlatMapping.ID 
        LEFT JOIN SocietyFlatMapping ON SocietyFlatMapping.ID=ResidentFlatMapping.FlatID
        LEFT JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID 
        LEFT join User on User.ID=ResidentRWAMapping.ResidentID  WHERE ComplaintRegister.ID IN (" . $ID . ") and ComplaintRegister.CategoryID ='" . $CategoryID . "' AND Status='1'  ");
        // print_r($sql);die;
        $sql->execute();
        //$items=array();
        if ($sql->rowCount() >= 1) {
            $data = array();
            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                $id = $row['ID'];
                $data['FirstName'] = $row['FirstName'];
                $data['FlatNbr'] = $row['FlatNbr'];
                $data['PhoneNbr'] = $row['PhoneNbr'];
                $data['Description'] = $row['Description'];
                $data['ComplaintDetails'] = base64_decode($row['ComplaintDetails']);
                $data['Status'] = $row['Status'];
                fputcsv($fp, $data);
            }
        } else {
        }
        exit;
    }
    public function notificationuser($param) {
        if (isset($param->ResidentRWAID)) {
            $ResidentRWAID = $param->ResidentRWAID;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT DISTINCT(RWANoticeMapping.NoticeID)  FROM RWANoticeMapping INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentID=RWANoticeMapping.ResidentID WHERE ResidentRWAMapping.ResidentRWAID='" . $ResidentRWAID . "' ");
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['ID'] = $items;
            } else {
                $response['ID'] = [];
                $response['message'] = "No Data";
                $response['Status'] = 0;
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function getrwavehiclepull($param) {
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        if (isset($param->LastSynctime)) {
            $LastSynctime = $param->LastSynctime;
        }
        $response = array();
        $tdat = date("U");
        $a = $tdat + 120;
        $c = '1296000';
        $b = $a - $c;
        $sql = $this->prdbh->prepare("SELECT ID, VehicleID, RFIDTag, RWAID, Allowentry, Allowexit, LastUpdateTime 
                FROM VehicleRFIDMapping WHERE RWAID = '" . $RWAID . "' AND LastUpdateTime >='" . $LastSynctime . "' ");
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['getrwavehiclepull'] = $items;
                $response['Status'] = 1;
            } else {
                $response['getrwavehiclepull'] = [];
                $response['message'] = "No Data";
                $response['Status'] = 0;
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function showcallout($param) {
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        $response = array();
        $tdat = date("U");
        $a = $tdat + 120;
        $c = '1296000';
        $b = $a - $c;
        $sql = $this->prdbh->prepare("SELECT CallOuts.ID, CallOuts.SenderID,CallOuts.DateSent, CallOuts.Description,(User.ID) AS UserID, User.FirstName, SocietyFlatMapping.FlatNbr
FROM CallOuts LEFT JOIN ResidentRWAMapping ON CallOuts.SenderID = ResidentRWAMapping.ResidentRWAID
LEFT JOIN User ON User.ID = ResidentRWAMapping.ResidentID LEFT JOIN ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID = ResidentRWAMapping.ResidentRWAID INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID
WHERE ResidentRWAMapping.RWAID = '" . $RWAID . "' AND CallOuts.IsActive='1' GROUP BY ID DESC");
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['CallOuts'] = $items;
                $response['Status'] = 1;
            } else {
                $response['CallOuts'] = [];
                $response['message'] = "No Data";
                $response['Status'] = 0;
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function advertisementevent($param) {
        if (isset($param->EventID)) {
            $EventID = $param->EventID;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT * from User ");
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['Advertisement'] = $items;
                $response['Status'] = 1;
            } else {
                $response['Advertisement'] = [];
                $response['message'] = "No Data";
                $response['Status'] = 0;
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function showeventadv($param) {
        if (isset($param->EventID)) {
            $EventID = $param->EventID;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT Advertisement.ImagePath, Advertisement.TimeInterval, Advertisement.PlayTime, Advertisement.AddedOn, Advertisement.IsActive FROM Advertisement 
                INNER JOIN AdvertisementEvent ON AdvertisementEvent.AdvertisementID=Advertisement.ID 
                WHERE AdvertisementEvent.EventID='" . $EventID . "' AND Advertisement.IsActive ='1' ");
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['Advertisement'] = $items;
                $response['Status'] = 1;
            } else {
                $response['Advertisement'] = [];
                $response['message'] = "No Data";
                $response['Status'] = 0;
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function merchantlogin($param) {
        if (isset($param->Email)) {
            $Email = $param->Email;
        }
        if (isset($param->Password)) {
            $Password = $param->Password;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT ID, EmailID, PassWord, PhoneNbr, UserName, BussinessName, ApprovalDate, ApprovedBy, ApprovalStatus, IsActive From MerchantsUser WHERE PassWord = '" . $Password . "' AND EmailID = '" . $Email . "' AND IsActive='1'  GROUP BY ID DESC");
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['MerchantsUser'] = $items;
                $response['Status'] = 1;
            } else {
                $response['MerchantsUser'] = [];
                $response['message'] = "No Data";
                $response['Status'] = 0;
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function couponsbyscane($param) {
        $tdat = date("U");
        $tdate = $tdat + 120;
        if (isset($param->QRCODE)) {
            $QRCODE = $param->QRCODE;
        }
        if (isset($param->MerchantID)) {
            $mrcntid = $param->MerchantID;
        }
        $response = array();
        $sql5 = $this->prdbh->prepare("SELECT CampaignMerchantShopMapping.MerchantID FROM CampaignMerchantShopMapping INNER JOIN CampaignUserMapping ON CampaignUserMapping.CampaignID= CampaignMerchantShopMapping.ID WHERE CampaignUserMapping.QrCode='" . $QRCODE . "' ");
        $sql5->execute();
        if ($sql5->execute()) {
            if ($sql5->rowCount() >= 1) {
                while ($row5 = $sql5->fetch(PDO::FETCH_ASSOC)) {
                    $MerchantID = $row5['MerchantID'];
                }
            }
        }
        if ($mrcntid == $MerchantID) {
            $sql = $this->prdbh->prepare("UPDATE CampaignUserMapping SET Availed='0',MerchantID ='" . $mrcntid . "',RedeemDate ='" . $tdate . "' WHERE QrCode = '" . $QRCODE . "' AND Availed = '1'  ");
            $sql->execute();
            if ($sql) {
                $count = $sql->rowCount();
                if ($count >= 1) {
                    $sql4 = $this->prdbh->prepare("SELECT * FROM CampaignMerchantShopMapping INNER JOIN CampaignUserMapping ON CampaignUserMapping.CampaignID =CampaignMerchantShopMapping.ID WHERE QrCode = '" . $QRCODE . "' ");
                    $sql4->execute();
                    if ($sql4->rowCount() > 0) {
                        while ($row4 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                            $CampaignTittle = $row4['CampaignTittle'];
                            $CampaignUrl = $row4['CampaignUrl'];
                            $DiscountValue = $row4['DiscountValue'];
                            $DiscountType = $row4['DiscountType'];
                        }
                    }
                    $response['CampaignTittle'] = $CampaignTittle;
                    $response['CampaignUrl'] = $CampaignUrl;
                    $response['DiscountValue'] = $DiscountValue;
                    $response['DiscountType'] = $DiscountType;
                    $response['message'] = "Coupon Redeemed";
                    $response['Status'] = 1;
                } else {
                    $response['message'] = "Coupon already redeemed!
";
                    $response['Status'] = 0;
                }
            } else {
                $response['message'] = $sql->errorInfo();
                $response['Status'] = 0;
            }
        } else {
            $response['message'] = "The coupon does not belong to this shop! ";
            $response['Status'] = 0;
        }
        return $response;
    }
    public function couponsdetailformerchant($param) {
        if (isset($param->MerchantID)) {
            $MerchantID = $param->MerchantID;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT CampaignMerchantShopMapping.CampaignTittle,CampaignMerchantShopMapping.DiscountType,CampaignMerchantShopMapping.CampaignUrl,IFNULL(CampaignUserMapping.RedeemDate,' ') as RedeemDate,(User.FirstName) as UserName FROM CampaignMerchantShopMapping INNER JOIN CampaignUserMapping ON CampaignUserMapping.CampaignID =CampaignMerchantShopMapping.ID INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID= CampaignUserMapping.ResidentRWAID INNER JOIN User ON User.ID= ResidentRWAMapping.ResidentID WHERE CampaignMerchantShopMapping.MerchantID = '" . $MerchantID . "' ");
        $sql->execute();
        if ($sql->execute()) {
            $count = $sql->rowCount();
            $items = array();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['Coupons detail'] = $items;
                $response['Status'] = 1;
            } else {
                $response['message'] = "No Coupon found";
                $response['Coupons detail'] = [];
                $response['Status'] = 0;
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function couponsdetailbyscane($param) {
        if (isset($param->QRCODE)) {
            $QRCODE = $param->QRCODE;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT CampaignMerchantShopMapping.ID, CampaignMerchantShopMapping.DiscountType, CampaignMerchantShopMapping.CampaignTittle, 
CampaignMerchantShopMapping.StartDate, CampaignMerchantShopMapping.EndDate,CampaignMerchantShopMapping.CampaignUrl, 
CampaignMerchantShopMapping.ShopID, CampaignMerchantShopMapping.MerchantID, CampaignMerchantShopMapping.NoOfCoupenPerUser, 
CampaignMerchantShopMapping.DiscountValue, CampaignMerchantShopMapping.RWAID FROM CampaignMerchantShopMapping INNER JOIN CampaignUserMapping ON CampaignUserMapping.CampaignID =CampaignMerchantShopMapping.ID INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID= CampaignUserMapping.ResidentRWAID INNER JOIN User ON User.ID= ResidentRWAMapping.ResidentID WHERE QrCode = '" . $QRCODE . "' ");
        $sql->execute();
        if ($sql->execute()) {
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items = $row;
                }
                $response['Coupons detail'] = $items;
                $response['Status'] = 1;
            } else {
                $response['message'] = "No Coupon found";
                $response['Coupons detail'] = [];
                $response['Status'] = 0;
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function calloutdetail($param) {
        if (isset($param->ID)) {
            $ID = $param->ID;
        }
        $response = array();
        $tdat = date("U");
        $a = $tdat + 120;
        $b = $a - 1296000;
        //  AND CallOuts.DateSent BETWEEN '".$b."' AND '".$a."'
        $sql = $this->prdbh->prepare("SELECT CallOuts.ID, CallOuts.SenderID,CallOuts.DateSent, CallOuts.Description,(User.ID) AS UserID, User.FirstName, SocietyFlatMapping.FlatNbr
FROM CallOuts INNER JOIN ResidentRWAMapping ON CallOuts.SenderID = ResidentRWAMapping.ResidentRWAID
INNER JOIN User ON User.ID = ResidentRWAMapping.ResidentID LEFT JOIN ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID = ResidentRWAMapping.ResidentRWAID INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID
WHERE CallOuts.ID = '" . $ID . "'  GROUP BY ID DESC");
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['CallOuts'] = $items;
                $response['Status'] = 1;
            } else {
                $response['CallOuts'] = "No Data";
                $response['Status'] = 0;
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function showhelperworkingflat($param) {
        if (isset($param->HelperID)) {
            $HelperID = $param->HelperID;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT ProfilePhoto, Name,ServiceOffered FROM Helpers WHERE ID ='" . $HelperID . "' AND IsActive= '1' ");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                $response = array();
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items = $row;
                    $sql4 = $this->prdbh->prepare("SELECT SocietyFlatMapping.ID,SocietyFlatMapping.FlatNbr FROM SocietyFlatMapping 
                        INNER JOIN SocietyHelperWorkingFlat ON SocietyHelperWorkingFlat.SocietyFlatID=SocietyFlatMapping.ID
                        where SocietyHelperWorkingFlat.HelperID='" . $HelperID . "' ");
                    if ($sql4->execute()) {
                        $Flatnbr = array();
                        while ($row4 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                            $Flatnbr[] = $row4;
                        }
                        $a['helperworkingflat'] = $Flatnbr;
                    }
                }
                $items = array_merge($items, $a);
                $response['HelperDetail'] = $items;
                $response['Status'] = '1';
            } else {
                $response['helperworkingflat'] = [];
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function helperworkingflat($rec) {
        $tdat = date("U");
        $tdate = $tdat + 120;
        $HelperID = $rec->HelperID;
        $UserID = $rec->UserID;
        $SocietyFlatID = $rec->SocietyFlatID;
        $sql2 = $this->prdbh->prepare("INSERT INTO HelperAuditTrail(HelperID, SocietyFlatID, AddedOn,Status)
                                VALUES(?,?,?,?)");
        $sql2->bindParam(1, $rec->HelperID);
        $sql2->bindParam(2, $rec->SocietyFlatID);
        $sql2->bindParam(3, $tdate);
        $sql2->bindParam(4, $rec->Status);
        $this->prdbh->beginTransaction();
        $sql2->execute();
        if ($sql2->rowCount() > 0) {
            $id1 = $this->prdbh->lastInsertId();
            $this->prdbh->commit();
        }
        if ($UserID != '') {
            $sql3 = $this->prdbh->prepare("SELECT UserID, HelperID, AddedOn FROM UserFavHelper WHERE HelperID='" . $HelperID . "' AND UserID='" . $UserID . "' ");
            $sql3->execute();
            if ($sql3->rowCount() > 0) {
                $sql4 = $this->prdbh->prepare("UPDATE UserFavHelper SET HelperID='0' WHERE  HelperID='" . $HelperID . "' AND UserID='" . $UserID . "' ");
            } else {
                $sql4 = $this->prdbh->prepare("INSERT INTO UserFavHelper(UserID, HelperID, AddedOn) VALUES(?,?,?)");
            }
            $sql4->bindParam(1, $rec->UserID);
            $sql4->bindParam(2, $rec->HelperID);
            $sql4->bindParam(3, $tdate);
            $this->prdbh->beginTransaction();
            $sql4->execute();
            $count4 = $sql4->rowCount();
            if ($count4 > 0) {
                $id = $this->prdbh->lastInsertId();
                $this->prdbh->commit();
            }
        }
        $sql1 = $this->prdbh->prepare("SELECT HelperID, SocietyFlatID, AddedOn FROM SocietyHelperWorkingFlat WHERE HelperID='" . $HelperID . "' AND SocietyFlatID='" . $SocietyFlatID . "' ");
        $sql1->execute();
        if ($sql1->rowCount() > 0) {
            $sql = $this->prdbh->prepare("UPDATE SocietyHelperWorkingFlat SET HelperID='0' WHERE  HelperID='" . $HelperID . "' AND SocietyFlatID='" . $SocietyFlatID . "' ");
        } else {
            $sql = $this->prdbh->prepare("INSERT INTO SocietyHelperWorkingFlat(HelperID, SocietyFlatID, AddedOn)
                                VALUES(?,?,?)");
        }
        $sql->bindParam(1, $rec->HelperID);
        $sql->bindParam(2, $rec->SocietyFlatID);
        $sql->bindParam(3, $tdate);
        $this->prdbh->beginTransaction();
        $sql->execute();
        $count = $sql->rowCount();
        if ($count > 0) {
            $id = $this->prdbh->lastInsertId();
            $this->prdbh->commit();
            $response['Added Id'] = $id;
            $response['Status'] = "1";
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function addfavshop($rec) {
        $tdat = date("U");
        $tdate = $tdat;
        $sql = $this->prdbh->prepare("INSERT INTO `UserFavouriteNearBy`(`NearByID`, `UserID`, `AddedOn`)
        VALUES(?,?,?)");
        $sql->bindParam(1, $rec->NearByID);
        $sql->bindParam(2, $rec->UserID);
        $sql->bindParam(3, $tdate);
        $this->prdbh->beginTransaction();
        $sql->execute();
        $count = $sql->rowCount();
        if ($count > 0) {
            $id = $this->prdbh->lastInsertId();
            $this->prdbh->commit();
            $response['Message'] = "Fav shop added";
            $response['Fev Shop Id'] = $id;
            $response['Status'] = "1";
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function servicecalllog($rec) {
        $tdat = date("U");
        $tdate = $tdat + 120;
        $sql = $this->prdbh->prepare("INSERT INTO ServiceCallLog(CallerNumber,CallerResidentRWAID,ReciverNumber,ServiceAgentID,CallingTime)
                                                            VALUES(?,?,?,?,?)");
        $sql->bindParam(1, $rec->CallerNumber);
        $sql->bindParam(2, $rec->CallerResidentRWAID);
        $sql->bindParam(3, $rec->ReciverNumber);
        $sql->bindParam(4, $rec->ServiceAgentID);
        $sql->bindParam(5, $tdate);
        $this->prdbh->beginTransaction();
        $sql->execute();
        $count = $sql->rowCount();
        if ($count > 0) {
            $id = $this->prdbh->lastInsertId();
            $this->prdbh->commit();
            $response['Message'] = "Successful";
            $response['Insert Id'] = $id;
            $response['Status'] = "1";
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function compfeedback($rec) {
        $tdat = date("U");
        $tdate = $tdat + 120;
        $sql = $this->prdbh->prepare("UPDATE ComplaintRegister SET AcknowledgementNote=?,Rating=?,Status=?,ResolutionAcknowledgedOn=? WHERE ID = ? AND RWAID = ?");
        $sql->bindParam(1, $rec->AcknowledgementNote);
        $sql->bindParam(2, $rec->Rating);
        $sql->bindParam(3, $rec->Status);
        $sql->bindParam(4, $tdate);
        $sql->bindParam(5, $rec->ID);
        $sql->bindParam(6, $rec->RWAID);
        $this->prdbh->beginTransaction();
        $sql->execute();
        $count = $sql->rowCount();
        if ($count > 0) {
            $this->prdbh->commit();
            $response['Message'] = "Feedback added successfully ";
            $response['Status'] = "1";
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function addhelper($rec) {
        $tdat = date("U");
        $tdate = $tdat;
        $Name = $rec->Name;
        $is_in = '2';
        $ServiceOffered = $rec->ServiceOffered;
        $PrimaryContactNbr = $rec->PrimaryContactNbr;
        $HelperQRCode = $rec->HelperQRCode;
        if ($Name != '' && $ServiceOffered != '' && $PrimaryContactNbr != '') {
            if ($HelperQRCode != '') {
                $sql = $this->prdbh->prepare("SELECT HelperQRCode FROM Helpers WHERE HelperQRCode = '" . $HelperQRCode . "'");
                $sql->execute();
                if ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $response['Message'] = "HelperQRCode already registered";
                    $response['Status'] = "0";
                } else {
                    $sql = $this->prdbh->prepare("INSERT INTO `Helpers`(`RWAID`, `Name`, `ServiceOffered`, `ServiceCharges`, `ResidentialAddress`, `PrimaryContactNbr`, `PhoneNbr2`, `EmailId`, `AddedBy`, `AddedOn`, `ProfilePhoto`, `OtherDocScanImage1`, `OtherDocScanImage2`, `OtherDocScanImage3`, `EntryCardExpiry`, `IsActive`,is_in,HelperQRCode) 
                        VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,1,?,?)");
                    $sql->bindParam(1, $rec->RWAID);
                    $sql->bindParam(2, $rec->Name);
                    $sql->bindParam(3, $rec->ServiceOffered);
                    $sql->bindParam(4, $rec->ServiceCharges);
                    $sql->bindParam(5, $rec->ResidentialAddress);
                    $sql->bindParam(6, $rec->PrimaryContactNbr);
                    $sql->bindParam(7, $rec->PhoneNbr2);
                    $sql->bindParam(8, $rec->EmailId);
                    $sql->bindParam(9, $rec->AddedBy);
                    $sql->bindParam(10, $tdate);
                    $sql->bindParam(11, $rec->ProfilePhoto);
                    $sql->bindParam(12, $rec->OtherDocScanImage1);
                    $sql->bindParam(13, $rec->OtherDocScanImage2);
                    $sql->bindParam(14, $rec->OtherDocScanImage3);
                    $sql->bindParam(15, $rec->EntryCardExpiry);
                    $sql->bindParam(16, $is_in);
                    $sql->bindParam(17, $rec->HelperQRCode);
                    $this->prdbh->beginTransaction();
                    $sql->execute();
                    $count = $sql->rowCount();
                    if ($count > 0) {
                        $id = $this->prdbh->lastInsertId();
                        $this->prdbh->commit();
                        $response['Message'] = "Helper added successfully";
                        $response['Helper Id'] = $id;
                        $response['Status'] = "1";
                    } else {
                        $response['Message'] = $sql->errorInfo();
                        $response['Status'] = "0";
                    }
                }
            } else {
                $sql = $this->prdbh->prepare("INSERT INTO `Helpers`(`RWAID`, `Name`, `ServiceOffered`, `ServiceCharges`, `ResidentialAddress`, `PrimaryContactNbr`, `PhoneNbr2`, `EmailId`, `AddedBy`, `AddedOn`, `ProfilePhoto`, `OtherDocScanImage1`, `OtherDocScanImage2`, `OtherDocScanImage3`, `EntryCardExpiry`, `IsActive`,is_in,HelperQRCode) 
                        VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,1,?,?)");
                $sql->bindParam(1, $rec->RWAID);
                $sql->bindParam(2, $rec->Name);
                $sql->bindParam(3, $rec->ServiceOffered);
                $sql->bindParam(4, $rec->ServiceCharges);
                $sql->bindParam(5, $rec->ResidentialAddress);
                $sql->bindParam(6, $rec->PrimaryContactNbr);
                $sql->bindParam(7, $rec->PhoneNbr2);
                $sql->bindParam(8, $rec->EmailId);
                $sql->bindParam(9, $rec->AddedBy);
                $sql->bindParam(10, $tdate);
                $sql->bindParam(11, $rec->ProfilePhoto);
                $sql->bindParam(12, $rec->OtherDocScanImage1);
                $sql->bindParam(13, $rec->OtherDocScanImage2);
                $sql->bindParam(14, $rec->OtherDocScanImage3);
                $sql->bindParam(15, $rec->EntryCardExpiry);
                $sql->bindParam(16, $is_in);
                $sql->bindParam(17, $rec->HelperQRCode);
                $this->prdbh->beginTransaction();
                $sql->execute();
                $count = $sql->rowCount();
                if ($count > 0) {
                    $id = $this->prdbh->lastInsertId();
                    $this->prdbh->commit();
                    $response['Message'] = "Helper added successfully";
                    $response['Helper Id'] = $id;
                    $response['Status'] = "1";
                } else {
                    $response['Message'] = $sql->errorInfo();
                    $response['Status'] = "0";
                }
            }
        } else {
            $response['Message'] = "All fields are mandatory";
            $response['Status'] = "0";
        }
        return $response;
    }
    public function addmaintenancestaff($rec) {
        $tdat = date("U");
        $tdate = $tdat;
        $Name = $rec->Name;
        $ServiceCategory = $rec->ServiceCategory;
        $PrimaryContactNbr = $rec->PrimaryContactNbr;
        $StaffQRCode1 = $rec->StaffQRCode;
        $StaffQRCode = substr($StaffQRCode1, 2);
        if ($Name != '' && $ServiceCategory != '' && $PrimaryContactNbr != '') {
            if ($StaffQRCode != '') {
                $sql = $this->prdbh->prepare("SELECT StaffQRCode FROM MaintenanceStaff WHERE StaffQRCode = '" . $StaffQRCode . "'");
                $sql->execute();
                if ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $response['Message'] = "StaffQRCode already registered";
                    $response['Status'] = "0";
                } else {
                    $sql = $this->prdbh->prepare("INSERT INTO MaintenanceStaff(Name, ProfilePic, RWAID,ServiceCategory,PrimaryContactNbr, SecondaryContactNbr, AddedOn,OtherDocScanImage1,OtherDocScanImage2,OtherDocScanImage3,StaffQRCode,StaffDesignation, EmployeeCode,AddedBy,StaffAddress,IsActive)
                        VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,1)");
                    $sql->bindParam(1, $rec->Name);
                    $sql->bindParam(2, $rec->ProfilePic);
                    $sql->bindParam(3, $rec->RWAID);
                    $sql->bindParam(4, $rec->ServiceCategory);
                    $sql->bindParam(5, $rec->PrimaryContactNbr);
                    $sql->bindParam(6, $rec->SecondaryContactNbr);
                    $sql->bindParam(7, $tdate);
                    $sql->bindParam(8, $rec->OtherDocScanImage1);
                    $sql->bindParam(9, $rec->OtherDocScanImage2);
                    $sql->bindParam(10, $rec->OtherDocScanImage3);
                    $sql->bindParam(11, $StaffQRCode);
                    $sql->bindParam(12, $rec->StaffDesignation);
                    $sql->bindParam(13, $rec->EmployeeCode);
                    $sql->bindParam(14, $rec->AddedBy);
                    $sql->bindParam(15, $rec->StaffAddress);
                    $this->prdbh->beginTransaction();
                    $sql->execute();
                    $count = $sql->rowCount();
                    if ($count > 0) {
                        $id = $this->prdbh->lastInsertId();
                        $this->prdbh->commit();
                        $response['Message'] = "Staff added successfully";
                        $response['Staff Id'] = $id;
                        $response['Status'] = "1";
                    } else {
                        $response['Message'] = $sql->errorInfo();
                        $response['Status'] = "0";
                    }
                }
            } else {
                $sql = $this->prdbh->prepare("INSERT INTO MaintenanceStaff(Name, ProfilePic, RWAID, ServiceCategory, PrimaryContactNbr, SecondaryContactNbr,AddedOn,OtherDocScanImage1,OtherDocScanImage2,OtherDocScanImage3,StaffDesignation,EmployeeCode,AddedBy,StaffAddress,IsActive)
                        VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,1)");
                $sql->bindParam(1, $rec->Name);
                $sql->bindParam(2, $rec->ProfilePic);
                $sql->bindParam(3, $rec->RWAID);
                $sql->bindParam(4, $rec->ServiceCategory);
                $sql->bindParam(5, $rec->PrimaryContactNbr);
                $sql->bindParam(6, $rec->SecondaryContactNbr);
                $sql->bindParam(7, $tdate);
                $sql->bindParam(8, $rec->OtherDocScanImage1);
                $sql->bindParam(9, $rec->OtherDocScanImage2);
                $sql->bindParam(10, $rec->OtherDocScanImage3);
                $sql->bindParam(11, $rec->StaffDesignation);
                $sql->bindParam(12, $rec->EmployeeCode);
                $sql->bindParam(13, $rec->AddedBy);
                $sql->bindParam(14, $rec->StaffAddress);
                $this->prdbh->beginTransaction();
                $sql->execute();
                $count = $sql->rowCount();
                if ($count > 0) {
                    $id = $this->prdbh->lastInsertId();
                    $this->prdbh->commit();
                    $response['Message'] = "Staff added successfully";
                    $response['Staff Id'] = $id;
                    $response['Status'] = "1";
                } else {
                    $response['Message'] = $sql->errorInfo();
                    $response['Status'] = "0";
                }
            }
        } else {
            $response['Message'] = "All fields are mandatory";
            $response['Status'] = "0";
        }
        return $response;
    }
    public function adminnotification($rec) {
        $tdat = date("U");
        $tdate = $tdat;
        $message = $rec->Text;
        $Text = $rec->Text;
        $uid = $rec->CreatedBy;
        $Severity = $rec->Severity;
        $Title = $rec->Title;
        $Image1 = $rec->Image1;
        $Image2 = $rec->Image2;
        $Image3 = $rec->Image3;
        $IsCalendarEvent = $rec->IsCalendarEvent;
        $EventStartDate = $rec->EventStartDate;
        $EventEndDateTime = $rec->EventEndDateTime;
        $Status = $rec->Status;
        if ((isset($rec->RWAID)) && ($rec->RWAID != '')) {
            $RWAID = $rec->RWAID;
            $RWAIDarray = explode(",", $RWAID);
            for ($i = 0;$i < count($RWAIDarray);$i++) {
                $sql = $this->prdbh->prepare("INSERT INTO `AppNotifications`(`RWAID`, `Severity`, `Title`, `Text`, `Image1`, `Image2`, `Image3`, `IsCalendarEvent`, 
                         `DateCreated`, `CreatedBy`, `EventStartDate`, `EventEndDateTime`, `Status`) VALUES('$RWAIDarray[$i]','$Severity','$Title','$Text','$Image1','$Image2','$Image3','$IsCalendarEvent','$tdate','$uid','$EventStartDate','$EventEndDateTime','$Status')");
                $this->prdbh->beginTransaction();
                if ($sql->execute()) {
                    $id = $this->prdbh->lastInsertId();
                    $this->prdbh->commit();
                    $response['Message'] = "Notification sent successfully";
                    $response['notificationId'] = $id;
                    $response['Status'] = "1";
                    $sql1 = $this->prdbh->prepare("SELECT User.ID FROM User
                                                            INNER JOIN ResidentRWAMapping ON User.ID=ResidentRWAMapping.ResidentID
                                                            LEFT JOIN RWA ON ResidentRWAMapping.RWAID=RWA.ID
                                                            WHERE ResidentRWAMapping.RWAID = '" . $RWAIDarray[$i] . "' And ResidentRWAMapping.ApprovalStatus='Y' AND User.ID!= '" . $uid . "' ");
                    $sql1->execute();
                    if ($sql1->rowCount() > 0) {
                        $UpushId1 = array();
                        while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                            $UpushId1[] = $row1['ID'];
                        }
                        $UpushId = implode(',', $UpushId1);
                        $ID = $id;
                        $message12 = base64_decode($rec->Text);
                        $message13 = html_entity_decode($message12);
                        $message1 = htmlspecialchars_decode($message13);
                        //$message= mysqli_real_escape_string($message1);
                        $message = strip_tags($message1);
                        $UrHd1 = "App notification";
                        $message1 = array("message" => $message, "UrHd2" => $UrHd1, "body" => $ID);
                        $result = $this->firebasenotification($UpushId, $message1);
                        $response['PushStatus'] = $result;
                    }
                    $msg = base64_decode($Title) . " :" . $message;
                    $Description = base64_encode($msg);
                    $sql3 = $this->prdbh->prepare("SELECT User.ID,User.FirstName,ResidentRWAMapping.ResidentRWAID,User.ProfilePic FROM User INNER JOIN AppNotificationsConfig ON AppNotificationsConfig.UserID= User.ID INNER JOIN ResidentRWAMapping ON User.ID=ResidentRWAMapping.ResidentID WHERE AppNotificationsConfig.RWAID = '" . $RWAIDarray[$i] . "' ");
                    $sql3->execute();
                    if ($sql3->rowCount() >= 1) {
                        while ($row3 = $sql3->fetch(PDO::FETCH_ASSOC)) {
                            $rid = $row3['ResidentRWAID'];
                            $ResidentRWAID = $row3['ResidentRWAID'];
                            $ResidentName = $row3['FirstName'];
                            $ProfilePic = $row3['ProfilePic'];
                            $ResidentID = $row3['ID'];
                        }
                    }
                    $sql5 = $this->prdbh->prepare("SELECT SocietyFlatMapping.FlatNbr FROM SocietyFlatMapping INNER JOIN ResidentFlatMapping ON ResidentFlatMapping.FlatID= SocietyFlatMapping.ID WHERE ResidentFlatMapping.ResidentRWAID ='" . $rid . "' ");
                    $sql5->execute();
                    if ($sql5->rowCount() > 0) {
                        $ResidentFlatNo = array();
                        while ($row5 = $sql5->fetch(PDO::FETCH_ASSOC)) {
                            $ResidentFlatNo = $row5['FlatNbr'];
                        }
                    }
                    $FeedType = "createPost";
                    $output = $this->onlineinsert($Description, $FeedType, $id, $ResidentRWAID, $ResidentFlatNo, $ProfilePic, $ResidentID, $RWAIDarray[$i], $ResidentName, $Image1);
                    $jsonObject = json_decode($output);
                    if ($jsonObject->Status == 1) {
                        $response['MongoResponce'] = "Mongo Response done";
                    }
                } else {
                    $response['Message'] = $sql->errorInfo();
                    $response['Status'] = "0";
                }
            }
        }
        return $response;
    }
    public function rejectionreson($rec) {
        $tdat = date("U");
        $tdate = $tdat;
        $umsg = $rec->RejectionReson;
        $email = $rec->Email;
        $name = $rec->Name;
        $RWAID = $rec->RWAID;
        $sql = $this->prdbh->prepare("INSERT INTO RejectionReson(ResidentRWAID, RejectionReson,Date) VALUES(?,?,?)");
        $sql->bindParam(1, $rec->ResidentRWAID);
        $sql->bindParam(2, $rec->RejectionReson);
        $sql->bindParam(3, $tdate);
        $this->prdbh->beginTransaction();
        if ($sql->execute()) {
            $id = $this->prdbh->lastInsertId();
            $this->prdbh->commit();
            $response['RejectionId'] = $id;
            $response['Status'] = "1";
            $result = $this->rejectionresonmail($umsg, $subject, $email, $name, $adminemailid, $adminnumber);
            //  print_r($result);die;
            
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
        return $result;
    }
    public function rejectionresonmail($umsg, $subject, $email, $name, $adminemailid, $adminnumber) {
        $response = array();
        $umail = $email;
        $umsg = "Hi " . $name . " ,
            
Thanks for sign-in up for mynukad. Admin has rejected you due to this reason :" . $umsg . ". 

Please contact admin, if any query.
Thank again.
                    
Regards Team mynukad  ";
        //on email ID : ".$adminemailid." or call on ".$adminnumber ."
        $usub = "[mynukad] : Admin has rejected you";
        require 'sendgrid/vendor/autoload.php';
        require 'sendgrid/lib/SendGrid.php';
        $sendgrid = new SendGrid('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');
        $email = new SendGrid\Email();
        $email->addTo($umail)->setFrom('noreply@mynukad.com')->setSubject($usub)->setText($umsg)->setHtml($dt);
        try {
            $sendgrid->send($email);
            $response = 1;
        }
        catch(\SendGrid\Exception $e) {
            $response = 0;
        }
        return $response;
    }
    public function approvemail($email, $name) {
        $response = array();
        $umail = $email;
        $umsg = "Hi " . $name . " ,
             
Thanks for sign-in up for mynukad. Admin has Approve you enjoy our features,
             
Please contact admin, if any query.
Thank again.
                    
Regards Team mynukad  ";
        //on email ID : ".$adminemailid." or call on ".$adminnumber ."
        $usub = "[mynukad] : Congrats Admin has approved you";
        require 'sendgrid/vendor/autoload.php';
        require 'sendgrid/lib/SendGrid.php';
        $sendgrid = new SendGrid('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');
        $email = new SendGrid\Email();
        $email->addTo($umail)->setFrom('noreply@mynukad.com')->setSubject($usub)->setText($umsg)->setHtml($dt);
        try {
            $sendgrid->send($email);
            $response = 1;
        }
        catch(\SendGrid\Exception $e) {
            $response = 0;
        }
        return $response;
    }
    public function addnotification($rec) {
        $tdat = date("U");
        $tdate = $tdat - 90;
        $CategoryWise = null;
        $cat = null;
        $block1 = null;
        $block = $rec->block;
        $IsOwner = $rec->IsOwner;
        $RWAID = $rec->RWAID;
        $CategoryWise = $rec->CategoryWise;
        $message = $rec->Text;
        $tvnotification = $rec->tvnotification;
        $uid = $rec->CreatedBy;
        $Description = $rec->Text;
        $image = $rec->image;
        $admin = $rec->admin;
        $FeedType = "notification";
        if ($IsOwner != '' && $block != '') {
            $blockarray = explode(",", $block);
            for ($i = 0;$i < count($blockarray);$i++) {
                $block1[].= "SocietyFlatMapping.FlatNbr like '$blockarray[$i]%' ";
            }
            $block2 = implode("or ", $block1);
            if ($IsOwner != '' && $admin != '') {
                $sql7 = $this->prdbh->prepare(" SELECT DISTINCT(ResidentRWAMapping.ResidentID) FROM ResidentRWAMapping LEFT JOIN  ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID= ResidentRWAMapping.ResidentRWAID INNER JOIN SocietyFlatMapping ON ResidentFlatMapping.FlatID=SocietyFlatMapping.ID LEFT JOIN UserRoles ON UserRoles.ResidentRWAId= ResidentRWAMapping.ResidentRWAID LEFT JOIN RoleFunctions ON RoleFunctions.RoleID=UserRoles.RoleID Where ($block2)  And ResidentRWAMapping.RWAID='" . $RWAID . "' And ResidentRWAMapping.ApprovalStatus='Y'  AND ResidentFlatMapping.IsOwner IN (" . $IsOwner . ") AND RoleFunctions.FunctionID = '" . $admin . "' Group by ResidentID ");
            } elseif ($IsOwner != '') {
                $sql7 = $this->prdbh->prepare(" SELECT DISTINCT(ResidentRWAMapping.ResidentID) FROM ResidentRWAMapping LEFT JOIN  ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID= ResidentRWAMapping.ResidentRWAID INNER JOIN SocietyFlatMapping ON ResidentFlatMapping.FlatID=SocietyFlatMapping.ID LEFT JOIN UserRoles ON UserRoles.ResidentRWAId= ResidentRWAMapping.ResidentRWAID LEFT JOIN RoleFunctions ON RoleFunctions.RoleID=UserRoles.RoleID Where ($block2)  And ResidentRWAMapping.RWAID='" . $RWAID . "' And ResidentRWAMapping.ApprovalStatus='Y'  AND ResidentFlatMapping.IsOwner IN (" . $IsOwner . ")  Group by ResidentID ");
            } else {
                $sql7 = $this->prdbh->prepare(" SELECT DISTINCT(ResidentRWAMapping.ResidentID) FROM ResidentRWAMapping LEFT JOIN  ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID= ResidentRWAMapping.ResidentRWAID INNER JOIN SocietyFlatMapping ON ResidentFlatMapping.FlatID=SocietyFlatMapping.ID LEFT JOIN UserRoles ON UserRoles.ResidentRWAId= ResidentRWAMapping.ResidentRWAID LEFT JOIN RoleFunctions ON RoleFunctions.RoleID=UserRoles.RoleID Where ($block2)  And ResidentRWAMapping.RWAID='" . $RWAID . "' And ResidentRWAMapping.ApprovalStatus='Y'  AND RoleFunctions.FunctionID = '" . $admin . "' Group by ResidentID ");
            }
        } elseif ($IsOwner != '' || $admin != '') {
            if ($IsOwner != '' && $admin != '') {
                $sql7 = $this->prdbh->prepare(" SELECT DISTINCT(ResidentRWAMapping.ResidentID) FROM ResidentRWAMapping LEFT JOIN  ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID= ResidentRWAMapping.ResidentRWAID INNER JOIN SocietyFlatMapping ON ResidentFlatMapping.FlatID=SocietyFlatMapping.ID LEFT JOIN UserRoles ON UserRoles.ResidentRWAId= ResidentRWAMapping.ResidentRWAID LEFT JOIN RoleFunctions ON RoleFunctions.RoleID=UserRoles.RoleID Where  ResidentRWAMapping.RWAID='" . $RWAID . "' And ResidentRWAMapping.ApprovalStatus='Y'  AND ResidentFlatMapping.IsOwner IN (" . $IsOwner . ") AND RoleFunctions.FunctionID = '" . $admin . "' Group by ResidentID ");
            } elseif ($IsOwner != '') {
                $sql7 = $this->prdbh->prepare(" SELECT DISTINCT(ResidentRWAMapping.ResidentID) FROM ResidentRWAMapping LEFT JOIN  ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID= ResidentRWAMapping.ResidentRWAID INNER JOIN SocietyFlatMapping ON ResidentFlatMapping.FlatID=SocietyFlatMapping.ID LEFT JOIN UserRoles ON UserRoles.ResidentRWAId= ResidentRWAMapping.ResidentRWAID LEFT JOIN RoleFunctions ON RoleFunctions.RoleID=UserRoles.RoleID Where  ResidentRWAMapping.RWAID='" . $RWAID . "' And ResidentRWAMapping.ApprovalStatus='Y'  AND ResidentFlatMapping.IsOwner IN (" . $IsOwner . ")  Group by ResidentID ");
            } else {
                $sql7 = $this->prdbh->prepare(" SELECT DISTINCT(ResidentRWAMapping.ResidentID) FROM ResidentRWAMapping LEFT JOIN  ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID= ResidentRWAMapping.ResidentRWAID INNER JOIN SocietyFlatMapping ON ResidentFlatMapping.FlatID=SocietyFlatMapping.ID LEFT JOIN UserRoles ON UserRoles.ResidentRWAId= ResidentRWAMapping.ResidentRWAID LEFT JOIN RoleFunctions ON RoleFunctions.RoleID=UserRoles.RoleID Where  ResidentRWAMapping.RWAID='" . $RWAID . "' And ResidentRWAMapping.ApprovalStatus='Y'  AND RoleFunctions.FunctionID = '" . $admin . "' Group by ResidentID ");
            }
        } elseif ($block != '') {
            $block = $rec->block;
            $blockarray = explode(",", $block);
            for ($i = 0;$i < count($blockarray);$i++) {
                $block1[].= "SocietyFlatMapping.FlatNbr like '$blockarray[$i]%' ";
            }
            $block2 = implode("or ", $block1);
            $sql7 = $this->prdbh->prepare(" SELECT DISTINCT(ResidentRWAMapping.ResidentID) FROM ResidentRWAMapping LEFT JOIN  ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID= ResidentRWAMapping.ResidentRWAID INNER JOIN SocietyFlatMapping ON ResidentFlatMapping.FlatID=SocietyFlatMapping.ID LEFT JOIN UserRoles ON UserRoles.ResidentRWAId= ResidentRWAMapping.ResidentRWAID LEFT JOIN RoleFunctions ON RoleFunctions.RoleID=UserRoles.RoleID Where ($block2)  And ResidentRWAMapping.RWAID='" . $RWAID . "' And ResidentRWAMapping.ApprovalStatus='Y'  Group by ResidentID ");
        } else {
            $sql7 = $this->prdbh->prepare(" SELECT DISTINCT(ResidentRWAMapping.ResidentID) FROM ResidentRWAMapping LEFT JOIN  ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID= ResidentRWAMapping.ResidentRWAID INNER JOIN SocietyFlatMapping ON ResidentFlatMapping.FlatID=SocietyFlatMapping.ID LEFT JOIN UserRoles ON UserRoles.ResidentRWAId= ResidentRWAMapping.ResidentRWAID LEFT JOIN RoleFunctions ON RoleFunctions.RoleID=UserRoles.RoleID Where  ResidentRWAMapping.RWAID='" . $RWAID . "' And ResidentRWAMapping.ApprovalStatus='Y' Group by ResidentID ");
        }
        $sql7->execute();
        if ($sql7->rowCount() > 0) {
            while ($row7 = $sql7->fetch(PDO::FETCH_ASSOC)) {
                $noticeResidentID[] = $row7['ResidentID'];
            }
        }
        //print_r($sql7);   die;
        $sql4 = $this->prdbh->prepare("SELECT User.ID,User.ProfilePic,User.FirstName,ResidentRWAMapping.RWAID,ResidentRWAMapping.ResidentRWAID FROM User INNER JOIN ResidentRWAMapping ON User.ID = ResidentRWAMapping.ResidentID WHERE ResidentRWAMapping.RWAID ='" . $RWAID . "' AND ResidentRWAMapping.ResidentID = '" . $uid . "'  ");
        $sql4->execute();
        if ($sql4->rowCount() > 0) {
            while ($row4 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                $ResidentRWAID = $row4['ResidentRWAID'];
                $ResidentName = $row4['FirstName'];
                $ProfilePic = $row4['ProfilePic'];
                $ResidentID = $row4['ID'];
            }
        }
        $sql5 = $this->prdbh->prepare("SELECT SocietyFlatMapping.FlatNbr FROM SocietyFlatMapping INNER JOIN ResidentFlatMapping ON ResidentFlatMapping.FlatID= SocietyFlatMapping.ID WHERE ResidentFlatMapping.ResidentRWAID ='" . $ResidentRWAID . "' ");
        $sql5->execute();
        if ($sql5->rowCount() > 0) {
            $ResidentFlatNo = array();
            while ($row5 = $sql5->fetch(PDO::FETCH_ASSOC)) {
                $ResidentFlatNbr[] = $row5['FlatNbr'];
            }
        }
        $ResidentFlatNo = implode(',', $ResidentFlatNbr);
        $sql = $this->prdbh->prepare("INSERT INTO `Notifications`(`RWAID`, `Severity`, `Title`, `Text`, `TvAdvertise`,`Image1`, `Image2`, `Image3`, `IsCalendarEvent`, 
             `DateCreated`, `CreatedBy`, `EventStartDate`, `EventEndDateTime`, `Status`,CategoryWise) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $sql->bindParam(1, $rec->RWAID);
        $sql->bindParam(2, $rec->Severity);
        $sql->bindParam(3, $rec->Title);
        $sql->bindParam(4, $rec->Text);
        $sql->bindParam(5, $rec->tvnotification);
        $sql->bindParam(6, $rec->Image1);
        $sql->bindParam(7, $rec->Image2);
        $sql->bindParam(8, $rec->Image3);
        $sql->bindParam(9, $rec->IsCalendarEvent);
        $sql->bindParam(10, $tdate);
        $sql->bindParam(11, $uid);
        $sql->bindParam(12, $rec->EventStartDate);
        $sql->bindParam(13, $rec->EventEndDateTime);
        $sql->bindParam(14, $rec->Status);
        $sql->bindParam(15, $rec->CategoryWise);
        $this->prdbh->beginTransaction();
        if ($sql->execute()) {
            $id = $this->prdbh->lastInsertId();
            $this->prdbh->commit();
            $response['Message'] = "Notification sent successfully";
            $response['notificationId'] = $id;
            $response['Status'] = "1";
            $notificationimagearray = explode(",", $image);
            for ($k = 0;$k < count($notificationimagearray);$k++) {
                $sql14 = $this->prdbh->prepare("INSERT INTO SocietyNotificationImage(NotificationID, NotificationImage) VALUES($id,'$notificationimagearray[$k]')");;
                $this->prdbh->beginTransaction();
                $sql14->execute();
                $this->prdbh->commit();
                $image1 = $notificationimagearray[0];
                $image2 = $notificationimagearray[1];
                $image3 = $notificationimagearray[2];
                $image4 = $notificationimagearray[3];
            }
            $output = $this->onlineinsert($Description, $FeedType, $id, $ResidentRWAID, $ResidentFlatNo, $ProfilePic, $ResidentID, $RWAID, $ResidentName, $image);
            $jsonObject = json_decode($output);
            if ($jsonObject->Status == 1) {
                $response['MongoResponce'] = "Photo added successfully";
            }
            for ($j = 0;$j < count($noticeResidentID);$j++) {
                $sql6 = $this->prdbh->prepare("INSERT INTO RWANoticeMapping(NoticeID, ResidentID) 
                    VALUES($id,$noticeResidentID[$j])");
                $this->prdbh->beginTransaction();
                $sql6->execute();
                $this->prdbh->commit();
            }
            $userid = implode(',', $noticeResidentID);
            $sql1 = $this->prdbh->prepare("SELECT  User.ID FROM User WHERE User.ID IN($userid) AND User.ID!= '" . $uid . "' ");
            $sql1->execute();
            if ($sql1->rowCount() > 0) {
                $UpushId1 = array();
                while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                    $UpushId1[] = $row1['ID'];
                }
                $UpushId = implode(',', $UpushId1);
                $ID = $id;
                $message = base64_decode($rec->Text);
                $UrHd1 = 'Society Notification';
                $message1 = array("message" => $message, "UrHd2" => $UrHd1, "body" => $ID);
                $result = $this->firebasenotification($UpushId, $message1);
                $response['PushStatus'] = $result;
            }
            $userid = implode(',', $noticeResidentID);
            $sql2 = $this->prdbh->prepare("SELECT User.ID,User.PhoneNbr,User.EmailID,User.FirstName 
                        FROM User INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentID=User.ID  
                        Where ResidentRWAMapping.RWAID= '" . $RWAID . "' AND ResidentID IN ($userid) GROUP BY ID ");
            $sql2->execute();
            if ($sql2->rowCount() > 0) {
                $email = array();
                $phonnbr = array();
                while ($row2 = $sql2->fetch(PDO::FETCH_ASSOC)) {
                    $email[] = $row2['EmailID'];
                    $phonnbr[] = $row2['PhoneNbr'];
                }
                $vn1 = $this->shorturlnew($image1);
                $vn2 = $this->shorturlnew($image2);
                $vn3 = $this->shorturlnew($image3);
                $vn4 = $this->shorturlnew($image4);
                if ($image4 != '') {
                    $imageurl = ' File1  : ' . $vn1 . "\r\n\r\n" . ' File2 : ' . $vn2 . "\r\n\r\n" . ' File3 : ' . $vn3 . "\r\n\r\n" . ' File4 : ' . $vn4;
                } elseif ($image3 != '') {
                    $imageurl = ' File1  : ' . $vn1 . "\r\n\r\n" . ' File2 : ' . $vn2 . "\r\n\r\n" . ' File3 : ' . $vn3;
                } elseif ($image2 != '') {
                    $imageurl = ' File1  : ' . $vn1 . "\r\n\r\n" . ' File2 : ' . $vn2;
                } elseif ($image1 != '') {
                    $imageurl = ' File1  : ' . $vn1;
                } else {
                    $imageurl = '';
                }
                if ($CategoryWise == '1') {
                    $cat = 'Informational';
                } elseif ($CategoryWise == '2') {
                    $cat = 'Critical';
                } elseif ($CategoryWise == '3') {
                    $cat = 'Need Action';
                }
                $subject = '[mynukad] : RWA sent new notice' . $cat;
                $message1 = base64_decode(urldecode($rec->Title));
                $msgdetail = base64_decode(urldecode($rec->Text));
                $msg = 'RWA sent a new notice with' . $message1 . 'Please check app for more details.';
                $message = preg_replace('/\s+/', '+', $msg);
                $d = 'Please check app for more details.' . "\r\n\r\n" . "\r\n\r\n\n\n\n\n\n\n\n\n\n\n\n" . 'Regards' . ',' . "\r\n\n" . 'Team mynukad ';
                $umsg = 'Hi ,' . "\r\n\n" . 'RWA sent new notice ' . "\r\n\n" . "Title :" . $message1 . "\r\n\n" . "Details :" . $msgdetail . " \r\n\n" . $imageurl . "\r\r\n\n" . $d;
                $sql6 = $this->prdbh->prepare("SELECT * FROM RWAConfigTable WHERE RWAID ='" . $RWAID . "' AND ConfigParamID='28' AND ConfigParamValue='1' ");
                $sql6->execute();
                if ($sql6->rowCount() > 0) {
                    $result = $this->newnotificationmail($umsg, $subject, $email);
                    //print_r($result);die;
                    
                }
            }
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function apologymail($param) {
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        $umsg = 'Dear Resident,

            We apologize for unsolicited mail earlier sent by our server regarding "General Body Meeting on 5th May" due to some technical glitch.

            Please ignore the mail, and we assure you of not repeating such errors in future.

             
            Regards,

            Team mynukad
            +91-9999598333, +91-9999698333
            www.mynukad.com';
        $subject = 'Apology for Technical Glitch';
        $response = array();
        $sql = $this->prdbh->prepare("SELECT User.ID,User.PhoneNbr,User.EmailID,User.FirstName 
                        FROM User INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentID=User.ID  
                        Where ResidentRWAMapping.RWAID= '" . $RWAID . "' AND ResidentRWAMapping.ApprovalStatus='Y' ");
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $email[] = $row['EmailID'];
                    $FirstName[] = $row['FirstName'];
                }
                $result = $this->newnotificationmail($umsg, $subject, $email);
                $response['Status'] = 1;
            } else {
                $response['MerchantsUser'] = [];
                $response['message'] = "No Data";
                $response['Status'] = 0;
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function newnotificationmail($umsg, $subject, $email) {
        $response = array();
        $umail = $email;
        $umsg = $umsg;
        $usub = $subject;
        //print_r($umail);die;
        require 'sendgrid/vendor/autoload.php';
        require 'sendgrid/lib/SendGrid.php';
        $sendgrid = new SendGrid('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');
        for ($i = 0;$i < count($umail);$i++) {
            $email = new SendGrid\Email();
            $email->addTo($umail[$i])->setFrom('noreply@mynukad.com')->setSubject($usub)->setText($umsg);
            //->addBcc('prashasya.choudhary@sanganan.com','anuj.sinha@sanganan.in');
            //->setHtml($dt);
            try {
                $sendgrid->send($email);
                $response = 1;
            }
            catch(\SendGrid\Exception $e) {
                $response = 0;
            }
        }
        return $response;
    }
    public function compstatuschangeandsms($rec) {
        //$Userid=$rec->ComplainByID;
        $Name = $rec->AssignedName;
        $s = $rec->Status;
        $RWAID = $rec->RWAID;
        //$flatno = $rec->flatno;
        $phonnbr = $rec->phonnbr;
        $ComplaintID = $rec->ComplaintID;
        //code to add flat no
        $flat= $this->prdbh->prepare("SELECT SocietyFlatMapping.FlatNbr FROM ComplaintRegister 
				LEFT JOIN ComplaintCategory ON ComplaintRegister.CategoryID = 

ComplaintCategory.ID 
				LEFT JOIN  ResidentFlatMapping ON ComplaintRegister.RWAResidentFlatID = 

ResidentFlatMapping.ID 
				LEFT JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ComplaintRegister.FlatID
				LEFT JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID = 

ResidentFlatMapping.ResidentRWAID 
				LEFT join User on User.ID=ResidentRWAMapping.ResidentID
                WHERE ComplaintRegister.ID='" . $ComplaintID . "'");
              $flatno = $flat->execute();
              // $flatno = array();
             while ($row5 = $flat->fetch(PDO::FETCH_ASSOC)) {
                 $flatno = $row5['FlatNbr'];
                // implode(',', $items3);
            }
             // echo $flatno;
              //echo "kkkkk";
              //die();
        //end code to add flat no
        $sql3 = $this->prdbh->prepare("SELECT 
            c.ResidentID FROM ComplaintRegister as a inner join ResidentFlatMapping as b on a.RWAResidentFlatID = b.ID
            inner join ResidentRWAMapping as c on b.ResidentRWAID = c.ResidentRWAID where a.ID ='" . $ComplaintID . "'");
        if ($sql3->execute()) {
            $items3 = array();
            while ($row3 = $sql3->fetch(PDO::FETCH_ASSOC)) {
                $items3[] = $row3['ResidentID'];
            }
        }
        $UserID = implode(',', $items3);
        $tdat = date("U");
        $tdate = $tdat;
        $message = 'Complaint NO. ' . $ComplaintID . ' is being assigned to you.Please visit Flat NO. ' . $flatno;
        $sql2 = $this->prdbh->prepare("SELECT * FROM RWAConfigTable WHERE RWAID='" . $RWAID . "' AND ConfigParamID='24' AND ConfigParamValue='1' ");
        $sql2->execute();
        if ($sql2->rowCount() > 0) {
            $msg = urlencode($message);
            $link = "http://textsms.co.in/app/smsapi/index.php?key=3588619015E605&routeid=6&type=text&contacts=$phonnbr&senderid=ALERTS&msg=$msg";
            $sql4 = $this->prdbh->prepare("INSERT INTO NotificationSmsDetail(RWAID, AddedOn, DebitCredit, SmsCount) VALUES('$rwaid','$tdate','2','1')");
            $this->prdbh->beginTransaction();
            if ($sql4->execute()) {
                $this->prdbh->commit();
            }
        }
        $homepage = file_get_contents($link);
        $response['message'] = $homepage;
        $sql1 = $this->prdbh->prepare("UPDATE ComplaintRegister SET Status =?, AssignedTo =?,DateAssigned =?, AssignedBy =? WHERE ID=?");
        $sql1->bindParam(1, $rec->Status);
        $sql1->bindParam(2, $rec->AssignedTo);
        $sql1->bindParam(3, $tdate);
        $sql1->bindParam(4, $rec->AssignedBy);
        $sql1->bindParam(5, $rec->ComplaintID);
        $this->prdbh->beginTransaction();
        if ($sql1->execute()) {
            $this->prdbh->commit();
            $response['Message'] = "Status changed";
            $response['Status'] = '1';
            // here write code for complain remark
            $sql = $this->prdbh->prepare("SELECT ID,FirstName,IFNULL(EmailID,'anuj@sanganan.com') as EmailID FROM User WHERE ID IN($UserID) ");
            $sql->execute();
            //print_r($sql);die;
            if ($sql->rowCount() > 0) {
                $UpushId1 = array();
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $UpushId1[] = $row['ID'];
                    $name[] = $row['FirstName'];
                    $email[] = $row['EmailID'];
                }
                $UpushId = implode(',', $UpushId1);
                $status = '';
                if ($s == 1) {
                    $status = 'Assigned';
                } elseif ($s == 2) {
                    $status = 'Resolved';
                } elseif ($s == 4) {
                    $status = 'Invalid';
                }
                $result = $this->mailoncomplainstatuschange($email, $name, $ComplaintID, $status);
                $ID = $rec->ComplaintID;
                if ($s == 1) {
                    $st = 'Your Complaint Is Being Assigned';
                } elseif ($s == 2) {
                    $st = 'Your Complaint Is Being Resolved';
                } elseif ($s == 4) {
                    $st = 'Your Complaint Is Being Invalid';
                } else {
                    $st == '';
                }
                $message = $st;
                $UrHd1 = 'Complaint Status';
                $message1 = array("message" => $message, "UrHd2" => $UrHd1, "body" => $ID);
                $result = $this->firebasenotification($UpushId, $message1);
                //print_r($result);die;
                $response['PushStatus'] = $result;
            } else {
                $response['Message'] = $sql->errorInfo();
                $response['Status'] = "0";
            }
        } else {
            $response['Message'] = $sql1->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function compstatuschange($rec) {
        $Userid = $rec->ComplainByID;
        $compID = $rec->ComplaintID;
        $s = $rec->Status;
        $tdat = date("U");
        $tdate = $tdat;
        $sql3 = $this->prdbh->prepare("SELECT 
        c.ResidentID FROM ComplaintRegister as a inner join ResidentFlatMapping as b on a.RWAResidentFlatID = b.ID
        inner join ResidentRWAMapping as c on b.ResidentRWAID = c.ResidentRWAID where a.ID ='" . $compID . "' ");
        if ($sql3->execute()) {
            $items3 = array();
            while ($row3 = $sql3->fetch(PDO::FETCH_ASSOC)) {
                $items3[] = $row3['ResidentID'];
            }
        }
        $UserID = implode(',', $items3);
        // $UserID='68';
        $sql1 = $this->prdbh->prepare("UPDATE ComplaintRegister SET Status =?, AssignedTo =?,DateAssigned =?, AssignedBy =? WHERE ID=?");
        $sql1->bindParam(1, $rec->Status);
        $sql1->bindParam(2, $rec->AssignedTo);
        $sql1->bindParam(3, $tdate);
        $sql1->bindParam(4, $rec->AssignedBy);
        $sql1->bindParam(5, $rec->ComplaintID);
        $this->prdbh->beginTransaction();
        if ($sql1->execute()) {
            $this->prdbh->commit();
            $response['Message'] = "Status changed";
            $response['Status'] = '1';
            // here write code for complain remark
            $sql = $this->prdbh->prepare("SELECT ID,FirstName,EmailID FROM User WHERE ID='" . $UserID . "' ");
            $sql->execute();
            //print_r($sql);die;
            if ($sql->rowCount() > 0) {
                $UpushId1 = array();
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $UpushId1[] = $row['ID'];
                    $name[] = $row['FirstName'];
                    $email[] = $row['EmailID'];
                }
                $UpushId = implode(',', $UpushId1);
                $status = '';
                if ($s == 1) {
                    $status = 'Assigned';
                } elseif ($s == 2) {
                    $status = 'Resolved';
                } elseif ($s == 4) {
                    $status = 'Invalid';
                }
                $result = $this->mailoncomplainstatuschange($email, $name, $ComplaintID, $status);
                $ID = $rec->ComplaintID;
                if ($s == 1) {
                    $st = 'Your Complaint Is Being Assigned';
                } elseif ($s == 2) {
                    $st = 'Your Complaint Is Being Resolved';
                } elseif ($s == 4) {
                    $st = 'Your Complaint Is Being Invalid';
                } else {
                    $st == '';
                }
                $message = $st;
                $UrHd1 = 'Complaint Status';
                $message1 = array("message" => $message, "UrHd2" => $UrHd1, "body" => $ID);
                $result = $this->firebasenotification($UpushId, $message1);
                //print_r($result);die;
                $response['PushStatus'] = $result;
            } else {
                $response['Message'] = $sql->errorInfo();
                $response['Status'] = "0";
            }
        } else {
            $response['Message'] = $sql1->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function compstatus($rec) {
        $ComplaintID = $rec->complainId;
        $s = $rec->status;
        if ($s == '1') {
            $flatno = $rec->flatno;
            $phonnbr = $rec->phone;
            $RWAID = $rec->RWAID;
            $sql3 = $this->prdbh->prepare("SELECT ResidentID FROM ResidentRWAMapping  
            INNER JOIN ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID=ResidentRWAMapping.ResidentRWAID 
            LEFT JOIN ComplaintRegister ON  ResidentFlatMapping.FlatID =  ComplaintRegister.FlatID WHERE ComplaintRegister.ID='" . $compID . "' ");
            if ($sql3->execute()) {
                $items3 = array();
                while ($row3 = $sql3->fetch(PDO::FETCH_ASSOC)) {
                    $items3[] = $row3['ResidentID'];
                }
            }
            $UserID = implode(',', $items3);
            $message = 'Complaint NO. ' . $ComplaintID . ' is being assigned to you.Please visit Flat NO. ' . $flatno;
            $sql2 = $this->prdbh->prepare("SELECT * FROM RWAConfigTable WHERE RWAID='" . $RWAID . "' AND ConfigParamID='24' AND ConfigParamValue='1' ");
            $sql2->execute();
            if ($sql2->rowCount() > 0) {
                $msg = urlencode($message);
                $link = "http://textsms.co.in/app/smsapi/index.php?key=3588619015E605&routeid=6&type=text&contacts=$phonnbr&senderid=ALERTS&msg=$msg";
                $sql4 = $this->prdbh->prepare("INSERT INTO NotificationSmsDetail(RWAID, AddedOn, DebitCredit, SmsCount) VALUES('$rwaid','$tdate','2','1')");
                $this->prdbh->beginTransaction();
                if ($sql4->execute()) {
                    $this->prdbh->commit();
                }
            }
            $homepage = file_get_contents($link);
            $response['message'] = $homepage;
        }
        //    $UserId=$rec->Userid;
        $sql = $this->prdbh->prepare("SELECT ID,FirstName,EmailID FROM User WHERE ID='" . $UserID . "' ");
        $sql->execute();
        if ($sql->rowCount() > 0) {
            $UpushId1 = array();
            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                $UpushId1[] = $row['ID'];
                $name[] = $row['FirstName'];
                $email[] = $row['EmailID'];
            }
            $UpushId = implode(',', $UpushId1);
            $ComplaintID = $rec->complainId;
            $message = $rec->remark;
            $UrHd1 = 'Complaint Status';
            $response['Status'] = "1";
            $message1 = array("message" => $message, "UrHd2" => $UrHd1, "body" => $ComplaintID);
            $result = $this->firebasenotification($UpushId, $message1);
            $response['PushStatus'] = $result;
            $status = '';
            if ($s == 1) {
                $status = 'Assigned';
            } elseif ($s == 2) {
                $status = 'Resolved';
            } elseif ($s == 4) {
                $status = 'Invalid';
            }
            $result = $this->mailoncomplainstatuschange($email, $name, $ComplaintID, $status);
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function chatnotification($rec) {
        $RWAID = $rec->RWAID;
        $UserID = $rec->UserID;
        $sql = $this->prdbh->prepare("SELECT User.ID FROM User INNER JOIN ResidentRWAMapping ON User.ID=ResidentRWAMapping.ResidentID
                                                LEFT JOIN UserRoles ON UserRoles.ResidentRWAID= ResidentRWAMapping.ResidentRWAID
                                                LEFT JOIN RoleFunctions ON UserRoles.RoleID= RoleFunctions.RoleID
                                                WHERE ResidentRWAMapping.RWAID='" . $RWAID . "' AND  RoleFunctions.FunctionID = 4 AND ResidentRWAMapping.ApprovalStatus='Y' AND User.ID!='" . $UserID . "' GROUP BY ID ");
        $sql->execute();
        if ($sql->rowCount() > 0) {
            $UpushId1 = array();
            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                $UpushId1[] = $row['ID'];
            }
            $UpushId = implode(',', $UpushId1);
            $message = $rec->Message;
            $UrHd1 = $rec->Title;
            $message1 = array("message" => $message, "UrHd2" => $UrHd1);
            $result = $this->firebasenotification($UpushId, $message1);
            $response['PushStatus'] = $result;
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function advertisement($rec) {
        $response = array();
        $EventID = $rec->EventID;
        $sql = $this->prdbh->prepare("SELECT ImagePath, TimeInterval, PlayTime, AddedOn, IsActive FROM Advertisement 
                INNER JOIN AdvertisementEvent ON AdvertisementEvent.AdvertisementID = Advertisement.ID 
                WHERE AdvertisementEvent.EventID='" . $EventID . "' AND IsActive =1 ");
        $sql->execute();
        $items = array();
        $count = $sql->rowCount();
        if ($count >= 1) {
            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                $items[] = $row;
            }
            //print_r($items);die;
            $response['Advertisement'] = $items;
            $response['Status'] = 1;
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function eventnotification($rec) {
        $RWAID = $rec->RWAID;
        //$UserID=$rec->UserID;
        $sql = $this->prdbh->prepare("SELECT User.ID FROM User INNER JOIN ResidentRWAMapping ON User.ID=ResidentRWAMapping.ResidentID
                                                LEFT JOIN UserRoles ON UserRoles.ResidentRWAID= ResidentRWAMapping.ResidentRWAID
                                                LEFT JOIN RoleFunctions ON UserRoles.RoleID= RoleFunctions.RoleID
                                                WHERE ResidentRWAMapping.RWAID='" . $RWAID . "' AND ResidentRWAMapping.ApprovalStatus='Y'  GROUP BY ID ");
        $sql->execute();
        if ($sql->rowCount() > 0) {
            $UpushId1 = array();
            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                $UpushId1[] = $row['ID'];
            }
            $UpushId = implode(',', $UpushId1);
            $message = $rec->Message;
            $UrHd1 = $rec->Title;
            $message1 = array("message" => $message, "UrHd2" => $UrHd1);
            $result = $this->firebasenotification($UpushId, $message1);
            $response['PushStatus'] = $result;
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function addnewpost($rec) {
        $ResidentRWAID = $rec->ResidentRWAID;
        $RWAID = $rec->RWAID;
        $ResidentName = $rec->ResidentName;
        $Postid = $rec->Postid;
        $userId = $rec->userId;
        $sql = $this->prdbh->prepare("SELECT RWAID,ResidentID FROM ResidentRWAMapping WHERE ApprovalStatus='Y' AND RWAID='" . $RWAID . "' AND ResidentID!= '" . $userId . "'");
        $sql->execute();
        if ($sql->rowCount() > 0) {
            $UpushId1 = array();
            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                $UpushId1[] = $row['ResidentID'];
            }
            $UpushId = implode(',', $UpushId1);
            $message = $ResidentName . ' added new post';
            $UrHd1 = 'mynukad post notification';
            $message1 = array("message" => $message, "UrHd2" => $UrHd1, "body" => $Postid);
            $result = $this->firebasenotification($UpushId, $message1);
            $response['PushStatus'] = $result;
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function flatbill($rec) {
        $FlatID = $rec->FlatID;
        $RWAID = $rec->RWAID;
        $Title = $rec->Title;
        $message = $rec->message;
        $PaymentID = $rec->PaymentID;
        $sql = $this->prdbh->prepare("SELECT ResidentRWAMapping.ResidentID FROM ResidentFlatMapping  INNER JOIN ResidentRWAMapping ON  ResidentRWAMapping.ResidentRWAID= ResidentFlatMapping.ResidentRWAID  WHERE ResidentFlatMapping.FlatID = '" . $FlatID . "' AND ResidentRWAMapping.ApprovalStatus='Y';");
        $sql->execute();
        if ($sql->rowCount() > 0) {
            $UpushId1 = array();
            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                $UpushId1[] = $row['ResidentID'];
            }
            $UpushId = implode(',', $UpushId1);
            $message = $message;
            $UrHd1 = $Title;
            $message1 = array("message" => $message, "UrHd2" => $UrHd1, "body" => $PaymentID);
            $result = $this->firebasenotification($UpushId, $message1);
            $response['PushStatus'] = $result;
        } else {
            $response['Message'] = "Input data is wrong";
            $response['Messag'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function postcommentnotif($rec) {
        $ResidentRWAID = $rec->ResidentRWAID;
        $UserID = $rec->UserID;
        $CmntByName = $rec->CmntByName;
        $SocietyFeedID = $rec->SocietyFeedID;
        $Uid = $rec->ActivityAllUser;
        $sql = $this->prdbh->prepare("SELECT RWAID,ResidentID FROM ResidentRWAMapping WHERE ApprovalStatus='Y' AND ResidentRWAID IN ($Uid) ");
        $sql->execute();
        //print_r($sql);die;
        if ($sql->rowCount() > 0) {
            $UpushId1 = array();
            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                $UpushId1[] = $row['ResidentID'];
            }
            $UpushId2 = implode(',', $UpushId1);
            // print_r($UpushId2);
            $message = $CmntByName . ' commented on post you liked/commeted';
            $UrHd12 = 'mynukad post notification';
            $message12 = array("message" => $message, "UrHd2" => $UrHd12, "body" => $SocietyFeedID);
            $result = $this->firebasenotification1($UpushId2, $message12);
            $response['PushStatus'] = $result;
            if ($UserID != '') {
                $UpushId = $UserID;
                $message2 = $CmntByName . ' commented on your post';
                $UrHd1 = 'mynukad post notification';
                $message3 = array("message" => $message2, "UrHd2" => $UrHd1, "body" => $SocietyFeedID);
                $result = $this->firebasenotification($UpushId, $message3);
            }
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function postlikenotif($rec) {
        $ResidentRWAID = $rec->ResidentRWAID;
        $UserID = $rec->UserID;
        $LikerName = $rec->LikerName;
        $SocietyFeedID = $rec->SocietyFeedID;
        $Uid = $rec->ActivityAllUser;
        $sql = $this->prdbh->prepare("SELECT RWAID,ResidentID FROM ResidentRWAMapping WHERE ApprovalStatus='Y' AND ResidentRWAID IN ($Uid) ");
        $sql->execute();
        if ($sql->rowCount() > 0) {
            $UpushId1 = array();
            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                $UpushId1[] = $row['ResidentID'];
            }
            $UpushId = implode(',', $UpushId1);
            $message = $LikerName . ' like your post';
            $UrHd1 = 'mynukad post notification';
            $message1 = array("message" => $message, "UrHd2" => $UrHd1, "body" => $SocietyFeedID);
            $result = $this->firebasenotification($UpushId, $message1);
            $response['PushStatus'] = $result;
            if ($UserID = '') {
                $message = $LikerName . ' like your post';
                $UrHd1 = 'mynukad post notification';
                $message1 = array("message" => $message, "UrHd2" => $UrHd1, "body" => $SocietyFeedID);
                $result = $this->firebasenotification($UpushId, $message1);
                $response['PushStatus'] = $result;
            }
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function onlineinsert($Description, $FeedType, $PostRefID, $ResidentRwaID, $ResidentFlatNo, $ProfilePic, $userId, $RWAID, $ResidentName, $PhotoPath) {
        $input = "Description=$Description&FeedType=$FeedType&PostRefID=$PostRefID&ResidentRwaID=$ResidentRwaID&RWAID=$RWAID&ResidentName=$ResidentName&ResidentFlatNo=$ResidentFlatNo&PhotoPath=$PhotoPath&ProfilePic=$ProfilePic&userId=$userId";
        $curl = curl_init();
        curl_setopt_array($curl, array(CURLOPT_URL => ConstantUrlMongo . "onlineinsert.php", CURLOPT_RETURNTRANSFER => true, CURLOPT_ENCODING => "", CURLOPT_MAXREDIRS => 10, CURLOPT_TIMEOUT => 30, CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1, CURLOPT_CUSTOMREQUEST => "POST", CURLOPT_POSTFIELDS => $input, CURLOPT_HTTPHEADER => array("content-type: application/x-www-form-urlencoded"),));
        $result = curl_exec($curl);
        $err = curl_error($curl);
        //print_r($response);die;
        return $result;
    }
    public function vehiclemovementstatus($rec) {
        $tdat = date("U");
        $tdate = $tdat;
        $RWAID = $rec->RWAID;
        $sql = $this->prdbh->prepare("INSERT INTO VehicleMovementStatus(VehicleID, EntryType, DateTimeStamp, GateNbr) VALUES (?,?,?,?)");
        $sql->bindParam(1, $rec->VehicleID);
        $sql->bindParam(2, $rec->EntryType);
        $sql->bindParam(3, $rec->DateTimeStamp);
        $sql->bindParam(4, $rec->GateNbr);
        $this->prdbh->beginTransaction();
        if ($sql->execute()) {
            $id = $this->prdbh->lastInsertId();
            $this->prdbh->commit();
            $response['Message'] = "Successfully";
            $response['Status'] = "1";
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function vehiclentryrfidget($rec) {
        $tdat = date("U");
        $tdate = $tdat;
        //$rwaid=$_GET['RWAID'];
        //$requestHeaders = apache_request_headers();
        $entrytype = $_GET['Entrytype'];
        $RFIDTag = $_GET['RFIDTag'];
        $RWAID = $_GET['RWAID'];
        $DateTimeStamp = $_GET['DateTimeStamp'];
        $GateNbr = $_GET['GateNbr'];
        // 1 =in, 0= out
        $sql13 = $this->prdbh->prepare("SELECT VehicleID, RFIDTag, RWAID FROM VehicleRFIDMapping WHERE RWAID= '" . $RWAID . "' AND RFIDTag = '" . $RFIDTag . "'  ");
        $sql13->execute();
        if ($sql13->rowCount() > 0) {
            if ($entrytype == 0) {
                $message = "out";
                $sql1 = $this->prdbh->prepare("SELECT VehicleID, RFIDTag, RWAID FROM VehicleRFIDMapping WHERE RWAID= '" . $RWAID . "' AND RFIDTag = '" . $RFIDTag . "' AND Allowexit='1'  ");
            } elseif ($entrytype == 1) {
                $message = "in";
                $sql1 = $this->prdbh->prepare("SELECT VehicleID, RFIDTag, RWAID FROM VehicleRFIDMapping WHERE RWAID= '" . $RWAID . "' AND RFIDTag = '" . $RFIDTag . "' AND Allowentry='1'  ");
            }
            $sql1->execute();
            //print_r($sql1);die;
            if ($sql1->rowCount() > 0) {
                while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                    $VehicleID = $row1['VehicleID'];
                }
                $sql = $this->prdbh->prepare("INSERT INTO VehicleMovementStatus(VehicleID, EntryType, DateTimeStamp, GateNbr) VALUES (?,?,?,?)");
                $sql->bindParam(1, $VehicleID);
                $sql->bindParam(2, $entrytype);
                $sql->bindParam(3, $DateTimeStamp);
                $sql->bindParam(4, $GateNbr);
                $this->prdbh->beginTransaction();
                if ($sql->execute()) {
                    $id = $this->prdbh->lastInsertId();
                    $this->prdbh->commit();
                    $response['Message'] = "You are permited to " . $message;
                    $response['Status'] = "1";
                } else {
                    $response['Message'] = $sql->errorInfo();
                    $response['Status'] = "0";
                }
            } else {
                $response['Message'] = "You are not permited to " . $message;
                $response['Status'] = "0";
            }
        } else {
            $response['Message'] = "You are not permited becouse RFID is wrong";
            $response['Status'] = "2";
        }
        return $response;
    }
    public function vehiclentryrfid($rec) {
        $tdat = date("U");
        $tdate = $tdat;
        //$RWAID=$rec->RWAID;
        //$RFIDTag=$rec->RFIDTag;
        //$EntryType=$rec->EntryType;
        $requestHeaders = apache_request_headers();
        $entrytype = $requestHeaders['Entrytype'];
        $RFIDTag = $requestHeaders['RFIDTag'];
        $RWAID = $requestHeaders['RWAID'];
        $DateTimeStamp = $requestHeaders['DateTimeStamp'];
        $GateNbr = $requestHeaders['GateNbr'];
        // 1 =in, 0= out
        if ($entrytype == 0) {
            $message = "out";
            $sql1 = $this->prdbh->prepare("SELECT VehicleID, RFIDTag, RWAID FROM VehicleRFIDMapping WHERE RWAID= '" . $RWAID . "' AND RFIDTag = '" . $RFIDTag . "' AND Allowexit='1'  ");
        } elseif ($entrytype == 1) {
            $message = "in";
            $sql1 = $this->prdbh->prepare("SELECT VehicleID, RFIDTag, RWAID FROM VehicleRFIDMapping WHERE RWAID= '" . $RWAID . "' AND RFIDTag = '" . $RFIDTag . "' AND Allowentry='1'  ");
        }
        $sql1->execute();
        //print_r($sql1);die;
        if ($sql1->rowCount() > 0) {
            while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                $VehicleID = $row1['VehicleID'];
            }
            $sql = $this->prdbh->prepare("INSERT INTO VehicleMovementStatus(VehicleID, EntryType, DateTimeStamp, GateNbr) VALUES (?,?,?,?)");
            $sql->bindParam(1, $VehicleID);
            $sql->bindParam(2, $entrytype);
            $sql->bindParam(3, $DateTimeStamp);
            $sql->bindParam(4, $GateNbr);
            $this->prdbh->beginTransaction();
            if ($sql->execute()) {
                $id = $this->prdbh->lastInsertId();
                $this->prdbh->commit();
                $response['Message'] = "You are permited to " . $message;
                $response['Status'] = "1";
            } else {
                $response['Message'] = $sql->errorInfo();
                $response['Status'] = "0";
            }
        } else {
            $response['Message'] = "You are not permited to " . $message;
            $response['Status'] = "0";
        }
        return $response;
    }
    public function addrfid($rec) {
        $tdat = date("U");
        $tdate = $tdat;
        $sql = $this->prdbh->prepare("INSERT INTO VehicleRFIDMapping(VehicleID, RFIDTag, RWAID, Allowentry, Allowexit, LastUpdateTime) VALUES (?,?,?,?,?,?)");
        $sql->bindParam(1, $rec->VehicleID);
        $sql->bindParam(2, $rec->RFIDTag);
        $sql->bindParam(3, $rec->RWAID);
        $sql->bindParam(4, $rec->Allowentry);
        $sql->bindParam(5, $rec->Allowexit);
        $sql->bindParam(6, $tdate);
        $this->prdbh->beginTransaction();
        if ($sql->execute()) {
            $id = $this->prdbh->lastInsertId();
            $this->prdbh->commit();
            $response['Message'] = "Added successfully";
            $response['Status'] = "1";
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function deleterfid($rec) {
        $tdat = date("U");
        $tdate = $tdat;
        $RFIDTag = $rec->RFIDTag;
        $sql4 = $this->prdbh->prepare("SELECT RFIDTag FROM VehicleRFIDMapping WHERE RFIDTag ='" . $RFIDTag . "' ");
        $sql4->execute();
        //print_r($sql4);die;
        if ($sql4->rowCount() > 0) {
            $sql = $this->prdbh->prepare("Update VehicleRFIDMapping set RFIDTag='' WHERE RFIDTag='" . $RFIDTag . "' ");
            if ($sql->execute()) {
                $response['Message'] = "Removed successfully";
                $response['Status'] = "1";
            } else {
                $response['Message'] = $sql->errorInfo();
                $response['Status'] = "0";
            }
        } else {
            $response['Message'] = "RFID is invalid";
            $response['Status'] = "0";
        }
        return $response;
    }
    public function modeofpayment($rec) {
        //$RWAID=$rec->RWAID;
        $rwaid = $_GET['RWAID'];
        $sql = $this->prdbh->prepare("SELECT ID,Description From ModeOfPayment Where RWAID='" . $rwaid . "' ");
        //print_r($sql);die;
        $sql->execute();
        $Description = array();
        if ($sql->rowCount() > 0) {
            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                $Description[] = $row;
            }
            $response['Description'] = $Description;
            $response['Status'] = "1";
        } else {
            $response['Description'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function paymentduehistory($rec) {
        $FlatID = $rec->FlatID;
        if (isset($rec->HistoryType)) {
            $historytype = $rec->HistoryType;
        } else {
            $response['Message'] = "Please Select History Type";
        }
        if ($historytype != '') {
            if ($historytype == 'Bill') {
                $sql = $this->prdbh->prepare("SELECT ID,BillAmount,BillingDate,DueAmount,PaymentDueDate,StatusOfPayment From MaintenanceBillingFlat  WHERE FlatID='" . $FlatID . "' ORDER BY ID DESC    ");
            } elseif ($historytype == 'Payment') {
                $sql = $this->prdbh->prepare("SELECT MaintPaymentHistory.ID,MaintPaymentHistory.PaidAmount,MaintPaymentHistory.RefranceNumber,MaintPaymentHistory.DateOfPayment,(ModeOfPayment.Description) as ModeOfPayment, MaintPaymentHistory.ApprovalStatus,MaintPaymentHistory.issueddate  FROM MaintPaymentHistory INNER JOIN ModeOfPayment on ModeOfPayment.ID= MaintPaymentHistory.ModeOfPayment  Where  FlatID ='" . $FlatID . "' ORDER BY ID DESC    ");
            } else {
                $response['Message'] = "Please Select History Type";
                $response['Status'] = "0";
            }
            $sql->execute();
            $items = array();
            if ($sql->rowCount() > 0) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['History'] = $items;
                $response['Status'] = "1";
            } else {
                //$response['History'] = $sql->errorInfo();
                $response['History'] = "";
                if ($historytype == 'Bill') {
                    $response['Message'] = "Bill is not not generate for your Flat";
                } elseif ($historytype == 'Payment') {
                    $response['Message'] = "Make First Payment";
                }
                $response['Status'] = "0";
            }
        } else {
            $response['Message'] = "Please Select History Type";
            $response['Status'] = "0";
        }
        return $response;
    }
    public function billpayhistorydetail($rec) {
        $ID = $rec->ID;
        if (isset($rec->HistoryType)) {
            $historytype = $rec->HistoryType;
        } else {
            $response['Message'] = "Please Select History Type";
        }
        if ($historytype != '') {
            if ($historytype == 'Bill') {
                $sql = $this->prdbh->prepare("SELECT ID,BillAmount,BillingDate,DueAmount,PaymentDueDate,StatusOfPayment, Penality From MaintenanceBillingFlat  WHERE ID='" . $ID . "' ");
            } elseif ($historytype == 'Payment') {
                $sql = $this->prdbh->prepare("SELECT MaintPaymentHistory.ID,MaintPaymentHistory.PaidAmount,MaintPaymentHistory.RefranceNumber,MaintPaymentHistory.DateOfPayment,(ModeOfPayment.Description) as ModeOfPayment, MaintPaymentHistory.ApprovalStatus,MaintPaymentHistory.BankName,MaintPaymentHistory.Image ,MaintPaymentHistory.Reason,MaintPaymentHistory.issueddate FROM MaintPaymentHistory INNER JOIN ModeOfPayment on ModeOfPayment.ID = MaintPaymentHistory.ModeOfPayment  Where  MaintPaymentHistory.ID ='" . $ID . "' ");
            } else {
                $response['Message'] = "Please Select History Type";
                $response['Status'] = "0";
            }
            $sql->execute();
            $items = array();
            if ($sql->rowCount() > 0) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['Historydetail'] = $items;
                $response['Status'] = "1";
            } else {
                //$response['History'] = $sql->errorInfo();
                $response['History'] = "";
                if ($historytype == 'Bill') {
                    $response['Message'] = "Bill is not not generate for your Flat";
                } elseif ($historytype == 'Payment') {
                    $response['Message'] = "Make First Payment";
                }
                $response['Status'] = "0";
            }
        } else {
            $response['Message'] = "Please Select History Type";
            $response['Status'] = "0";
        }
        return $response;
    }
    public function makepayment($rec) {
        $tdat = date("U");
        $tdate = $tdat - 185;
        $FlatID = $rec->FlatID;
        $PaidAmount = $rec->PaidAmount;
        $RefranceNumber = $rec->RefranceNumber;
        //$DateOfPayment=$rec->DateOfPayment;
        $ModeOfPayment = $rec->ModeOfPayment;
        $BankDetail = $rec->BankDetail;
        $screenshortImage = $rec->screenshortImage;
        $issueddate = $rec->issueddate;
        $sql = $this->prdbh->prepare("INSERT INTO MaintPaymentHistory(FlatID, PaidAmount, RefranceNumber, DateOfPayment, ModeOfPayment,ApprovalStatus,Image,issueddate) VALUES('$FlatID','$PaidAmount','$RefranceNumber','$tdate','$ModeOfPayment','0','$screenshortImage','$issueddate')");
        $this->prdbh->beginTransaction();
        if ($sql->execute()) {
            $id = $this->prdbh->lastInsertId();
            $this->prdbh->commit();
            $sql1 = $this->prdbh->prepare("UPDATE MaintenanceBillingFlat SET StatusOfPayment='1' WHERE FlatID='" . $FlatID . "' order by id desc limit 1 ");
            $sql1->execute();
            $response['Message'] = "paid successfully";
            $response['paymentid'] = $id;
            $response['Status'] = '1';
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function callout($rec) {
        $tdat = date("U");
        $tdate = $tdat - 185;
        $RWAID = $rec->RWAID;
        $UserID = $rec->UserID;
        $message = $rec->Description;
        $ResidentRwaID = $rec->SenderID;
        $FeedType = "callout";
        $sql4 = $this->prdbh->prepare("SELECT FirstName,ProfilePic FROM User WHERE ID ='" . $UserID . "' ");
        $sql4->execute();
        if ($sql4->rowCount() > 0) {
            while ($row4 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                $ResidentName = $row4['FirstName'];
                $ProfilePic = $row4['ProfilePic'];
            }
        }
        $sql5 = $this->prdbh->prepare("SELECT SocietyFlatMapping.FlatNbr FROM SocietyFlatMapping INNER JOIN ResidentFlatMapping ON ResidentFlatMapping.FlatID= SocietyFlatMapping.ID WHERE ResidentFlatMapping.ResidentRWAID ='" . $ResidentRwaID . "' ");
        $sql5->execute();
        if ($sql5->rowCount() > 0) {
            $ResidentFlatNo = array();
            while ($row5 = $sql5->fetch(PDO::FETCH_ASSOC)) {
                $ResidentFlatNbr[] = $row5['FlatNbr'];
            }
        }
        $ResidentFlatNo = implode(',', $ResidentFlatNbr);
        $sql = $this->prdbh->prepare("INSERT INTO CallOuts(SenderID, DateSent, Description ,IsActive) VALUES (?,?,?,1)");
        $sql->bindParam(1, $rec->SenderID);
        $sql->bindParam(2, $tdate);
        $sql->bindParam(3, $rec->Description);
        $this->prdbh->beginTransaction();
        if ($sql->execute()) {
            $id = $this->prdbh->lastInsertId();
            $this->prdbh->commit();
            $output = $this->onlineinsert($message, $FeedType, $id, $ResidentRwaID, $ResidentFlatNo, $ProfilePic, $UserID, $RWAID, $ResidentName, $PhotoPath);
            $jsonObject = json_decode($output);
            if ($jsonObject->Status == 1) {
                $response['MongoResponce'] = "Callout sent successfully";
            }
            $response['Message'] = "Callout sent successfully";
            $sql1 = $this->prdbh->prepare("SELECT User.ID FROM User
                                                INNER JOIN ResidentRWAMapping ON User.ID=ResidentRWAMapping.ResidentID
                                                LEFT JOIN RWA ON ResidentRWAMapping.RWAID=RWA.ID
                                                WHERE RWAID='" . $RWAID . "' AND User.ID!='" . $UserID . "' ");
            $sql1->execute();
            if ($sql1->rowCount() > 0) {
                $UpushId1 = array();
                while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                    $UpushId1[] = $row1['ID'];
                }
                $UpushId = implode(',', $UpushId1);
                $ID = '' . $id;
                $message = base64_decode($rec->Description);
                $UrHd1 = 'Society CallOuts';
                //$ID='C'.$id;
                $response['calloutId'] = $ID;
                $response['Status'] = "1";
                $message1 = array("message" => $message, "UrHd2" => $UrHd1, "body" => $ID);
                $result = $this->firebasenotification($UpushId, $message1);
                $response['PushStatus'] = $result;
            }
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function firebaselogpopup($rec) {
        $tdat = date("U");
        $tdate = $tdat;
        $sql = $this->prdbh->prepare("INSERT INTO FirebaseLogPopUp(PopupID, UserID, RWAID, Hits) VALUES (?,?,?,?)");
        $sql->bindParam(1, $rec->PopupID);
        $sql->bindParam(2, $rec->UserID);
        $sql->bindParam(3, $rec->RWAID);
        $sql->bindParam(4, $tdate);
        $this->prdbh->beginTransaction();
        if ($sql->execute()) {
            $id = $this->prdbh->lastInsertId();
            $this->prdbh->commit();
            $response['Message'] = "firebaselogPopup successfully";
            $response['Status'] = "1";
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function firebaselogbanner($rec) {
        $tdat = date("U");
        $tdate = $tdat;
        $sql = $this->prdbh->prepare("INSERT INTO FirebaseLogBanner(BannerID, UserID, RWAID, Hits) VALUES (?,?,?,?)");
        $sql->bindParam(1, $rec->BannerID);
        $sql->bindParam(2, $rec->UserID);
        $sql->bindParam(3, $rec->RWAID);
        $sql->bindParam(4, $tdate);
        $this->prdbh->beginTransaction();
        if ($sql->execute()) {
            $id = $this->prdbh->lastInsertId();
            $this->prdbh->commit();
            $response['Message'] = "FirebaseLogBanner successfully";
            $response['Status'] = "1";
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function complaincommentmail($flatnbr, $name, $message, $compid, $email) {
        $response = array();
        $umail = $email;
        $usub = '[mynukad] : ' . $name . ',' . $flatnbr . ' added comment on complaint No. ' . $compid;
        $d = 'Please check app or website for details' . "\r\r\n\r\n\n\n\n\n\n\n\n\n\n\n\n" . 'Regards' . ',' . "\r\n\n" . 'Team,mynukad';
        $umsg = $name . ', ' . $flatnbr . ' added a comment :-' . "\r\r\n\r\n\n\n\n\n\n\n\n\n\n\n\n" . $message . ',' . "\r\n\n\n" . $d;
        require 'sendgrid/vendor/autoload.php';
        require 'sendgrid/lib/SendGrid.php';
        $sendgrid = new SendGrid('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');
        $email = new SendGrid\Email();
        $email->addTo($umail)->setFrom('noreply@mynukad.com')->setSubject($usub)->setText($umsg)->setHtml($dt);
        try {
            $sendgrid->send($email);
            $response['message'] = "Sent mail successfully";
            $response['status'] = 1;
        }
        catch(\SendGrid\Exception $e) {
            echo $e->getCode();
            $response['status'] = 0;
            $response['message'] = "Something went wrong.Mail not sent";
            foreach ($e->getErrors() as $er) {
                echo $er;
            }
        }
        return $response;
    }
    public function complaincomment($rec) {
        $tdat = date("U");
        $tdate = $tdat;
        $useremail = $rec->Useremail;
        $flatnbr = $rec->FlatNbr;
        $name = $rec->FirstName;
        $sql = $this->prdbh->prepare("INSERT INTO CompRemark(Remark, CompID, RemarkDate, EnteredBy) VALUES (?,?,?,?)");
        $sql->bindParam(1, $rec->Remark);
        $sql->bindParam(2, $rec->CompID);
        $sql->bindParam(3, $tdate);
        $sql->bindParam(4, $rec->EnteredBy);
        $this->prdbh->beginTransaction();
        if ($sql->execute()) {
            $id = $this->prdbh->lastInsertId();
            $this->prdbh->commit();
            $response['Message'] = "Comments added successfully";
            $response['Status'] = "1";
            $sql1 = $this->prdbh->prepare("SELECT User.FirstName,ResidentRWAMapping.ResidentID,SocietyFlatMapping.FlatNbr,User.EmailID FROM
                                                ResidentRWAMapping INNER JOIN User On User.ID=ResidentRWAMapping.ResidentID
                                                INNER JOIN ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID= ResidentRWAMapping.ResidentRWAID INNER JOIN ComplaintRegister ON ComplaintRegister.RWAResidentFlatID= ResidentFlatMapping.ID LEFT JOIN SocietyFlatMapping ON SocietyFlatMapping.ID=ResidentFlatMapping.FlatID
                                                WHERE ComplaintRegister.ID='" . $rec->CompID . "' ");
            $sql1->execute();
            if ($sql1->rowCount() > 0) {
                $UpushId1 = array();
                while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                    $UpushId1[] = $row1['ResidentID'];
                    $email = $row1['EmailID'];
                }
                $UpushId = implode(',', $UpushId1);
                $ID = $rec->CompID;
                $message = base64_decode($rec->Remark);
                $UrHd1 = 'Complain comment';
                $message1 = array("message" => $message, "UrHd2" => $UrHd1, "body" => $ID);
                $result = $this->firebasenotification($UpushId, $message1);
                $response['PushStatus'] = $result;
                if ($email != $useremail) {
                    $result1 = $this->complaincommentmail($flatnbr, $name, $message, $ID, $email);
                }
            }
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function devicetoken($rec) {
        $response = array();
        $UserID = $rec->UserID;
        $DeviceType = $rec->DeviceType;
        $sql = $this->prdbh->prepare("SELECT DeviceUniqueID,UserID,DeviceType FROM UserDeviceMapping WHERE UserID='" . $UserID . "' AND DeviceType='" . $DeviceType . "'");
        $sql->execute();
        if ($sql->rowCount() > 0) {
            $sql1 = $this->prdbh->prepare("UPDATE UserDeviceMapping SET DeviceUniqueID=? WHERE UserID=? AND DeviceType=?");
        } else {
            $sql1 = $this->prdbh->prepare("INSERT INTO UserDeviceMapping(DeviceUniqueID,UserID,DeviceType) VALUES (?,?,?)");
        }
        $sql1->bindParam(1, $rec->DeviceUniqueID);
        $sql1->bindParam(2, $rec->UserID);
        $sql1->bindParam(3, $rec->DeviceType);
        $this->prdbh->beginTransaction();
        if ($sql1->execute()) {
            //$id= $this->prdbh->lastInsertId();
            $this->prdbh->commit();
            $response['Message'] = "Token inserted";
            $response['Status'] = 1;
        } else {
            $response['Message'] = $sql1->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function postlike($rec) {
        $response = array();
        $UserID = $rec->UserID;
        $PostID = $rec->PostID;
        $IsLike = $rec->IsLike;
        $sql = $this->prdbh->prepare("SELECT IsLike, UserID, PostID FROM Postlike WHERE UserID='" . $UserID . "' AND PostID='" . $PostID . "'");
        $sql->execute();
        if ($sql->rowCount() > 0) {
            $sql1 = $this->prdbh->prepare("UPDATE Postlike SET IsLike= '$IsLike' WHERE PostID= '$PostID' AND UserID= '$UserID'  ");
        } else {
            $sql1 = $this->prdbh->prepare("INSERT INTO Postlike(IsLike, UserID, PostID) VALUES('$IsLike','$UserID','$PostID')");
        }
        $this->prdbh->beginTransaction();
        if ($sql1->execute()) {
            $this->prdbh->commit();
            if ($rec->IsLike == '1') $response['Message'] = "Post Liked";
            elseif ($rec->IsLike == '0') $response['Message'] = "Post Unliked";
            $response['Status'] = 1;
        } else {
            $response['Message'] = $sql1->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function photolike($rec) {
        $response = array();
        $ResidentID = $rec->ResidentID;
        $PhotoID = $rec->PhotoID;
        $Likes = $rec->Likes;
        $sql = $this->prdbh->prepare("SELECT PhotoID,Likes, ResidentID, IsActive FROM PhotoGalleryLike WHERE ResidentID='" . $ResidentID . "' AND PhotoID='" . $PhotoID . "'");
        $sql->execute();
        if ($sql->rowCount() > 0) {
            $sql1 = $this->prdbh->prepare("UPDATE PhotoGalleryLike SET Likes= '$Likes' WHERE PhotoID= '$PhotoID' AND ResidentID= '$ResidentID'  ");
        } else {
            $sql1 = $this->prdbh->prepare("INSERT INTO PhotoGalleryLike(PhotoID,Likes, ResidentID, IsActive) VALUES('$PhotoID','$Likes','$ResidentID',1)");
        }
        $this->prdbh->beginTransaction();
        if ($sql1->execute()) {
            $this->prdbh->commit();
            if ($rec->Likes == '1') $response['Message'] = "Photo Liked";
            elseif ($rec->Likes == '0') $response['Message'] = "Photo Unliked";
            $response['Status'] = 1;
            if ($rec->Likes == '1') {
                $sql2 = $this->prdbh->prepare("SELECT ResidentRWAMapping.ResidentID FROM PhotoGallery 
                        INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID = PhotoGallery.AddedBy 
                        WHERE PhotoID ='" . $PhotoID . "' AND ResidentRWAMapping.ResidentID!= '" . $ResidentID . "' ");
                $sql2->execute();
                if ($sql2->rowCount() > 0) {
                    while ($row2 = $sql2->fetch(PDO::FETCH_ASSOC)) {
                        $UpushId = $row2['ResidentID'];
                    }
                    $sql3 = $this->prdbh->prepare("SELECT FirstName FROM User WHERE ID ='" . $ResidentID . "' ");
                    $sql3->execute();
                    if ($sql3->rowCount() > 0) {
                        while ($row3 = $sql3->fetch(PDO::FETCH_ASSOC)) {
                            $name = $row3['FirstName'];
                        }
                    }
                    $message = $name . ' likes your pic';
                    $UrHd1 = 'Society Photo Gallery';
                    $message1 = array("message" => $message, "UrHd2" => $UrHd1, "body" => $PhotoID);
                    $result = $this->firebasenotification($UpushId, $message1);
                    $response['PushStatus'] = $result;
                }
            }
        } else {
            $response['Message'] = $sql1->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function societypost($param) {
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        if (isset($param->UserID)) {
            $UserID = $param->UserID;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT SocietyPost.ID, SocietyPost.RWAID, SocietyPost.Post, SocietyPost.Image, SocietyPost.Image2, SocietyPost.Image3, SocietyPost.ResidentRWAID,User.FirstName,User.ProfilePic, SocietyPost.DateCreated FROM SocietyPost INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentRWAID= SocietyPost.ResidentRWAID LEFT JOIN User On User.ID= ResidentRWAMapping.ResidentID WHERE SocietyPost.RWAID = '" . $RWAID . "' ");
        $sql->execute();
        if ($sql) {
            $items = array();
            $gal = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $gal['ID'] = $row['ID'];
                    $gal['RWAID'] = $row['RWAID'];
                    $gal['Post'] = $row['Post'];
                    $gal['Image'] = $row['Image'];
                    $gal['Image2'] = $row['Image2'];
                    $gal['Image3'] = $row['Image3'];
                    $gal['ResidentRWAID'] = $row['ResidentRWAID'];
                    $gal['FirstName'] = $row['FirstName'];
                    $gal['ProfilePic'] = $row['ProfilePic'];
                    $gal['DateCreated'] = $row['DateCreated'];
                    $PostID = $row['ID'];
                    $sql3 = $this->prdbh->prepare("SELECT COUNT(PostID) AS IsLike FROM Postlike WHERE PostID = '" . $PostID . "' AND IsLike =  '1' ");
                    $sql3->execute();
                    if ($sql3->rowCount() > 0) {
                        while ($row3 = $sql3->fetch(PDO::FETCH_ASSOC)) {
                            $gal['LikeCount'] = $row3['IsLike'];
                        }
                    } else {
                        $gal['LikeCount'] = '0';
                    }
                    $sql4 = $this->prdbh->prepare("SELECT COUNT(PostId) AS Commentcount FROM PostComment WHERE PostId = '" . $PostID . "' ");
                    $sql4->execute();
                    if ($sql4->rowCount() > 0) {
                        while ($row4 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                            $gal['CommentCount'] = $row4['Commentcount'];
                        }
                    } else {
                        $gal['CommentCount'] = '0';
                    }
                    $sql5 = $this->prdbh->prepare("SELECT PostID FROM Postlike WHERE IsLike =  '1' AND UserID ='" . $UserID . "' AND PostID =  '" . $PostID . "' ");
                    $sql5->execute();
                    if ($sql5->rowCount() > 0) {
                        $gal['IsLIKES'] = '1';
                    } else {
                        $gal['IsLIKES'] = '0';
                    }
                    $items[] = $gal;
                }
                $response['Society Post'] = $items;
                $response['Status'] = '1';
            } else {
                $response['Society Post'] = [];
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function devicetoken1($rec) {
        $sql = $this->prdbh->prepare("INSERT INTO UserDeviceMapping(UserID,DeviceUniqueID, DeviceType)
        VALUES(?,?,?)");
        $sql->bindParam(1, $rec->UserID);
        $sql->bindParam(2, $rec->DeviceUniqueID);
        $sql->bindParam(3, $rec->DeviceType);
        $this->prdbh->beginTransaction();
        $sql->execute();
        $count = $sql->rowCount();
        if ($count > 0) {
            $id = $this->prdbh->lastInsertId();
            $this->prdbh->commit();
            $response['Message'] = "Device token added";
            $response['Device Token Id'] = $id;
            $response['Status'] = "1";
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function addfavhelper($rec) {
        $tdat = date("U");
        $tdate = $tdat;
        $sql = $this->prdbh->prepare("INSERT INTO UserFavHelper(UserID,HelperID, AddedOn)
        VALUES(?,?,?)");
        $sql->bindParam(1, $rec->UserID);
        $sql->bindParam(2, $rec->HelperID);
        $sql->bindParam(3, $tdate);
        $this->prdbh->beginTransaction();
        $sql->execute();
        $count = $sql->rowCount();
        if ($count > 0) {
            $id = $this->prdbh->lastInsertId();
            $this->prdbh->commit();
            $response['Message'] = "Fav helper added";
            $response['Fev Helper Id'] = $id;
            $response['Status'] = "1";
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function addpostcomment($rec) {
        $tdat = date("U");
        $tdate = $tdat;
        $sql = $this->prdbh->prepare("INSERT INTO PostComment(Comment, PostId, UserID,CommentDate, IsActive) VALUES (?,?,?,?,1)");
        $sql->bindParam(1, $rec->Comment);
        $sql->bindParam(2, $rec->PostId);
        $sql->bindParam(3, $rec->UserID);
        $sql->bindParam(4, $tdate);
        $this->prdbh->beginTransaction();
        $sql->execute();
        $count = $sql->rowCount();
        if ($count > 0) {
            $id = $this->prdbh->lastInsertId();
            $this->prdbh->commit();
            $response['Message'] = "Commneted on post";
            $response['Comment Id'] = $id;
            $response['Status'] = "1";
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function createpost($rec) {
        $tdat = date("U");
        $tdate = $tdat;
        $sql = $this->prdbh->prepare("INSERT INTO SocietyPost(RWAID, Post, Image, Image2, Image3, ResidentRWAID, DateCreated, IsActive) VALUES (?,?,?,?,?,?,?,1)");
        $sql->bindParam(1, $rec->RWAID);
        $sql->bindParam(2, $rec->Post);
        $sql->bindParam(3, $rec->Image);
        $sql->bindParam(4, $rec->Image2);
        $sql->bindParam(5, $rec->Image3);
        $sql->bindParam(6, $rec->ResidentRWAID);
        $sql->bindParam(7, $tdate);
        $this->prdbh->beginTransaction();
        $sql->execute();
        $count = $sql->rowCount();
        if ($count > 0) {
            $id = $this->prdbh->lastInsertId();
            $this->prdbh->commit();
            $response['Message'] = "Post Created";
            $response['Post Id'] = $id;
            $response['Status'] = "1";
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function addshopdetail($rec) {
        $tdat = date("U");
        $tdate = $tdat;
        $sql = $this->prdbh->prepare("INSERT INTO NearBy(Name,CategoryID, Address1, Address2, City, Latitude, Longitude, Is247,IsHomeDelivery, 
                                        PhoneNbr1, PhoneNbr2, PhoneNbr3, BannerImage, Image2, Image3, Description, AddedBy, AddedOn, IsActive)
                                        VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,0)");
        $sql->bindParam(1, $rec->Name);
        $sql->bindParam(2, $rec->CategoryID);
        $sql->bindParam(3, $rec->Address1);
        $sql->bindParam(4, $rec->Address2);
        $sql->bindParam(5, $rec->City);
        $sql->bindParam(6, $rec->Latitude);
        $sql->bindParam(7, $rec->Longitude);
        $sql->bindParam(8, $rec->Is247);
        $sql->bindParam(9, $rec->IsHomeDelivery);
        $sql->bindParam(10, $rec->PhoneNbr1);
        $sql->bindParam(11, $rec->PhoneNbr2);
        $sql->bindParam(12, $rec->PhoneNbr3);
        $sql->bindParam(13, $rec->BannerImage);
        $sql->bindParam(14, $rec->Image2);
        $sql->bindParam(15, $rec->Image3);
        $sql->bindParam(16, $rec->Description);
        $sql->bindParam(17, $rec->AddedBy);
        $sql->bindParam(18, $tdate);
        $this->prdbh->beginTransaction();
        $sql->execute();
        $count = $sql->rowCount();
        if ($count > 0) {
            $id = $this->prdbh->lastInsertId();
            $this->prdbh->commit();
            $response['Message'] = "Shop created successfully";
            $response['ShopID'] = $id;
            $response['Status'] = "1";
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function addcomplain($rec) {
        $tdat = date("U");
        $tdate = $tdat - 90;
        $RWAResidentFlatID = $rec->RWAResidentFlatID;
        $Description = $rec->ComplaintDetails;
        $RWAID = $rec->RWAID;
        $PhotoPath = $rec->Image1;
        $FeedType = "complain";
        $sql6 = $this->prdbh->prepare("SELECT  * FROM ComplainCommenArea WHERE  CompCatID= '" . $rec->CategoryID . "' ");
        $sql6->execute();
        if ($sql6->rowCount() > 0) {
            $IsCommonAreaComplaint = '1';
        } else {
            $IsCommonAreaComplaint = $rec->IsCommonAreaComplaint;
        }
        $sql4 = $this->prdbh->prepare("SELECT User.ID,User.ProfilePic,User.FirstName,ResidentRWAMapping.RWAID,ResidentRWAMapping.ResidentRWAID,SocietyFlatMapping.FlatNbr FROM User INNER JOIN ResidentRWAMapping ON User.ID = ResidentRWAMapping.ResidentID INNER JOIN ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID= ResidentRWAMapping.ResidentRWAID INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID WHERE ResidentFlatMapping.ID ='" . $RWAResidentFlatID . "' ");
        $sql4->execute();
        if ($sql4->rowCount() > 0) {
            while ($row4 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                $ResidentRWAID = $row4['ResidentRWAID'];
                $RWAID = $row4['RWAID'];
                $compbyname = $row4['FirstName'];
                $ProfilePic = $row4['ProfilePic'];
                $ResidentID = $row4['ID'];
                $FlatNbr = $row4['FlatNbr'];
            }
        }
        $sql5 = $this->prdbh->prepare("SELECT  Description FROM ComplaintCategory WHERE  ID= '" . $rec->CategoryID . "' ");
        $sql5->execute();
        if ($sql5->rowCount() > 0) {
            while ($row5 = $sql5->fetch(PDO::FETCH_ASSOC)) {
                $cate = $row5['Description'];
            }
        }
        $RWAResidentFlatID = $rec->RWAResidentFlatID;
        $sql = $this->prdbh->prepare("INSERT INTO `ComplaintRegister`(`RWAID`, `CategoryID`, `RWAResidentFlatID`, `ComplaintDetails`,IsCommonAreaComplaint, `Image1`, `Image2`, `Image3`, `Status`, `DateCreated`,ManualComplaintNumber,ShortDescription,FlatID,LastActivityDate)
            VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $sql->bindParam(1, $RWAID);
        $sql->bindParam(2, $rec->CategoryID);
        $sql->bindParam(3, $RWAResidentFlatID);
        $sql->bindParam(4, $rec->ComplaintDetails);
        $sql->bindParam(5, $IsCommonAreaComplaint);
        $sql->bindParam(6, $rec->Image1);
        $sql->bindParam(7, $rec->Image2);
        $sql->bindParam(8, $rec->Image3);
        $sql->bindParam(9, $rec->Status);
        $sql->bindParam(10, $tdate);
        $sql->bindParam(11, $rec->ManualComplaintNumber);
        $sql->bindParam(12, $rec->ShortDescription);
        $sql->bindParam(13, $rec->FlatID);
        $sql->bindParam(14, $tdate);
        $this->prdbh->beginTransaction();
        $sql->execute();
        $count = $sql->rowCount();
        if ($count > 0) {
            $id = $this->prdbh->lastInsertId();
            $this->prdbh->commit();
            $response['Message'] = "Complaint created successfully";
            $response['complainId'] = $id;
            $response['Status'] = "1";
            $sql7 = $this->prdbh->prepare("SELECT * FROM RWAConfigTable where RWAID = '" . $RWAID . "' AND ConfigParamID='27' AND ConfigParamValue='1' ");
            $sql7->execute();
            if ($sql7->rowCount() > 0) {
                $sql8 = $this->prdbh->prepare("SELECT ID, Name, ProfilePic, RWAID, PrimaryContactNbr, SecondaryContactNbr FROM MaintenanceStaff WHERE RWAID='" . $RWAID . "'  AND IsActive = 1 AND ServiceCategory ='" . $rec->CategoryID . "' AND HeadStaff='1' order by ID asc");
                $sql8->execute();
                $count8 = $sql8->rowCount();
                if ($count8 >= 1) {
                    $row8 = $sql8->fetch(PDO::FETCH_ASSOC);
                    $staffid = $row8['ID'];
                    $PrimaryContactNbr = $row8['PrimaryContactNbr'];
                    $message = 'Complaint NO. ' . $id . ' is being assigned to you.Please visit Flat NO. ' . $FlatNbr;
                    $msg = urlencode($message);
                    $link = "http://textsms.co.in/app/smsapi/index.php?key=3588619015E605&routeid=6&type=text&contacts=$PrimaryContactNbr&senderid=ALERTS&msg=$msg";
                    $homepage = file_get_contents($link);
                    //print_r($homepage);die;
                    $response['message'] = $homepage;
                }
                $sql9 = $this->prdbh->prepare("UPDATE ComplaintRegister SET Status ='1', AssignedTo ='" . $staffid . "',DateAssigned ='" . $tdate . "', AssignedBy ='" . $RWAResidentFlatID . "' WHERE ID='" . $id . "' ");
                $this->prdbh->beginTransaction();
                if ($sql9->execute()) {
                    $this->prdbh->commit();
                }
            } else {
                $Cmpstatus = '0';
            }
            if ($IsCommonAreaComplaint == 1) {
                $output = $this->onlineinsert($Description, $FeedType, $id, $ResidentRWAID, $FlatNbr, $ProfilePic, $ResidentID, $RWAID, $compbyname, $PhotoPath);
                $jsonObject = json_decode($output);
                if ($jsonObject->Status == 1) {
                    $response['MongoResponce'] = "Photo added successfully";
                }
            }
            $sql1 = $this->prdbh->prepare("SELECT User.ID,User.PhoneNbr,User.EmailID,User.FirstName FROM User
                                                INNER JOIN ResidentRWAMapping ON User.ID=ResidentRWAMapping.ResidentID
                                                LEFT JOIN RWA ON ResidentRWAMapping.RWAID=RWA.ID
                                                LEFT JOIN UserRoles ON UserRoles.ResidentRWAID= ResidentRWAMapping.ResidentRWAID
                                                LEFT JOIN RoleFunctions ON UserRoles.RoleID= RoleFunctions.RoleID
                                                WHERE ResidentRWAMapping.RWAID='" . $rec->RWAID . "' And ResidentRWAMapping.ApprovalStatus='Y' AND RoleFunctions.FunctionID = 23 GROUP BY ID ");
            $sql1->execute();
            if ($sql1->rowCount() > 0) {
                $email = array();
                $phonnbr = array();
                while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                    $email[] = $row1['EmailID'];
                    $phonnbr[] = $row1['PhoneNbr'];
                    $name = $row1['FirstName'];
                    $subject = $compbyname . ', ' . $FlatNbr . ' has registered new complaint';
                    $message1 = base64_decode($rec->ComplaintDetails);
                    $compmessage = 'Category:' . $cate . "\r\n\n" . 'Details :' . $message1;
                    $message = preg_replace('/\s+/', '+', $message1);
                }
                $result = $this->newcomplainmail($compmessage, $subject, $email, $name);
                /* $phonnbr = implode(',',$phonnbr);
                if(count($phonnbr)){
                
                $link="http://textsms.co.in/app/smsapi/index.php?key=3588619015E605&routeid=6&type=text&contacts='$phonnbr'&senderid=ALERTS&msg='$message'" ;
                
                $homepage = file_get_contents($link);
                $response['message']=$homepage;
                
                }   */
            }
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function addcomplaindecember($rec) {
        $tdat = date("U");
        $tdate = $tdat - 90;
        $RWAResidentFlatID = $rec->RWAResidentFlatID;
        $Description = $rec->ComplaintDetails;
        $RWAID = $rec->RWAID;
        $PhotoPath = $rec->Image1;
        $FeedType = "complain";
        $filter = "1";
        $sql6 = $this->prdbh->prepare("SELECT  * FROM ComplainCommenArea WHERE  CompCatID= '" . $rec->CategoryID . "' ");
        $sql6->execute();
        if ($sql6->rowCount() > 0) {
            $IsCommonAreaComplaint = '1';
        } else {
            $IsCommonAreaComplaint = $rec->IsCommonAreaComplaint;
        }
        $sql4 = $this->prdbh->prepare("SELECT User.ID,User.ProfilePic,User.FirstName,ResidentRWAMapping.RWAID,ResidentRWAMapping.ResidentRWAID,SocietyFlatMapping.FlatNbr FROM User INNER JOIN ResidentRWAMapping ON User.ID = ResidentRWAMapping.ResidentID INNER JOIN ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID= ResidentRWAMapping.ResidentRWAID INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID WHERE ResidentFlatMapping.ID ='" . $RWAResidentFlatID . "' ");
        $sql4->execute();
        if ($sql4->rowCount() > 0) {
            while ($row4 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                $ResidentRWAID = $row4['ResidentRWAID'];
                $RWAID = $row4['RWAID'];
                $compbyname = $row4['FirstName'];
                $ProfilePic = $row4['ProfilePic'];
                $ResidentID = $row4['ID'];
                $FlatNbr = $row4['FlatNbr'];
            }
        }
        $sql5 = $this->prdbh->prepare("SELECT  Description FROM ComplaintCategory WHERE  ID= '" . $rec->CategoryID . "' ");
        $sql5->execute();
        if ($sql5->rowCount() > 0) {
            while ($row5 = $sql5->fetch(PDO::FETCH_ASSOC)) {
                $cate = $row5['Description'];
            }
        }
        $RWAResidentFlatID = $rec->RWAResidentFlatID;
        $sql = $this->prdbh->prepare("INSERT INTO `ComplaintRegister`(`RWAID`, `CategoryID`, `RWAResidentFlatID`, `ComplaintDetails`,IsCommonAreaComplaint, `Image1`, `Image2`, `Image3`, `Status`, `DateCreated`,ManualComplaintNumber,ShortDescription,FlatID,LastActivityDate)
            VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $sql->bindParam(1, $RWAID);
        $sql->bindParam(2, $rec->CategoryID);
        $sql->bindParam(3, $RWAResidentFlatID);
        $sql->bindParam(4, $rec->ComplaintDetails);
        $sql->bindParam(5, $IsCommonAreaComplaint);
        $sql->bindParam(6, $rec->Image1);
        $sql->bindParam(7, $rec->Image2);
        $sql->bindParam(8, $rec->Image3);
        $sql->bindParam(9, $rec->Status);
        $sql->bindParam(10, $tdate);
        $sql->bindParam(11, $rec->ManualComplaintNumber);
        $sql->bindParam(12, $rec->ShortDescription);
        $sql->bindParam(13, $rec->FlatID);
        $sql->bindParam(14, $tdate);
        $this->prdbh->beginTransaction();
        $sql->execute();
        $count = $sql->rowCount();
        if ($count > 0) {
            $id = $this->prdbh->lastInsertId();
            $this->prdbh->commit();
            $response['Message'] = "Complaint created successfully";
            $response['complainId'] = $id;
            $response['Status'] = "1";
            $sql7 = $this->prdbh->prepare("SELECT * FROM RWAConfigTable where RWAID = '" . $RWAID . "' AND ConfigParamID='27' AND ConfigParamValue='1' ");
            $sql7->execute();
            if ($sql7->rowCount() > 0) {
                $sql8 = $this->prdbh->prepare("SELECT ID, Name, ProfilePic, RWAID, PrimaryContactNbr, SecondaryContactNbr FROM MaintenanceStaff WHERE RWAID='" . $RWAID . "'  AND IsActive = 1 AND ServiceCategory ='" . $rec->CategoryID . "' AND HeadStaff='1' order by ID asc");
                $sql8->execute();
                $count8 = $sql8->rowCount();
                if ($count8 >= 1) {
                    $row8 = $sql8->fetch(PDO::FETCH_ASSOC);
                    $staffid = $row8['ID'];
                    $PrimaryContactNbr = $row8['PrimaryContactNbr'];
                    $message = 'Complaint NO. ' . $id . ' is being assigned to you.Please visit Flat NO. ' . $FlatNbr;
                    $msg = urlencode($message);
                    $link = "http://textsms.co.in/app/smsapi/index.php?key=3588619015E605&routeid=6&type=text&contacts=$PrimaryContactNbr&senderid=ALERTS&msg=$msg";
                    $homepage = file_get_contents($link);
                    //print_r($homepage);die;
                    $response['message'] = $homepage;
                }
                $sql9 = $this->prdbh->prepare("UPDATE ComplaintRegister SET Status ='1', AssignedTo ='" . $staffid . "',DateAssigned ='" . $tdate . "', AssignedBy ='" . $RWAResidentFlatID . "' WHERE ID='" . $id . "' ");
                $this->prdbh->beginTransaction();
                if ($sql9->execute()) {
                    $this->prdbh->commit();
                }
            } else {
                $Cmpstatus = '0';
            }
            if ($IsCommonAreaComplaint == 1) {
                $output = $this->onlineinsert($Description, $FeedType, $id, $ResidentRWAID, $FlatNbr, $ProfilePic, $ResidentID, $RWAID, $compbyname, $PhotoPath);
                $jsonObject = json_decode($output);
                if ($jsonObject->Status == 1) {
                    $response['MongoResponce'] = "Photo added successfully";
                }
            }
            $sql1 = $this->prdbh->prepare("SELECT User.ID,User.PhoneNbr,User.EmailID,User.FirstName FROM User
                                                INNER JOIN ResidentRWAMapping ON User.ID=ResidentRWAMapping.ResidentID
                                                LEFT JOIN RWA ON ResidentRWAMapping.RWAID=RWA.ID
                                                LEFT JOIN UserRoles ON UserRoles.ResidentRWAID= ResidentRWAMapping.ResidentRWAID
                                                LEFT JOIN RoleFunctions ON UserRoles.RoleID= RoleFunctions.RoleID
                                                WHERE ResidentRWAMapping.RWAID='" . $rec->RWAID . "' And ResidentRWAMapping.ApprovalStatus='Y' AND RoleFunctions.FunctionID = 23 GROUP BY ID ");
            $sql1->execute();
            if ($sql1->rowCount() > 0) {
                $email = array();
                $phonnbr = array();
                while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                    $email[] = $row1['EmailID'];
                    $phonnbr[] = $row1['PhoneNbr'];
                    $name = $row1['FirstName'];
                    $subject = $compbyname . ', ' . $FlatNbr . ' has registered new complaint';
                    $message1 = base64_decode($rec->ComplaintDetails);
                    $compmessage = 'Category:' . $cate . "\r\n\n" . 'Details :' . $message1;
                    $message = preg_replace('/\s+/', '+', $message1);
                }
                $result = $this->newcomplainmail($compmessage, $subject, $email, $name);
                /* $phonnbr = implode(',',$phonnbr);
                if(count($phonnbr)){
                
                $link="http://textsms.co.in/app/smsapi/index.php?key=3588619015E605&routeid=6&type=text&contacts='$phonnbr'&senderid=ALERTS&msg='$message'" ;
                
                $homepage = file_get_contents($link);
                $response['message']=$homepage;
                
                }   */
            }
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        // admin complain notification starts push branch satyam
        if ($filter == "1") {
            $sqlP = $this->prdbh->prepare("SELECT User.ID,User.PhoneNbr,User.EmailID,User.FirstName FROM User
                                    INNER JOIN ResidentRWAMapping ON User.ID=ResidentRWAMapping.ResidentID
                                    LEFT JOIN RWA ON ResidentRWAMapping.RWAID=RWA.ID
                                    LEFT JOIN UserRoles ON UserRoles.ResidentRWAID= ResidentRWAMapping.ResidentRWAID
                                    LEFT JOIN RoleFunctions ON UserRoles.RoleID= RoleFunctions.RoleID
                                    WHERE ResidentRWAMapping.RWAID='" . $rec->RWAID . "' And ResidentRWAMapping.ApprovalStatus='Y' AND RoleFunctions.FunctionID = 23 GROUP BY ID ");
            $sqlP->execute();
            if ($sqlP->rowCount() > 0) {
                $UpushId1 = array();
                while ($row1 = $sqlP->fetch(PDO::FETCH_ASSOC)) {
                    $UpushId1[] = $row1['ID'];
                }
                $UpushId = implode(',', $UpushId1);
                $ID = $id;
                $message12 = base64_decode($rec->ComplaintDetails);
                $message13 = html_entity_decode($message12);
                $message1 = htmlspecialchars_decode($message13);
                //$message= mysqli_real_escape_string($message1);
                $message = strip_tags($message1);
                $UrHd1 = "Complain notification";
                //echo $message1;die;
                $message1 = array("message" => $message, "UrHd2" => $UrHd1, "body" => $ID);
                $result = $this->firebasenotification($UpushId, $message1);
                $response['PushStatus'] = $result;
            }
        }
        // admin complain notification ends
        return $response;
    }
    public function mailoncomplainstatuschange($umail, $name, $ComplaintID, $status) {
        $response = array();
        require 'sendgrid/vendor/autoload.php';
        require 'sendgrid/lib/SendGrid.php';
        $sendgrid = new SendGrid('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');
        $subject = "Your Complaint, ID : " . $ComplaintID . " status has been changed to " . $status;
        $usub = '[mynukad]' . $subject;
        for ($i = 0;$i < count($umail);$i++) {
            $umsg = "Hi " . $name[$i] . ",
            
Your Complaint, ID : " . $ComplaintID . " status has been changed to " . $status . ".
Please check mynukad app to check more about your complaint.

Thanks
Team mynukad";
            $email = new SendGrid\Email();
            $email->addTo($umail[$i])->setFrom('noreply@mynukad.com')->setSubject($usub)->setText($umsg)->setHtml($dt);
            try {
                $sendgrid->send($email);
                $response['status'] = 1;
            }
            catch(\SendGrid\Exception $e) {
                $response['status'] = 0;
            }
        }
        return $response;
    }
    public function newcomplainmail($compmessage, $subject, $email, $name) {
        $response = array();
        $umail = $email;
        $umsg1 = $compmessage;
        $name = $name;
        $usub = '[mynukad]' . $subject;
        $d = 'Please login to http://mynukad.com and check its status' . "\r\n\r\n" . "\r\n\r\n\n\n\n\n\n\n\n\n\n\n\n" . 'Regards' . ',' . "\r\n\n" . 'Team,mynukad';
        $umsg = 'Hi ,' . "\r\n\n" . $subject . "\r\n\n" . $umsg1 . "\r\n\n" . $d;
        $umsg = $umsg;
        require 'sendgrid/vendor/autoload.php';
        require 'sendgrid/lib/SendGrid.php';
        $sendgrid = new SendGrid('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');
        $email = new SendGrid\Email();
        $email->addTo($umail)->setFrom('noreply@mynukad.com')->setSubject($usub)->setText($umsg)->setHtml($dt);
        try {
            $sendgrid->send($email);
            $response['status'] = 1;
        }
        catch(\SendGrid\Exception $e) {
            $response['status'] = 0;
        }
        return $response;
    }
    public function newcomplainmailstaff($compmessage, $subject, $email, $name) {
        $response = array();
        $umail = $email;
        $umsg1 = $compmessage;
        $name = $name;
        $usub = '[mynukad]' . $subject;
        $d = 'Please login to http://mynukad.com and check its status' . "\r\n\r\n" . "\r\n\r\n\n\n\n\n\n\n\n\n\n\n\n" . 'Regards' . ',' . "\r\n\n" . 'Team,mynukad';
        $umsg = 'Hi ,' . "\r\n\n" . $subject . "\r\n\n" . $umsg1 . "\r\n\n" . $d;
        $umsg = $umsg;
        $sendgrid = new SendGrid('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');
        $email = new SendGrid\Email();
        $email->addTo($umail)->setFrom('noreply@mynukad.com')->setSubject($usub)->setText($umsg)->setHtml($dt)->setAttachment(realpath(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'file.csv');
        try {
            $sendgrid->send($email);
            $response['status'] = 1;
        }
        catch(\SendGrid\Exception $e) {
            $response['status'] = 0;
        }
        return $response;
    }
    public function pendinguserslamail($compmessage, $subject, $email, $name) {
        $response = array();
        $umail = $email;
        $umsg1 = $compmessage;
        $name = $name;
        $usub = '[mynukad]' . $subject;
        $d = 'Please login to http://mynukad.com and check its status' . "\r\n\r\n" . "\r\n\r\n\n\n\n\n\n\n\n\n\n\n\n" . 'Regards' . ',' . "\r\n\n" . 'Team,mynukad';
        $umsg = 'Hi Admin,' . "\r\n\n" . $subject . "\r\n\n" . $umsg1 . "\r\n\n" . $d;
        $umsg = $umsg;
        $sendgrid = new SendGrid('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');
        $email = new SendGrid\Email();
        $email->addTo($umail)->setFrom('noreply@mynukad.com')->setSubject($usub)->setText($umsg)->setHtml($dt)->setAttachment(realpath(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'file1.csv');
        try {
            $sendgrid->send($email);
            $response['status'] = 1;
        }
        catch(\SendGrid\Exception $e) {
            $response['status'] = 0;
        }
        return $response;
    }
    public function addclassified($rec) {
        $tdat = date("U");
        $tdate = $tdat - 210;
        $Description = $rec->Description;
        $ResidentFlatMappingID = $rec->AddedBy;
        $FeedType = "classified";
        $PhotoPath = $rec->PhotoPath;
        $Image2 = $rec->Image2;
        $Image3 = $rec->Image3;
        $sql4 = $this->prdbh->prepare("SELECT User.ID,User.ProfilePic,User.FirstName,ResidentRWAMapping.RWAID,ResidentRWAMapping.ResidentRWAID FROM User INNER JOIN ResidentRWAMapping ON User.ID = ResidentRWAMapping.ResidentID INNER JOIN ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID= ResidentRWAMapping.ResidentRWAID WHERE ResidentFlatMapping.ID ='" . $ResidentFlatMappingID . "' ");
        $sql4->execute();
        if ($sql4->rowCount() > 0) {
            while ($row4 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                $ResidentRWAID = $row4['ResidentRWAID'];
                $RWAID = $row4['RWAID'];
                $ResidentName = $row4['FirstName'];
                $ProfilePic = $row4['ProfilePic'];
                $ResidentID = $row4['ID'];
            }
        }
        $sql5 = $this->prdbh->prepare("SELECT SocietyFlatMapping.FlatNbr FROM SocietyFlatMapping INNER JOIN ResidentFlatMapping ON ResidentFlatMapping.FlatID= SocietyFlatMapping.ID WHERE ResidentFlatMapping.ID ='" . $ResidentFlatMappingID . "' ");
        $sql5->execute();
        if ($sql5->rowCount() > 0) {
            $ResidentFlatNo = array();
            while ($row5 = $sql5->fetch(PDO::FETCH_ASSOC)) {
                $ResidentFlatNbr[] = $row5['FlatNbr'];
            }
        }
        $ResidentFlatNo = implode(',', $ResidentFlatNbr);
        $sql = $this->prdbh->prepare("INSERT INTO `Classifieds`(`Title`, `Description`, `Image1`, `Image2`, `Image3`, `AddedBy`, `AddedOn`, `ExpiryDt`,IsActive) VALUES(?,?,?,?,?,?,?,?,1)");
        $sql->bindParam(1, $rec->Title);
        $sql->bindParam(2, $rec->Description);
        $sql->bindParam(3, $rec->Image1);
        $sql->bindParam(4, $rec->Image2);
        $sql->bindParam(5, $rec->Image3);
        $sql->bindParam(6, $rec->AddedBy);
        $sql->bindParam(7, $tdate);
        $sql->bindParam(8, $rec->ExpiryDt);
        $this->prdbh->beginTransaction();
        $sql->execute();
        $count = $sql->rowCount();
        if ($count > 0) {
            $id = $this->prdbh->lastInsertId();
            $this->prdbh->commit();
            $response['Message'] = "Classified created  successfully";
            $output = $this->onlineinsert($Description, $FeedType, $id, $ResidentRWAID, $ResidentFlatNo, $ProfilePic, $ResidentID, $RWAID, $ResidentName, $PhotoPath);
            $jsonObject = json_decode($output);
            if ($jsonObject->Status == 1) {
                $response['MongoResponce'] = "Photo added successfully";
            }
            $sql2 = $this->prdbh->prepare("SELECT User.ID,User.FirstName,ResidentRWAMapping.RWAID FROM User INNER JOIN ResidentRWAMapping ON User.ID=ResidentRWAMapping.ResidentID LEFT JOIN ResidentFlatMapping on ResidentFlatMapping.ResidentRWAID = ResidentRWAMapping.ResidentRWAID  WHERE ResidentFlatMapping.ID ='" . $rec->AddedBy . "'  ");
            $sql2->execute();
            if ($sql2->rowCount() > 0) {
                while ($row2 = $sql2->fetch(PDO::FETCH_ASSOC)) {
                    $UserID = $row2['ID'];
                    $name = $row2['FirstName'];
                    $RWAID = $row2['RWAID'];
                }
            }
            $sql1 = $this->prdbh->prepare("SELECT User.ID FROM User
                                                INNER JOIN ResidentRWAMapping ON User.ID=ResidentRWAMapping.ResidentID
                                                LEFT JOIN RWA ON ResidentRWAMapping.RWAID=RWA.ID
                                                WHERE RWAID='" . $RWAID . "' AND User.ID!='" . $UserID . "' ");
            $sql1->execute();
            if ($sql1->rowCount() > 0) {
                $UpushId1 = array();
                while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                    $UpushId1[] = $row1['ID'];
                }
                $UpushId = implode(',', $UpushId1);
                $message = $name . " has created Classified, tap here to view";
                $UrHd1 = "mynukad Classified";
                $response['ClassifiedId'] = $id;
                $response['Status'] = "1";
                $message1 = array("message" => $message, "UrHd2" => $UrHd1, "body" => $id);
                $result = $this->firebasenotification($UpushId, $message1);
                $response['PushStatus'] = $result;
            }
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function addpole($rec) {
        $tdat = date("U");
        $tdate = $tdat;
        $FeedType = "poll";
        $Description = $rec->Question;
        $ResidentRWAID = $rec->ResidentRWAID;
        $sql4 = $this->prdbh->prepare("SELECT User.ID,User.ProfilePic,User.FirstName,ResidentRWAMapping.RWAID,ResidentRWAMapping.ResidentRWAID FROM User INNER JOIN ResidentRWAMapping ON User.ID = ResidentRWAMapping.ResidentID WHERE ResidentRWAMapping.ResidentRWAID ='" . $ResidentRWAID . "' ");
        $sql4->execute();
        if ($sql4->rowCount() > 0) {
            while ($row4 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                $RWAID = $row4['RWAID'];
                $ResidentName = $row4['FirstName'];
                $ProfilePic = $row4['ProfilePic'];
                $ResidentID = $row4['ID'];
            }
        }
        $sql5 = $this->prdbh->prepare("SELECT SocietyFlatMapping.FlatNbr FROM SocietyFlatMapping INNER JOIN ResidentFlatMapping ON ResidentFlatMapping.FlatID= SocietyFlatMapping.ID WHERE ResidentFlatMapping.ResidentRWAID = '" . $ResidentRWAID . "' ");
        $sql5->execute();
        if ($sql5->rowCount() > 0) {
            $ResidentFlatNo = array();
            while ($row5 = $sql5->fetch(PDO::FETCH_ASSOC)) {
                $ResidentFlatNbr[] = $row5['FlatNbr'];
            }
        }
        $ResidentFlatNo = implode(',', $ResidentFlatNbr);
        $sql = $this->prdbh->prepare("INSERT INTO PollQuestions( ResidentRWAID, Question, DateAdded, ExpirationDt, IsActive) VALUES  (?,?,?,?,1)");
        $sql->bindParam(1, $rec->ResidentRWAID);
        $sql->bindParam(2, $rec->Question);
        $sql->bindParam(3, $tdate);
        $sql->bindParam(4, $rec->ExpirationDt);
        $this->prdbh->beginTransaction();
        $sql->execute();
        $count = $sql->rowCount();
        if ($count > 0) {
            $id = $this->prdbh->lastInsertId();
            $this->prdbh->commit();
            $response['Message'] = "Poll created successfully";
            $output = $this->onlineinsert($Description, $FeedType, $id, $ResidentRWAID, $ResidentFlatNo, $ProfilePic, $ResidentID, $RWAID, $ResidentName, $PhotoPath);
            $jsonObject = json_decode($output);
            if ($jsonObject->Status == 1) {
                $response['MongoResponce'] = "Photo added successfully";
            }
            $sql2 = $this->prdbh->prepare("SELECT User.ID,User.FirstName,ResidentRWAMapping.RWAID FROM User INNER JOIN ResidentRWAMapping ON User.ID=ResidentRWAMapping.ResidentID WHERE ResidentRWAMapping.ResidentRWAID='" . $rec->ResidentRWAID . "'  ");
            $sql2->execute();
            if ($sql2->rowCount() > 0) {
                while ($row2 = $sql2->fetch(PDO::FETCH_ASSOC)) {
                    $UserID = $row2['ID'];
                    $name = $row2['FirstName'];
                    $RWAID = $row2['RWAID'];
                }
            }
            $sql1 = $this->prdbh->prepare("SELECT User.ID FROM User
                                                INNER JOIN ResidentRWAMapping ON User.ID=ResidentRWAMapping.ResidentID
                                                LEFT JOIN RWA ON ResidentRWAMapping.RWAID=RWA.ID
                                                WHERE RWAID='" . $RWAID . "' AND User.ID!='" . $UserID . "' ");
            $sql1->execute();
            if ($sql1->rowCount() > 0) {
                $UpushId1 = array();
                while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                    $UpushId1[] = $row1['ID'];
                }
                $UpushId = implode(',', $UpushId1);
                $ID = '' . $id;
                $message = $name . " has created Poll, tap here to view";
                $UrHd1 = "mynukad Poll";
                $response['PoleID'] = $id;
                $response['Status'] = "1";
                $message1 = array("message" => $message, "UrHd2" => $UrHd1, "body" => $ID);
                $result = $this->firebasenotification($UpushId, $message1);
                $response['PushStatus'] = $result;
            }
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function addvehicle($rec) {
        $sql = $this->prdbh->prepare("INSERT INTO `ResidentVehicleMapping`(`ResidentRWAFlatID`, `VehicleNbr`, `VehicleType`,IsActive)  
                           VALUES(?,?,?,1)");
        $sql->bindParam(1, $rec->ResidentRWAFlatID);
        $sql->bindParam(2, $rec->VehicleNbr);
        $sql->bindParam(3, $rec->VehicleType);
        $this->prdbh->beginTransaction();
        $sql->execute();
        $count = $sql->rowCount();
        if ($count > 0) {
            $id = $this->prdbh->lastInsertId();
            $this->prdbh->commit();
            $response['Message'] = "Vehicle added successfully";
            $response['VehicleId'] = $id;
            $response['Status'] = "1";
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function usefulcontactssociety($param) {
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT `ID`, `RWAID`, `Name`, `Designation`, `OfficeAddress`, `OfficePhoneNbr`, `PrimaryContactNbr`, `PhoneNbr2`, `PhoneNbr3`, `EmailId`, `AddedBy`, `AddedOn`, `IsActive` FROM `UsefulContactsSociety` WHERE  RWAID = '" . $RWAID . "' ORDER BY Name ASC  ");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['Useful Contacts Society'] = $items;
                $response['Status'] = '1';
            } else {
                $response['Useful Contacts Society'] = [];
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function complainstatus($param) {
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT ID, Status, RWAID, IsActive FROM ComplainStatus WHERE IsActive='Active' ");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['ComplainStatus'] = $items;
                $response['Status'] = '1';
            } else {
                $response['ComplainStatus'] = '';
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function showfavshop($param) {
        if (isset($param->UserID)) {
            $UserID = $param->UserID;
        }
        $response = array();
        $sql2 = $this->prdbh->prepare("SELECT RWA.Latitude,RWA.Longitude FROM User INNER JOIN ResidentRWAMapping ON User.ID = ResidentRWAMapping.ResidentID
                                LEFT JOIN RWA ON ResidentRWAMapping.RWAID=RWA.ID WHERE User.ID = '" . $UserID . "'");
        $sql2->execute();
        if ($sql2->execute()) {
            $count1 = $sql2->rowCount();
            if ($count1 >= 1) {
                while ($row2 = $sql2->fetch(PDO::FETCH_ASSOC)) {
                    $long = $row2[Longitude];
                    $lat = $row2[Latitude];
                }
            }
        }
        $sql = $this->prdbh->prepare("SELECT UserFavouriteNearBy.UserFavID, UserFavouriteNearBy.NearByID,UserFavouriteNearBy.UserID,NearBy.ID, NearBy.Name, NearBy.CategoryID, NearBy.Address1,NearBy.Address2,NearBy.City, NearBy.Latitude, NearBy.Longitude, NearBy.Is247, NearBy.IsHomeDelivery, NearBy.PhoneNbr1, NearBy.PhoneNbr2,NearBy.PhoneNbr3, NearBy.BannerImage, NearBy.Image2,NearBy.Image3,NearBy.Description,(NearByCategory.Description) as Type,NearBy.AddedBy,NearBy.IsActive, UserFavouriteNearBy.AddedOn,((ACOS(SIN($lat * PI() / 180) * SIN(latitude * PI() / 180) + COS($lat * PI() / 180) * COS(latitude * PI() / 180) * COS(($long - longitude) * PI() / 180)) * 180 / PI()) * 60 * 1.8532) AS distance FROM UserFavouriteNearBy INNER JOIN NearBy ON NearBy.ID = UserFavouriteNearBy.NearByID INNER JOIN NearByCategory ON NearBy.CategoryID = NearByCategory.ID   WHERE UserFavouriteNearBy.UserID = '" . $UserID . "' group by NearBy.ID ");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['Fav Shop '] = $items;
                $response['Status'] = '1';
            } else {
                $response['Fav Shop'] = [];
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function adminrole($param) {
        if (isset($param->UserID)) {
            $UserID = $param->UserID;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT User.ID, User.PhoneNbr, User.EmailID,  User.FirstName, User.MiddleName, User.ProfilePic, User.Gender, User.Occupation, (
Roles.Name ) AS Role,User.AddedOn, User.IsActive,Roles.RoleID
FROM User INNER JOIN UserRoles ON User.ID = UserRoles.UserID
INNER JOIN Roles ON UserRoles.RoleID = Roles.RoleID LEFT JOIN RoleFunctions ON RoleFunctions.RoleID = Roles.RoleID
 WHERE UserRoles.UserID = '" . $UserID . "'  ORDER by User.FirstName ");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                    $response['Admin'] = $items;
                    $rid = $row['RoleID'];
                    $sql1 = $this->prdbh->prepare(" select r.FunctionID,func.FunctionName from RoleFunctions as r inner join Functions as func on func.FunctionID=r.FunctionID where r.RoleID='" . $rid . "'");
                    $sql1->execute();
                    $count1 = $sql1->rowCount();
                    if ($count1 > 0) {
                        while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                            $subitems[] = $row1;
                        }
                        $response['Admin'][0]['functions'] = $subitems;
                    }
                }
                $response['Status'] = '1';
            } else {
                $response['Admin'] = [];
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function approvesocietymembernew($param) {
        $ApprovalStatus = '';
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        if (isset($param->Uid)) {
            $Uid = $param->Uid;
        }
        if (isset($param->Name)) {
            $Name = $param->Name;
        }
        if (isset($param->PageNo)) {
            $PageNo = $param->PageNo;
        }
        if (isset($param->ApprovalStatus)) {
            $ApprovalStatus = $param->ApprovalStatus;
        }
        $Start = $PageNo * 20 - 20;
        $response = array();
        if ($ApprovalStatus == '') {
            if ($Name != '') {
                $sql = $this->prdbh->prepare("SELECT User.ID, User.PhoneNbr, User.EmailID,  SocietyFlatMapping.FlatNbr,ResidentFlatMapping.IsOwner,ResidentRWAMapping.ApprovalStatus,ResidentRWAMapping.ResidentRWAID, ResidentRWAMapping.ApprovedBy, from_unixtime( (
                ResidentRWAMapping.ApprovedOn +19660), '%d/%m/%Y  %h:%i %p' ) AS ApprovedOn, User.FirstName, User.MiddleName, User.LastName, User.ProfilePic, User.Gender, User.Occupation, from_unixtime( (
                User.AddedOn +19660), '%d/%m/%Y  %h:%i %p' ) AS AddedOn, User.IsActive,
                Case ResidentRWAMapping.ApprovalStatus
                WHEN 'P' THEN 1
                WHEN 'Y' THEN 2
                WHEN 'N' THEN 3
                END
                AS Status FROM User INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentID = User.ID
                LEFT JOIN ResidentFlatMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID LEFT JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID WHERE User.FirstName like '%" . $Name . "%' AND ResidentRWAMapping.RWAID = '" . $RWAID . "'  AND User.ID!='" . $Uid . "'  ORDER BY  Status ASC,User.AddedOn DESC ");
            } else {
                $sql = $this->prdbh->prepare("SELECT User.ID, User.PhoneNbr, User.EmailID,  SocietyFlatMapping.FlatNbr,ResidentFlatMapping.IsOwner,ResidentRWAMapping.ApprovalStatus,ResidentRWAMapping.ResidentRWAID, ResidentRWAMapping.ApprovedBy, from_unixtime( (
                ResidentRWAMapping.ApprovedOn +19660), '%d/%m/%Y  %h:%i %p' ) AS ApprovedOn, User.FirstName, User.MiddleName, User.LastName, User.ProfilePic, User.Gender, User.Occupation, from_unixtime( (
                User.AddedOn +19660), '%d/%m/%Y  %h:%i %p' ) AS AddedOn, User.IsActive,
                Case ResidentRWAMapping.ApprovalStatus
                WHEN 'P' THEN 1
                WHEN 'Y' THEN 2
                WHEN 'N' THEN 3
                END
                AS Status FROM User INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentID = User.ID
                LEFT JOIN ResidentFlatMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID LEFT JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID WHERE ResidentRWAMapping.RWAID = '" . $RWAID . "' AND User.ID!='" . $Uid . "' ORDER BY  Status ASC ,User.AddedOn DESC Limit $Start,20");
            }
        } elseif ($ApprovalStatus == 'P') {
            if ($Name != '') {
                $sql = $this->prdbh->prepare("SELECT User.ID, User.PhoneNbr, User.EmailID,  SocietyFlatMapping.FlatNbr,ResidentFlatMapping.IsOwner,ResidentRWAMapping.ApprovalStatus,ResidentRWAMapping.ResidentRWAID, ResidentRWAMapping.ApprovedBy, from_unixtime( (
                ResidentRWAMapping.ApprovedOn +19660), '%d/%m/%Y  %h:%i %p' ) AS ApprovedOn, User.FirstName, User.MiddleName, User.LastName, User.ProfilePic, User.Gender, User.Occupation, from_unixtime( (
                User.AddedOn +19660), '%d/%m/%Y  %h:%i %p' ) AS AddedOn, User.IsActive,
                Case ResidentRWAMapping.ApprovalStatus
                WHEN 'P' THEN 1
                WHEN 'Y' THEN 2
                WHEN 'N' THEN 3
                END
                AS Status FROM User INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentID = User.ID
                LEFT JOIN ResidentFlatMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID LEFT JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID WHERE User.FirstName like '%" . $Name . "%' AND ResidentRWAMapping.RWAID = '" . $RWAID . "'  AND User.ID!='" . $Uid . "' AND ResidentRWAMapping.ApprovalStatus= '" . $ApprovalStatus . "' ORDER BY  Status ASC,User.AddedOn DESC ");
            } else {
                $sql = $this->prdbh->prepare("SELECT User.ID, User.PhoneNbr, User.EmailID,  SocietyFlatMapping.FlatNbr,ResidentFlatMapping.IsOwner,ResidentRWAMapping.ApprovalStatus,ResidentRWAMapping.ResidentRWAID, ResidentRWAMapping.ApprovedBy, from_unixtime( (
                ResidentRWAMapping.ApprovedOn +19660), '%d/%m/%Y  %h:%i %p' ) AS ApprovedOn, User.FirstName, User.MiddleName, User.LastName, User.ProfilePic, User.Gender, User.Occupation, from_unixtime( (
                User.AddedOn +19660), '%d/%m/%Y  %h:%i %p' ) AS AddedOn, User.IsActive,
                Case ResidentRWAMapping.ApprovalStatus
                WHEN 'P' THEN 1
                WHEN 'Y' THEN 2
                WHEN 'N' THEN 3
                END
                AS Status FROM User INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentID = User.ID
                LEFT JOIN ResidentFlatMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID LEFT JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID WHERE ResidentRWAMapping.RWAID = '" . $RWAID . "' AND User.ID!='" . $Uid . "' AND ResidentRWAMapping.ApprovalStatus= '" . $ApprovalStatus . "' ORDER BY  Status ASC ,User.AddedOn DESC Limit $Start,20");
            }
        } elseif ($ApprovalStatus == 'Y') {
            if ($Name != '') {
                $sql = $this->prdbh->prepare("SELECT User.ID, User.PhoneNbr, User.EmailID,  SocietyFlatMapping.FlatNbr,ResidentFlatMapping.IsOwner,ResidentRWAMapping.ApprovalStatus,ResidentRWAMapping.ResidentRWAID, ResidentRWAMapping.ApprovedBy, from_unixtime( (
                ResidentRWAMapping.ApprovedOn +19660), '%d/%m/%Y  %h:%i %p' ) AS ApprovedOn, User.FirstName, User.MiddleName, User.LastName, User.ProfilePic, User.Gender, User.Occupation, from_unixtime( (
                User.AddedOn +19660), '%d/%m/%Y  %h:%i %p' ) AS AddedOn, User.IsActive,
                Case ResidentRWAMapping.ApprovalStatus
                WHEN 'P' THEN 1
                WHEN 'Y' THEN 2
                WHEN 'N' THEN 3
                END
                AS Status FROM User INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentID = User.ID
                LEFT JOIN ResidentFlatMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID LEFT JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID WHERE User.FirstName like '%" . $Name . "%' AND ResidentRWAMapping.RWAID = '" . $RWAID . "'  AND User.ID!='" . $Uid . "' AND ResidentRWAMapping.ApprovalStatus= '" . $ApprovalStatus . "' ORDER BY  Status ASC,User.AddedOn DESC ");
            } else {
                $sql = $this->prdbh->prepare("SELECT User.ID, User.PhoneNbr, User.EmailID,  SocietyFlatMapping.FlatNbr,ResidentFlatMapping.IsOwner,ResidentRWAMapping.ApprovalStatus,ResidentRWAMapping.ResidentRWAID, ResidentRWAMapping.ApprovedBy, from_unixtime( (
                ResidentRWAMapping.ApprovedOn +19660), '%d/%m/%Y  %h:%i %p' ) AS ApprovedOn, User.FirstName, User.MiddleName, User.LastName, User.ProfilePic, User.Gender, User.Occupation, from_unixtime( (
                User.AddedOn +19660), '%d/%m/%Y  %h:%i %p' ) AS AddedOn, User.IsActive,
                Case ResidentRWAMapping.ApprovalStatus
                WHEN 'P' THEN 1
                WHEN 'Y' THEN 2
                WHEN 'N' THEN 3
                END
                AS Status FROM User INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentID = User.ID
                LEFT JOIN ResidentFlatMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID LEFT JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID WHERE ResidentRWAMapping.RWAID = '" . $RWAID . "' AND User.ID!='" . $Uid . "' AND ResidentRWAMapping.ApprovalStatus= '" . $ApprovalStatus . "' ORDER BY  Status ASC ,User.AddedOn DESC Limit $Start,20");
            }
        } elseif ($ApprovalStatus == 'N') {
            if ($Name != '') {
                $sql = $this->prdbh->prepare("SELECT User.ID, User.PhoneNbr, User.EmailID,  SocietyFlatMapping.FlatNbr,ResidentFlatMapping.IsOwner,ResidentRWAMapping.ApprovalStatus,ResidentRWAMapping.ResidentRWAID, ResidentRWAMapping.ApprovedBy, from_unixtime( (
                ResidentRWAMapping.ApprovedOn +19660), '%d/%m/%Y  %h:%i %p' ) AS ApprovedOn, User.FirstName, User.MiddleName, User.LastName, User.ProfilePic, User.Gender, User.Occupation, from_unixtime( (
                User.AddedOn +19660), '%d/%m/%Y  %h:%i %p' ) AS AddedOn, User.IsActive,
                Case ResidentRWAMapping.ApprovalStatus
                WHEN 'P' THEN 1
                WHEN 'Y' THEN 2
                WHEN 'N' THEN 3
                END
                AS Status FROM User INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentID = User.ID
                LEFT JOIN ResidentFlatMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID LEFT JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID WHERE User.FirstName like '%" . $Name . "%' AND ResidentRWAMapping.RWAID = '" . $RWAID . "'  AND User.ID!='" . $Uid . "' AND ResidentRWAMapping.ApprovalStatus= '" . $ApprovalStatus . "' ORDER BY  Status ASC,User.AddedOn DESC ");
            } else {
                $sql = $this->prdbh->prepare("SELECT User.ID, User.PhoneNbr, User.EmailID,  SocietyFlatMapping.FlatNbr,ResidentFlatMapping.IsOwner,ResidentRWAMapping.ApprovalStatus,ResidentRWAMapping.ResidentRWAID, ResidentRWAMapping.ApprovedBy, from_unixtime( (
                ResidentRWAMapping.ApprovedOn +19660), '%d/%m/%Y  %h:%i %p' ) AS ApprovedOn, User.FirstName, User.MiddleName, User.LastName, User.ProfilePic, User.Gender, User.Occupation, from_unixtime( (
                User.AddedOn +19660), '%d/%m/%Y  %h:%i %p' ) AS AddedOn, User.IsActive,
                Case ResidentRWAMapping.ApprovalStatus
                WHEN 'P' THEN 1
                WHEN 'Y' THEN 2
                WHEN 'N' THEN 3
                END
                AS Status FROM User INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentID = User.ID
                LEFT JOIN ResidentFlatMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID LEFT JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID WHERE ResidentRWAMapping.RWAID = '" . $RWAID . "' AND User.ID!='" . $Uid . "' AND ResidentRWAMapping.ApprovalStatus= '" . $ApprovalStatus . "' ORDER BY  Status ASC ,User.AddedOn DESC Limit $Start,20");
            }
        }
        //print_r($sql);die;
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $sql1 = $this->prdbh->prepare("SELECT ApprovalStatus FROM  ResidentRWAMapping 
                    WHERE RWAID= '" . $RWAID . "' AND ResidentID!= '" . $Uid . "'  ");
                $sql1->execute();
                if ($sql1->execute()) {
                    $count1 = $sql1->rowCount();
                } else {
                    $count1 = '0';
                }
                $sql1 = $this->prdbh->prepare("SELECT ApprovalStatus FROM  ResidentRWAMapping 
                    WHERE RWAID= '" . $RWAID . "' AND ResidentID!= '" . $Uid . "' AND ApprovalStatus ='Y' ");
                $sql1->execute();
                if ($sql1->execute()) {
                    $approved = $sql1->rowCount();
                } else {
                    $approved = '0';
                }
                $sql1 = $this->prdbh->prepare("SELECT ApprovalStatus FROM  ResidentRWAMapping 
                    WHERE RWAID= '" . $RWAID . "' AND ResidentID!= '" . $Uid . "' AND ApprovalStatus ='N' ");
                $sql1->execute();
                if ($sql1->execute()) {
                    $rejected = $sql1->rowCount();
                } else {
                    $rejected = '0';
                }
                $sql1 = $this->prdbh->prepare("SELECT ApprovalStatus FROM  ResidentRWAMapping 
                    WHERE RWAID= '" . $RWAID . "' AND ResidentID!= '" . $Uid . "' AND ApprovalStatus ='P' ");
                $sql1->execute();
                if ($sql1->execute()) {
                    $pending = $sql1->rowCount();
                } else {
                    $pending = '0';
                }
            //    $response['Count'] = $count1;
                $response['Count'] = $pending;
                $response['pending'] = $pending;
                $response['approved'] = $approved;
                $response['rejected'] = $rejected;
                $response['approvesocietymember'] = $items;
                $response['Status'] = '1';
            } else {
                $response['approvesocietymember'] = [];
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function approvesocietymember($param) {
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        if (isset($param->Uid)) {
            $Uid = $param->Uid;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT User.ID, User.PhoneNbr, User.EmailID,  ResidentFlatMapping.FlatNbr,ResidentFlatMapping.IsOwner,ResidentRWAMapping.ApprovalStatus, ResidentRWAMapping.ApprovedBy, from_unixtime( (
                ResidentRWAMapping.ApprovedOn +19660), '%d/%m/%Y  %h:%i %p' ) AS ApprovedOn, User.FirstName, User.MiddleName, User.LastName, User.ProfilePic, User.Gender, User.Occupation, from_unixtime( (
                User.AddedOn +19660), '%d/%m/%Y  %h:%i %p' ) AS AddedOn, User.IsActive,
                Case ResidentRWAMapping.ApprovalStatus
                WHEN 'P' THEN 1
                WHEN 'Y' THEN 2
                WHEN 'N' THEN 3
                END
                AS Status FROM User INNER JOIN ResidentRWAMapping ON ResidentRWAMapping.ResidentID = User.ID
                INNER JOIN ResidentFlatMapping ON ResidentRWAMapping.ResidentRWAID = ResidentFlatMapping.ResidentRWAID WHERE ResidentRWAMapping.RWAID = '" . $RWAID . "' AND User.ID!='" . $Uid . "' ORDER BY  Status ASC");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['approvesocietymember'] = $items;
                $response['Status'] = '1';
            } else {
                $response['approvesocietymember'] = [];
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function approve($rec) {
        $response = array();
        $approve = $rec->ApprovalStatus;
        $ApprovedBy = $rec->ApprovedBy;
        $ResidentID = $rec->ResidentID;
        $RWAID = $rec->RWAID;
        if (!isset($approve) && $approve == '') {
            $response['Status'] = 0;
        } elseif (!isset($ApprovedBy) && $ApprovedBy == '') {
            $response['Status'] = 0;
        } elseif (!isset($ResidentID) && $ResidentID == '') {
            $response['Status'] = 0;
        } elseif (!isset($RWAID) && $RWAID == '') {
            $response['Status'] = 0;
        } else {
            $tdat = date("U");
            $tdate = $tdat;
            $sql = $this->prdbh->prepare("UPDATE ResidentRWAMapping SET ApprovalStatus=?,ApprovedBy=?,ApprovedOn=? WHERE ResidentID = ? AND RWAID = ?");
            $sql->bindParam(1, $rec->ApprovalStatus);
            $sql->bindParam(2, $rec->ApprovedBy);
            $sql->bindParam(3, $tdate);
            $sql->bindParam(4, $rec->ResidentID);
            $sql->bindParam(5, $rec->RWAID);
            $this->prdbh->beginTransaction();
            if ($sql->execute()) {
                $this->prdbh->commit();
                if ($rec->ApprovalStatus == 'Y') $response['message'] = "Approved successfully";
                elseif ($rec->ApprovalStatus == 'N') $response['message'] = "Rejected successfully";
                $response['Status'] = "1";
                if ($rec->ApprovalStatus == 'Y') {
                    $sql4 = $this->prdbh->prepare("SELECT ID, RWAID, RolesID, AdminEmail, AdminPhonenbr FROM RWARoleMaster WHERE RWAID='" . $RWAID . "'");
                    $sql4->execute();
                    if ($sql4->rowCount() > 0) {
                        while ($row4 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                            $rolid = $row4['RolesID'];
                            $AdminEmail = $row4['AdminEmail'];
                            $AdminPhonenbr = $row4['AdminPhonenbr'];
                        }
                    }
                    $sql1 = $this->prdbh->prepare("SELECT User.ID,User.EmailID,User.FirstName,ResidentRWAMapping.ResidentRWAID FROM User
                INNER JOIN ResidentRWAMapping ON User.ID=ResidentRWAMapping.ResidentID
                WHERE ResidentRWAMapping.ResidentID='" . $ResidentID . "'");
                    $sql1->execute();
                    if ($sql1->rowCount() > 0) {
                        $UpushId1 = array();
                        while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                            $rrwid = $row1['ResidentRWAID'];
                            $UpushId1[] = $row1['ID'];
                            $name = $row1['FirstName'];
                            $email = $row1['EmailID'];
                        }
                        $UpushId = implode(',', $UpushId1);
                        $message = 'Congratulations! You have been approved by RWA';
                        $UrHd1 = 'Approval Status';
                        $message1 = array("message" => $message, "UrHd2" => $UrHd1);
                        $result = $this->firebasenotification($UpushId, $message1);
                        $response['PushStatus'] = $result;
                        $result = $this->approvemail($email, $name);
                        //print_r($result);die;
                        
                    }
                    $sql9 = $this->prdbh->prepare("SELECT ResidentRWAId FROM UserRoles WHERE ResidentRWAId= '" . $rrwid . "' AND RoleID='" . $rolid . "'");
                    if ($sql9->execute()) {
                        $count9 = $sql9->rowCount();
                        if ($count9 >= 1) {
                            $sql2 = $this->prdbh->prepare("UPDATE UserRoles SET RoleID= $rolid WHERE ResidentRWAId= $rrwid");
                        } else {
                            $sql2 = $this->prdbh->prepare("INSERT INTO UserRoles (RoleID,ResidentRWAId) VALUES($rolid,$rrwid)");
                        }
                        $this->prdbh->beginTransaction();
                        if ($sql2->execute()) {
                            $this->prdbh->commit();
                        }
                    }
                } elseif ($rec->ApprovalStatus == 'N') {
                    $umsg = $rec->RejectionReson;
                    $email = $rec->Email;
                    $name = $rec->Name;
                    $sql4 = $this->prdbh->prepare("SELECT ID, RWAID, RolesID, AdminEmail, AdminPhonenbr FROM RWARoleMaster WHERE RWAID='" . $RWAID . "'");
                    $sql4->execute();
                    if ($sql4->rowCount() > 0) {
                        $rolid = array();
                        while ($row4 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                            $rolid = $row4['RolesID'];
                            $AdminEmail = $row4['AdminEmail'];
                            $AdminPhonenbr = $row4['AdminPhonenbr'];
                        }
                    }
                    if ($ResidentID != '') {
                        $UpushId = $ResidentID;
                        $message = 'Alas! You have been rejected by RWA';
                        $UrHd1 = 'Rejection Status';
                        $message1 = array("message" => $message, "UrHd2" => $UrHd1);
                        $result = $this->firebasenotification($UpushId, $message1);
                        $response['PushStatus'] = $result;
                    }
                    $sql = $this->prdbh->prepare("INSERT INTO RejectionReson(ResidentRWAID, RejectionReson,Date) VALUES(?,?,?)");
                    $sql->bindParam(1, $rec->ResidentRWAID);
                    $sql->bindParam(2, $rec->RejectionReson);
                    $sql->bindParam(3, $tdate);
                    $this->prdbh->beginTransaction();
                    if ($sql->execute()) {
                        $this->prdbh->commit();
                        $result = $this->rejectionresonmail($umsg, $subject, $email, $name, $AdminEmail, $AdminPhonenbr);
                    }
                }
            } else {
                $response['message'] = $sql->errorInfo();
                $response['Status'] = 0;
            }
        }
        return $response;
    }
    public function approvalmail($email, $name) {
        $response = array();
        $umail = $email;
        $umsg = "Dear " . $name . ",
                    Your access to mynukad is approved by your society RWA / maintenance office. 
                    Login to mynukad App on your mobile device and gain access to all society functions of your society through the App.
                    Regards,
                    Team mynukad
                     ";
        //on email ID : ".$adminemailid." or call on ".$adminnumber ."
        $usub = "[mynukad] : Admin has approved you";
        require 'sendgrid/vendor/autoload.php';
        require 'sendgrid/lib/SendGrid.php';
        $sendgrid = new SendGrid('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');
        $email = new SendGrid\Email();
        $email->addTo($umail)->setFrom('noreply@mynukad.com')->setSubject($usub)->setText($umsg)->setHtml($dt);
        try {
            $sendgrid->send($email);
            $response = 1;
        }
        catch(\SendGrid\Exception $e) {
            $response = 0;
        }
        return $response;
    }
    public function polestatus($rec) {
        $QuestionId = $rec->QuestionId;
        $tdat = date("U");
        $tdate = $tdat;
        $sql1 = $this->prdbh->prepare("INSERT INTO PollResponses(RespondentId,QuestionId,ResponseOptionId, ResponseDt) VALUES (?,?,?,?)");
        $sql1->bindParam(1, $rec->RespondentId);
        $sql1->bindParam(2, $QuestionId);
        $sql1->bindParam(3, $rec->ResponseOptionId);
        $sql1->bindParam(4, $tdate);
        $this->prdbh->beginTransaction();
        $sql1->execute();
        $count1 = $sql1->rowCount();
        if ($count1 > 0) {
            $id1 = $this->prdbh->lastInsertId();
            $this->prdbh->commit();
            $response['Message'] = "Polled successfully";
            $response['PoleResponceId'] = $id1;
            $response['Status'] = "1";
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function addphotogallery1($param) {
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        $RWAID = $rec->RWAID;
        $ResidentID = $rec->ResidentID;
        if (isset($param->ResidentID)) {
            $ResidentID = $param->ResidentID;
        }
        $sql = $this->prdbh->prepare("SELECT ResidentRWAID FROM `ResidentRWAMapping` WHERE RWAID = '" . $RWAID . "' AND ResidentID = '" . $ResidentID . "' ");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response[' gallary '] = $items;
                $response['Status'] = '1';
            } else {
                $response[' ResidentRWAID'] = [];
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function addphotogallery($rec) {
        $RWAID = $rec->RWAID;
        $ResidentID = $rec->ResidentID;
        $PhotoPath = $rec->PhotoPath;
        $sql = $this->prdbh->prepare("SELECT User.FirstName,User.ProfilePic, ResidentRWAMapping.ResidentRWAID FROM ResidentRWAMapping INNER JOIN User ON User.ID=ResidentRWAMapping.ResidentID WHERE RWAID = '" . $RWAID . "' AND ResidentID = '" . $ResidentID . "' ");
        $sql->execute();
        if ($sql->rowCount() >= 1) {
            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                $rid = $row['ResidentRWAID'];
                $ResidentName = $row['FirstName'];
                $ProfilePic = $row['ProfilePic'];
            }
        }
        $sql5 = $this->prdbh->prepare("SELECT SocietyFlatMapping.FlatNbr FROM SocietyFlatMapping INNER JOIN ResidentFlatMapping ON ResidentFlatMapping.FlatID= SocietyFlatMapping.ID WHERE ResidentFlatMapping.ResidentRWAID ='" . $rid . "' ");
        $sql5->execute();
        if ($sql5->rowCount() > 0) {
            $ResidentFlatNo = array();
            while ($row5 = $sql5->fetch(PDO::FETCH_ASSOC)) {
                $ResidentFlatNbr[] = $row5['FlatNbr'];
            }
        }
        $ResidentFlatNo = implode(',', $ResidentFlatNbr);
        $PhotoCaption = $rec->PhotoCaption;
        $FeedType = "photos";
        $tdat = date("U");
        $tdate = $tdat;
        $sql = $this->prdbh->prepare("INSERT INTO `PhotoGallery`( `PhotoPath`, `PhotoCaption`, `Date`, `IsDeleted`, `AddedBy`,IsActive) VALUES(?,?,?,?,?,1)");
        $sql->bindParam(1, $rec->PhotoPath);
        $sql->bindParam(2, $rec->PhotoCaption);
        $sql->bindParam(3, $tdate);
        $sql->bindParam(4, $rec->IsDeleted);
        $sql->bindParam(5, $rid);
        $this->prdbh->beginTransaction();
        $sql->execute();
        $count = $sql->rowCount();
        if ($count > 0) {
            $id = $this->prdbh->lastInsertId();
            $this->prdbh->commit();
            $response['Message'] = "Photo added successfully";
            $output = $this->onlineinsert($PhotoCaption, $FeedType, $id, $rid, $ResidentFlatNo, $ProfilePic, $ResidentID, $RWAID, $ResidentName, $PhotoPath);
            $jsonObject = json_decode($output);
            if ($jsonObject->Status == 1) {
                $response['MongoResponce'] = "Photo added successfully";
            }
            $sql1 = $this->prdbh->prepare("SELECT User.ID FROM User
                                                INNER JOIN ResidentRWAMapping ON User.ID=ResidentRWAMapping.ResidentID
                                                LEFT JOIN RWA ON ResidentRWAMapping.RWAID=RWA.ID
                                                WHERE RWAID='" . $RWAID . "' AND User.ID!='" . $ResidentID . "'  ");
            $sql1->execute();
            if ($sql1->rowCount() > 0) {
                $UpushId1 = array();
                while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                    $UpushId1[] = $row1['ID'];
                }
                $UpushId = implode(',', $UpushId1);
                $ID = '' . $id;
                //$message=base64_decode($rec->PhotoCaption);
                $message = "Click here to view pic";
                $UrHd1 = 'Society Photo Gallery';
                //$ID='C'.$id;
                $response['PhotoGallery'] = $id;
                $response['Status'] = "1";
                $message1 = array("message" => $message, "UrHd2" => $UrHd1, "body" => $ID);
                $result = $this->firebasenotification($UpushId, $message1);
                $response['PushStatus'] = $result;
            }
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function photogallery($param) {
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        if (isset($param->PageNo)) {
            $PageNo = $param->PageNo;
        }
        if (isset($param->ResidentID)) {
            $ResidentID = $param->ResidentID;
        }
        $Start = $PageNo * 10 - 10;
        $response = array();
        $sql = $this->prdbh->prepare("SELECT DISTINCT from_unixtime((Date + 19660),'%Y-%m-%d') as  date  from PhotoGallery 
            inner join ResidentRWAMapping on PhotoGallery.AddedBy = ResidentRWAMapping.ResidentRWAID  WHERE  ResidentRWAMapping.RWAID = '" . $RWAID . "' 
            GROUP BY date DESC Limit $Start,10");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                $i = 0;
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    //$dta[]=$row['date'];
                    $tdate = $row['date'];
                    $date = strtotime($tdate);
                    $response['Date'][$i]['Date'] = "$date";
                    $date1 = $date + 24 * 3600 - 1;
                    $sql1 = $this->prdbh->prepare("select PhotoGallery.PhotoID,PhotoGallery.PhotoPath,PhotoGallery.PhotoCaption,
                        (ResidentRWAMapping.ResidentID) AS AddedBy,User.FirstName,ResidentRWAMapping.ResidentRWAID  FROM PhotoGallery
                        inner join ResidentRWAMapping on PhotoGallery.AddedBy = ResidentRWAMapping.ResidentRWAID 
                        INNER JOIN ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID = ResidentRWAMapping.ResidentRWAID  
                        LEFT JOIN User ON User.ID = ResidentRWAMapping.ResidentID 
                        where PhotoGallery.Date BETWEEN '" . $date . "' AND '" . $date1 . "'  AND PhotoGallery.IsActive='1' AND ResidentRWAMapping.RWAID = '" . $RWAID . "' GROUP BY PhotoID ORDER BY PhotoID  DESC ");
                    //print_r($sql1);die;
                    $sql1->execute();
                    $count1 = $sql1->rowCount();
                    if ($count1 > 0) {
                        $subitems = array();
                        while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                            $gal['PhotoID'] = $row1['PhotoID'];
                            $gal['PhotoPath'] = $row1['PhotoPath'];
                            $gal['PhotoCaption'] = $row1['PhotoCaption'];
                            $gal['AddedBy'] = $row1['AddedBy'];
                            $gal['FirstName'] = $row1['FirstName'];
                            $gal['ResidentRWAID'] = $row1['ResidentRWAID'];
                            $RerwaID = $row1['ResidentRWAID'];
                            $PhotoID = $row1['PhotoID'];
                            $sql3 = $this->prdbh->prepare("SELECT COUNT(PhotoID) AS LIKES FROM PhotoGalleryLike WHERE PhotoID = '" . $PhotoID . "' AND Likes =  '1'  ");
                            $sql3->execute();
                            if ($sql3->rowCount() > 0) {
                                while ($row3 = $sql3->fetch(PDO::FETCH_ASSOC)) {
                                    $gal['LIKES'] = $row3['LIKES'];
                                }
                            } else {
                                $gal['LIKES'] = '0';
                            }
                            $sql4 = $this->prdbh->prepare("SELECT PhotoID FROM PhotoGalleryLike WHERE Likes =  '1' AND ResidentID ='" . $ResidentID . "' AND PhotoID =  '" . $PhotoID . "' ");
                            $sql4->execute();
                            if ($sql4->rowCount() > 0) {
                                $gal['IsLIKES'] = '1';
                            } else {
                                $gal['IsLIKES'] = '0';
                            }
                            $sql2 = $this->prdbh->prepare("SELECT ResidentFlatMapping.ID,SocietyFlatMapping.FlatNbr FROM ResidentFlatMapping INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID  WHERE ResidentRWAID ='" . $RerwaID . "'");
                            $sql2->execute();
                            $FlatNbr = array();
                            if ($sql2->rowCount() > 0) {
                                while ($row2 = $sql2->fetch(PDO::FETCH_ASSOC)) {
                                    $FlatNbr[] = $row2['FlatNbr'];
                                }
                                $FlatNbrString = implode(',', $FlatNbr);
                            } else {
                                $FlatNbrString = "";
                            }
                            $a['FlatNbr'] = $FlatNbrString;
                            $row1 = array_merge($gal, $a);
                            $subitems[] = $row1;
                            $gal = array();
                        }
                        $response['Date'][$i]['PhotoGallery'] = $subitems;
                    }
                    $i++;
                }
                $response['Status'] = '1';
                $sql2 = $this->prdbh->prepare("SELECT DISTINCT Date FROM PhotoGallery inner join ResidentRWAMapping on PhotoGallery.AddedBy = ResidentRWAMapping.ResidentRWAID  WHERE  RWAID = '" . $RWAID . "'");
                $sql2->execute();
                $count2 = $sql2->rowCount();
                $response['Count'] = $count2;
            } else {
                $response['PhotoGallery'] = [];
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function changemaidpicurl($param) {
        if (isset($param->PicUrl)) {
            $PicUrl = $param->PicUrl;
        }
        if (isset($param->HelperID)) {
            $HelperID = $param->HelperID;
        }
        $sql = $this->prdbh->prepare("UPDATE Helpers SET  ProfilePhoto='" . $PicUrl . "' WHERE ID = '" . $HelperID . "'  ");
        if ($sql->execute()) {
            $response['MaidProfilePhoto'] = 'Updated successfully';
            $response['Status'] = '1';
        } else {
            $response['MaidProfilePhoto'] = 'Not updated';
            $response['Status'] = '0';
        }
        return $response;
    }
    public function gallerypicdelete($param) {
        if (isset($param->ID)) {
            $ID = $param->ID;
        }
        $response = array();
        $FeedType = "societygallery";
        $sql = $this->prdbh->prepare("UPDATE PhotoGallery SET  IsActive='0' WHERE PhotoID = '" . $ID . "'  ");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                $output = $this->postdelete($FeedType, $ID);
                $jsonObject = json_decode($output);
                if ($jsonObject->Status == 1) {
                    $response['MongoResponce'] = "Societygallery Removed successfully";
                }
                $response['PhotoGallery'] = 'Removed successfully';
                $response['Status'] = '1';
            } else {
                $response['PhotoGallery'] = 'Not Removed';
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function deletefavshop($param) {
        if (isset($param->UserID)) {
            $UserID = $param->UserID;
        }
        if (isset($param->NearByID)) {
            $NearByID = $param->NearByID;
        }
        $response = array();
        $sql = $this->prdbh->prepare("DELETE FROM UserFavouriteNearBy WHERE NearByID  = '" . $NearByID . "' AND UserID = '" . $UserID . "' ");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response[' Delete Fav Shop '] = $items;
                $response['Status'] = '1';
            } else {
                $response['Delete Fav Shop'] = [];
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function removeclassified($param) {
        if (isset($param->AddedBy)) {
            $AddedBy = $param->AddedBy;
        }
        if (isset($param->ID)) {
            $ID = $param->ID;
        }
        $InActiveDate = date("U");
        $response = array();
        $FeedType = "classified";
        $sql = $this->prdbh->prepare("UPDATE Classifieds SET InActiveDate='" . $InActiveDate . "', IsActive='0' WHERE Id = '" . $ID . "' AND AddedBy = '" . $AddedBy . "' ");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                $output = $this->postdelete($FeedType, $ID);
                $jsonObject = json_decode($output);
                if ($jsonObject->Status == 1) {
                    $response['MongoResponce'] = "Classified Removed successfully";
                }
                $response['Classifieds'] = 'Removed successfully';
                $response['Status'] = '1';
            } else {
                $response['Classifieds'] = 'Not Removed';
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function removepoles($param) {
        if (isset($param->ResidentRWAID)) {
            $ResidentRWAID = $param->ResidentRWAID;
        }
        if (isset($param->QuestionId)) {
            $QuestionId = $param->QuestionId;
        }
        $FeedType = "poll";
        $response = array();
        $InActiveDate = date("U");
        $sql = $this->prdbh->prepare("DELETE FROM PollResponses WHERE  QuestionId = '" . $QuestionId . "' ");
        $sql->execute();
        if ($sql) {
            $sql1 = $this->prdbh->prepare("UPDATE PollQuestions SET  IsActive='0' ,InActiveDate='" . $InActiveDate . "' WHERE Id= '" . $QuestionId . "' ");
            $sql1->execute();
            if ($sql1) {
                $output = $this->postdelete($FeedType, $QuestionId);
                $jsonObject = json_decode($output);
                if ($jsonObject->Status == 1) {
                    $response['MongoResponce'] = "Pole Removed successfully";
                }
                $response['Poles'] = 'Removed successfully';
                $response['Status'] = '1';
            } else {
                $response['Poles'] = 'Not Removed';
                $response['Status'] = '0';
            }
        } else {
            $response['Poles'] = 'Not Removed';
            $response['Status'] = '0';
        }
        return $response;
    }
    public function removecallout($param) {
        if (isset($param->SenderID)) {
            $SenderID = $param->SenderID;
        }
        if (isset($param->ID)) {
            $ID = $param->ID;
        }
        $response = array();
        $FeedType = "callout";
        $InActiveDate = date("U");
        $sql = $this->prdbh->prepare("UPDATE CallOuts SET  IsActive='0',InActiveDate='" . $InActiveDate . "' WHERE  ID  = '" . $ID . "' ");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                $output = $this->postdelete($FeedType, $ID);
                $jsonObject = json_decode($output);
                if ($jsonObject->Status == 1) {
                    $response['MongoResponce'] = "Callout Removed successfully";
                }
                $response['CallOuts'] = 'Removed successfully';
                $response['Status'] = '1';
            } else {
                $response['CallOuts'] = 'Not Removed';
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function deletevehicle($param) {
        if (isset($param->ID)) {
            $ID = $param->ID;
        }
        $InActiveDate = date("U");
        $response = array();
        $sql = $this->prdbh->prepare("UPDATE ResidentVehicleMapping SET  IsActive='0',InActiveDate='" . $InActiveDate . "' WHERE ID = '" . $ID . "' ");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['Message'] = "Vehicle deleted";
                $response['Status'] = '1';
            } else {
                $response['Deleted Vehicle'] = [];
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function deletefavhelper($param) {
        if (isset($param->UserID)) {
            $UserID = $param->UserID;
        }
        if (isset($param->HelperID)) {
            $HelperID = $param->HelperID;
        }
        $response = array();
        $sql = $this->prdbh->prepare("DELETE FROM UserFavHelper WHERE HelperID = '" . $HelperID . "' AND UserID = '" . $UserID . "' ");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['Delete Fav Helper '] = $items;
                $response['Status'] = '1';
            } else {
                $response['Delete Fav Helper'] = [];
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function showfavhelper($param) {
        if (isset($param->UserID)) {
            $UserID = $param->UserID;
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT UserFavHelper.UserFavHelperID, UserFavHelper.UserID, UserFavHelper.HelperID,Helpers.ID,Helpers.RWAID, Helpers.Name, Helpers.ServiceOffered,Helpers.ServiceCharges,Helpers.ResidentialAddress, Helpers.PrimaryContactNbr, Helpers.PhoneNbr2,Helpers.EmailId,IFNULL(Helpers.PoliceVerification,'0') as PoliceVerification, Helpers.AddedBy,Helpers.ProfilePhoto,Helpers.OtherDocScanImage1,Helpers.OtherDocScanImage2,Helpers.OtherDocScanImage3,Helpers.EntryCardExpiry,Helpers.IsActive,IFNULL(Helpers.is_in,'2') as is_in, UserFavHelper.AddedOn,Helpers.LastScaneTimestamp FROM UserFavHelper LEFT JOIN Helpers ON UserFavHelper.HelperID = Helpers.ID WHERE UserFavHelper.UserID ='" . $UserID . "' AND Helpers.IsActive= '1' GROUP BY Helpers.ID ");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                $i = 0;
                $response = array();
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                    $ID = $row['ID'];
                    $LastScaneTimestamp = $row['LastScaneTimestamp'];
                    $is_in = $row['is_in'];
                    $sql1 = $this->prdbh->prepare("SELECT (AVG(Rating) +0.0001) as rated from HelperReviews WHERE HelperID = '" . $ID . "'");
                    $sql1->execute();
                    if ($sql1) {
                        $subitems = array();
                        $count1 = $sql->rowCount();
                        if ($count1 > 0) {
                            $subitems = array();
                            while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                                $subitems[] = $row1['rated'];
                                $a = implode('', $subitems);
                                $items[$i]['Rated'] = $a;
                            }
                        }
                    }
                    $tdat = date("U") + 600;
                    $t24 = $tdat - 86400;
                    $t48 = $tdat - 172800;
                    $color1 = '';
                    if ($LastScaneTimestamp != '') {
                        if ($LastScaneTimestamp < $t48 && $is_in == 1) {
                            $color1 = "Red";
                        } else if (($LastScaneTimestamp > $t48 && $is_in == 1) && ($LastScaneTimestamp <= $t24 && $is_in == 1)) {
                            $color1 = "Yellow";
                        } else if (($LastScaneTimestamp > $t24 && $is_in == 1) && ($LastScaneTimestamp <= $tdat && $is_in == 1)) {
                            $color1 = "Green";
                        } elseif ($is_in == 2 || $is_in == 0) {
                            $color1 = "Grey";
                        }
                    } else {
                        $color1 = "Grey";
                    }
                    $items[$i]['color'] = $color1;
                    $i++;
                }
                $response['Fav Helpers'] = $items;
                $response['Status'] = '1';
            } else {
                $response['Fav Helpers'] = [];
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function showfavhelper1($param) {
        $RWAID = $param->RWAID;
        $response = array();
        $sql = $this->prdbh->prepare("SELECT * FROM Helpers WHERE RWAID = '" . $RWAID . "'");
        if ($sql->execute()) {
            if ($sql->rowCount() > 0) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $HeID[] = $row['ID'];
                }
                if (isset($param->UserID)) {
                    $UserID = $param->UserID;
                    $sql1 = $this->prdbh->prepare("SELECT * FROM UserFavHelper WHERE UserID =$UserID");
                    $sql1->execute();
                    if ($sql1->rowCount() > 0) {
                        while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                            $UHeID[] = $row1['HelperID'];
                        }
                        $RHeID = array_diff($HeID, $UHeID);
                        $RHeID = implode(",", $RHeID);
                        $UHeID = implode(",", $UHeID);
                        $sqlR = $this->prdbh->prepare("SELECT * FROM Helpers WHERE ID IN($RHeID)");
                        $sqlR->execute();
                        while ($rowR = $sqlR->fetch(PDO::FETCH_ASSOC)) {
                            $a['IsFav'] = 0;
                            $rowR = array_merge($rowR, $a);
                            $dataR[] = $rowR;
                        }
                        $sqlU = $this->prdbh->prepare("SELECT * FROM Helpers WHERE ID IN($UHeID)");
                        $sqlU->execute();
                        while ($rowU = $sqlU->fetch(PDO::FETCH_ASSOC)) {
                            $a['IsFav'] = 1;
                            $rowU = array_merge($rowU, $a);
                            $dataU[] = $rowU;
                        }
                        $response['FavHelper'] = $dataU;
                        $response['Helper'] = $dataR;
                        $response['status'] = 1;
                    }
                }
            } else {
                $response['message'] = "No Helper";
                $response['status'] = 0;
            }
        } else {
            $response['message'] = $sql->errorInfo();
            $response['status'] = 0;
        }
        return $response;
    }
    public function categories($param) {
        $response = array();
        $sql = $this->prdbh->prepare("SELECT * from NearByCategory order by Description asc");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['list'] = $items;
                $response['Status'] = '1';
            } else {
                $response['list'] = [];
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function complainshordescription($param) {
        if (isset($param->RWAID)) {
            $rwaid = $param->RWAID;
        }
        if (isset($param->CategoryID)) {
            $CategoryID = $param->CategoryID;
        }
        $response = array();
        $sql2 = $this->prdbh->prepare("SELECT * from ComplainShorDescription WHERE RWAID ='$rwaid' AND CategoryID='$CategoryID'");
        $sql2->execute();
        $items2 = array();
        $count1 = $sql2->rowCount();
        if ($count1 >= 1) {
            while ($row2 = $sql2->fetch(PDO::FETCH_ASSOC)) {
                $items2[] = $row2;
            }
            $response['Short Description'] = $items2;
            $response['Status'] = '1';
        } else {
            $response['Short Description'] = [];
            $response['Status'] = '0';
        }
        return $response;
    }
    public function rejectresondesc($param) {
        $response = array();
        $rwaid = $_GET['RWAID'];
        //  echo $rwaid;die;
        $sql = $this->prdbh->prepare("SELECT RWAID FROM RejectReasonTable WHERE RWAID = '$rwaid' ");
        $sql->execute();
        if ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
            $sql = $this->prdbh->prepare("SELECT * from RejectReasonTable  WHERE RWAID = '$rwaid' AND IsActive='1' order by ID asc");
            $sql->execute();
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                //print_r($items);die;
                $response['list'] = $items;
                $response['Status'] = '1';
            }
        } else {
            $sql = $this->prdbh->prepare("SELECT * from RejectReasonTable  WHERE RWAID = '0' AND IsActive='1' order by ID asc");
            $sql->execute();
            if ($sql) {
                $items = array();
                $count = $sql->rowCount();
                if ($count >= 1) {
                    while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                        $items[] = $row;
                    }
                    $response['list'] = $items;
                    $response['Status'] = '1';
                } else {
                    $response['list'] = [];
                    $response['Status'] = '0';
                }
            }
        }
        return $response;
    }
    public function complaincategories($param) {
        $response = array();
        $rwaid = $_GET['Id'];
        $sql = $this->prdbh->prepare("SELECT RWAID FROM ComplaintCategory WHERE RWAID = '$rwaid' ");
        $sql->execute();
        if ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
            $sql = $this->prdbh->prepare("SELECT * from ComplaintCategory  WHERE RWAID = '$rwaid' AND IsActive='1' order by ID asc");
            $sql->execute();
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['list'] = $items;
                $response['Status'] = '1';
            }
        } else {
            $sql = $this->prdbh->prepare("SELECT * from ComplaintCategory  WHERE RWAID = '0' AND IsActive='1' order by ID asc");
            $sql->execute();
            if ($sql) {
                $items = array();
                $count = $sql->rowCount();
                if ($count >= 1) {
                    while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                        $items[] = $row;
                    }
                    $response['list'] = $items;
                    $response['Status'] = '1';
                } else {
                    $response['list'] = [];
                    $response['Status'] = '0';
                }
            }
        }
        return $response;
    }
    public function visiterentrybyotp($param) {
        if (isset($param->OTP)) {
            $OTP = $param->OTP;
        }
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        $find = substr($OTP, 0, 1); // later use for verify visitor or helper
        $OTP = substr($OTP, 2);
        $tdat = date("U");
        $c = $tdat;
        $d = $c - 864000;
        $e = $c + 864000;
        $response = array();
        $sql = $this->prdbh->prepare("SELECT VisitorTable.VisitorID,VisitorTable.VisitorName,VisitorDetails.VisitorAddress,VisitorTable.VisitorPhoneNumber,(VisitorDetails.VisitorPurpose) as PurposeID,VisitorPurpos.DetailList,VisitorDetails.NumberOfPeople,VisitorDetails.VisitorVehicleNumber,SocietyFlatMapping.FlatNbr,SocietyFlatMapping.ID FROM VisitorTable Left JOIN VisitorDetails ON VisitorDetails.VisitorID= VisitorTable.VisitorID LEFT JOIN ResidentFlatMapping ON ResidentFlatMapping.ID=VisitorTable.FlatID LEFT JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID LEFT JOIN VisitorPurpos ON VisitorPurpos.ID = VisitorDetails.VisitorPurpose WHERE  VisitorTable.OTP='" . $OTP . "' AND VisitorTable.RWAID= '" . $RWAID . "'  AND VisitorTable.VisitorInOut = '0' AND VisitorDetails.VisitorETA BETWEEN '" . $d . "' AND '" . $e . "' ");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['Data'] = $items;
                $response['Status'] = '1';
            } else {
                $response['Data'] = "";
                $response['Message'] = "Visitor Code is already used or invalid Visitor Code";
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function visitorpurpose($param) {
        $response = array();
        $rwaid = $_GET['Id'];
        $sql = $this->prdbh->prepare("SELECT RWAID FROM VisitorPurpos WHERE RWAID = '$rwaid' ");
        $sql->execute();
        if ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
            $sql = $this->prdbh->prepare("SELECT ID, RWAID, UPPER(DetailList) as DetailList from VisitorPurpos  WHERE RWAID = '$rwaid' order by ID asc");
            $sql->execute();
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['purpose'] = $items;
                $response['Status'] = '1';
            }
        } else {
            $sql = $this->prdbh->prepare("SELECT ID, RWAID,UPPER(DetailList) as DetailList from VisitorPurpos  WHERE RWAID = '0' order by ID asc");
            $sql->execute();
            if ($sql) {
                $items = array();
                $count = $sql->rowCount();
                if ($count >= 1) {
                    while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                        $items[] = $row;
                    }
                    $response['purpose'] = $items;
                    $response['Status'] = '1';
                } else {
                    $response['purpose'] = [];
                    $response['Status'] = '0';
                }
            }
        }
        return $response;
    }
    public function visitorcompany($param) {
        $response = array();
        $rwaid = $_GET['Id'];
        $sql = $this->prdbh->prepare("SELECT ID, CompanyName, Image, Date from Visitorcompany  WHERE IsActive = '1' ");
        $sql->execute();
        $items = array();
        $count = $sql->rowCount();
        if ($count >= 1) {
            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                $items[] = $row;
            }
            $response['Visitorcompany'] = $items;
            $response['Status'] = '1';
        }
        return $response;
    }
    public function visitorverificationid($param) {
        $response = array();
        $rwaid = $_GET['Id'];
        $sql = $this->prdbh->prepare("SELECT RWAID FROM VerificationTable WHERE RWAID = '$rwaid' ");
        $sql->execute();
        if ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
            $sql = $this->prdbh->prepare("SELECT * from VerificationTable  WHERE RWAID = '$rwaid' order by ID asc");
            $sql->execute();
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row;
                }
                $response['VerificationIDType'] = $items;
                $response['Status'] = '1';
            }
        } else {
            $sql = $this->prdbh->prepare("SELECT * from VerificationTable  WHERE RWAID = '0' order by ID asc");
            $sql->execute();
            if ($sql) {
                $items = array();
                $count = $sql->rowCount();
                if ($count >= 1) {
                    while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                        $items[] = $row;
                    }
                    $response['VerificationIDType'] = $items;
                    $response['Status'] = '1';
                } else {
                    $response['purpose'] = [];
                    $response['Status'] = '0';
                }
            }
        }
        return $response;
    }
    public function assignstaff($param) {
        $response = array();
        $rwaid = $_GET['Id'];
        $type = $_GET['type'];
        $sql = $this->prdbh->prepare("SELECT ID, Name, ProfilePic, RWAID, ServiceCategory, PrimaryContactNbr, SecondaryContactNbr FROM MaintenanceStaff WHERE RWAID='$rwaid'  AND IsActive = 1 AND ServiceCategory ='$type'  order by ID asc");
        $sql->execute();
        $items = array();
        $count = $sql->rowCount();
        if ($count >= 1) {
            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                $items[] = $row;
            }
            $response['list'] = $items;
            $response['Status'] = '1';
        } else {
            $response['list'] = "No Staff in this categorie";
            $response['Status'] = '0';
        }
        return $response;
    }
    public function compfeedbackstatus($rec) {
        $response = array();
        $FlatID = $rec->FlatID;
        $sql1 = $this->prdbh->prepare("SELECT ID,Status,ComplaintDetails,AcknowledgementNote,Status FROM ComplaintRegister Where FlatID='" . $FlatID . "'  ORDER BY DateCreated DESC LIMIT 1 ; ");
        $sql1->execute();
        if ($sql1->rowCount() > 0) {
            while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                $Status = $row1['Status'];
                $AcknowledgementNote = $row1['AcknowledgementNote'];
                $ID = $row1['ID'];
                $ComplaintDetails = $row1['ComplaintDetails'];
                $Stat = $row1['Status'];
            }
            if ($AcknowledgementNote != '') {
                $response['Status'] = '0';
                $response['Message'] = "Acknowledged";
            } elseif ($Stat == '2') {
                $response['Message'] = "please fill acknowledgementNote";
                $response['Status'] = 1;
                $response['ComplaintID'] = $ID;
                $response['ComplaintDetails'] = $ComplaintDetails;
            } else {
                $response['Message'] = "Unresolve complain";
                $response['Status'] = 0;
                $response['ComplaintID'] = $ID;
                $response['ComplaintDetails'] = $ComplaintDetails;
            }
        } else {
            $response['Status'] = '0';
            $response['Message'] = "Acknowledged";
        }
        return $response;
    }
    public function notify($rec) {
        $response = array();
        $HelperID = $rec->HelperID;
        $ResidentRWAID = $rec->ResidentRWAID;
        $sql1 = $this->prdbh->prepare("SELECT * FROM HelperSubscribe WHERE HelperID='" . $HelperID . "' AND ResidentRWAID='" . $ResidentRWAID . "' ");
        $sql1->execute();
        if ($sql1->rowCount() > 0) {
            $sql = $this->prdbh->prepare("UPDATE HelperSubscribe SET Subscribe=? WHERE HelperID=? AND ResidentRWAID=?");
        } else {
            $sql = $this->prdbh->prepare("INSERT INTO HelperSubscribe (Subscribe,HelperID, ResidentRWAID) VALUES(?,?,?)");
        }
        $sql->bindParam(1, $rec->Subscribe);
        $sql->bindParam(2, $rec->HelperID);
        $sql->bindParam(3, $rec->ResidentRWAID);
        $this->prdbh->beginTransaction();
        if ($sql->execute()) {
            $this->prdbh->commit();
            if ($rec->Subscribe == '1') $response['message'] = ""; //Subscribe notify me
            elseif ($rec->Subscribe == '0') $response['message'] = "";
            $response['Status'] = "1";
        } else {
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function deletehelper($param) {
        $response = array();
        $InActiveDate = date("U");
        $sql = $this->prdbh->prepare("UPDATE Helpers SET InActiveDate = '" . $InActiveDate . "', IsActive=0  WHERE ID = '" . $_GET['Id'] . "' ");
        $sql->execute();
        if ($sql->execute()) {
            $response['Message'] = 'Helper deleted';
            $response['Status'] = '1';
        } else {
            $response['Message'] = 'Helper not deleted';
            $response['Status'] = '0';
        }
        return $response;
    }
    public function registration($rec) {
        $tdat = date("U");
        $tdate = $tdat;
        $PhoneNbr = $rec->PhoneNbr;
        $Passw = trim($rec->Password);
        $Password = md5($Passw);
        $FirstName = $rec->FirstName;
        $Gender = $rec->Gender;
        if ($PhoneNbr != '' && $Password != '' && $FirstName != '' && $Gender != '') {
            $sql = $this->prdbh->prepare("SELECT * FROM User WHERE PhoneNbr = '" . $PhoneNbr . "'");
            $sql->execute();
            if ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                $response['message'] = "Number already registered, please try with other number";
                $response['Status'] = "0";
            } else {
                $sql = $this->prdbh->prepare("INSERT INTO `User`(`PhoneNbr`, `EmailID`, `Password`, `FirstName`, `ProfilePic`, `Gender`, `About`, `Occupation`, `IsPhonePublic`, `AddedOn`, `IsActive`) VALUES (?,?,?,?,?,?,?,?,1,?,1)");
                $sql->bindParam(1, $rec->PhoneNbr);
                $sql->bindParam(2, $rec->EmailID);
                $sql->bindParam(3, $Password);
                $sql->bindParam(4, $rec->FirstName);
                $sql->bindParam(5, $rec->ProfilePic);
                $sql->bindParam(6, $rec->Gender);
                $sql->bindParam(7, $rec->About);
                $sql->bindParam(8, $rec->Occupation);
                $sql->bindParam(9, $tdate);
                $this->prdbh->beginTransaction();
                if ($sql->execute()) {
                    $id = $this->prdbh->lastInsertId();
                    $this->prdbh->commit();
                    $response['message'] = "Registration Successful";
                    $response['userid'] = $id;
                    $response['Status'] = "1";
                    if ((isset($rec->RWAID)) && ($rec->RWAID != '')) {
                        $rwid = $rec->RWAID;
                        $sql1 = $this->prdbh->prepare("INSERT INTO `ResidentRWAMapping`(`ResidentID`, `RWAID`, `AddedOn`, `ApprovalStatus`, `IsActive`) VALUES (?,?,?,'P','1')");
                        $sql1->bindParam(1, $id);
                        $sql1->bindParam(2, $rwid);
                        $sql1->bindParam(3, $tdate);
                        $this->prdbh->beginTransaction();
                        if ($sql1->execute()) {
                            $ResidentRWAID = $this->prdbh->lastInsertId();
                            $this->prdbh->commit();
                            if ((isset($rec->FlatNbr)) && ($rec->FlatNbr != '')) {
                                $FlatNbr = $rec->FlatNbr;
                                $sql2 = $this->prdbh->prepare("INSERT INTO `ResidentFlatMapping`(`ResidentRWAID`, `FlatNbr`, `IsOwner`, `IsActive`) VALUES (?,?,?,1)");
                                $sql2->bindParam(1, $ResidentRWAID);
                                $sql2->bindParam(2, $FlatNbr);
                                $sql2->bindParam(3, $rec->IsOwner);
                                $this->prdbh->beginTransaction();
                                if ($sql2->execute()) {
                                    $ResidentRWAFlatID = $this->prdbh->lastInsertId();
                                    $this->prdbh->commit();
                                    if ((isset($rec->VehicleNbr)) && ($rec->VehicleNbr != '')) {
                                        $VehicleNbr = $rec->VehicleNbr;
                                        $VehicleNbrArray = explode(",", $VehicleNbr);
                                        for ($i = 0;$i < count($VehicleNbrArray);$i++) {
                                            $sql3 = $this->prdbh->prepare("INSERT INTO ResidentVehicleMapping(ResidentRWAFlatID, VehicleNbr,VehicleType) VALUES(?,?,?)");
                                            $sql3->bindParam(1, $ResidentRWAFlatID);
                                            $sql3->bindParam(2, $VehicleNbrArray[$i]);
                                            $sql3->bindParam(3, $rec->VehicleType);
                                            $this->prdbh->beginTransaction();
                                            if ($sql3->execute()) {
                                                $this->prdbh->commit();
                                            } else {
                                                $response['vehicleERROR'] = $sql->errorInfo();
                                            }
                                        }
                                    }
                                } else {
                                    $response['flatERROR'] = $sql->errorInfo();
                                }
                            }
                            $rwid = $rec->RWAID;
                            $sql4 = $this->prdbh->prepare("SELECT User.ID FROM User INNER JOIN ResidentRWAMapping ON User.ID=ResidentRWAMapping.ResidentID
                                                LEFT JOIN RWA ON ResidentRWAMapping.RWAID=RWA.ID LEFT JOIN UserRoles ON UserRoles.ResidentRWAId=ResidentRWAMapping.ResidentRWAID
                                                WHERE RWAID='" . $rwid . "' AND UserRoles.RoleID IN ('1','5') ");
                            $sql4->execute();
                            if ($sql4->rowCount() > 0) {
                                $UpushId1 = array();
                                while ($row4 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                                    $UpushId1[] = $row4['ID'];
                                }
                                $UpushId = implode(',', $UpushId1);
                                $message = 'One more member registered, need your approval!';
                                $UrHd1 = 'New Registration';
                                $message1 = array("message" => $message, "UrHd2" => $UrHd1, "body" => $ID);
                                $result = $this->firebasenotification($UpushId, $message1);
                                $response['PushStatus'] = $result;
                            }
                        } else {
                            $response['rwaERRROR'] = $sql1->errorInfo();
                        }
                    }
                } else {
                    $response['message'] = $sql->errorInfo();
                    $response['Status'] = "0";
                }
            }
        } else {
            $response['message'] = "All fields are mandatory";
            $response['Status'] = "0";
        }
        $sql = $this->prdbh->prepare("SELECT User.ID, User.PhoneNbr, User.EmailID, User.Password, User.FirstName, IFNULL(User.MiddleName,'') as MiddleName, User.LastName,
                                                User.ProfilePic, User.Gender, User.Occupation, User.AddedOn, User.IsActive,User.About,User.IsPhonePublic,
                                                ResidentRWAMapping.ResidentRWAID, ResidentRWAMapping.RWAID,RWA.Name,RWA.Latitude,RWA.Longitude,RWA.Locality,RWA.SocietyPrivateinfo,RWA.SocietyPublicInfo,RWA.SocietyBanner,RWA.SocietyPendingInfo,RWA.ChatUrl,ResidentRWAMapping.AddedOn,
                                                ResidentRWAMapping.ApprovalStatus, IFNULL(ResidentRWAMapping.ApprovedBy,'') as ApprovedBy, IFNULL(ResidentRWAMapping.ApprovedOn,'') as ApprovedOn
                                                FROM User INNER JOIN ResidentRWAMapping ON User.ID = ResidentRWAMapping.ResidentID
                                                LEFT JOIN RWA ON ResidentRWAMapping.RWAID=RWA.ID
                                                WHERE User.ID = '" . $id . "'");
        $sql->execute();
        if ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
            $rid = $row['ResidentRWAID'];
            $sql1 = $this->prdbh->prepare(" SELECT ID AS FlatID, FlatNbr, IFNULL(IsOwner,'') as IsOwner FROM ResidentFlatMapping WHERE ResidentRWAID='" . $rid . "'");
            $sql1->execute();
            $items = array();
            $fid1 = array();
            if ($sql1->rowCount() > 0) {
                while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row1;
                    $fid1[] = $row1['FlatID'];
                }
                $a['Flats'] = $items;
                //print_r($fid1);die;
                if (count($fid1)) {
                    $fid = implode(',', $fid1);
                    $sql2 = $this->prdbh->prepare("SELECT VehicleNbr,VehicleType FROM ResidentVehicleMapping WHERE ResidentRWAFlatID IN($fid)");
                    $sql2->execute();
                    $items1 = array();
                    if ($sql2->rowCount() > 0) {
                        while ($row2 = $sql2->fetch(PDO::FETCH_ASSOC)) {
                            $items1[] = $row2;
                        }
                        $a['Vehicles'] = $items1;
                    }
                }
            }
            $row = array_merge($row, $a);
            $response['User'] = $row;
            $response['Status'] = 1;
        }
        return $response;
    }
    public function registrationmail($email, $FirstName, $mailmessage, $mailfile) {
        $response = array();
        $umail = $email;
        $uname = $FirstName;
        $em = $rec->passcode;
        $usub = 'Welcome to mynukad';
        $umsg = $mailmessage;
        require 'sendgrid/vendor/autoload.php';
        require 'sendgrid/lib/SendGrid.php';
        $sendgrid = new SendGrid('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');
        $email = new SendGrid\Email();
        $email->addTo($umail)->setFrom('noreply@mynukad.com')->setSubject($usub)->setText($umsg)
        //->setHtml($dt)
        ->setAttachment(realpath(dirname(__FILE__)) . DIRECTORY_SEPARATOR . $mailfile);
        try {
            $sendgrid->send($email);
            $response['message'] = "Sent mail successfully";
            $response['status'] = 1;
        }
        catch(\SendGrid\Exception $e) {
            echo $e->getCode();
            $response['status'] = 0;
            $response['message'] = "Something went wrong.Mail not sent";
            foreach ($e->getErrors() as $er) {
                echo $er;
            }
        }
        return $response;
    }
    public function phoneverification($param) {
        if (isset($param->PhoneNbr)) {
            $PhoneNbr = $param->PhoneNbr;
        }
        if (isset($param->Process)) {
            $Process = $param->Process;
        }
        $response = array();
        if ($PhoneNbr != '' && $Process == 'Signup') {
            $sql = $this->prdbh->prepare("SELECT * FROM User WHERE PhoneNbr = '" . $PhoneNbr . "'");
            $sql->execute();
            if ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                $response['message'] = "Phone number already exists, may be you have been registered by your RWA/Maintainence dept from back-end ,so please use Forgot password ,change it and then login with new one";
                $response['Status'] = "0";
            } else {
                $otp = mt_rand(1000, 9999);
                $message1 = "Use this One Time Password to validate your registration:" . $otp;
                $message = preg_replace('/\s+/', '+', $message1);
                $link = "http://textsms.co.in/app/smsapi/index.php?key=3588619015E605&routeid=6&type=text&contacts=$PhoneNbr&senderid=ALERTS&msg=$message";
                $homepage = file_get_contents($link);
                $response['message'] = $homepage;
                $response['Status'] = "1";
                $response['OTP'] = $otp;
            }
        } elseif ($PhoneNbr != '' && $Process == 'Forgot') {
            $sql = $this->prdbh->prepare("SELECT * FROM User WHERE PhoneNbr = '" . $PhoneNbr . "'");
            $sql->execute();
            if ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                $otp = mt_rand(1000, 9999);
                $message1 = "OTP for forgot password is :" . $otp;
                $message = preg_replace('/\s+/', '+', $message1);
                $link = "http://textsms.co.in/app/smsapi/index.php?key=3588619015E605&routeid=6&type=text&contacts=$PhoneNbr&senderid=ALERTS&msg=$message";
                $homepage = file_get_contents($link);
                $response['message'] = $homepage;
                $response['Status'] = "1";
                $response['OTP'] = $otp;
            } else {
                $response['message'] = "Number does not exit, please Sign-up";
                $response['Status'] = "0";
            }
        } else {
            $response['message'] = "All fields are mandatory";
            $response['Status'] = "0";
        }
        return $response;
    }
    public function registrationnewrollback($rec) {
        $tdat = date("U");
        $tdate = $tdat;
        $PhoneNbr = $rec->PhoneNbr;
        $Passw = trim($rec->Password);
        $Password = md5($Passw);
        $FirstName = $rec->FirstName;
        $rwid = $rec->RWAID;
        $email = $rec->EmailID;
        $FlatNbr = $rec->FlatNbr;
        $Gender = $rec->Gender;
        $sql5 = $this->prdbh->prepare("SELECT  Message, FilePath FROM RegistrationWelcome Where RWAID='" . $rwid . "' ");
        $sql5->execute();
        if ($sql5->rowCount() > 0) {
            while ($row5 = $sql5->fetch(PDO::FETCH_ASSOC)) {
                $mailmessage = $row5['Message'];
                $mailfile = $row5['FilePath'];
            }
        } else {
            $sql6 = $this->prdbh->prepare("SELECT  Message, FilePath FROM RegistrationWelcome Where RWAID is NULL ");
            $sql6->execute();
            if ($sql6->rowCount() > 0) {
                while ($row6 = $sql6->fetch(PDO::FETCH_ASSOC)) {
                    $mailmessage = $row6['Message'];
                    $mailfile = $row6['FilePath'];
                }
            }
        }
        if ($PhoneNbr != '' && $Password != '' && $FirstName != '' && $Gender != '') {
            /* try {
             $this->prdbh->beginTransaction(); */
            $sql = $this->prdbh->prepare("SELECT * FROM User WHERE PhoneNbr = '" . $PhoneNbr . "'");
            $sql->execute();
            if ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                $response['message'] = "Phone number already exist, may be you have been registered from backend by your RWA/Maintainence dept so please use forgot password  change it and then login with new one";
                $response['Status'] = "0";
            } else {
                $sql6 = $this->prdbh->prepare("SELECT ConfigParamValue FROM RWAConfigTable WHERE 
                RWAID = '" . $rwid . "',ConfigParamID='21' AND ConfigParamValue='1' ");
                $sql6->execute();
                if ($row6 = $sql6->fetch(PDO::FETCH_ASSOC)) {
                    $sql = $this->prdbh->prepare("INSERT INTO `User`(`PhoneNbr`, `EmailID`, `Password`, `FirstName`, `ProfilePic`, `Gender`, `About`, `Occupation`, `IsPhonePublic`, `AddedOn`, `IsActive`) VALUES (?,?,?,?,?,?,?,?,1,?,1)");
                } else {
                    $sql = $this->prdbh->prepare("INSERT INTO `User`(`PhoneNbr`, `EmailID`, `Password`, `FirstName`, `ProfilePic`, `Gender`, `About`, `Occupation`, `IsPhonePublic`, `AddedOn`, `IsActive`) VALUES (?,?,?,?,?,?,?,?,0,?,1)");
                }
                $sql->bindParam(1, $rec->PhoneNbr);
                $sql->bindParam(2, $rec->EmailID);
                $sql->bindParam(3, $Password);
                $sql->bindParam(4, $rec->FirstName);
                $sql->bindParam(5, $rec->ProfilePic);
                $sql->bindParam(6, $rec->Gender);
                $sql->bindParam(7, $rec->About);
                $sql->bindParam(8, $rec->Occupation);
                $sql->bindParam(9, $tdate);
                $this->prdbh->beginTransaction();
                if ($sql->execute()) {
                    $id = $this->prdbh->lastInsertId();
                    $this->prdbh->commit();
                    //$response['userid'] = $id;
                    $response['Status'] = "1";
                    $result = $this->registrationmail($email, $FirstName, $mailmessage, $mailfile);
                    if ((isset($rec->RWAID)) && ($rec->RWAID != '')) {
                        $rwid = $rec->RWAID;
                        $sql1 = $this->prdbh->prepare("INSERT INTO `ResidentRWAMapping`(`ResidentID`, `RWAID`, `AddedOn`, `ApprovalStatus`, `IsActive`) VALUES (?,?,?,'P','1')");
                        $sql1->bindParam(1, $id);
                        $sql1->bindParam(2, $rwid);
                        $sql1->bindParam(3, $tdate);
                        $this->prdbh->beginTransaction();
                        if ($sql1->execute()) {
                            $ResidentRWAID = $this->prdbh->lastInsertId();
                            $this->prdbh->commit();
                            if ((isset($rec->FlatNbr)) && ($rec->FlatNbr != '')) {
                                $FlatNbr = $rec->FlatNbr;
                                $sql2 = $this->prdbh->prepare("INSERT INTO `ResidentFlatMapping`(`ResidentRWAID`, `FlatID`, `IsOwner`, `IsActive`) VALUES (?,?,?,1)");
                                //chnage table name flatenber to flatid
                                $sql2->bindParam(1, $ResidentRWAID);
                                $sql2->bindParam(2, $FlatNbr);
                                $sql2->bindParam(3, $rec->IsOwner);
                                $this->prdbh->beginTransaction();
                                if ($sql2->execute()) {
                                    $ResidentRWAFlatID = $this->prdbh->lastInsertId();
                                    $this->prdbh->commit();
                                    $rwid = $rec->RWAID;
                                    $sql4 = $this->prdbh->prepare("SELECT User.ID FROM User INNER JOIN ResidentRWAMapping ON User.ID=ResidentRWAMapping.ResidentID
                                                            LEFT JOIN RWA ON ResidentRWAMapping.RWAID=RWA.ID LEFT JOIN UserRoles ON UserRoles.ResidentRWAId=ResidentRWAMapping.ResidentRWAID
                                                            WHERE RWAID='" . $rwid . "' AND UserRoles.RoleID IN ('1','5') ");
                                    $sql4->execute();
                                    if ($sql4->rowCount() > 0) {
                                        $UpushId1 = array();
                                        while ($row4 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                                            $UpushId1[] = $row4['ID'];
                                        }
                                        $UpushId = implode(',', $UpushId1);
                                        $message = 'One more member registered, need your approval!';
                                        $UrHd1 = 'New Registration';
                                        $message1 = array("message" => $message, "UrHd2" => $UrHd1, "body" => $id);
                                        $result = $this->firebasenotification($UpushId, $message1);
                                        //print_r($result);die;
                                        $response['PushStatus'] = $result;
                                    }
                                } else {
                                    $response['flatERROR'] = $sql->errorInfo();
                                }
                            }
                        } else {
                            $response['rwaERRROR'] = $sql1->errorInfo();
                        }
                    }
                } else {
                    $response['message'] = $sql->errorInfo();
                    $response['Status'] = "0";
                }
            }
            /* }
            catch(PDOException $e) {
            $this->prdbh->rollBack();
            die($e->getMessage());
            } */
        } else {
            $response['message'] = "All fields are mandatory";
            $response['Status'] = "0";
        }
        if ($id != '' && $ResidentRWAID != '' && $ResidentRWAFlatID != '') {
            $response['userid'] = $id;
            $sql = $this->prdbh->prepare("SELECT User.ID, User.PhoneNbr, User.EmailID, User.Password, User.FirstName, IFNULL(User.MiddleName,'') as MiddleName, User.LastName,
                                                User.ProfilePic, User.Gender, User.Occupation, User.AddedOn, User.IsActive,User.About,User.IsPhonePublic,
                                                ResidentRWAMapping.ResidentRWAID, ResidentRWAMapping.RWAID,RWA.Name,RWA.Latitude,RWA.Longitude,RWA.Locality,RWA.SocietyPrivateinfo,RWA.SocietyPublicInfo,RWA.SocietyBanner,RWA.SocietyPendingInfo,RWA.ChatUrl,ResidentRWAMapping.AddedOn,
                                                ResidentRWAMapping.ApprovalStatus, IFNULL(ResidentRWAMapping.ApprovedBy,'') as ApprovedBy, IFNULL(ResidentRWAMapping.ApprovedOn,'') as ApprovedOn
                                                FROM User INNER JOIN ResidentRWAMapping ON User.ID = ResidentRWAMapping.ResidentID
                                                LEFT JOIN RWA ON ResidentRWAMapping.RWAID=RWA.ID
                                                WHERE User.ID = '" . $id . "'");
            $sql->execute();
            if ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                $rid = $row['ResidentRWAID'];
                $rwaName = $row['Name'];
                //$sql1=$this->prdbh->prepare(" SELECT ID AS FlatID, FlatNbr, IFNULL(IsOwner,'') as IsOwner FROM ResidentFlatMapping WHERE ResidentRWAID='".$rid."'");
                $sql1 = $this->prdbh->prepare(" SELECT (ResidentFlatMapping.ID) as FlatID,SocietyFlatMapping.FlatNbr,IFNULL(ResidentFlatMapping.IsOwner,'') as IsOwner FROM ResidentFlatMapping INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID  WHERE ResidentRWAID ='" . $rid . "'");
                $sql1->execute();
                $items = array();
                $fid1 = array();
                if ($sql1->rowCount() > 0) {
                    while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                        $items[] = $row1;
                        $fid1[] = $row1['FlatID'];
                    }
                    $a['Flats'] = $items;
                    if (count($fid1)) {
                        $fid = implode(',', $fid1);
                        $sql2 = $this->prdbh->prepare("SELECT VehicleNbr,VehicleType FROM ResidentVehicleMapping WHERE ResidentRWAFlatID IN($fid)");
                        $sql2->execute();
                        $items1 = array();
                        if ($sql2->rowCount() > 0) {
                            while ($row2 = $sql2->fetch(PDO::FETCH_ASSOC)) {
                                $items1[] = $row2;
                            }
                            $a['Vehicles'] = $items1;
                        }
                    }
                }
                $row = array_merge($row, $a);
                //print_r($row);die;
                $response['User'] = $row;
                $response['Status'] = 1;
                $response['message'] = "You are registered with " . $rwaName . ". Now RWA/Maintainence dept. will verify your data and on approval, you can browse other features of app";
            }
        } else {
            $sql11 = $this->prdbh->prepare("DELETE FROM User WHERE ID = '" . $id . "' ");
            $sql11->execute();
            $response['Status'] = "0";
        }
        return $response;
    }
    public function registrationnew($rec) {
        $tdat = date("U");
        $tdate = $tdat;
        $PhoneNbr = $rec->PhoneNbr;
        $Passw = trim($rec->Password);
        $Password = md5($Passw);
        $FirstName = $rec->FirstName;
        $rwid = $rec->RWAID;
        $email = $rec->EmailID;
        $FlatNbr = $rec->FlatNbr;
        $Gender = $rec->Gender;
        $sql5 = $this->prdbh->prepare("SELECT  Message, FilePath FROM RegistrationWelcome Where RWAID='" . $rwid . "' ");
        $sql5->execute();
        if ($sql5->rowCount() > 0) {
            while ($row5 = $sql5->fetch(PDO::FETCH_ASSOC)) {
                $mailmessage = $row5['Message'];
                $mailfile = $row5['FilePath'];
            }
        } else {
            $sql6 = $this->prdbh->prepare("SELECT  Message, FilePath FROM RegistrationWelcome Where RWAID is NULL ");
            $sql6->execute();
            if ($sql6->rowCount() > 0) {
                while ($row6 = $sql6->fetch(PDO::FETCH_ASSOC)) {
                    $mailmessage = $row6['Message'];
                    $mailfile = $row6['FilePath'];
                }
            }
        }
        if ($PhoneNbr != '' && $Password != '' && $FirstName != '' && $Gender != '' && $FlatNbr != '') {
            $sql = $this->prdbh->prepare("SELECT * FROM User WHERE PhoneNbr = '" . $PhoneNbr . "'");
            $sql->execute();
            if ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                $response['message'] = "Phone number already exist, may be you have been registered from backend by your RWA/Maintainence dept so please use forgot password  change it and then login with new one";
                $response['Status'] = "0";
            } else {
                $sql6 = $this->prdbh->prepare("SELECT ConfigParamValue FROM RWAConfigTable WHERE 
                RWAID = '" . $rwid . "',ConfigParamID='21' AND ConfigParamValue='1' ");
                $sql6->execute();
                if ($row6 = $sql6->fetch(PDO::FETCH_ASSOC)) {
                    $sql = $this->prdbh->prepare("INSERT INTO `User`(`PhoneNbr`, `EmailID`, `Password`, `FirstName`, `ProfilePic`, `Gender`, `About`, `Occupation`, `IsPhonePublic`, `AddedOn`, `IsActive`) VALUES (?,?,?,?,?,?,?,?,1,?,1)");
                } else {
                    $sql = $this->prdbh->prepare("INSERT INTO `User`(`PhoneNbr`, `EmailID`, `Password`, `FirstName`, `ProfilePic`, `Gender`, `About`, `Occupation`, `IsPhonePublic`, `AddedOn`, `IsActive`) VALUES (?,?,?,?,?,?,?,?,0,?,1)");
                }
                $sql->bindParam(1, $rec->PhoneNbr);
                $sql->bindParam(2, $rec->EmailID);
                $sql->bindParam(3, $Password);
                $sql->bindParam(4, $rec->FirstName);
                $sql->bindParam(5, $rec->ProfilePic);
                $sql->bindParam(6, $rec->Gender);
                $sql->bindParam(7, $rec->About);
                $sql->bindParam(8, $rec->Occupation);
                $sql->bindParam(9, $tdate);
                $this->prdbh->beginTransaction();
                if ($sql->execute()) {
                    $id = $this->prdbh->lastInsertId();
                    $this->prdbh->commit();
                    $response['userid'] = $id;
                    $response['Status'] = "1";
                    $result = $this->registrationmail($email, $FirstName, $mailmessage, $mailfile);
                    if ((isset($rec->RWAID)) && ($rec->RWAID != '')) {
                        $rwid = $rec->RWAID;
                        $sql1 = $this->prdbh->prepare("INSERT INTO `ResidentRWAMapping`(`ResidentID`, `RWAID`, `AddedOn`, `ApprovalStatus`, `IsActive`) VALUES (?,?,?,'P','1')");
                        $sql1->bindParam(1, $id);
                        $sql1->bindParam(2, $rwid);
                        $sql1->bindParam(3, $tdate);
                        $this->prdbh->beginTransaction();
                        if ($sql1->execute()) {
                            $ResidentRWAID = $this->prdbh->lastInsertId();
                            $this->prdbh->commit();
                            if ((isset($rec->FlatNbr)) && ($rec->FlatNbr != '')) {
                                $FlatNbr = $rec->FlatNbr;
                                $sql2 = $this->prdbh->prepare("INSERT INTO `ResidentFlatMapping`(`ResidentRWAID`, `FlatID`, `IsOwner`, `IsActive`) VALUES (?,?,?,1)");
                                //chnage table name flatenber to flatid
                                $sql2->bindParam(1, $ResidentRWAID);
                                $sql2->bindParam(2, $FlatNbr);
                                $sql2->bindParam(3, $rec->IsOwner);
                                $this->prdbh->beginTransaction();
                                if ($sql2->execute()) {
                                    $ResidentRWAFlatID = $this->prdbh->lastInsertId();
                                    $this->prdbh->commit();
                                    if ((isset($rec->VehicleNbr)) && ($rec->VehicleNbr != '')) {
                                        $VehicleNbr = $rec->VehicleNbr;
                                        $VehicleNbrArray = explode(",", $VehicleNbr);
                                        for ($i = 0;$i < count($VehicleNbrArray);$i++) {
                                            $sql3 = $this->prdbh->prepare("INSERT INTO ResidentVehicleMapping(ResidentRWAFlatID, VehicleNbr,VehicleType) VALUES(?,?,?)");
                                            $sql3->bindParam(1, $ResidentRWAFlatID);
                                            $sql3->bindParam(2, $VehicleNbrArray[$i]);
                                            $sql3->bindParam(3, $rec->VehicleType);
                                            $this->prdbh->beginTransaction();
                                            if ($sql3->execute()) {
                                                $this->prdbh->commit();
                                            } else {
                                                $response['vehicleERROR'] = $sql->errorInfo();
                                            }
                                        }
                                    }
                                } else {
                                    $response['flatERROR'] = $sql->errorInfo();
                                }
                            }
                            $rwid = $rec->RWAID;
                            $sql4 = $this->prdbh->prepare("SELECT User.ID FROM User INNER JOIN ResidentRWAMapping ON User.ID=ResidentRWAMapping.ResidentID
                                                LEFT JOIN RWA ON ResidentRWAMapping.RWAID=RWA.ID LEFT JOIN UserRoles ON UserRoles.ResidentRWAId=ResidentRWAMapping.ResidentRWAID
                                                WHERE RWAID='" . $rwid . "' AND UserRoles.RoleID IN ('1','5') ");
                            $sql4->execute();
                            if ($sql4->rowCount() > 0) {
                                $UpushId1 = array();
                                while ($row4 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                                    $UpushId1[] = $row4['ID'];
                                }
                                $UpushId = implode(',', $UpushId1);
                                $message = 'One more member registered, need your approval!';
                                $UrHd1 = 'New Registration';
                                $message1 = array("message" => $message, "UrHd2" => $UrHd1, "body" => $id);
                                $result = $this->firebasenotification($UpushId, $message1);
                                //print_r($result);die;
                                $response['PushStatus'] = $result;
                            }
                        } else {
                            $response['rwaERRROR'] = $sql1->errorInfo();
                        }
                    }
                } else {
                    $response['message'] = $sql->errorInfo();
                    $response['Status'] = "0";
                }
            }
        } else {
            $response['message'] = "All fields are mandatory";
            $response['Status'] = "0";
        }
        $sql = $this->prdbh->prepare("SELECT User.ID, User.PhoneNbr, User.EmailID, User.Password, User.FirstName, IFNULL(User.MiddleName,'') as MiddleName, User.LastName,
                                                User.ProfilePic, User.Gender, User.Occupation, User.AddedOn, User.IsActive,User.About,User.IsPhonePublic,
                                                ResidentRWAMapping.ResidentRWAID, ResidentRWAMapping.RWAID,RWA.Name,RWA.Latitude,RWA.Longitude,RWA.Locality,RWA.SocietyPrivateinfo,RWA.SocietyPublicInfo,RWA.SocietyBanner,RWA.SocietyPendingInfo,RWA.ChatUrl,ResidentRWAMapping.AddedOn,
                                                ResidentRWAMapping.ApprovalStatus, IFNULL(ResidentRWAMapping.ApprovedBy,'') as ApprovedBy, IFNULL(ResidentRWAMapping.ApprovedOn,'') as ApprovedOn
                                                FROM User INNER JOIN ResidentRWAMapping ON User.ID = ResidentRWAMapping.ResidentID
                                                LEFT JOIN RWA ON ResidentRWAMapping.RWAID=RWA.ID
                                                WHERE User.ID = '" . $id . "'");
        $sql->execute();
        if ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
            $rid = $row['ResidentRWAID'];
            $rwaName = $row['Name'];
            //$sql1=$this->prdbh->prepare(" SELECT ID AS FlatID, FlatNbr, IFNULL(IsOwner,'') as IsOwner FROM ResidentFlatMapping WHERE ResidentRWAID='".$rid."'");
            $sql1 = $this->prdbh->prepare(" SELECT (ResidentFlatMapping.ID) as FlatID,SocietyFlatMapping.FlatNbr,IFNULL(ResidentFlatMapping.IsOwner,'') as IsOwner FROM ResidentFlatMapping INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID  WHERE ResidentRWAID ='" . $rid . "'");
            $sql1->execute();
            $items = array();
            $fid1 = array();
            if ($sql1->rowCount() > 0) {
                while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row1;
                    $fid1[] = $row1['FlatID'];
                }
                $a['Flats'] = $items;
                if (count($fid1)) {
                    $fid = implode(',', $fid1);
                    $sql2 = $this->prdbh->prepare("SELECT VehicleNbr,VehicleType FROM ResidentVehicleMapping WHERE ResidentRWAFlatID IN($fid)");
                    $sql2->execute();
                    $items1 = array();
                    if ($sql2->rowCount() > 0) {
                        while ($row2 = $sql2->fetch(PDO::FETCH_ASSOC)) {
                            $items1[] = $row2;
                        }
                        $a['Vehicles'] = $items1;
                    }
                }
            }
            $row = array_merge($row, $a);
            //print_r($row);die;
            $response['User'] = $row;
            $response['Status'] = 1;
            $response['message'] = "You are registered with " . $rwaName . ". Now RWA/Maintainence dept. will verify your data and on approval, you can browse other features of app";
        }
        return $response;
    }
    public function editmaintenancestaff($rec) {
        $StaffQRCode = $rec->StaffQRCode;
        $isQrCodeModified = $rec->isQrCodeModified;
        $qr = substr($StaffQRCode, 2);
        if ($isQrCodeModified == 0) {
            $sql = $this->prdbh->prepare("UPDATE MaintenanceStaff SET Name=?, ProfilePic=?, ServiceCategory=?, PrimaryContactNbr=?,StaffDesignation=?,EmployeeCode=?,OtherDocScanImage1=?,OtherDocScanImage2=?,OtherDocScanImage3=?,StaffAddress=? WHERE ID =?");
            $sql->bindParam(1, $rec->Name);
            $sql->bindParam(2, $rec->ProfilePic);
            $sql->bindParam(3, $rec->ServiceCategory);
            $sql->bindParam(4, $rec->PrimaryContactNbr);
            $sql->bindParam(5, $rec->StaffDesignation);
            $sql->bindParam(6, $rec->EmployeeCode);
            $sql->bindParam(7, $rec->OtherDocScanImage1);
            $sql->bindParam(8, $rec->OtherDocScanImage2);
            $sql->bindParam(9, $rec->OtherDocScanImage3);
            $sql->bindParam(10, $rec->StaffAddress);
            $sql->bindParam(11, $rec->ID);
            $this->prdbh->beginTransaction();
            $sql->execute();
            $count = $sql->rowCount();
            if ($count > 0) {
                $this->prdbh->commit();
                $response['Message'] = "Staff edited successfully";
                $response['Status'] = "1";
            } else {
                $response['Message'] = "Staff edited with same info";
                $response['Status'] = "0";
            }
        } elseif ($isQrCodeModified == 1) {
            $sqlqr = $this->prdbh->prepare("select StaffQRCode from  MaintenanceStaff where StaffQRCode='" . $qr . "'");
            $sqlqr->execute();
            if ($sqlqr->rowCount() != '0' && $qr != '') {
                $response['Message'] = "QR Code already exists";
                $response['Status'] = 0;
            } else {
                $sql = $this->prdbh->prepare("UPDATE MaintenanceStaff SET Name=?, ProfilePic=?, ServiceCategory=?, PrimaryContactNbr=?,StaffDesignation=?,EmployeeCode=?,StaffQRCode=?,OtherDocScanImage1=?,OtherDocScanImage2=?,OtherDocScanImage3=?,StaffAddress=? WHERE ID =?");
                $sql->bindParam(1, $rec->Name);
                $sql->bindParam(2, $rec->ProfilePic);
                $sql->bindParam(3, $rec->ServiceCategory);
                $sql->bindParam(4, $rec->PrimaryContactNbr);
                $sql->bindParam(5, $rec->StaffDesignation);
                $sql->bindParam(6, $rec->EmployeeCode);
                $sql->bindParam(7, $qr);
                $sql->bindParam(8, $rec->OtherDocScanImage1);
                $sql->bindParam(9, $rec->OtherDocScanImage2);
                $sql->bindParam(10, $rec->OtherDocScanImage3);
                $sql->bindParam(11, $rec->StaffAddress);
                $sql->bindParam(12, $rec->ID);
                $this->prdbh->beginTransaction();
                $sql->execute();
                $count = $sql->rowCount();
                if ($count > 0) {
                    $this->prdbh->commit();
                    $response['Message'] = "Staff edited successfully";
                    $response['Status'] = "1";
                } else {
                    $response['Message'] = "Staff edited with same info";
                    $response['Status'] = "0";
                }
            }
        }
        return $response;
    }
    public function edithelper($rec) {
        $qr = $rec->HelperQRCode;
        $isQrCodeModified = $rec->isQrCodeModified;
        if ($isQrCodeModified == 0) {
            $sql = $this->prdbh->prepare("UPDATE Helpers SET Name=?,ProfilePhoto=?,ServiceOffered=?,ResidentialAddress=?,PrimaryContactNbr=?,ServiceCharges=?,OtherDocScanImage1=?,OtherDocScanImage2=?,OtherDocScanImage3=?,PhoneNbr2=? WHERE ID =?");
            $sql->bindParam(1, $rec->Name);
            $sql->bindParam(2, $rec->ProfilePhoto);
            $sql->bindParam(3, $rec->ServiceOffered);
            $sql->bindParam(4, $rec->ResidentialAddress);
            $sql->bindParam(5, $rec->PrimaryContactNbr);
            $sql->bindParam(6, $rec->ServiceCharges);
            $sql->bindParam(7, $rec->OtherDocScanImage1);
            $sql->bindParam(8, $rec->OtherDocScanImage2);
            $sql->bindParam(9, $rec->OtherDocScanImage3);
            $sql->bindParam(10, $rec->PhoneNbr2);
            $sql->bindParam(11, $rec->ID);
            $this->prdbh->beginTransaction();
            $sql->execute();
            $count = $sql->rowCount();
            if ($count > 0) {
                $this->prdbh->commit();
                $response['Message'] = "Helper edited successfully";
                $response['Status'] = "1";
            } else {
                $response['Message'] = "Helper edited with same info";
                $response['Status'] = "0";
            }
        } elseif ($isQrCodeModified == 1) {
            $sqlqr = $this->prdbh->prepare("select HelperQRCode from  Helpers where HelperQRCode='" . $qr . "'");
            $sqlqr->execute();
            if ($sqlqr->rowCount() != '0' && $qr != '') {
                $response['Message'] = "QR Code already exists";
                $response['Status'] = 0;
            } else {
                $sql = $this->prdbh->prepare("UPDATE Helpers SET Name=?,ProfilePhoto=?,ServiceOffered=?,ResidentialAddress=?,PrimaryContactNbr=?,ServiceCharges=?,OtherDocScanImage1=?,OtherDocScanImage2=?,OtherDocScanImage3=?,PhoneNbr2=?,HelperQRCode=? WHERE ID =?");
                $sql->bindParam(1, $rec->Name);
                $sql->bindParam(2, $rec->ProfilePhoto);
                $sql->bindParam(3, $rec->ServiceOffered);
                $sql->bindParam(4, $rec->ResidentialAddress);
                $sql->bindParam(5, $rec->PrimaryContactNbr);
                $sql->bindParam(6, $rec->ServiceCharges);
                $sql->bindParam(7, $rec->OtherDocScanImage1);
                $sql->bindParam(8, $rec->OtherDocScanImage2);
                $sql->bindParam(9, $rec->OtherDocScanImage3);
                $sql->bindParam(10, $rec->PhoneNbr2);
                $sql->bindParam(11, $rec->HelperQRCode);
                $sql->bindParam(12, $rec->ID);
                $this->prdbh->beginTransaction();
                $sql->execute();
                $count = $sql->rowCount();
                if ($count > 0) {
                    $this->prdbh->commit();
                    $response['Message'] = "Helper edited successfully";
                    $response['Status'] = "1";
                } else {
                    $response['Message'] = "Helper edited with same info";
                    $response['Status'] = "0";
                }
            }
        }
        return $response;
    }
    public function updateuserinfo($rec) {
        $id = $rec->ID;
        $Phone = $rec->Phone;
        if (isset($rec->PhoneNbr)) {
            $sql = $this->prdbh->prepare("UPDATE User SET PhoneNbr=? WHERE ID = ?");
            $sql->bindParam(1, $rec->PhoneNbr);
            $sql->bindParam(2, $rec->ID);
        } else if (isset($rec->EmailID)) {
            $sql = $this->prdbh->prepare("UPDATE User SET EmailID=? WHERE ID = ?");
            $sql->bindParam(1, $rec->EmailID);
            $sql->bindParam(2, $rec->ID);
        } else if (isset($rec->Password)) {
            $Password = md5($rec->Password);
            $sql = $this->prdbh->prepare("UPDATE User SET Password=? WHERE PhoneNbr= ?");
            $sql->bindParam(1, $Password);
            $sql->bindParam(2, $Phone);
        } else if (isset($rec->OldPass) && ($rec->NewPass)) {
            $Password = md5($rec->OldPass);
            $newPassword = md5($rec->NewPass);
            $sql = $this->prdbh->prepare("UPDATE User SET Password=? WHERE ID = ? AND Password=?");
            $sql->bindParam(1, $newPassword);
            $sql->bindParam(2, $rec->ID);
            $sql->bindParam(3, $Password);
        } else if (isset($rec->FirstName)) {
            $sql = $this->prdbh->prepare("UPDATE User SET FirstName=? WHERE ID = ?");
            $sql->bindParam(1, $rec->FirstName);
            $sql->bindParam(2, $rec->ID);
        } else if (isset($rec->ProfilePic)) {
            $sql = $this->prdbh->prepare("UPDATE User SET ProfilePic=? WHERE ID = ?");
            $sql->bindParam(1, $rec->ProfilePic);
            $sql->bindParam(2, $rec->ID);
        } else if (isset($rec->Occupation)) {
            $sql = $this->prdbh->prepare("UPDATE User SET  Occupation=? WHERE ID = ?");
            $sql->bindParam(1, $rec->Occupation);
            $sql->bindParam(2, $rec->ID);
        } else if (isset($rec->About)) {
            $sql = $this->prdbh->prepare("UPDATE User SET  About=? WHERE ID = ?");
            $sql->bindParam(1, $rec->About);
            $sql->bindParam(2, $rec->ID);
        } else if (isset($rec->IsPhonePublic)) {
            $sql = $this->prdbh->prepare("UPDATE User SET  IsPhonePublic=? WHERE ID = ?");
            $sql->bindParam(1, $rec->IsPhonePublic);
            $sql->bindParam(2, $rec->ID);
        }
        $this->prdbh->beginTransaction();
        $sql->execute();
        $count = $sql->rowCount();
        if ($count > 0) {
            $this->prdbh->commit();
            $response['Message'] = "User info updated";
            $response['Status'] = "1";
        } else {
            $response['Message'] = "Nothing to update"; // User info not updated neeraj
            $response['Status'] = "0";
        }
        $sql = $this->prdbh->prepare("SELECT User.ID, User.PhoneNbr, User.EmailID, User.Password, User.FirstName, IFNULL(User.MiddleName,'') as MiddleName, User.LastName,
                                                User.ProfilePic, User.Gender, User.Occupation, User.AddedOn, User.IsActive,User.About,User.IsPhonePublic,
                                                ResidentRWAMapping.ResidentRWAID, ResidentRWAMapping.RWAID,RWA.Name,ResidentRWAMapping.AddedOn,
                                                ResidentRWAMapping.ApprovalStatus, ResidentRWAMapping.ApprovedBy, ResidentRWAMapping.ApprovedOn
                                                FROM User INNER JOIN ResidentRWAMapping ON User.ID = ResidentRWAMapping.ResidentID
                                                LEFT JOIN RWA ON ResidentRWAMapping.RWAID=RWA.ID
                                                 WHERE User.ID = '" . $id . "'");
        $sql->execute();
        if ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
            $rid = $row['ResidentRWAID'];
            $sql1 = $this->prdbh->prepare("SELECT (ResidentFlatMapping.ID) as FlatID,SocietyFlatMapping.FlatNbr,IFNULL(ResidentFlatMapping.IsOwner,'') as IsOwner FROM ResidentFlatMapping INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID  WHERE ResidentRWAID ='" . $rid . "'");
            $sql1->execute();
            $items = array();
            $fid1 = array();
            if ($sql1->rowCount() > 0) {
                while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                    $items[] = $row1;
                    $fid1[] = $row1['FlatID'];
                }
                $a['Flats'] = $items;
                if (count($fid1)) {
                    $fid = implode(',', $fid1);
                    $sql2 = $this->prdbh->prepare("SELECT ID, VehicleNbr,IFNULL(VehicleType,'') as VehicleType FROM ResidentVehicleMapping WHERE ResidentRWAFlatID IN($fid)");
                    $sql2->execute();
                    $items1 = array();
                    if ($sql2->rowCount() > 0) {
                        while ($row2 = $sql2->fetch(PDO::FETCH_ASSOC)) {
                            $items1[] = $row2;
                        }
                        $a['Vehicles'] = $items1;
                    }
                }
            }
            $row = array_merge($row, $a);
            $response['User'] = $row;
        }
        return $response;
    }
    public function login($param) {
        if (isset($param->PhoneNbr)) {
            $PhoneNbr = $param->PhoneNbr;
        }
        if (isset($param->Password)) {
            $Pass = trim($param->Password);
            $Password = md5($Pass);
        }
        $response = array();
        $sql = $this->prdbh->prepare("SELECT User.ID, User.PhoneNbr, User.EmailID, User.Password, User.FirstName, IFNULL(User.MiddleName,'') as MiddleName, User.LastName,
                                                User.ProfilePic, User.Gender, User.Occupation, User.AddedOn, User.IsActive,User.About,User.IsPhonePublic,
                                                ResidentRWAMapping.ResidentRWAID, ResidentRWAMapping.RWAID,RWA.Name,RWA.Latitude,RWA.Longitude,RWA.SocietyPrivateinfo,RWA.SocietyPublicInfo,RWA.SocietyBanner,RWA.SocietyPendingInfo,RWA.ChatUrl,ResidentRWAMapping.AddedOn,
                                                ResidentRWAMapping.ApprovalStatus, ResidentRWAMapping.ApprovedBy, ResidentRWAMapping.ApprovedOn
                                                FROM User INNER JOIN ResidentRWAMapping ON User.ID = ResidentRWAMapping.ResidentID
                                                LEFT JOIN RWA ON ResidentRWAMapping.RWAID=RWA.ID
                                                WHERE User.PhoneNbr ='" . $PhoneNbr . "' AND User.Password='" . $Password . "' AND RWA.IsActive=1  ");
        $sql->execute();
        if ($sql) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $rid = $row['ResidentRWAID'];
                    $sql1 = $this->prdbh->prepare(" SELECT (ResidentFlatMapping.ID) as FlatID,(SocietyFlatMapping.ID) as Flatorignalid,SocietyFlatMapping.FlatNbr,IFNULL(ResidentFlatMapping.IsOwner,'') as IsOwner FROM ResidentFlatMapping INNER JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= ResidentFlatMapping.FlatID  WHERE ResidentRWAID ='" . $rid . "'");
                    $sql1->execute();
                    $items = array();
                    $fid1 = array();
                    if ($sql1->rowCount() > 0) {
                        while ($row1 = $sql1->fetch(PDO::FETCH_ASSOC)) {
                            $items[] = $row1;
                            $fid1[] = $row1['Flatorignalid'];
                        }
                        $a['Flats'] = $items;
                        if (count($fid1)) {
                            $fid = implode(',', $fid1);
                            $sql2 = $this->prdbh->prepare("SELECT ID,ParkingNo FROM ParkingTable  WHERE Flat_ID IN ('" . $fid . "')");
                            $sql2->execute();
                            $items1 = array();
                            $parkingid = array();
                            if ($sql2->rowCount() > 0) {
                                while ($row2 = $sql2->fetch(PDO::FETCH_ASSOC)) {
                                    $items1[] = $row2;
                                    $parkingid[] = $row2['ID'];
                                }
                                $a['Parking No'] = $items1;
                                if (count($parkingid)) {
                                    $parkid = implode(',', $parkingid);
                                    $sql3 = $this->prdbh->prepare("SELECT ID, VehicleNbr,IFNULL(VehicleType,'') as VehicleType FROM ResidentVehicleByParking WHERE ParkingID IN ('" . $parkid . "') ");
                                    //print_r($sql3);die;
                                    $sql3->execute();
                                    $items2 = array();
                                    if ($sql3->rowCount() > 0) {
                                        while ($row3 = $sql3->fetch(PDO::FETCH_ASSOC)) {
                                            $items2[] = $row3;
                                        }
                                        $a['Vehicles'] = $items2;
                                    }
                                }
                            }
                        }
                    }
                    $row = array_merge($row, $a);
                    $response['User'] = $row;
                }
                $response['message'] = "Login successfully";
                $response['Status'] = '1';
            } else {
                $response['message'] = "Invalid username or password";
                $response['Status'] = '0';
            }
        }
        return $response;
    }
    public function importusers($rec) {
        $tdat = date("U");
        $tdate = $tdat - 280;
        $PhoneNbr = $rec->PhoneNbr;
        $Passw = trim($rec->Password);
        $Password = md5($Passw);
        $FirstName = $rec->FirstName;
        $Gender = $rec->Gender;
        if ($PhoneNbr != '') {
            $sql = $this->prdbh->prepare("SELECT PhoneNbr FROM User WHERE PhoneNbr = '" . $PhoneNbr . "'");
            $sql->execute();
            if ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                $response['message'] = "Number already registered, please try with other number";
                $response['Status'] = "0";
            } else {
                $sql = $this->prdbh->prepare("INSERT INTO `User`(`PhoneNbr`,`Password`,`FirstName`,`Gender`,`Occupation`, `IsPhonePublic`, `AddedOn`, `IsActive`) 
                VALUES (?,?,?,?,?,1,?,1)");
                $sql->bindParam(1, $rec->PhoneNbr);
                $sql->bindParam(2, $Password);
                $sql->bindParam(3, $rec->FirstName);
                $sql->bindParam(4, $rec->Gender);
                $sql->bindParam(5, $rec->Occupation);
                $sql->bindParam(6, $tdate);
                $this->prdbh->beginTransaction();
                if ($sql->execute()) {
                    $id = $this->prdbh->lastInsertId();
                    $this->prdbh->commit();
                    $response['message'] = "Registration Successful";
                    $response['userid'] = $id;
                    $response['Status'] = "1";
                    if ((isset($rec->RWAID)) && ($rec->RWAID != '')) {
                        $rwid = $rec->RWAID;
                        $sql1 = $this->prdbh->prepare("INSERT INTO `ResidentRWAMapping`(`ResidentID`, `RWAID`, `AddedOn`, `ApprovalStatus`, `IsActive`) VALUES (?,?,?,'P','1')");
                        $sql1->bindParam(1, $id);
                        $sql1->bindParam(2, $rwid);
                        $sql1->bindParam(3, $tdate);
                        $this->prdbh->beginTransaction();
                        if ($sql1->execute()) {
                            $ResidentRWAID = $this->prdbh->lastInsertId();
                            $this->prdbh->commit();
                            if ((isset($rec->FlatID)) && ($rec->FlatID != '')) {
                                $FlatID = $rec->FlatID;
                                $isowner = $rec->IsOwner;
                                $sql2 = $this->prdbh->prepare("INSERT INTO `ResidentFlatMapping`(`ResidentRWAID`, `FlatID`, `IsOwner`, `IsActive`) VALUES (?,?,?,1)");
                                $sql2->bindParam(1, $ResidentRWAID);
                                $sql2->bindParam(2, $FlatID);
                                $sql2->bindParam(3, $isowner);
                                $this->prdbh->beginTransaction();
                                if ($sql2->execute()) {
                                    $ResidentRWAFlatID = $this->prdbh->lastInsertId();
                                    $this->prdbh->commit();
                                    if ((isset($rec->VehicleNbr)) && ($rec->VehicleNbr != '')) {
                                        $VehicleNbr = $rec->VehicleNbr;
                                        $VehicleNbrArray = explode(",", $VehicleNbr);
                                        $VehicleType = $rec->VehicleType;
                                        $VehicleTypeArray = explode(",", $VehicleType);
                                        for ($i = 0;$i < count($VehicleNbrArray);$i++) {
                                            $sql3 = $this->prdbh->prepare("INSERT INTO ResidentVehicleMapping(ResidentRWAFlatID, VehicleNbr,VehicleType) VALUES(?,?,?)");
                                            $sql3->bindParam(1, $ResidentRWAFlatID);
                                            $sql3->bindParam(2, $VehicleNbrArray[$i]);
                                            $sql3->bindParam(3, $VehicleTypeArray[$i]);
                                            $this->prdbh->beginTransaction();
                                            if ($sql3->execute()) {
                                                $this->prdbh->commit();
                                            } else {
                                                $response['vehicleERROR'] = $sql->errorInfo();
                                            }
                                        }
                                    }
                                } else {
                                    $response['flatERROR'] = $sql->errorInfo();
                                }
                            }
                        } else {
                            $response['rwaERRROR'] = $sql1->errorInfo();
                        }
                    }
                } else {
                    $response['message'] = $sql->errorInfo();
                    $response['Status'] = "0";
                }
            }
        } else {
            $response['message'] = "All fields are mandatory";
            $response['Status'] = "0";
        }
        return $response;
    }
    // public function unplannedvisitorpush($rec)
    //      {
    //          $VisitorPurpose=$rec->VisitorPurpose;
    //          $FlatID=$rec->FlatID;
    //          $VisitorID=$rec->VisitorID;
    //          $VisitorName=$rec->VisitorName;
    //                  $sql4 = $this->prdbh->prepare("SELECT ResidentRWAMapping.ResidentID FROM ResidentRWAMapping LEFT JOIN ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID= ResidentRWAMapping.ResidentRWAID WHERE  ResidentFlatMapping.FlatID IN ($FlatID)  ");
    //                  $sql4->execute();
    //                  if($sql4->rowCount()>0)
    //                  {
    //                       $UpushId1=array();
    //                      while($row4=$sql4->fetch(PDO::FETCH_ASSOC))
    //                      {
    //                          $UpushId1[]=$row4['ResidentID'];
    //                      }
    //                      $UpushId=implode(',',$UpushId1);
    //                      // $date=date('h:i a ',$VisitorEntryTime+19660);
    //                      $date=time();
    //                      //$ID=$id;//insert id
    //                       if($VisitorName!=''){
    //                          $message= $VisitorName.' with purpose -'.$VisitorPurpose.' will be arriving at your flat soon.';
    //                          }
    //                      else{
    //                          $message='Someone with purpose -'.$VisitorPurpose.' will be arriving at your flat soon.';
    //                          }
    //                      $UrHd1='mynukad Visitor';
    //                      // $message1 = array("message"=>$message,"UrHd2"=>$UrHd1,"body"=>$ID);
    //                      $message1 = array("message"=>$message,"UrHd2"=>$UrHd1,"body"=>$VisitorID);
    //                      $result=$this->firebasenotification($UpushId,$message1);
    //                      $response['PushStatus']=$result;
    //                  }
    //          return $response;
    //         }
    public function visitorallow($rec) {
        $visitorApprovalStatus = $rec->visitorApprovalStatus;
        $FlatID = $rec->flatId;
        $VisitorID = $rec->visitorId;
        $id = '';
        if (!isset($rec->flatId) || !isset($rec->visitorId)) {
            $response['message'] = "All fields are mandatory";
            $response['Status'] = "0";
            return $response;
        }
        //-----------
        $sql = $this->prdbh->prepare("SELECT id FROM visitorApprovalStatus  WHERE flatId = '" . $FlatID . "' and visitorId = '" . $VisitorID . "' and approvalStatus (1,0) ");
        $sql->execute();
        if ($sql->rowCount() > 0) {
            $response['message'] = "Visitor's entry already approved/denied by Someone from your flat.";
            $response['Status'] = "0";
        } else {
            $sql2 = $this->prdbh->prepare("update visitorApprovalStatus set approvalStatus = '" . $visitorApprovalStatus . "' where flatId = '" . $FlatID . "' and visitorId = '" . $VisitorID . "' ");
            if ($sql2->execute()) {
                //$sql3 = $this->prdbh->prepare("update VisitorTable set VisitorInOut = '" . $visitorApprovalStatus . "' where  VisitorID = '" . $VisitorID . "' and ");
                $sql3 = $this->prdbh->prepare("update VisitorTable set VisitorInOut = '" . $visitorApprovalStatus . "' where  VisitorID = '" . $VisitorID . "' and (Select count(flatid) from visitorApprovalStatus where VisitorID = '". $VisitorID ."'and approvalStatus = '2') = 0");
                if ($sql3->execute()) {
                    $response['message'] = "Success";
                    $response['Status'] = "1";
                } else {
                    $response['message'] = $sql3->errorInfo();
                    $response['Status'] = "0";
                }
            } else {
                $response['message'] = $sql->errorInfo();
                $response['Status'] = "0";
            }
        }
        return $response;
    }
    public function overridevisitorstatus($rec) {
        $visitorApprovalStatus = $rec->visitorApprovalStatus;
        $FlatID = $rec->flatId;
        $VisitorID = $rec->visitorId;
        $LoginID = $rec->LoginID;
        $id = '';
        if (empty($visitorApprovalStatus) || empty($LoginID) || empty($FlatID) || empty($VisitorID)) {
            $response['message'] = "All fields are mandatory";
            $response['Status'] = "0";
            return $response;
        }
        //-----------
        $sql = $this->prdbh->prepare("SELECT id FROM visitorApprovalStatus  WHERE flatId = '" . $FlatID . "' and visitorId = '" . $VisitorID . "' and status = 1 ");
        $sql->execute();
        if ($sql->rowCount() > 0) {
            $response['message'] = "Visitor's entry already approved/denied by Someone from your flat.";
            $response['Status'] = "0";
        } else {
            $sql2 = $this->prdbh->prepare("INSERT INTO `visitorApprovalStatus`(`flatId`, `visitorId`, `approvalStatus`, `status`) VALUES (?,?,?,?,1)");
            $sql2->bindParam(1, $FlatID);
            $sql2->bindParam(2, $VisitorID);
            $sql2->bindParam(3, $visitorApprovalStatus);
            $sql2->bindParam(4, $LoginID);
            $this->prdbh->beginTransaction();
            if ($sql2->execute()) {
                $id = $this->prdbh->lastInsertId();
                $this->prdbh->commit();
                $response['message'] = "Success";
                $response['Status'] = "1";
            } else {
                $response['message'] = $sql->errorInfo();
                $response['Status'] = "0";
            }
        }
        return $response;
    }
    public function updatevisitorparkingavailability($rec) {
        $parkingid = $rec->parkingid;
        $VisitorID = $rec->VisitorID;
        if (isset($rec->Flat_ID)) {
            $Flat_ID = $rec->Flat_ID;
        }
        if (isset($rec->sticker_num)) {
            $sticker_num = $rec->sticker_num;
        }
        //1 for check in, 2 for checkout
        if (!isset($rec->entryexit) || !isset($rec->parkingid)) {
            $response['Message'] = "Enter mandatory fields ";
            return $response;
        }
       
        if ($rec->entryexit == 1) {
            $sql = $this->prdbh->prepare("Update ParkingTable set Flat_ID = '" . $Flat_ID . "',sticker_issued = 1, sticker_num = '" . $sticker_num . "', VisitorID = '" . $VisitorID . "'  WHERE ID='" . $parkingid . "'  ");
        } else if ($rec->entryexit == 2) {
            $sql = $this->prdbh->prepare("Update ParkingTable set Flat_ID = 0,sticker_issued = 0, sticker_num = '' , VisitorID = NULL WHERE ID='" . $parkingid . "'  ");
        }
        if ($sql->execute()) {
            if ($rec->entryexit == 2) {
                $response['Message'] = "Parking cleared";
            } else {
                $response['Message'] = "Parking allotted";
            }
            $response['Status'] = "1";
        } else {
            $response['Message'] = $sql->errorInfo();
            $response['Status'] = "0";
        }
        return $response;
    }
    //TVaddvertisement start
    public function tvadd($param) {
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        $response = array();
        
        // $sql1 = $this->prdbh->prepare("SELECT 
        //   TvAdd.ID,
        //     VideoURl,
        //     Imageurl,
        //     Notifications.Text as NoticeText,
        //     UNIX_TIMESTAMP(NoticeDate),
        //     TickerText,
        //     SocietyName,
        //    TvAdd.RWAID
        // FROM
        //     TvAdd inner join Notifications on TvAdd.RWAID=Notifications.RWAID
        // WHERE
        // TvAdd.RWAID = '" . $RWAID . "'and Notifications.TvAdvertise=1 ");     
        $sql1 = $this->prdbh->prepare("SELECT ID,VideoURl,Imageurl, NoticeText, UNIX_TIMESTAMP(NoticeDate), TickerText , SocietyName, RWAID  FROM TvAdd WHERE RWAID = '" . $RWAID . "' "); 
        $this->prdbh->beginTransaction();
        if ($sql1->execute()) {
            $items = array();
          
            $items2 = array();
            $items3 = array();
            $items4 = array();
            $items5 = array();
            $count = $sql1->rowCount();
            if ($count >= 1) {
                while ($row = $sql1->fetch(PDO::FETCH_ASSOC)) 
                {
                    $items[] = $row['VideoURl'];
                    $items2[]= $row['TickerText'];
                    $items3[] = $row['Imageurl'];
                    $items4[] = $row['SocietyName'];
                    $items5[] = $row['UNIX_TIMESTAMP(NoticeDate)'];
                }
                if ($items !== NULL) {
                    $response['message'] = "Success";
                    $response['Status'] = "1";
                    $response['TvAdd'] = $items;
                    $response['TickerText'] = $items2;
                    $response['Imageurl'] = $items3;
                    $response['SocietyName'] = $items4;
                    $response['NoticeDate'] = $items5;
                   
                    }
                    $sql = $this->prdbh->prepare("SELECT Text as NoticeText , RWAID FROM Notifications where TvAdvertise=1 and RWAID='" . $RWAID . "'") ;
                    if ($sql->execute()) {
                        $items1 = array();
                        $count = $sql->rowCount();
                        if ($count >= 1) {
                            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                                $items1[]= $row['NoticeText'];
                            }
                            $response['NoticeText'] = $items1;
                        }
                        else {
                           $response['NoticeText'] = "Noticetext not found";
                        }
                    }
            } else {
                $response['Status'] = "0";
                $response['message'] = "RWAID is not found";
            }
        }
        return $response;
    }
    public function visitorstatus($rec) {
        $FlatID = $rec->flatId;
        $VisitorID = $rec->visitorId;
        $visitorStatus = 0;
        if (!isset($rec->flatId) || !isset($rec->visitorId)) {
            $response['message'] = "All fields are mandatory";
            $response['Status'] = "0";
            return $response;
        }
        //-----------
        $sql = $this->prdbh->prepare("SELECT 
        approvalStatus, flatID
    FROM
        visitorApprovalStatus
    WHERE
        flatID IN $FlatID
            AND visitorId = '" . $VisitorID . "'
            AND status IN (1 , 2)");
        $sql->execute();
        if ($sql->rowCount() > 0) {
            $items = array();
            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                $items[] = $row;
            }
            //approval/rejection counts
            $sql5 = $this->prdbh->prepare("SELECT max(total) as total,max(defaultvalue) as defaultvalue,max(approved) as approved,max(rejected) as rejected from (SELECT 
    COUNT(id) AS total,
    NULL AS defaultvalue,
    NULL AS approved,
    NULL AS rejected
FROM
    visitorApprovalStatus
WHERE
    approvalStatus IN (0 , 1, 2)
        AND status = 1
        AND visitorId = '" . $VisitorID . "' 
UNION SELECT 
    NULL, COUNT(id), NULL, NULL
FROM
    visitorApprovalStatus
WHERE
    approvalStatus = 2 AND status = 1
        AND visitorId = '" . $VisitorID . "' 
UNION SELECT 
    NULL, NULL, COUNT(id), NULL
FROM
    visitorApprovalStatus
WHERE
    approvalStatus = 1 AND status = 1
        AND visitorId = '" . $VisitorID . "' 
UNION SELECT 
    NULL, NULL, NULL, COUNT(id)
FROM
    visitorApprovalStatus
WHERE
    approvalStatus = 0 AND status = 1
        AND visitorId = '" . $VisitorID . "') as z ");
            $sql5->execute();
            if ($sql5->rowCount() >= 1) {
                while ($row5 = $sql5->fetch(PDO::FETCH_ASSOC)) {
                    $total = $row5['total'];
                    $defaultvalue = $row5['defaultvalue'];
                    $approved = $row5['approved'];
                    $rejected = $row5['rejected'];
                }
            }
            if ($rejected > 0 && $rejected == $total) {
                $allrejected = 1;
                $anyapproval = 0;
                $statusUpdated = 1;
                $allapproved = 0;
            } else if ($defaultvalue > 0 && $defaultvalue == $total) {
                $allrejected = 0;
                $anyapproval = 0;
                $statusUpdated = 0;
                $allapproved = 0;
            } else if ($approved > 0 && $approved == $total) {
                $allrejected = 0;
                $anyapproval = 1;
                $statusUpdated = 1;
                $allapproved = 1;
            } else if ($approved > 0 && $approved < $total) {
                $allrejected = 0;
                $anyapproval = 1;
                $statusUpdated = 1;
                $allapproved = 0;
            } else {
                $allrejected = '';
                $anyapproval = '';
                $statusUpdated = '';
                $allapproved = '';
            }
            //end
            $response['visitorApprovalStatus'] = $items;
            $response['allrejected'] = $allrejected;
            $response['anyapproval'] = $anyapproval;
            $response['statusUpdated'] = $statusUpdated;
            $response['allapproved'] = $allapproved;
            $response['message'] = "Success";
            $response['Status'] = '1';
        } else {
            $response['message'] = "No record found";
            $response['Status'] = "0";
        }
        return $response;
    }
    public function updatevisitorqrcode($rec) {
        $VisitorQrCode = $rec->VisitorQrCode;
        $VisitorID = $rec->VisitorID;
        $VisitorName = "";
        $Purpose = "";
        $GateName = "unknown";
        if (isset($rec->GateName)) {
            $GateName = $rec->GateName;
        }
        if (!isset($rec->VisitorQrCode) || !isset($rec->VisitorID) || empty($rec->VisitorID)) {
            $response['message'] = "Provide mandatory fields";
            $response['Status'] = "0";
            return $response;
        }
        $sql7 = $this->prdbh->prepare("SELECT a.VisitorName,c.DetailList FROM VisitorTable AS a INNER JOIN VisitorDetails AS b ON a.VisitorID = b.VisitorID inner join VisitorPurpos as c on b.VisitorPurpose = c.ID where a.VisitorID = '" . $VisitorID . "' ");
        $sql7->execute();
        if ($sql7->rowCount() >= 1) {
            while ($row7 = $sql7->fetch(PDO::FETCH_ASSOC)) {
                $VisitorName = $row7['VisitorName'];
                $Purpose = $row7['DetailList'];
            }
        }
        $sql = $this->prdbh->prepare("SELECT VisitorQrCode FROM VisitorTable  WHERE VisitorQrCode='" . $VisitorQrCode . "' and visitorId != '" . $VisitorID . "' ");
        $sql->execute();
        if ($sql->rowCount() < 1 || $rec->skipqr == 1) {
            $sql1 = $this->prdbh->prepare("UPDATE VisitorTable SET VisitorQrCode='" . $VisitorQrCode . "', VisitorInOut = '1'  WHERE VisitorID= '" . $VisitorID . "' ");
            $this->prdbh->beginTransaction();
            if ($sql1->execute()) {
                $this->prdbh->commit();
                //notification on qrscan
                $sql2 = $this->prdbh->prepare("SELECT ResidentRWAMapping.ResidentID FROM ResidentRWAMapping LEFT JOIN ResidentFlatMapping ON ResidentFlatMapping.ResidentRWAID= ResidentRWAMapping.ResidentRWAID WHERE  ResidentFlatMapping.FlatID IN (SELECT flatId FROM visitorApprovalStatus where visitorId = '" . $VisitorID . "' and approvalStatus = 1 and status = 1)  ");
                $sql2->execute();
                if ($sql2->rowCount() > 0) {
                    $UpushId1 = array();
                    while ($row2 = $sql2->fetch(PDO::FETCH_ASSOC)) {
                        $UpushId1[] = $row2['ResidentID'];
                    }
                    $UpushId = implode(',', $UpushId1);
                    $date = date('h:i a ', $VisitorEntryTime + 19660);
                    $ID = '' . $VisitorID;
                    if ($VisitorName != '') {
                        $message = $VisitorName . ' with purpose -' . $Purpose . ' has arrived at ' . $GateName;
                    } else {
                        $message = 'Someone with purpose -' . $Purpose . ' has arrived at ' . $GateName;
                    }
                    $UrHd1 = 'mynukad Visitor';
                    $message1 = array("message" => $message, "UrHd2" => $UrHd1, "body" => $ID);
                    $result = $this->firebasenotification($UpushId, $message1);
                    $response['PushStatus'] = $result;
                }
                //end
                $response['Message'] = "Visitor QRCODE successfully updated";
                $response['Status'] = "1";
            } else {
                $response['Message'] = $sql1->errorInfo();
                $response['Status'] = "0";
            }
        } else {
            $response['Message'] = "QRCODE has already been issued";
            $response['Status'] = "0";
        }
        return $response;
    }
    public function onedayvisitorlistsociety($param) {
        $Search = '';
        if (isset($param->Search)) {
            $Search = $param->Search;
        }
        if (isset($param->RWAID)) {
            $RWAID = $param->RWAID;
        }
        if (isset($param->timestamp)) {
            $timestamp = $param->timestamp;
        }
        $endtimestamp = $timestamp + 86399;
        $PageNo = '';
        if (isset($param->PageNo)) {
            $PageNo = $param->PageNo;
        }
        $Start = $PageNo * 10 - 10;
        $response = array();
        if ($Search != '') {
            $sql = $this->prdbh->prepare("SELECT VisitorTable.VisitorID, VisitorTable.VisitorName, VisitorTable.VisitorImageUrl, TRIM(LEADING '+91' FROM VisitorPhoneNumber) as VisitorPhoneNumber, VisitorTable.Saved, VisitorTable.RWAID, VisitorTable.OTP, VisitorTable.ResidentRWAID, VisitorTable.VisitorInOut, VisitorTable.IsActive, VisitorTable.QrPath, VisitorTable.VerificationIDType, VisitorTable.VerificationIDNumber, VisitorTable.VisitorQrCode,VisitorDetails.ID, VisitorDetails.VisitorType, VisitorDetails.VisitorAddress, VisitorDetails.VisitorPicPath, VisitorDetails.VisitorDocPath, VisitorDetails.VisitorETA, VisitorDetails.VisitorEntryTime, VisitorDetails.VisitorExitTime, VisitorDetails.VisitorVehicleNumber, VisitorDetails.VisitorPurpose, VisitorDetails.NumberOfPeople, VisitorDetails.VisitorCompany,VisitorPurpos.DetailList FROM VisitorTable LEFT JOIN  VisitorDetails ON VisitorTable.VisitorID= VisitorDetails.VisitorID LEFT JOIN VisitorPurpos ON VisitorDetails.VisitorPurpose =VisitorPurpos.ID   WHERE VisitorTable.RWAID ='" . $RWAID . "' AND VisitorTable.IsActive='1' AND VisitorTable.VisitorInOut='1' AND  (VisitorTable.VisitorName like '%" . $Search . "%' OR  VisitorTable.VisitorPhoneNumber like '" . $Search . "%') AND VisitorDetails.VisitorEntryTime BETWEEN '" . $timestamp . "' AND '" . $endtimestamp . "'  ORDER BY VisitorTable.VisitorID DESC ");
        } else {
            $sql = $this->prdbh->prepare("SELECT VisitorTable.VisitorID, VisitorTable.VisitorName, VisitorTable.VisitorImageUrl, TRIM(LEADING '+91' FROM VisitorPhoneNumber) as VisitorPhoneNumber, VisitorTable.Saved, VisitorTable.RWAID, VisitorTable.OTP, VisitorTable.ResidentRWAID, VisitorTable.VisitorInOut, VisitorTable.IsActive,   VisitorTable.VerificationIDNumber,VisitorTable.VerificationIDType,  VisitorTable.VisitorQrCode,  VisitorDetails.VisitorAddress, VisitorDetails.VisitorPicPath,  VisitorDetails.VisitorETA, VisitorDetails.VisitorEntryTime, VisitorDetails.VisitorExitTime, VisitorDetails.VisitorVehicleNumber, VisitorDetails.VisitorPurpose, VisitorDetails.NumberOfPeople, VisitorDetails.VisitorCompany,VisitorPurpos.DetailList FROM VisitorTable LEFT JOIN  VisitorDetails ON VisitorTable.VisitorID= VisitorDetails.VisitorID LEFT JOIN VisitorPurpos ON VisitorDetails.VisitorPurpose =VisitorPurpos.ID   WHERE VisitorTable.RWAID ='" . $RWAID . "'  AND VisitorTable.IsActive='1' AND VisitorTable.VisitorInOut in (0,1) AND VisitorDetails.VisitorEntryTime BETWEEN '" . $timestamp . "' AND '" . $endtimestamp . "'  ORDER BY VisitorTable.VisitorID DESC Limit $Start,10   ");
        }
        //print_r($sql);die;
        if ($sql->execute()) {
            $items = array();
            $count = $sql->rowCount();
            if ($count >= 1) {
                $i = 0;
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $VisitorID = $row['VisitorID'];
                    $items[] = $row;
                    $sql2 = '';
                    $sql2 = $this->prdbh->prepare("SELECT SocietyFlatMapping.FlatNbr,VisitorFlatMapping.FlatID FROM VisitorFlatMapping inner JOIN SocietyFlatMapping ON SocietyFlatMapping.ID= VisitorFlatMapping.FlatID WHERE VisitorFlatMapping.VisitorID='" . $VisitorID . "' ");
                    if ($sql2->execute()) {
                        //get qrcode issuance status
                        $sql3 = $this->prdbh->prepare("SELECT VisitorQrCode FROM VisitorTable WHERE VisitorID = '" . $VisitorID . "' AND VisitorQrCode LIKE 'VE:%'");
                        $sql3->execute();
                        if ($sql3->rowCount() >= 1) {
                            $qrCodeAssigned = 1;
                        } else {
                            $qrCodeAssigned = 0;
                        }
                        //end
                        $FlatNbr = array();
                        $FlatNbrString = "";
                        $count2 = $sql2->rowCount();
                        if ($count2 >= 1) {
                            while ($row2 = $sql2->fetch(PDO::FETCH_ASSOC)) {
                                $FlatNbr[] = $row2['FlatNbr'];
                                $FlatID[] = $row2['FlatID'];
                            }
                            $FlatNbrString = implode(',', $FlatNbr);
                            $FlatIdString = implode(',', $FlatID);
                        } else {
                            $FlatNbrString = "";
                        }
                    }
                    $items[$i]['FlatNbr'] = $FlatNbrString;
                    $items[$i]['FlatID'] = $FlatIdString;
                    $items[$i]['qrCodeAssigned'] = $qrCodeAssigned;
                    $sql4 = $this->prdbh->prepare("SELECT a.flatId,a.approvalStatus,b.FlatNbr FROM visitorApprovalStatus as a inner join SocietyFlatMapping as b on a.flatId = b.ID WHERE VisitorID = '" . $VisitorID . "' AND flatId in ($FlatIdString) ");
                    // print_r($sql4);die;
                    $sql4->execute();
                    if ($sql4->rowCount() >= 1) {
                        while ($row4 = $sql4->fetch(PDO::FETCH_ASSOC)) {
                            $items1[] = $row4;
                        }
                        $items[$i]['approvalStatus'] = $items1;
                    }
                    $i++;
                }
                $response['vistordetail'] = $items;
                $response['Status'] = 1;
            } else {
                $response['vistordetail'] = '';
                $response['message'] = "";
                $response['Status'] = 0;
            }
        } else {
            $response['vistordetail'] = '';
            $response['message'] = $sql->errorInfo();
            $response['Status'] = 0;
        }
        return $response;
    }
    public function getuser($param) {

        if (isset($param->PhoneNbr)) {
            $PhoneNbr = $param->PhoneNbr;
         
        }
            $sql=$this->prdbh->prepare("SELECT  ResidentRWAID,ResidentID,RWAID,FirstName,EmailID,SocietyName,FlatNbr,PhoneNbr FROM mynukad.mynukad_bi_ResidentDetails_view  where PhoneNbr = '" . $PhoneNbr . "' ");
            $sql->execute();
            if($sql->rowCount() >= 1){
                

                $item=[];
                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $item[] = $row;
                   
                }
                $response['user_details'] = $item;
                $response['Status'] = '1';
             }
             else  { 
                 $response['Message'] = "phone number dose'nt exist";
                $response['Status'] = '0';
            }
             return $response;
                }
}
?>