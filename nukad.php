 <?php

class nukad
{
    public $request;
    
    public function _response($data, $Status = 200)
    {
        header('HTTP/1.1 ' . $Status . ' ' . $this->_requestStatus($Status));
        header("AUTH_TOKEN: 1333");
        return json_encode($data);
    }
    
    public function _requestStatus($code)
    {
        $Status = array(
            200 => 'OK',
            201 => 'Created',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            500 => 'Internal Server Error'
        );
        return ($Status[$code]) ? $Status[$code] : $Status[500];
    }
    
    public function invalid()
    {
        
        $response['description'] = 'Invalid request';
        $response['Status']      = '200 OK';
        print json_encode($response);
    }
    
    
    public function maintenancestaffattendance()
    {
        global $prdbh;
        
        $response = $prdbh->maintenancestaffattendance($this->params);
        //print_r($response);die;
        if (isset($response) && !empty($response)) {
            // $response['Status']=1;
            
            print $this->_response($response, 201);
        } else {
            // $response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    public function mtest()
    {
        global $prdbh;
        
        $response = $prdbh->newcomplainmail1($this->params);
        //print_r($response);die;
        if (isset($response) && !empty($response)) {
            // $response['Status']=1;
            
            print $this->_response($response, 201);
        } else {
            // $response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function firebaselogbanner()
    {
        global $prdbh;
        $response = $prdbh->firebaselogbanner($this->params);
        if (isset($response) && !empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    
    public function clubentry()
    {
        global $prdbh;
        $response = $prdbh->clubentry($this->params);
        if (isset($response) && !empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    
    public function apologymail()
    {
        global $prdbh;
        $response = $prdbh->apologymail($this->params);
        if (isset($response) && !empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    
    public function generatebill()
    {
        global $prdbh;
        $response = $prdbh->generatebill($this->params);
        if (isset($response) && !empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    
    public function offersonhomepage()
    {
        global $prdbh;
        $response = $prdbh->offersonhomepage($this->params);
        if (isset($response) && !empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    public function eventnotification()
    {
        global $prdbh;
        $response = $prdbh->eventnotification($this->params);
        if (isset($response) && !empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    
    public function notificationuser()
    {
        global $prdbh;
        $response = $prdbh->notificationuser($this->params);
        if (isset($response) && !empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    
    public function getrwavehiclepull()
    {
        global $prdbh;
        $response = $prdbh->getrwavehiclepull($this->params);
        if (isset($response) && !empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    
    public function vehiclemovementstatus()
    {
        global $prdbh;
        $response = $prdbh->vehiclemovementstatus($this->params);
        if (isset($response) && !empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    
    public function helperentrytawerlevel()
    {
        global $prdbh;
        $response = $prdbh->helperentrytawerlevel($this->params);
        if (isset($response) && !empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    public function visitercheckoutbyflat()
    {
        global $prdbh;
        $response = $prdbh->visitercheckoutbyflat($this->params);
        if (isset($response) && !empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    public function visitertowercheckinout()
    {
        global $prdbh;
        $response = $prdbh->visitertowercheckinout($this->params);
        if (isset($response) && !empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    
    public function compfeedbackstatus()
    {
        global $prdbh;
        $response = $prdbh->compfeedbackstatus($this->params);
        if (isset($response) && !empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    
    public function userdetailsnew()
    {
        global $prdbh;
        $response = $prdbh->userdetailsnew($this->params);
        if (isset($response) && !empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    
    public function modeofpayment()
    {
        global $prdbh;
        $response = $prdbh->modeofpayment($this->params);
        if (isset($response) && !empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    
    public function vehiclemovementstatus1()
    {
        global $prdbh;
        $response = $prdbh->vehiclemovementstatus1($this->params);
        if (isset($response) && !empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    
    public function userdetailbyresidentrwaid()
    {
        global $prdbh;
        $response = $prdbh->userdetailbyresidentrwaid($this->params);
        if (isset($response) && !empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    
    public function rwa()
    {
        global $prdbh;
        
        $response = $prdbh->rwa($this->params);
        //print_r($response);die;
        if (isset($response) && !empty($response)) {
            // $response['Status']=1;
            
            print $this->_response($response, 201);
        } else {
            // $response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function rwafunctionconfig()
    {
        global $prdbh;
        
        $response = $prdbh->rwafunctionconfig($this->params);
        
        if (isset($response) && !empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    
    public function firebaselogpopup()
    {
        global $prdbh;
        
        $response = $prdbh->firebaselogpopup($this->params);
        
        if (isset($response) && !empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    
    public function makeflatprimarymember()
    {
        global $prdbh;
        
        $response = $prdbh->makeflatprimarymember($this->params);
        
        if (isset($response) && !empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    
    public function addrfid()
    {
        global $prdbh;
        
        $response = $prdbh->addrfid($this->params);
        
        if (isset($response) && !empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    
    public function usercallrecord()
    {
        global $prdbh;
        $responce = $prdbh->usercallrecord($this->params);
        
        if (isset($response) && !empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    
    public function clubentryconfirm()
    {
        global $prdbh;
        $response = $prdbh->clubentryconfirm($this->params);
        
        if (isset($response) && !empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    
    public function advertisement()
    {
        global $prdbh;
        $response = $prdbh->advertisement($this->params);
        
        if (isset($response) && !empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    public function advertisementevent()
    {
        global $prdbh;
        $response = $prdbh->advertisementevent($this->params);
        
        if (isset($response) && !empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    
    public function servicecalllog()
    {
        global $prdbh;
        $responce = $prdbh->servicecalllog($this->params);
        
        if (isset($responce) && !empty($responce)) {
            print $this->_response($responce, 201);
        } else {
            print $this->_response($responce, 200);
        }
    }
    
    public function allhelperforscanner()
    {
        global $prdbh;
        
        $response = $prdbh->allhelperforscanner($this->params);
        
        if (isset($response) && !empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    
    public function postlikenotif()
    {
        global $prdbh;
        
        $response = $prdbh->postlikenotif($this->params);
        
        if (isset($response) && !empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    
    public function shorturl()
    {
        global $prdbh;
        
        $response = $prdbh->shorturl($this->params);
        
        if (isset($response) && !empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    
    public function visitorparking()
    {
        global $prdbh;
        
        $response = $prdbh->visitorparking($this->params);
        
        if (isset($response) && !empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    
    public function servicelist()
    {
        global $prdbh;
        
        $response = $prdbh->servicelist($this->params);
        
        if (isset($response) && !empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    
    public function visitorpurpose()
    {
        global $prdbh;
        
        $response = $prdbh->visitorpurpose($this->params);
        
        if (isset($response) && !empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    
    public function postpoledetail()
    {
        global $prdbh;
        
        $response = $prdbh->postpoledetail($this->params);
        //print_r($response);die;
        if (isset($response) && !empty($response)) {
            // $response['Status']=1;
            
            print $this->_response($response, 201);
        } else {
            
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function shopdetailbyid()
    {
        global $prdbh;
        
        $response = $prdbh->shopdetailbyid($this->params);
        //print_r($response);die;
        if (isset($response) && !empty($response)) {
            // $response['Status']=1;
            
            print $this->_response($response, 201);
        } else {
            
            
            print $this->_response($response, 200);
        }
        
    }
    
    
    public function rwabyname()
    {
        global $prdbh;
        
        $response = $prdbh->rwabyname($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            // $response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            //$response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    
    public function registrationnewrollback()
    {
        global $prdbh;
        
        $response = $prdbh->registrationnewrollback($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            // $response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function registrationnew()
    {
        global $prdbh;
        
        $response = $prdbh->registrationnew($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            // $response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function photodetail()
    {
        global $prdbh;
        
        $response = $prdbh->photodetail($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            // $response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function notify()
    {
        global $prdbh;
        
        $response = $prdbh->notify($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            // $response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            //$response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function gallerypicdelete()
    {
        global $prdbh;
        
        $response = $prdbh->gallerypicdelete($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            // $response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            //$response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function isin()
    {
        global $prdbh;
        
        $response = $prdbh->isin($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    
    public function deleterfid()
    {
        global $prdbh;
        
        $response = $prdbh->deleterfid($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    
    public function couponreedim()
    {
        global $prdbh;
        
        $response = $prdbh->couponreedim($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    
    public function couponsdetailbyscane()
    {
        global $prdbh;
        
        $response = $prdbh->couponsdetailbyscane($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
        
    }
    
    public function versioncheck()
    {
        global $prdbh;
        
        $response = $prdbh->versioncheck($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            // $response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            //$response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function userdetails()
    {
        global $prdbh;
        
        $response = $prdbh->userdetails($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            // $response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            //$response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    public function likerusers()
    {
        global $prdbh;
        
        $response = $prdbh->likerusers($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            // $response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            //$response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function compstatuschange()
    {
        global $prdbh;
        
        $response = $prdbh->compstatuschange($this->params);
        
        if (!empty($response)) {
            // $response['Status']='1';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            //$response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function chatnotification()
    {
        global $prdbh;
        $response = $prdbh->chatnotification($this->params);
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    
    public function userdetail()
    {
        global $prdbh;
        
        $response = $prdbh->userdetail($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            // $response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            //$response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function nearby()
    {
        global $prdbh;
        
        $response = $prdbh->nearby($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            // $response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            $response['Status'] = 0;
            $response['data']   = "Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function societybanner()
    {
        global $prdbh;
        
        $response = $prdbh->societybanner($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            // $response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            $response['Status'] = 0;
            $response['data']   = "Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function rejectionreson()
    {
        global $prdbh;
        
        $response = $prdbh->rejectionreson($this->params);
        if (!empty($response)) {
            // $response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            $response['Status'] = 0;
            $response['data']   = "Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    
    
    public function neighborsbydistance()
    {
        global $prdbh;
        
        $response = $prdbh->neighborsbydistance($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            // $response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            // $response['Status']=0;
            //$response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    public function rejectresondesc()
    {
        global $prdbh;
        
        $response = $prdbh->rejectresondesc($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            // $response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            // $response['Status']=0;
            //$response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function rwamember()
    {
        global $prdbh;
        
        $response = $prdbh->rwamember($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    
    
    public function shopdetail()
    {
        global $prdbh;
        
        $response = $prdbh->shopdetail($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    
    
    public function allhelper()
    {
        global $prdbh;
        
        $response = $prdbh->allhelper($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    
    
    public function appnotificationdetail()
    {
        global $prdbh;
        
        $response = $prdbh->appnotificationdetail($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function photogallery()
    {
        global $prdbh;
        
        $response = $prdbh->photogallery($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    public function addphotogallery()
    {
        global $prdbh;
        
        $response = $prdbh->addphotogallery($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    
    public function usefulcontactssociety()
    {
        global $prdbh;
        
        $response = $prdbh->usefulcontactssociety($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    
    
    public function addcomplain()
    {
        
        global $prdbh;
        $response = array();
        
        $result = $prdbh->addcomplain($this->params);
        
        
        
        print $this->_response($result, 201);
        
    }

    public function addcomplaindecember()
    {
        
        global $prdbh;
        $response = array();
        
        $result = $prdbh->addcomplaindecember($this->params);
        
        
        
        print $this->_response($result, 201);
        
    }
    
    
    public function addshopdetail()
    {
        
        global $prdbh;
        $response = array();
        
        $result = $prdbh->addshopdetail($this->params);
        
        print $this->_response($result, 201);
        
    }
    
    
    public function registration()
    {
        global $prdbh;
        $result = $prdbh->registration($this->params);
        if ($result['status'] == 1) {
            print $this->_response($result, 201);
        } else {
            print $this->_response($result, 200);
        }
    }
    
    public function importusers()
    {
        
        global $prdbh;
        $result = $prdbh->importusers($this->params);
        if ($result['Status'] == 1) {
            print $this->_response($result, 201);
        } else {
            print $this->_response($result, 200);
        }
    }
    
    
    public function signup()
    {
        
        global $prdbh;
        $response = array();
        
        $result = $prdbh->signup($this->params);
        
        /*
        if (isset($result['error'])) {
        
        $response['message']=$result;
        $response['Status']=0;
        } else {
        $response['message']='you are registered with  Society. Now RWA will verify your data and approval,you can browse other features of app';
        $response['register Id']=$result;
        $response['Status']=1;
        //$response['member']= $item;
        
        }*/
        
        //if(isset($response) && $response['Status']==1)
        print $this->_response($result, 201);
        // else
        //   print $this->_response($response,200);
    }
    
    public function addvehicle()
    {
        
        global $prdbh;
        
        $response = $prdbh->addvehicle($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function visitorentryconformtwo()
    {
        
        global $prdbh;
        
        $response = $prdbh->visitorentryconformtwo($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function phoneverification()
    {
        
        global $prdbh;
        
        $response = $prdbh->phoneverification($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    
    public function updateuserinfo()
    {
        
        global $prdbh;
        
        $response = $prdbh->updateuserinfo($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function login()
    {
        
        global $prdbh;
        
        $response = $prdbh->login($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    
    public function yourcomplain()
    {
        
        global $prdbh;
        
        $response = $prdbh->yourcomplain($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function vistordetailbynumber()
    {
        
        global $prdbh;
        
        $response = $prdbh->vistordetailbynumber($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    public function vistordetailbynumberdecember()
    {
        
        global $prdbh;
        
        $response = $prdbh->vistordetailbynumberdecember($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function vistorunplanentrytwo()
    {
        
        global $prdbh;
        
        $response = $prdbh->vistorunplanentrytwo($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    
    public function compstatuschangeandsms()
    {
        
        global $prdbh;
        
        $response = $prdbh->compstatuschangeandsms($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function vehiclemovementstatusby()
    {
        
        global $prdbh;
        
        $response = $prdbh->vehiclemovementstatusby($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function showcomplainnew()
    {
        
        global $prdbh;
        
        $response = $prdbh->showcomplainnew($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function deletefavshop()
    {
        
        global $prdbh;
        
        $response = $prdbh->deletefavshop($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function deletefavhelper()
    {
        
        global $prdbh;
        
        $response = $prdbh->deletefavhelper($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function deletevehicle()
    {
        
        global $prdbh;
        
        $response = $prdbh->deletevehicle($this->params);
        
        if (!empty($response)) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function showcomplain()
    {
        
        global $prdbh;
        
        $response = $prdbh->showcomplain($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function edithelper()
    {
        global $prdbh;
        
        $response = $prdbh->edithelper($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function neighbors()
    {
        global $prdbh;
        
        $response = $prdbh->neighbors($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function categories()
    {
        global $prdbh;
        
        $response = $prdbh->categories($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function allhelperforscannertwo()
    {
        global $prdbh;
        
        $response = $prdbh->allhelperforscannertwo($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function maintenancestaffattendancetwo()
    {
        global $prdbh;
        
        $response = $prdbh->maintenancestaffattendancetwo($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    
    public function couponsbyscane()
    {
        global $prdbh;
        
        $response = $prdbh->couponsbyscane($this->params);
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    public function merchantlogin()
    {
        global $prdbh;
        
        $response = $prdbh->merchantlogin($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function showhelper()
    {
        global $prdbh;
        
        $response = $prdbh->showhelper($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function addfavhelper()
    {
        global $prdbh;
        
        $response = $prdbh->addfavhelper($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    public function showhelperworkingflat()
    {
        global $prdbh;
        
        $response = $prdbh->showhelperworkingflat($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            print $this->_response($response, 200);
        }
        
    }
    
    public function helperworkingflat()
    {
        global $prdbh;
        
        $response = $prdbh->helperworkingflat($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            print $this->_response($response, 200);
        }
        
    }
    
    public function helperfeedback()
    {
        global $prdbh;
        
        $response = $prdbh->helperfeedback($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function visitorsearchbycar()
    {
        global $prdbh;
        
        $response = $prdbh->visitorsearchbycar($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function addfavshop()
    {
        global $prdbh;
        
        $response = $prdbh->addfavshop($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function vistorentryform()
    {
        global $prdbh;
        $response = $prdbh->vistorentryform($this->params);
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }

    public function vistorentryformdecember()
    {
        global $prdbh;
        $response = $prdbh->vistorentryformdecember($this->params);
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function showfavshop()
    {
        global $prdbh;
        
        $response = $prdbh->showfavshop($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function addhelper()
    {
        global $prdbh;
        
        $response = $prdbh->addhelper($this->params);
        
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function showfavhelper()
    {
        global $prdbh;
        
        $response = $prdbh->showfavhelper($this->params);
        //print_r($response);die;
        
        print $this->_response($response, 201);
        
        
    }
    
    
    public function adminrole()
    {
        global $prdbh;
        
        $response = $prdbh->adminrole($this->params);
        //print_r($response);die;
        
        print $this->_response($response, 201);
        
        
    }
    
    public function vistorunplanentryform()
    {
        global $prdbh;
        
        $response = $prdbh->vistorunplanentryform($this->params);
        //print_r($response);die;
        if ($response['Status'] == 1) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    
    public function approvesocietymember()
    {
        global $prdbh;
        
        $response = $prdbh->approvesocietymember($this->params);
        //print_r($response);die;
        
        print $this->_response($response, 201);
        
        
    }
    
    public function approvesocietymembernew()
    {
        global $prdbh;
        
        $response = $prdbh->approvesocietymembernew($this->params);
        //print_r($response);die;
        
        print $this->_response($response, 201);
        
        
    }
    
    public function approve()
    {
        global $prdbh;
        
        $response = $prdbh->approve($this->params);
        //print_r($response);die;
        if ($response['Status'] == 1) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function polestatus()
    {
        global $prdbh;
        
        $response = $prdbh->polestatus($this->params);
        //print_r($response);die;
        if ($response['Status'] == 1) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function notification()
    {
        global $prdbh;
        
        $response = $prdbh->notification($this->params);
        //print_r($response);die;
        if ($response['Status'] == 1) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    
    public function compfeedback()
    {
        global $prdbh;
        
        $response = $prdbh->compfeedback($this->params);
        //print_r($response);die;
        if ($response['Status'] == 1) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    public function helperdetailnew()
    {
        global $prdbh;
        
        $response = $prdbh->helperdetailnew($this->params);
        //print_r($response);die;
        if ($response['Status'] == 1) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function helperdetail()
    {
        global $prdbh;
        
        $response = $prdbh->helperdetail($this->params);
        //print_r($response);die;
        if ($response['Status'] == 1) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function updateprofile()
    {
        global $prdbh;
        
        $response = $prdbh->updateprofile($this->params);
        //print_r($response);die;
        if ($response['Status'] == 1) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function devicetoken()
    {
        global $prdbh;
        
        $response = $prdbh->devicetoken($this->params);
        //print_r($response);die;
        if ($response['Status'] == 1) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function addpole()
    {
        global $prdbh;
        
        $response = $prdbh->addpole($this->params);
        //print_r($response);die;
        if ($response['Status'] == 1) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function showpoles()
    {
        global $prdbh;
        
        $response = $prdbh->showpoles($this->params);
        //print_r($response);die;
        if ($response['Status'] == 1) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function showmaintenancestaffdetail()
    {
        global $prdbh;
        
        $response = $prdbh->showmaintenancestaffdetail($this->params);
        //print_r($response);die;
        if ($response['Status'] == 1) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function showmaintenancestaff()
    {
        global $prdbh;
        
        $response = $prdbh->showmaintenancestaff($this->params);
        //print_r($response);die;
        if ($response['Status'] == 1) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function addnotification()
    {
        global $prdbh;
        
        $response = $prdbh->addnotification($this->params);
        //print_r($response);die;
        if ($response['Status'] == 1) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function adminnotification()
    {
        global $prdbh;
        
        $response = $prdbh->adminnotification($this->params);
        //print_r($response);die;
        if ($response['Status'] == 1) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    
    
    public function callout()
    {
        global $prdbh;
        
        $response = $prdbh->callout($this->params);
        
        if ($response['Status'] == 1) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function addclassified()
    {
        global $prdbh;
        
        $response = $prdbh->addclassified($this->params);
        
        if ($response['Status'] == 1) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function classified()
    {
        global $prdbh;
        
        $response = $prdbh->classified($this->params);
        
        if ($response['Status'] == 1) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function appnotification()
    {
        global $prdbh;
        
        $response = $prdbh->appnotification($this->params);
        
        if ($response['Status'] == 1) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function removecallout()
    {
        global $prdbh;
        
        $response = $prdbh->removecallout($this->params);
        
        if ($response['Status'] == 1) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function removeclassified()
    {
        global $prdbh;
        
        $response = $prdbh->removeclassified($this->params);
        
        if ($response['Status'] == 1) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    public function showcallout()
    {
        global $prdbh;
        
        $response = $prdbh->showcallout($this->params);
        
        if ($response['Status'] == 1) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function showeventadv()
    {
        global $prdbh;
        
        $response = $prdbh->showeventadv($this->params);
        
        if ($response['Status'] == 1) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function calloutdetail()
    {
        global $prdbh;
        
        $response = $prdbh->calloutdetail($this->params);
        
        if ($response['Status'] == 1) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function notificationdetail()
    {
        global $prdbh;
        
        $response = $prdbh->notificationdetail($this->params);
        
        if ($response['Status'] == 1) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function complaindetail()
    {
        global $prdbh;
        
        $response = $prdbh->complaindetail($this->params);
        
        if ($response['Status'] == 1) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }

    public function complaindetaildecember()
    {
        global $prdbh;
        
        $response = $prdbh->complaindetaildecember($this->params);
        
        if ($response['Status'] == 1) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    
    public function removepoles()
    {
        global $prdbh;
        
        $response = $prdbh->removepoles($this->params);
        
        if ($response['Status'] == 1) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    
    public function flatno()
    {
        global $prdbh;
        
        $response = $prdbh->flatno($this->params);
        
        if ($response['Status'] == 1) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function notificationcount()
    {
        global $prdbh;
        
        $response = $prdbh->notificationcount($this->params);
        
        if ($response['Status'] == 1) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function editmaintenancestaff()
    {
        global $prdbh;
        
        $response = $prdbh->editmaintenancestaff($this->params);
        
        if ($response['Status'] == 1) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function complainslastaff()
    {
        global $prdbh;
        
        $response = $prdbh->complainslastaff();
        
        if ($response['Status'] == 1) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function pendingusersla()
    {
        global $prdbh;
        
        $response = $prdbh->pendingusersla();
        
        if ($response['Status'] == 1) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function addmaintenancestaff()
    {
        global $prdbh;
        
        $response = $prdbh->addmaintenancestaff($this->params);
        
        if ($response['Status'] == 1) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function complainsla()
    {
        global $prdbh;
        
        $response = $prdbh->complainsla($this->params);
        
        if ($response['Status'] == 1) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function vistorunplanentryonlinetwo()
    {
        global $prdbh;
        
        $response = $prdbh->vistorunplanentryonlinetwo($this->params);
        
        if ($response['Status'] == 1) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
        public function vistorunplanentryonlinetwodecember()
    {
        global $prdbh;
        
        $response = $prdbh->vistorunplanentryonlinetwodecember($this->params);
        
        if ($response['Status'] == 1) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function complainslacsv()
    {
        global $prdbh;
        
        $response = $prdbh->complainslacsv($this->params);
        
        if ($response['Status'] == 1) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function bannerimage()
    {
        global $prdbh;
        
        $response = $prdbh->bannerimage($this->params);
        
        if ($response['Status'] == 1) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function nearbannerimage()
    {
        global $prdbh;
        
        $response = $prdbh->nearbannerimage($this->params);
        
        if ($response['Status'] == 1) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function allcount()
    {
        global $prdbh;
        $response = $prdbh->allcount($this->params);
        
        print $this->_response($response, 200);
    }
    
    public function visitorverificationid()
    {
        global $prdbh;
        
        $response = $prdbh->visitorverificationid($this->params);
        
        if ($response['Status'] == 1) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    public function profession()
    {
        global $prdbh;
        
        $response = $prdbh->profession($this->params);
        
        if ($response['Status'] == 1) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function societyinfo()
    {
        global $prdbh;
        
        $response = $prdbh->societyinfo($this->params);
        
        if ($response['Status'] == 1) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function complainshordescription()
    {
        global $prdbh;
        
        $response = $prdbh->complainshordescription($this->params);
        
        if ($response['Status'] == 1) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function helperattendance()
    {
        global $prdbh;
        
        $response = $prdbh->helperattendance($this->params);
        
        if ($response['Status'] == 1) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function newcomplainmail()
    {
        global $prdbh;
        
        $response = $prdbh->newcomplainmail($this->params);
        
        if ($response['Status'] == 1) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    
    public function sendmail()
    {
        global $prdbh;
        
        $response = $prdbh->sendmail($this->params);
        
        if ($response['Status'] == 1) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function compstatus()
    {
        global $prdbh;
        
        $response = $prdbh->compstatus($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            
            
            print $this->_response($response, 201);
        } else {
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function complaincategories()
    {
        global $prdbh;
        
        $response = $prdbh->complaincategories($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
    
    public function helperisinnew()
    {
        global $prdbh;
        $response = $prdbh->helperisinnew($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function selectgate()
    {
        global $prdbh;
        $response = $prdbh->selectgate($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    
    
    public function helperisin()
    {
        global $prdbh;
        $response = $prdbh->helperisin($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function gateuserlogin()
    {
        global $prdbh;
        $response = $prdbh->gateuserlogin($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function newnotificationmail()
    {
        global $prdbh;
        $response = $prdbh->newnotificationmail($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    
    
    public function addpostcomment()
    {
        global $prdbh;
        $response = $prdbh->addpostcomment($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function photolike()
    {
        global $prdbh;
        $response = $prdbh->photolike($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function changemaidpicurl()
    {
        global $prdbh;
        $response = $prdbh->changemaidpicurl($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function assignstaff()
    {
        global $prdbh;
        $response = $prdbh->assignstaff($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function complainstatus()
    {
        global $prdbh;
        $response = $prdbh->complainstatus($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function poledetail()
    {
        global $prdbh;
        $response = $prdbh->poledetail($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    
    public function classifieddetail()
    {
        global $prdbh;
        $response = $prdbh->classifieddetail($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    
    
    public function addnewpost()
    {
        global $prdbh;
        $response = $prdbh->addnewpost($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function vistorlist()
    {
        global $prdbh;
        $response = $prdbh->vistorlist($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function postcommentnotif()
    {
        global $prdbh;
        $response = $prdbh->postcommentnotif($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function createpost()
    {
        global $prdbh;
        $response = $prdbh->createpost($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function societypost()
    {
        global $prdbh;
        $response = $prdbh->societypost($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    
    
    public function postlike()
    {
        global $prdbh;
        $response = $prdbh->postlike($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    
    public function flatnumber()
    {
        global $prdbh;
        $response = $prdbh->flatnumber($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    
    public function visitercheckoutbyotp()
    {
        global $prdbh;
        $response = $prdbh->visitercheckoutbyotp($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function visiterentrybyotp()
    {
        global $prdbh;
        $response = $prdbh->visiterentrybyotp($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function flatlistforvisitor()
    {
        global $prdbh;
        $response = $prdbh->flatlistforvisitor($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function visitordetailtawerlevel()
    {
        global $prdbh;
        $response = $prdbh->visitordetailtawerlevel($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function visitorentryconform()
    {
        global $prdbh;
        $response = $prdbh->visitorentryconform($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function visitercheckoutbyexternalqr()
    {
        global $prdbh;
        $response = $prdbh->visitercheckoutbyexternalqr($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function visiterqrlink()
    {
        global $prdbh;
        $response = $prdbh->visiterqrlink($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function visitercheckout()
    {
        global $prdbh;
        $response = $prdbh->visitercheckout($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }

    public function visitercheckoutdecember()
    {
        global $prdbh;
        $response = $prdbh->visitercheckoutdecember($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    
    public function editvisitorentry()
    {
        global $prdbh;
        $response = $prdbh->editvisitorentry($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function flatmember()
    {
        global $prdbh;
        $response = $prdbh->flatmember($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function vehiclentryrfid()
    {
        global $prdbh;
        $response = $prdbh->vehiclentryrfid($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function vehiclentryrfidget()
    {
        global $prdbh;
        $response = $prdbh->vehiclentryrfidget($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function deletevisitor()
    {
        global $prdbh;
        $response = $prdbh->deletevisitor($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function couponsdetailformerchant()
    {
        global $prdbh;
        $response = $prdbh->couponsdetailformerchant($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function complaincomment()
    {
        global $prdbh;
        $response = $prdbh->complaincomment($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function agentfeedback()
    {
        global $prdbh;
        $response = $prdbh->agentfeedback($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function serviceagentdetail()
    {
        global $prdbh;
        $response = $prdbh->serviceagentdetail($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function serviceagentlist()
    {
        global $prdbh;
        $response = $prdbh->serviceagentlist($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function paymentduehistory()
    {
        global $prdbh;
        $response = $prdbh->paymentduehistory($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function billpayhistorydetail()
    {
        global $prdbh;
        $response = $prdbh->billpayhistorydetail($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function visitorcompany()
    {
        global $prdbh;
        $response = $prdbh->visitorcompany($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function onedayvisitorlist()
    {
        global $prdbh;
        $response = $prdbh->onedayvisitorlist($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }


    public function onedayvisitorlistdecember()
    {
        global $prdbh;
        $response = $prdbh->onedayvisitorlistdecember($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function flatbill()
    {
        global $prdbh;
        $response = $prdbh->flatbill($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function makepayment()
    {
        global $prdbh;
        $response = $prdbh->makepayment($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function flatdueamount()
    {
        global $prdbh;
        $response = $prdbh->flatdueamount($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function deletehelper()
    {
        global $prdbh;
        $response = $prdbh->deletehelper($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function unplannedvisitorpush()
    {
        global $prdbh;
        $response = $prdbh->unplannedvisitorpush($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function visitorallow()
    {
        global $prdbh;
        $response = $prdbh->visitorallow($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function overridevisitorstatus()
    {
        global $prdbh;
        $response = $prdbh->overridevisitorstatus($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function updatevisitorparkingavailability()
    {
        global $prdbh;
        $response = $prdbh->updatevisitorparkingavailability($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function tvadd()
    {
        
        global $prdbh;
        
        $response = $prdbh->tvadd($this->params);
        //print_r($response);die;
        if (!empty($response)) {
            //$response['Status']='';
            
            print $this->_response($response, 201);
        } 
        else {
            //$response['Status']=0;
            // $response['data']="Nothing Found";
            
            print $this->_response($response, 200);
        }
        
    }
     public function visitorstatus()
    {
        global $prdbh;
        $response = $prdbh->visitorstatus($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }

     public function updatevisitorqrcode()
    {
        global $prdbh;
        $response = $prdbh->updatevisitorqrcode($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    public function getuser()
    {
        global $prdbh;
        $response = $prdbh->getuser($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    public function onedayvisitorlistsociety()
    {
        global $prdbh;
        $response = $prdbh->onedayvisitorlistsociety($this->params);
        
        if (!empty($response)) {
            print $this->_response($response, 201);
        } else {
            print $this->_response($response, 200);
        }
    }
    
    public function execute($request)
    {
        $this->request = $request;
        $this->id      = $request->id;
        $this->params  = $request->params;
        switch ($this->request->method) {
            case 'POST': {
                
                
                if ($this->request->action == 'servicecalllog')
                    $this->servicecalllog();
                
                if ($this->request->action == 'usercallrecord')
                    $this->usercallrecord();
                
                if ($this->request->action == 'helperworkingflat')
                    $this->helperworkingflat();
                
                if ($this->request->action == 'showhelperworkingflat')
                    $this->showhelperworkingflat();
                
                if ($this->request->action == 'removepoles')
                    $this->removepoles();
                
                if ($this->request->action == 'removeclassified')
                    $this->removeclassified();
                
                if ($this->request->action == 'visiterqrlink')
                    $this->visiterqrlink();
                
                if ($this->request->action == 'makepayment')
                    $this->makepayment();
                
                if ($this->request->action == 'notify')
                    $this->notify();
                
                if ($this->request->action == 'helperattendance')
                    $this->helperattendance();
                
                if ($this->request->action == 'advertisement')
                    $this->advertisement();
                
                if ($this->request->action == 'vistorlist')
                    $this->vistorlist();
                
                if ($this->request->action == 'rwa')
                    $this->rwa();
                
                if ($this->request->action == 'deleterfid')
                    $this->deleterfid();
                
                if ($this->request->action == 'addrfid')
                    $this->addrfid();
                
                if ($this->request->action == 'createpost')
                    $this->createpost();
                
                if ($this->request->action == 'societypost')
                    $this->societypost();
                
                if ($this->request->action == 'visiterentrybyotp')
                    $this->visiterentrybyotp();
                
                if ($this->request->action == 'visitertowercheckinout')
                    $this->visitertowercheckinout();
                
                if ($this->request->action == 'visitordetailtawerlevel')
                    $this->visitordetailtawerlevel();
                
                if ($this->request->action == 'visitercheckoutbyotp')
                    $this->visitercheckoutbyotp();
                
                if ($this->request->action == 'visitercheckout')
                    $this->visitercheckout();

                if ($this->request->action == 'visitercheckoutdecember')
                    $this->visitercheckoutdecember();
                
                if ($this->request->action == 'agentfeedback')
                    $this->agentfeedback();
                
                if ($this->request->action == 'serviceagentdetail')
                    $this->serviceagentdetail();
                
                if ($this->request->action == 'postpoledetail')
                    $this->postpoledetail();
                
                if ($this->request->action == 'visitorentryconform')
                    $this->visitorentryconform();

                if ($this->request->action == 'visitercheckoutbyexternalqr')
                    $this->visitercheckoutbyexternalqr();
                
                if ($this->request->action == 'newnotificationmail')
                    $this->newnotificationmail();
                
                if ($this->request->action == 'showhelper')
                    $this->showhelper();
                
                if ($this->request->action == 'poledetail')
                    $this->poledetail();
                
                if ($this->request->action == 'deletevisitor')
                    $this->deletevisitor();
                
                if ($this->request->action == 'flatlistforvisitor')
                    $this->flatlistforvisitor();
                
                if ($this->request->action == 'classifieddetail')
                    $this->classifieddetail();
                
                if ($this->request->action == 'complaincomment')
                    $this->complaincomment();
                
                if ($this->request->action == 'postlike')
                    $this->postlike();
                
                if ($this->request->action == 'shorturl')
                    $this->shorturl();
                
                if ($this->request->action == 'onedayvisitorlist')
                    $this->onedayvisitorlist();

                if ($this->request->action == 'onedayvisitorlistdecember')
                    $this->onedayvisitorlistdecember();
                
                if ($this->request->action == 'showfavhelper')
                    $this->showfavhelper();
                
                if ($this->request->action == 'vistorentryform')
                    $this->vistorentryform();

                if ($this->request->action == 'vistorentryformdecember')
                    $this->vistorentryformdecember();

															
										 
                
                if ($this->request->action == 'addpostcomment')
                    $this->addpostcomment();
                
                if ($this->request->action == 'helperisin')
                    $this->helperisin();
                
                if ($this->request->action == 'rwabyname')
                    $this->rwabyname();
                
                if ($this->request->action == 'nearbannerimage')
                    $this->nearbannerimage();
                
                if ($this->request->action == 'advertisementevent')
                    $this->advertisementevent();
                
                if ($this->request->action == 'allhelperforscannertwo')
                    $this->allhelperforscannertwo();

                if ($this->request->action == 'allhelper')
                    $this->allhelper();
                
                if ($this->request->action == 'notificationuser')
                    $this->notificationuser();
                
                if ($this->request->action == 'nearby')
                    $this->nearby();
                
                if ($this->request->action == 'versioncheck')
                    $this->versioncheck();
                
                
                if ($this->request->action == 'approve')
                    $this->approve();
                
                if ($this->request->action == 'neighborsbydistance')
                    $this->neighborsbydistance();
                
                if ($this->request->action == 'rwamember')
                    $this->rwamember();
                
                
                if ($this->request->action == 'gateuserlogin')
                    $this->gateuserlogin();
                
                if ($this->request->action == 'eventnotification')
                    $this->eventnotification();
                
                if ($this->request->action == 'clubentry')
                    $this->clubentry();
                
                if ($this->request->action == 'userdetailbyresidentrwaid')
                    $this->userdetailbyresidentrwaid();
                
                if ($this->request->action == 'firebaselogbanner')
                    $this->firebaselogbanner();
                
                if ($this->request->action == 'removecallout')
                    $this->removecallout();
                
                if ($this->request->action == 'userdetail')
                    $this->userdetail();
                
                if ($this->request->action == 'complainshordescription')
                    $this->complainshordescription();
                
                if ($this->request->action == 'showmaintenancestaffdetail')
                    $this->showmaintenancestaffdetail();
                
                if ($this->request->action == 'complaindetail')
                    $this->complaindetail();

                if ($this->request->action == 'complaindetaildecember')
                    $this->complaindetaildecember();
                
                if ($this->request->action == 'servicelist')
                    $this->servicelist();
                
                if ($this->request->action == 'serviceagentlist')
                    $this->serviceagentlist();
                
                if ($this->request->action == 'userdetails')
                    $this->userdetails();
                
                if ($this->request->action == 'visitorsearchbycar')
                    $this->visitorsearchbycar();

														   
										
                
                if ($this->request->action == 'shopdetail')
                    $this->shopdetail();
                
                if ($this->request->action == 'photogallery')
                    $this->photogallery();
                
                if ($this->request->action == 'apologymail')
                    $this->apologymail();
                
                if ($this->request->action == 'selectgate')
                    $this->selectgate();
                
                if ($this->request->action == 'postlikenotif')
                    $this->postlikenotif();
                
                if ($this->request->action == 'notification')
                    $this->notification();
                
                if ($this->request->action == 'usefulcontactssociety')
                    $this->usefulcontactssociety();
                
                if ($this->request->action == 'addvehicle')
                    $this->addvehicle();
                
                if ($this->request->action == 'calloutdetail')
                    $this->calloutdetail();
                
                if ($this->request->action == 'notificationdetail')
                    $this->notificationdetail();
                
                if ($this->request->action == 'vehiclentryrfid')
                    $this->vehiclentryrfid();
                
                
                if ($this->request->action == 'editvisitorentry')
                    $this->editvisitorentry();
                
                if ($this->request->action == 'complainsla')
                    $this->complainsla();
                
                if ($this->request->action == 'deletevehicle')
                    $this->deletevehicle();
                
                if ($this->request->action == 'rwafunctionconfig')
                    $this->rwafunctionconfig();
                
                if ($this->request->action == 'addcomplain')
                    $this->addcomplain();

                if ($this->request->action == 'addcomplaindecember')
                    $this->addcomplaindecember();
                
                if ($this->request->action == 'signup')
                    $this->signup();
                
                if ($this->request->action == 'sendmail')
                    $this->sendmail();
                
                if ($this->request->action == 'clubentryconfirm')
                    $this->clubentryconfirm();
                
                if ($this->request->action == 'societyinfo')
                    $this->societyinfo();
                
                if ($this->request->action == 'polestatus')
                    $this->polestatus();
                
                if ($this->request->action == 'classified')
                    $this->classified();
                
                if ($this->request->action == 'addhelper')
                    $this->addhelper();
                
                if ($this->request->action == 'login')
                    $this->login();
                
                if ($this->request->action == 'showcomplain')
                    $this->showcomplain();

				if ($this->request->action == 'edithelper')
                    $this->edithelper();										   
										
                
                if ($this->request->action == 'yourcomplain')
                    $this->yourcomplain();
                
                if ($this->request->action == 'helperdetailnew')
                    $this->helperdetailnew();
                
                if ($this->request->action == 'couponsbyscane')
                    $this->couponsbyscane();
                
                if ($this->request->action == 'couponreedim')
                    $this->couponreedim();
                
                if ($this->request->action == 'helperfeedback')
                    $this->helperfeedback();
                
                if ($this->request->action == 'helperdetail')
                    $this->helperdetail();
                
                if ($this->request->action == 'gallerypicdelete')
                    $this->gallerypicdelete();
                
                if ($this->request->action == 'visitorentryconformtwo')
                    $this->visitorentryconformtwo();
                
                if ($this->request->action == 'billpayhistorydetail')
                    $this->billpayhistorydetail();
                
                
                if ($this->request->action == 'neighbors')
                    $this->neighbors();
                
                if ($this->request->action == 'adminrole')
                    $this->adminrole();
                
                if ($this->request->action == 'appnotification')
                    $this->appnotification();
                
                if ($this->request->action == 'rejectionreson')
                    $this->rejectionreson();
                
                if ($this->request->action == 'allhelperforscanner')
                    $this->allhelperforscanner();
                
                if ($this->request->action == 'appnotificationdetail')
                    $this->appnotificationdetail();
                
                if ($this->request->action == 'helperisinnew')
                    $this->helperisinnew();
                
                if ($this->request->action == 'flatmember')
                    $this->flatmember();
                
                if ($this->request->action == 'approvesocietymember')
                    $this->approvesocietymember();
                
                if ($this->request->action == 'couponsdetailformerchant')
                    $this->couponsdetailformerchant();
                
                if ($this->request->action == 'approvesocietymembernew')
                    $this->approvesocietymembernew();
                
                if ($this->request->action == 'deletefavshop')
                    $this->deletefavshop();
                
                if ($this->request->action == 'couponsdetailbyscane')
                    $this->couponsdetailbyscane();
                
                if ($this->request->action == 'flatbill')
                    $this->flatbill();
                
                if ($this->request->action == 'deletefavhelper')
                    $this->deletefavhelper();
                
                if ($this->request->action == 'devicetoken')
                    $this->devicetoken();
                
                if ($this->request->action == 'visitorparking')
                    $this->visitorparking();
                
                if ($this->request->action == 'addfavhelper')
                    $this->addfavhelper();
                
                if ($this->request->action == 'offersonhomepage')
                    $this->offersonhomepage();
                
                if ($this->request->action == 'maintenancestaffattendancetwo')
                    $this->maintenancestaffattendancetwo();
                
                if ($this->request->action == 'vistordetailbynumber')
                    $this->vistordetailbynumber();
                
                if ($this->request->action == 'vistordetailbynumberdecember')
                    $this->vistordetailbynumberdecember();
                
                if ($this->request->action == 'updateprofile')
                    $this->updateprofile();
                
                if ($this->request->action == 'vistorunplanentrytwo')
                    $this->vistorunplanentrytwo();
                
                if ($this->request->action == 'vehiclemovementstatusby')
                    $this->vehiclemovementstatusby();
                
                if ($this->request->action == 'showcomplainnew')
                    $this->showcomplainnew();
                
                if ($this->request->action == 'userdetailsnew')
                    $this->userdetailsnew();
                
                
                
                if ($this->request->action == 'phoneverification')
                    $this->phoneverification();
                
                if ($this->request->action == 'vistorunplanentryonlinetwo')
                    $this->vistorunplanentryonlinetwo();

                 if ($this->request->action == 'vistorunplanentryonlinetwodecember')
                    $this->vistorunplanentryonlinetwodecember();

															   
											
                
                if ($this->request->action == 'addnotification')
                    $this->addnotification();

	                if ($this->request->action == 'adminnotification')
                    $this->adminnotification();
															  
											   

                if ($this->request->action == 'addphotogallery')
                    $this->addphotogallery();
                
                if ($this->request->action == 'societybanner')
                    $this->societybanner();
                
                if ($this->request->action == 'updateuserinfo')
                    $this->updateuserinfo();
                
                if ($this->request->action == 'addnewpost')
                    $this->addnewpost();
                
                if ($this->request->action == 'addpole')
                    $this->addpole();

                if ($this->request->action == 'showpoles')
                    $this->showpoles();
                
                if ($this->request->action == 'addshopdetail')
                    $this->addshopdetail();
                
                if ($this->request->action == 'registration')
                    $this->registration();
                
                if ($this->request->action == 'vistorunplanentryform')
                    $this->vistorunplanentryform();
                
                if ($this->request->action == 'importusers')
                    $this->importusers();
                
                if ($this->request->action == 'shopdetailbyid')
                    $this->shopdetailbyid();
                
                if ($this->request->action == 'compstatuschange')
                    $this->compstatuschange();
                
                if ($this->request->action == 'registrationnew')
                    $this->registrationnew();
                
                if ($this->request->action == 'registrationnewrollback')
                    $this->registrationnewrollback();
                
                if ($this->request->action == 'addmaintenancestaff')
                    $this->addmaintenancestaff();
                
                if ($this->request->action == 'showmaintenancestaff')
                    $this->showmaintenancestaff();
                
                if ($this->request->action == 'allcount')
                    $this->allcount();
                
                if ($this->request->action == 'callout')
                    $this->callout();
                
                if ($this->request->action == 'maintenancestaffattendance')
                    $this->maintenancestaffattendance();
                
                if ($this->request->action == 'merchantlogin')
                    $this->merchantlogin();
                
                if ($this->request->action == 'mtest')
                    $this->mtest();
                
                if ($this->request->action == 'firebaselogpopup')
                    $this->firebaselogpopup();
                
                if ($this->request->action == 'makeflatprimarymember')
                    $this->makeflatprimarymember();
                
                if ($this->request->action == 'isin')
                    $this->isin();
                
                if ($this->request->action == 'photodetail')
                    $this->photodetail();
                
                if ($this->request->action == 'flatnumber')
                    $this->flatnumber();
                
                if ($this->request->action == 'visitercheckoutbyflat')
                    $this->visitercheckoutbyflat();
                
                if ($this->request->action == 'getrwavehiclepull')
                    $this->getrwavehiclepull();
                
                if ($this->request->action == 'vehiclemovementstatus')
                    $this->vehiclemovementstatus();

																	  
																														
                
                if ($this->request->action == 'compfeedbackstatus')
                    $this->compfeedbackstatus();

                if ($this->request->action == 'compfeedback')
                    $this->compfeedback();
                
                if ($this->request->action == 'vehiclemovementstatus1')
                    $this->vehiclemovementstatus1();
                
                if ($this->request->action == 'showcallout')
                    $this->showcallout();
                
                if ($this->request->action == 'showeventadv')
                    $this->showeventadv();
                
                if ($this->request->action == 'addclassified')
                    $this->addclassified();
                
                
                if ($this->request->action == 'newcomplainmail')
                    $this->newcomplainmail();
                
                if ($this->request->action == 'flatno')
                    $this->flatno();
                
                if ($this->request->action == 'profession')
                    $this->profession();
                
                if ($this->request->action == 'compstatus')
                    $this->compstatus();
                
                if ($this->request->action == 'notificationcount')
                    $this->notificationcount();

		if ($this->request->action == 'editmaintenancestaff')
                    $this->editmaintenancestaff();															 
																													 
                
                if ($this->request->action == 'likerusers')
                    $this->likerusers();
                
                if ($this->request->action == 'chatnotification')
                    $this->chatnotification();
                
                if ($this->request->action == 'compstatuschangeandsms')
                    $this->compstatuschangeandsms();
                
                if ($this->request->action == 'paymentduehistory')
                    $this->paymentduehistory();
                
                if ($this->request->action == 'bannerimage')
                    $this->bannerimage();
                
                if ($this->request->action == 'photolike')
                    $this->photolike();
                
                if ($this->request->action == 'changemaidpicurl')
                    $this->changemaidpicurl();
                
                if ($this->request->action == 'unplannedvisitorpush')
                    $this->unplannedvisitorpush();
                
                if ($this->request->action == 'updatevisitorparkingavailability')
                    $this->updatevisitorparkingavailability();
                
                if ($this->request->action == 'visitorallow')
                    $this->visitorallow();

																	  
																														
                
                if ($this->request->action == 'tvadd')
                    $this->tvadd();
                
                if ($this->request->action == 'visitorstatus')
                    $this->visitorstatus();

                if ($this->request->action == 'updatevisitorqrcode')
                    $this->updatevisitorqrcode();
                       
                if ($this->request->action == 'onedayvisitorlistsociety')
                $this->onedayvisitorlistsociety();
                
                if ($this->request->action == 'getuser')
                $this->getuser();
                
                break;
            }
            
            
            case 'GET': {
                
                
                
                if ($this->request->action == 'visitorverificationid')
                    $this->visitorverificationid();
                
                if ($this->request->action == 'modeofpayment')
                    $this->modeofpayment();
                
                if ($this->request->action == 'categories')
                    $this->categories();
                
                if ($this->request->action == 'complaincategories')
                    $this->complaincategories();
                
                if ($this->request->action == 'deletehelper')
                    $this->deletehelper();
                
                if ($this->request->action == 'assignstaff')
                    $this->assignstaff();
                
                if ($this->request->action == 'complainstatus')
                    $this->complainstatus();
                
                if ($this->request->action == 'visitorpurpose')
                    $this->visitorpurpose();
                
                if ($this->request->action == 'complainslastaff')
                    $this->complainslastaff();
                
                if ($this->request->action == 'pendingusersla')
                    $this->pendingusersla();
                
                if ($this->request->action == 'visitorcompany')
                    $this->visitorcompany();
                
                if ($this->request->action == 'rejectresondesc')
                    $this->rejectresondesc();
                
                if ($this->request->action == 'vehiclentryrfidget')
                    $this->vehiclentryrfidget();
                
                if ($this->request->action == 'generatebill')
                    $this->generatebill();
                
                if ($this->request->action == 'flatdueamount')
                    $this->flatdueamount();
                
                
                
                
                
                
                
                
                break;
            }
            default:
                return $this->invalid();
                break;
        }
    }
    
}
?>